//
//  BringListContentAddItemToPurchaseActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 09.07.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentAddItemToPurchaseActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let sectionLoader = TestSectionLoader()

    override func setUp() {}

    override func tearDown() {}

    func test_add_item_that_exists_in_the_catalog() {
        let action = BringListContentAddItemToPurchaseAction(item: createItem())
        let listContent = createDefaultListContent()
        let newListContent = listContentReducer.reduce(listContent, action: action)
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[0].itemId, "item_0")
    }
    
    func test_add_items_with_same_item_id() {
        let action = BringListContentAddItemToPurchaseAction(item: createItem())
        let listContent = createDefaultListContent()
        var newListContent = listContentReducer.reduce(listContent, action: action)
        
        let sectionAction = BringListContentAddItemToPurchaseAction(item: createItem())
        newListContent = listContentReducer.reduce(newListContent, action: sectionAction)
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 2)
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[0].itemId, "item_0")
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[1].itemId, "item_0")
    }
    
    func test_add_item_that_does_not_exist_in_catalog() {
        let action = BringListContentAddItemToPurchaseAction(item: createItem(itemId: "test"))
        let listContent = createDefaultListContent()
        let newListContent = listContentReducer.reduce(listContent, action: action)
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getOwnArticlesSectionCopy()?.items[0].itemId, "test")
    }
    
    private func createDefaultListContent() -> BringListContent {
        return BringListContent(
            sections: sectionLoader.loadSections(),
            recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(),
            recommendedSection: BringListContent.createRecommendedSection(),
            toBeBurchasedSection: BringListContent.createToBePurchasedSection(),
            listStatus: .unregistered,
            listUuid: "listUuid",
            maxItemsInRecently: 12
        )
    }

}
