//
//  BringCoreDataUserItemStoreTests.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

class BringCoreDataUserItemStoreTests: XCTestCase {

    
    var store: BringCoreDataUserItemStore!
    
    override func setUp() {
        store = BringCoreDataUserItemStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
    }

    override func tearDown() {
    }
    
    func test_create_and_get_userItem() {
        let itemKey = "Custom"
        
        let exp = expectation(description: "Should call callback")
        
        store.create(itemKey) { _ in
            self.store.getAllUserItems { result in
                switch result {
                case .success(let items):
                    XCTAssertEqual(items.count, 1)
                    XCTAssertEqual(itemKey, items[0].itemKey)
                    exp.fulfill()
                case .failure:
                    XCTFail()
                }
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_create_and_get_userItems() {
        let itemKey = "Custom"
        let itemKey2 = "Custom2"
        let exp = expectation(description: "Should call callback")
        
        store.create([itemKey, itemKey2]) { _ in
            self.store.getAllUserItems { result in
                switch result {
                case .success(let items):
                    XCTAssertEqual(items.count, 2)
                    let itemKeys = items.map { $0.itemKey }
                    XCTAssertTrue(itemKeys.contains(itemKey))
                    XCTAssertTrue(itemKeys.contains(itemKey2))
                    exp.fulfill()
                case .failure:
                    XCTFail()
                }
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_remove_userItem() {
        let itemKey = "Custom"
        
        let exp = expectation(description: "Should call callback")
        
        store.create(itemKey) { _ in
            self.store.remove(itemKey) { _ in
                self.store.getAllUserItems { result in
                    switch result {
                    case .success(let items):
                        XCTAssertEqual(items.count, 0)
                        exp.fulfill()
                    case .failure:
                        XCTFail()
                    }
                }
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_remove_all_userItems() {
        let itemKey = "Custom"
        let itemKey2 = "Custom2"
        let exp = expectation(description: "Should call callback")
        
        store.create([itemKey, itemKey2]) { _ in
            self.store.removeAll() { _ in
                self.store.getAllUserItems { result in
                    switch result {
                    case .success(let items):
                        XCTAssertEqual(items.count, 0)
                        exp.fulfill()
                    case .failure:
                        XCTFail()
                    }
                }
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }

}
