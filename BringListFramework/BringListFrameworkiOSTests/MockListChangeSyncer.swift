//
//  MockListChangeSyncer.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 25.01.19.
//

import Foundation
@testable import BringListFrameworkiOS

public class MockListChangeSyncer: ListChangeSyncer {
    
    public init() {}
    
    public weak var delegate: ListChangeSyncerDelegate?
    public var didCallSyncAllLists = false
    public var syncAllListsAndFetchCurrentListShouldFail = false
    
    public func syncLists(mode: BringListSyncManager.BringListSyncMode, callback: @escaping (Result<Void, Error>) -> Void) {
        didCallSyncAllLists = true
        if !syncAllListsAndFetchCurrentListShouldFail {
            callback(Result.success(()))
        } else {
            callback(Result.failure(NSError()))
        }
    }
    
    public var didCallGetCurrentListState = false
    public var getCurrentListStateReturnValue = BringListState(purchase: [], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
    public func getCurrentListState(forList listUuid: String, callback: @escaping (Result<BringListState, Error>) -> Void) {
        didCallGetCurrentListState = true
        callback(Result.success(getCurrentListStateReturnValue))
    }
    
    public var didCallSyncListChange = false
    public var syncedListChanges = [BringListChange]()
    
    public func sync(_ listChange: BringListChange, shouldSyncimmediately: Bool) {
        didCallSyncListChange = true
        syncedListChanges.append(listChange)
    }
    
    public var didcallGetCurrentListStateSync = false
    
    public func getCurrentListState(forList listuuid: String) -> BringListState? {
        didcallGetCurrentListStateSync = true
        return getCurrentListStateReturnValue
    }

}
