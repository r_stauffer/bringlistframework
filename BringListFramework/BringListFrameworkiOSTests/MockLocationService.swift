//
//  File.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 25.01.19.
//

import Foundation
import CoreLocation
@testable import BringListFrameworkiOS

class MockLocationService: LocationService {
    
    var location: CLLocation?
    
    var lastLocation: CLLocation? {
        return location
    }
}
