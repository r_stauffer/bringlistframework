//
//  BringListContentReorderSectionActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 03.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentReorderSectionActionTests: XCTestCase {

    let reducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testSectionReorder() {
        // Given we have a catalog with section order Tierbedarf = 0 and meat = 1
        let currentListContent = listContentLoader.createDefaultListContent()
        XCTAssertEqual(currentListContent.getSections()[0].sectionId, "Tierbedarf")
        XCTAssertEqual(currentListContent.getSections()[1].sectionId, "meat")
        
        // when we reorder the section to be meat = 0 and Tierbedarf = 1
        let action = BringListContentReorderSectionAction(sectionOrder: ["meat", "Tierbedarf"])
        let newListContent = reducer.reduce(currentListContent, action: action)
        
        // then the section order changes to be meat = 0 and Tierbedarf = 1
        XCTAssertEqual(newListContent.getSections()[0].sectionId, "meat")
        XCTAssertEqual(newListContent.getSections()[1].sectionId, "Tierbedarf")
    }

}
