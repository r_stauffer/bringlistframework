//
//  XCTestCase+Utils.swift
//  Bring
//
//  Created by Beat on 13.03.18.
//

import XCTest
@testable import BringListFrameworkiOS

extension XCTestCase {
    
    // MARK: Resources
    
    func loadResource(res: String, ext: String) -> Data? {
        
        let bundle = Bundle(for: type(of: self))
        
        if let url = bundle.url(forResource: res, withExtension: ext) {
            return try? Data(contentsOf: url, options: Data.ReadingOptions())
        } else {
            return nil
        }
    }
    
    func loadResourceString(res: String, ext: String) -> String? {
        
        if let data = self.loadResource(res: res, ext: ext) as Data? {
            return String(data: data, encoding: .utf8)
        } else {
            return nil
        }
    }
    
    func loadJson(_ res: String) -> String? {
        return self.loadResourceString(res: res, ext: "json")
    }
    
    
    // MARK: Helpers
    
    func dataWithHexString(hex: String) -> Data {
        
        var hex = hex
        var data = Data()
        
        while (hex.count > 0) {
            let indexFirstTwoCharacters = hex.index(hex.startIndex, offsetBy: 2)
            let c: String = String(hex[hex.startIndex..<indexFirstTwoCharacters])
            hex = String(hex[indexFirstTwoCharacters..<hex.endIndex])
            print(hex)
            var ch: UInt32 = 0
            Scanner(string: c).scanHexInt32(&ch)
            var char = UInt8(ch)
            data.append(&char, count: 1)
        }
        
        return data
    }
}
