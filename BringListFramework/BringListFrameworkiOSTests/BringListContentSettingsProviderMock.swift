//
//  BringListContentSettingsProviderMock.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 03.12.19.
//

import Foundation
@testable import BringListFrameworkiOS

class BringListContentSettingsProviderMock: ListContentSettingsProvider {

    var didCallArticleLanguage = false
    var didCallUserSectionOrder = false
    var didCallSeletectedList = false
    var didCallUserUuid = false
    var didCallDefaultList = false
    
    var articleLanguageReturnValue: String = "de-CH"
    var userSectionOrderReturnValue: [String]? = nil
    var seletectedListReturnValue: String = "uuid"
    
    
    func articleLanguage(forList listUuid: String) -> String? {
        didCallArticleLanguage = true
        return articleLanguageReturnValue
    }
    
    func userSectionOrder(forList listUuid: String) -> [String]? {
        didCallUserSectionOrder = true
        return userSectionOrderReturnValue
    }
    
    func seletectedList() -> String? {
        didCallSeletectedList = true
        return seletectedListReturnValue
    }
    
    func userUuid() -> String? {
        didCallUserUuid = true
        return nil
    }
    
    func defaultList() -> String? {
        didCallDefaultList = true
        return nil
    }
}
