//
//  BringListModelTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 18.01.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListModelTests: XCTestCase {

    var model: BringListContent!
    
    override func setUp() {
        
        // Create first section
        let section1 = createSection(name: "Tierbedarf", key: "Tierbedarf")
        let item1 = createItem(uuid: "T1", name: "Item 1", key: "item_1", section: section1)
        let item2 = createItem(uuid: "T2", name: "Item 2", key: "item_2", section: section1)
        let item3 = createItem(uuid: "T3", name: "Item 3", key: "item_3", section: section1)
        let item4 = createItem(uuid: "T4", name: "Item 4", key: "item_4", section: section1)
        section1.addItemAtEnd(item: item1)
        section1.addItemAtEnd(item: item2)
        section1.addItemAtEnd(item: item3)
        section1.addItemAtEnd(item: item4)
        
        // Create second section
        let section2 = createSection(name: "Meat", key: "meat")
        let item1Section2 = createItem(uuid: "M1", name: "Item 1 Section 2", key: "item_1_section_2", section: section2)
        let item2Section2 = createItem(uuid: "M2", name: "Item 2 Section 2", key: "item_2_section_2", section: section2)
        let item3Section2 = createItem(uuid: "M3", name: "Item 3 Section 2", key: "item_3_section_2", section: section2)
        let item4Section2 = createItem(uuid: "M4", name: "Item 4 Section 2", key: "item_4_section_2", section: section2)
        section2.addItemAtEnd(item: item1Section2)
        section2.addItemAtEnd(item: item2Section2)
        section2.addItemAtEnd(item: item3Section2)
        section2.addItemAtEnd(item: item4Section2)
        
        let recentlyPurchasedSection = BringListContent.createRecentlyPurchasedSection()
        recentlyPurchasedSection.replaceItems(with: [item1])
        
        let recommendedSection = BringListContent.createRecommendedSection()
        recommendedSection.replaceItems(with: [item2])
        
        let toBePurchasedSection = BringListContent.createToBePurchasedSection()
        toBePurchasedSection.replaceItems(with: [item3])
        
        model = BringListContent(sections: [section1, section2], recommendedItemKeys: [], recentlyPurchasedSection: recentlyPurchasedSection, recommendedSection: recommendedSection, toBeBurchasedSection: toBePurchasedSection, listStatus: .unregistered, listUuid: "uuid", maxItemsInRecently: 12)
    }

    override func tearDown() {}
    
    func testCopyOfModelItems() {
        let modelCopy = model.copy() as! BringListContent
        XCTAssert(modelCopy !== model)
        
        let copyItem = modelCopy.item(for: "T1")
        let item = model.item(for: "T1")
        
        // Test copied item
        XCTAssertTrue(item !== copyItem)
        XCTAssert(item?.name == copyItem?.name)
    }
    
    func testCopyOfListContentSections() {
        let modelCopy = model.copy() as! BringListContent
        XCTAssert(modelCopy !== model)
        
        let copySection = modelCopy.getSections()
        let sections = model.getSections()
        
        // Test copied items
        XCTAssertEqual(copySection.count, sections.count)
        for (index, _) in sections.enumerated() {
            XCTAssert(copySection[index] !== sections[index])
        }
    }
    
    
    func createSection(name: String, key: String, items: [BringItem] = []) -> BringSection {
        return BringSection(sectionId: key, name: name, items: items)
    }
    
    func createItem(uuid: String, name: String, key: String, section: BringSection? = nil) -> BringItem {
        return BringItem(uuid: uuid, itemId: key, name: name)
    }

}
