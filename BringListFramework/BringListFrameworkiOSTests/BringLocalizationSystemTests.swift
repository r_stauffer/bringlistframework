//
//  BringLocalizationSystemTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 16.04.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringLocalizationSystemTests: XCTestCase {

    var localizationSystem: BringLocalizationSystem!
    var mutalbleDictionary = [String: String]()
    
    override func setUp() {
        localizationSystem = BringLocalizationSystem.shared
        mutalbleDictionary["Jedi Meister"] = "Maître Jedi"
    }
    
    override func tearDown() {
        localizationSystem.reset()
    }

    func test_enriching_locales() {
        // Given we provide the localization system translations for french and set the article language to french
        localizationSystem.enrichLocalizationSystem(mutalbleDictionary)
        
        // when we ask for the item with key "Jedi Meister"
        let translation = localizationSystem.localizedString(forKey: "Jedi Meister")
        
        // then we receive the french translation
        XCTAssertEqual(translation, "Maître Jedi")
    }
}
