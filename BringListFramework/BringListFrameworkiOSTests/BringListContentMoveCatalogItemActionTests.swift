//
//  BringListContentMoveCatalogItemActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 23.02.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentMoveCatalogItemActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    override func setUp() {}

    override func tearDown() {}
    
    func testMovingItemThatHasNeverBeenSelectedBefore() {
        // Given we have a new list and we have not selected item_0 before
        let currentListContent = listContentLoader.createDefaultListContent()
        
        // when we move the item to a new section
        let section = createSection(name: "Meat", key: "meat")
        let item = createItem(itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        let action = BringListContentMoveCatalogItemAction(itemToMove: item, sectionToMoveTo: section)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then we expect the item to be in the new section
        XCTAssertEqual(newListContent.getSections()[1].items(withItemId: "item_0").count, 1)
        XCTAssertEqual(newListContent.getSections()[0].items(withItemId: "item_0").count, 0)
    }
    
    func testMovingItemThatHasBeenSelectedBefore() {
        // Given we have a new list and we have selected item_0 before
        var currentListContent = listContentLoader.createDefaultListContent()
        
        let item = createItem(itemId: "item_0", name: "Item 0")
        
        let selectAction = BringListContentSelectAction(item: item)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        
        // when we move the item to a new section
        let section = createSection(name: "Meat", key: "meat")
        let action = BringListContentMoveCatalogItemAction(itemToMove: item, sectionToMoveTo: section)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then we expect the item to be in the new section
        XCTAssertTrue(newListContent.getSections()[1].items.contains(item))
        XCTAssertFalse(newListContent.getSections()[0].items.contains(item))
    }
    
    func testMovingItemsWithSameItemIdThatHaveBeenSelectedBefore() {
        // Given we have a new list with items with the same itemId in the section
        let currentListContent = listContentLoader.createDefaultListContent()
        let itemWithSameUuid = createItem(itemId: "item_0", name: "Item XY")
        currentListContent.getSections()[0].add(item: itemWithSameUuid)
        
        let item = createItem(itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        
        XCTAssertEqual(currentListContent.items(forItemId: "item_0").count, 2)
        
        // when we move the items with the samme itemId
        let section = createSection(name: "Meat", key: "meat")
        let action = BringListContentMoveCatalogItemAction(itemToMove: item, sectionToMoveTo: section)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then we expect the items to be in the new section
        XCTAssertEqual(newListContent.getSections()[1].items(withItemId: "item_0").count, 2)
        XCTAssertEqual(newListContent.getSections()[0].items(withItemId: "item_0").count, 0)
    }
}
