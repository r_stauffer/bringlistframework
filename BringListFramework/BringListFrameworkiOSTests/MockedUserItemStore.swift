//
//  MockedUserItemStore.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 29.01.19.
//

import Foundation
@testable import BringListFrameworkiOS

class MockedUserItemStore: UserItemStore {
    
    var didCallCreateArray = false
    func create(_ itemKeys: [String], callback: @escaping (Result<Void, Error>) -> Void) {
        didCallCreateArray = true
        callback(Result.success(()))
    }

    var didCallCreate = false
    func create(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void) {
        didCallCreate = true
        callback(Result.success(()))
    }
    
    var didCallGetAllUserItems = false
    func getAllUserItems(callback: @escaping (Result<[BringUserItem], Error>) -> Void) {
        didCallGetAllUserItems = true
        callback(Result.success([]))
    }
    
    var didCallRemove = false
    func remove(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void) {
        didCallRemove = true
        callback(Result.success(()))
    }
    
    var didCallRemoveAll = false    
    func removeAll(callback: @escaping (Result<Void, Error>) -> Void) {
        didCallRemoveAll = true
        callback(Result.success(()))
    }
    
}
