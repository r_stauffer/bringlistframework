//
//  BringListFetchOperationTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 11.03.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

class BringListFetchOperationTests: XCTestCase {

    var operation: BringListFetchOperation!
    var listContentService: MockedListContentService!
    var listStateStore: BringCoreDataListStateStore!
    let queue = OperationQueue()
    let privateQueue = OperationQueue()
    
    override func setUp() {
        queue.maxConcurrentOperationCount = 1
        privateQueue.maxConcurrentOperationCount = 1
        listContentService = MockedListContentService()
        listStateStore = BringCoreDataListStateStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
        listStateStore.delete(withPredicate: nil) { _ in }
    }

    override func tearDown() {
    }

    func test_list_fetch_when_we_do_not_have_a_list_state_saved_yet() {
        // Given we have no saved list state
        let expt = expectation(description: "Operation should update the list state")


        // when we perform a BringListFetchOperation operation with new items in purchase and recently
        let newPurchaseItem = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        let newRecentlyItem = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )

        listContentService.getItemsResponse = BringListState(
            purchase: [newPurchaseItem],
            recently: [newRecentlyItem],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )

        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()

            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the new items
                    XCTAssertEqual(listState?.recently.count, 1)
                    XCTAssertEqual(listState?.purchase.count, 1)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }

        wait(for: [expt], timeout: 5.0)
    }
    
    func test_moving_an_item_to_purchase_while_syncing_and_backend_returns_older_item_in_recently() {
        // Given we have a saved list state with uuid_0 in purchase while the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid_0 from the server with it beeing in recently
        let newRecentlyItem2 = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [],
            recently: [newRecentlyItem2],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the item with uuid_0 in the purchase section.
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].uuid, "uuid_0")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_moving_an_item_to_purchase_while_syncing_and_backend_returns_newer_item_in_recently() {
        // Given we have a saved list state with uuid_0 in purchase before the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and a newer modification date from the server
        let newRecentlyItem2 = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [],
            recently: [newRecentlyItem2],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the item with uuid_0 in the purchase section.
                    XCTAssertEqual(listState?.purchase.count, 0)
                    XCTAssertEqual(listState?.recently[0].uuid, "uuid_0")
                    XCTAssertEqual(listState?.recently.count, 1)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_moving_an_item_to_recently_while_syncing_and_backend_returns_older_item_in_purchase() {
        // Given we have a saved list state with uuid_0 in recently while the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid_0 from the server with it beeing in purchase
        let purchaseItem = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [purchaseItem],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the item with uuid_0 in the recently section.
                    XCTAssertEqual(listState?.purchase.count, 0)
                    XCTAssertEqual(listState?.recently[0].uuid, "uuid_0")
                    XCTAssertEqual(listState?.recently.count, 1)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_moving_an_item_to_recently_while_syncing_and_backend_returns_newer_item_in_purchase() {
        // Given we have a saved list state with uuid_0 in recently before the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and a newer modification date from the server
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the item with uuid_0 in the purchase section.
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].uuid, "uuid_0")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_newer_remote_item_is_saved_when_local_and_remote_item_are_in_purchase() {
        // Given we have a saved list state with uuid_0 in purchase before the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and a newer modification date from the server
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the item from the server with uuid_0 in the purchase section.
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].specification, "2")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_newer_local_item_is_saved_when_local_and_remote_item_are_in_purchase() {
        // Given we have a saved list state with uuid_0 in purchase before the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and an older modification date from the server
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the local item with uuid_0 in the purchase section.
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].specification, "")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_newer_local_item_is_saved_when_local_and_remote_item_are_in_recently() {
        // Given we have a saved list state with uuid_0 in recently while the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and a older modification date from the server
        let newRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [],
            recently: [newRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the local item with uuid_0 in the recently section.
                    XCTAssertEqual(listState?.recently.count, 1)
                    XCTAssertEqual(listState?.recently[0].specification, "")
                    XCTAssertEqual(listState?.purchase.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_newer_remote_item_is_saved_when_local_and_remote_item_are_in_recently() {
        // Given we have a saved list state with uuid_0 in recently before the fetch is happening
        let expt = expectation(description: "Operation should update the list state")
        
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid")
        
        self.listStateStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we fetch item with uuid uuid_0 and a newer modification date from the server
        let newRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        listContentService.getItemsResponse = BringListState(
            purchase: [],
            recently: [newRecently],
            status: BringListStatus.registered.rawValue,
            uuid: "listUuid"
        )
        
        self.privateQueue.addOperation {
            self.queue.addOperation(BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: "listUuid", syncStartDate: Date()))
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStateStore.getListState(forUuid: "listUuid", isWaiting: true, callback: { (result) in
                switch result {
                case .success(let listState):
                    // then the list state contains the remote item with uuid_0 in the recently section.
                    XCTAssertEqual(listState?.recently.count, 1)
                    XCTAssertEqual(listState?.recently[0].specification, "2")
                    XCTAssertEqual(listState?.purchase.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }

}
