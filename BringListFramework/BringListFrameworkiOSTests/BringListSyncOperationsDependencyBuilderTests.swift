//
//  BringListSyncOperationsDependencyBuilderTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 16.10.19.
//

import XCTest
@testable import BringListFrameworkiOS

final class BringMockFetchOperation: AsynchronousOperation, ListSyncOperation {
    
    let listUuid: String
    let delegate: BringMockOperationDelegate
    
    init(listUuid: String, delegate: BringMockOperationDelegate) {
        self.listUuid = listUuid
        self.delegate = delegate
    }
    
    override func main() {
        
        DispatchQueue.global().async {
            print("BringFetchOperation \(self.listUuid)")
            self.delegate.didFinishOperation(listUuid: "BringMockFetchOperation_\(self.listUuid)")
            self.finish()
        }
    }
}

protocol BringMockOperationDelegate {
    func didFinishOperation(listUuid: String)
}

final class BringMockBatchupdateOperation: AsynchronousOperation, ListSyncOperation {
    
    let listUuid: String
    let delegate: BringMockOperationDelegate
    
    init(listUuid: String, delegate: BringMockOperationDelegate) {
        self.listUuid = listUuid
        self.delegate = delegate
    }
    
    override func main() {
        
        DispatchQueue.global().async {
            print("BringLocalListUpdateOperation \(self.listUuid)")
            self.delegate.didFinishOperation(listUuid: "BringMockBatchupdateOperation_\(self.listUuid)")
            self.finish()
        }
    }
}

class BringListSyncOperationsDependencyBuilderTests: XCTestCase, BringMockOperationDelegate {

    let operationResultsQueue = DispatchQueue(label: "BringListSyncOperationsDependencyBuilderTests")
    
    var operationResult = [String]()
    
    override func setUp() {
        operationResult = []
    }

    override func tearDown() {
        operationResult = []
    }

    func test_dependency_creation() {
        
        let queue = OperationQueue()
        
        let exp = expectation(description: "should call finish operation")
        
        var updateOperations = [BringMockBatchupdateOperation]()
        var fetchOperations = [BringMockFetchOperation]()
        
        let totalNumberOfOperations = 100
        
        for i in 0..<totalNumberOfOperations {
            updateOperations.append(BringMockBatchupdateOperation(listUuid: "\(i)", delegate: self))
            fetchOperations.append(BringMockFetchOperation(listUuid: "\(i)", delegate: self))
        }
        
        
        
        let group1 = BringListSyncOperationGroup(operations: updateOperations, priority: 1)
        let group2 = BringListSyncOperationGroup(operations: fetchOperations, priority: 2)
        
        let finishOperation = BlockOperation {
            DispatchQueue.main.async {
                for i in 0..<totalNumberOfOperations {
                    XCTAssertGreaterThan(self.operationResult.index(of: "BringMockFetchOperation_\(i)")  ?? 0, self.operationResult.index(of: "BringMockBatchupdateOperation_\(i)") ?? 0)
                    XCTAssertTrue(self.operationResult.contains("BringMockFetchOperation_\(i)"))
                    XCTAssertTrue(self.operationResult.contains("BringMockBatchupdateOperation_\(i)"))
                }
                exp.fulfill()
            }
        }
        
        let operations = BringListSyncOperationsDependencyBuilder.buildDependencies(for: [group1, group2], finishOperation: finishOperation)
        
        for fetchOperation in fetchOperations {
            XCTAssertEqual(fetchOperation.dependencies.count, 1)
        }
        
        for updateOperation in updateOperations {
            XCTAssertEqual(updateOperation.dependencies.count, 0)
        }
        
        XCTAssertEqual(finishOperation.dependencies.count, totalNumberOfOperations * 2)
        
        queue.addOperations(operations, waitUntilFinished: false)
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func test_create_dependencies_with_unequal_nr() {
        let queue = OperationQueue()
        
        let exp = expectation(description: "should call finish operation")
        
        let op1 = BringMockBatchupdateOperation(listUuid: "1", delegate: self)
        let op2 = BringMockBatchupdateOperation(listUuid: "2", delegate: self)
        let op3 = BringMockBatchupdateOperation(listUuid: "3", delegate: self)
        
        let fetchOp1 = BringMockFetchOperation(listUuid: "2", delegate: self)

        let group1 = BringListSyncOperationGroup(operations: [op1, op2, op3], priority: 1)
        let group2 = BringListSyncOperationGroup(operations: [fetchOp1], priority: 2)
        
        let finishOperation = BlockOperation {
            DispatchQueue.main.async {
                XCTAssertGreaterThan(self.operationResult.index(of: "BringMockFetchOperation_2")  ?? 0, self.operationResult.index(of: "BringMockBatchupdateOperation_2") ?? 0)
                XCTAssertTrue(self.operationResult.contains("BringMockFetchOperation_2"))
                XCTAssertTrue(self.operationResult.contains("BringMockBatchupdateOperation_1"))
                XCTAssertTrue(self.operationResult.contains("BringMockBatchupdateOperation_2"))
                XCTAssertTrue(self.operationResult.contains("BringMockBatchupdateOperation_3"))
                exp.fulfill()
            }
        }
        
        let operations = BringListSyncOperationsDependencyBuilder.buildDependencies(for: [group1, group2], finishOperation: finishOperation)
        
        XCTAssertEqual(fetchOp1.dependencies.count, 1)
        XCTAssertEqual(op1.dependencies.count, 0)
        XCTAssertEqual(op2.dependencies.count, 0)
        XCTAssertEqual(op3.dependencies.count, 0)
        XCTAssertEqual(finishOperation.dependencies.count, 4)
        
        queue.addOperations(operations, waitUntilFinished: false)
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func didFinishOperation(listUuid: String) {
        DispatchQueue.main.async {
            self.operationResult.append(listUuid)
        }
    }

}
