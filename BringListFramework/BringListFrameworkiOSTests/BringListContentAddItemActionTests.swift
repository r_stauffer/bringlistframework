//
//  BringListContentAddItemActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 02.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentAddItemActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testAddingNewItem() {
        // Given we have a section which does not contain item "newItem"
        let currentListContent = listContentLoader.createDefaultListContent()
        XCTAssertFalse(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "newItem") ?? true)
        
        // when we perform a `BringListContentAddItemAction` for item with itemId "newItem"
        let newItem = createItem(uuid: "random", itemId: "newItem", name: "New Item", shouldAssignUuid: false)
        let action = BringListContentAddItemAction(item: newItem, sectionId: "Tierbedarf")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the item is added to the category
        XCTAssertTrue(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "newItem") ?? false)
    }
    
    func testAddingNewItemToSectionThatDoesNotExist() {
        // Given we have a catalog without section "Random"
        let currentListContent = listContentLoader.createDefaultListContent()
        XCTAssertFalse(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "newItem") ?? true)
        
        // when we perform a `BringListContentAddItemAction` for an item with sectionId "Random"
        let newItem = createItem(uuid: "random", itemId: "newItem", name: "New Item", shouldAssignUuid: false)
        let action = BringListContentAddItemAction(item: newItem, sectionId: "Random")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the section does not exist affter the BringListContentAddItemAction
        XCTAssertNil(newListContent.catalogSectionReference(bySectionId: "Random"))
    }

}
