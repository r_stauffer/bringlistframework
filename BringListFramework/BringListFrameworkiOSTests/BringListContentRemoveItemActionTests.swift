//
//  BringListContentRemoveItemActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 01.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentRemoveItemActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testRemoveItemActionRemovesItemFromCatalog() {
        // Given we have a new list and we have item item_0 in the catalog
        let currentListContent = listContentLoader.createDefaultListContent()
        XCTAssertTrue(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "item_0") ?? false)
        
        // when we perform a `BringListContentRemoveItemAction` for item with itemId item_0
        let action = BringListContentRemoveItemAction(itemId: "item_0")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the item is remove from the catalog
        XCTAssertFalse(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "item_0") ?? true)
    }
    
    func testRemoveItemActionRemovesItemFromPurchaseSection() {
        // Given we have a new list and we have selected item_0
        var currentListContent = listContentLoader.createDefaultListContent()
        
        let item = createItem(itemId: "item_0", name: "Item 0")
        
        let selectAction = BringListContentSelectAction(item: item)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        
        XCTAssertTrue(currentListContent.getToBePurchasedSection().containsItems(withItemId: "item_0"))
        
        // when we perform a `BringListContentRemoveItemAction` for item with itemId item_0
        let action = BringListContentRemoveItemAction(itemId: "item_0")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the item is remove from the to-be-purchased section
        XCTAssertFalse(newListContent.getToBePurchasedSection().containsItems(withItemId: "item_0"))
    }
    
    func testRemoveItemActionRemovesItemFromRecentlySection() {
        // Given we have a new list and we have item item_0 in the recently section
        var currentListContent = listContentLoader.createDefaultListContent()
        
        let item = createItem(itemId: "item_0", name: "Item 0")
        
        let selectAction = BringListContentSelectAction(item: item)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        
        let toRecentlyAction = BringListContentSelectAction(item: item)
        currentListContent = listContentReducer.reduce(currentListContent, action: toRecentlyAction)
        
        XCTAssertTrue(currentListContent.getRecentlyPurchasedSection().containsItems(withItemId: "item_0"))
        
        // when we perform a `BringListContentRemoveItemAction` for item with itemId item_0
        let action = BringListContentRemoveItemAction(itemId: "item_0")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the item is remove from the to-be-purchased section
        XCTAssertFalse(newListContent.getRecentlyPurchasedSection().containsItems(withItemId: "item_0"))
    }
    
    func testRemoveMultipleItemsWithSameItemId() {
        // Given we have a new list and we have two items with itemId item_0 in the catalog
        let currentListContent = listContentLoader.createDefaultListContent()
        let secondItem = createItem(uuid: "random", itemId: "item_0", name: "Second item_0 item", shouldAssignUuid: false)
        currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.add(item: secondItem)
        XCTAssertEqual(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0").count, 2)
        
        // when we perform a `BringListContentRemoveItemAction` for item with itemId item_0
        let action = BringListContentRemoveItemAction(itemId: "item_0")
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then both items are removed from the catalog
        XCTAssertEqual(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0").count, 0)
        
    }
    

}
