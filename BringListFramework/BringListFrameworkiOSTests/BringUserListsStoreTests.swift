//
//  BringUserListsStoreTests.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import XCTest
@testable import BringListFrameworkiOS
import CoreData

class BringUserListsStoreTests: XCTestCase {

    
    var store: BringCoreDataUserListStore!
    
    override func setUp() {
        store = BringCoreDataUserListStore(stack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
    }

    override func tearDown() {
        
    }
    
    func test_saveOrUpdate_and_get_lists_closure() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.saveOrUpdate([list]) { _ in
            self.store.getUserList(for: "uuid") { userlist in
                XCTAssertNotNil(userlist)
                XCTAssertEqual(userlist?.name, "name")
                XCTAssertEqual(userlist?.uuid, "uuid")
                XCTAssertEqual(userlist?.theme, "theme")
                exp.fulfill()
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_saveOrUpdate_and_get_lists() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.saveOrUpdate([list]) { _ in
            let userlist = self.store.getUserList(for: "uuid")
            XCTAssertNotNil(userlist)
            XCTAssertEqual(userlist?.name, "name")
            XCTAssertEqual(userlist?.uuid, "uuid")
            XCTAssertEqual(userlist?.theme, "theme")
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_store_and_get_lists() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.store(lists: [list]) { _ in
            let userlist = self.store.getUserList(for: "uuid")
            XCTAssertNotNil(userlist)
            XCTAssertEqual(userlist?.name, "name")
            XCTAssertEqual(userlist?.uuid, "uuid")
            XCTAssertEqual(userlist?.theme, "theme")
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_store_and_get_lists_closure() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.store(lists: [list]) { _ in
            self.store.getUserList(for: "uuid") { userlist in
                XCTAssertNotNil(userlist)
                XCTAssertEqual(userlist?.name, "name")
                XCTAssertEqual(userlist?.uuid, "uuid")
                XCTAssertEqual(userlist?.theme, "theme")
                exp.fulfill()
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_get_all_lists() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        let list2 = BringUserList(uuid: "uuid2", name: "name2", theme: "theme2")
        
        store.store(lists: [list, list2]) { _ in
            let lists = self.store.allLists(forPredicate: nil)
            XCTAssertEqual(lists.count, 2)
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_get_all_lists_closure() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        let list2 = BringUserList(uuid: "uuid2", name: "name2", theme: "theme2")
        
        store.store(lists: [list, list2]) { _ in
            self.store.allLists(forPredicate: nil) { result in
                switch result {
                case .success(let lists):
                    XCTAssertEqual(lists.count, 2)
                    exp.fulfill()
                case .failure:
                    break
                }
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_update_list() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.store(lists: [list]) { _ in
            let newList = BringUserList(uuid: "uuid", name: "newName", theme: "newTheme")
            self.store.saveOrUpdate([newList]) { _ in
                let list = self.store.getUserList(for: "uuid")
                XCTAssertEqual(list?.name, "newName")
                XCTAssertEqual(list?.theme, "newTheme")
                XCTAssertEqual(list?.uuid, "uuid")
                exp.fulfill()
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_delete_all_lists() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        
        store.store(lists: [list]) { _ in
            
            let lists = self.store.allLists(forPredicate: nil)
            XCTAssertEqual(lists.count, 1)
            
            self.store.delete(predicate: nil) { _ in
                let lists = self.store.allLists(forPredicate: nil)
                XCTAssertEqual(lists.count, 0)
                exp.fulfill()
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }
    
    func test_delete_specific_list() {
        
        let exp = expectation(description: "Should call callback")
        
        let list = BringUserList(uuid: "uuid", name: "name", theme: "theme")
        let list2 = BringUserList(uuid: "uuid2", name: "name2", theme: "theme2")
        
        store.store(lists: [list, list2]) { _ in
            
            let lists = self.store.allLists(forPredicate: nil)
            XCTAssertEqual(lists.count, 2)
            
            self.store.delete(predicate: list.uuidPredicate) { _ in
                let lists = self.store.allLists(forPredicate: nil)
                XCTAssertEqual(lists.count, 1)
                XCTAssertEqual(lists[0].uuid, "uuid2")
                XCTAssertEqual(lists[0].name, "name2")
                XCTAssertEqual(lists[0].theme, "theme2")
                exp.fulfill()
            }
        }
        
        wait(for: [exp], timeout: 2.0)
    }

}
