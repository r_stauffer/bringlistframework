//
//  BringListStateStoreTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 30.01.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

class BringListStateStoreTests: XCTestCase {
    
    var listStateStore: BringCoreDataListStateStore!
    
    override func setUp() {
        listStateStore = BringCoreDataListStateStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testSavingListState() {
        let json = loadResource(res: "ListContentResponse", ext: "json")!
        let listContentResponse = try! BringListContentDecoder().decode(BringListContentResponse.self, from: json)
        let listState = BringListState(contentResponse: listContentResponse, syncStartDate: Date())
        
        let exp = expectation(description: "Save should succeed")
        
        listStateStore.save(listState, isWaiting: false) { (result) in
            switch result {
            case .success:
                exp.fulfill()
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func testGettingListStateBack() {
        let json = loadResource(res: "ListContentResponse", ext: "json")!
        let listContentResponse = try! BringListContentDecoder().decode(BringListContentResponse.self, from: json)
        
        let dateString = DateFormatter.yyymmddTHHmmssSSSZ().string(from: Date())
        let now = DateFormatter.yyymmddTHHmmssSSSZ().date(from: dateString)!
        
        let listState = BringListState(contentResponse: listContentResponse, syncStartDate: now)
        
        let exp = expectation(description: "Save should get the list status back")
        
        listStateStore.save(listState, isWaiting: false) { (result) in
            switch result {
            case .success:
                self.listStateStore.getListState(forUuid: "f5310454-adee-4b94-8413-f4149faf7be0", isWaiting: false, callback: { (result) in
                    switch result {
                    case .success(let listState):
                        XCTAssertNotNil(listState)
                        
                        XCTAssertEqual(listState?.purchase[0].uuid, "07fa3322-14fd-45e5-ae7f-1f12065a5249")
                        XCTAssertEqual(listState?.purchase[0].specification, "")
                        XCTAssertEqual(listState?.purchase[0].itemId, "buttermilch")
                        XCTAssertEqual(listState?.status, "REGISTERED")
                        
                        let recentlySorted = listState?.recently.sorted(by: { (state0, state1) -> Bool in
                            return state0.itemId < state1.itemId
                        })
                        
                        XCTAssertEqual(recentlySorted?[1].uuid, "54c8590e-6a83-474d-bfb9-3f237df9fc30")
                        XCTAssertEqual(recentlySorted?[1].specification, "")
                        XCTAssertEqual(recentlySorted?[1].itemId, "Peperoni")
                        XCTAssertEqual(recentlySorted?[1].modificationDate, now)
                        
                        XCTAssertEqual(recentlySorted?[0].uuid, "a37afde0-5ffb-4423-bbe3-e3dd4b45b447")
                        XCTAssertEqual(recentlySorted?[0].specification, "")
                        XCTAssertEqual(recentlySorted?[0].itemId, "Beeren")
                        XCTAssertEqual(recentlySorted?[0].modificationDate, now)
                        
                        recentlySorted
                        exp.fulfill()
                    case .failure:
                        break
                    }
                })
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }

}
