//
//  BringListContentRemoveMultipleItemsActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 27.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentRemoveMultipleItemsActionTests: XCTestCase, BringListContentReducerTestUtils {
    
    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    let uuidGenerator = BringUuidGeneratorMock()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_removing_muliple_items() {
        // Given we have selected multiple items
        var currentListContent = listContentLoader.createDefaultListContent()
        uuidGenerator.generatedUuid = "item_0_uuid"
        let itemToSelect1 = createItem(itemId: "item_0", name: "item 0", uuidGenerator: uuidGenerator)
        uuidGenerator.generatedUuid = "item_1_uuid"
        let itemToSelect2 = createItem(itemId: "item_1", name: "item 1", uuidGenerator: uuidGenerator)
        
        currentListContent = select(items: [itemToSelect1, itemToSelect2], in: currentListContent)
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items.count, 2)
        
        // when we perform a `BringListContentRemoveMultipleItemsAction` on the recently section with both items
        let action = BringListContentRemoveMultipleItemsAction(items: ["item_0_uuid", "item_1_uuid"], section: BringListContent.recentlySectionKey)
        currentListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the item are removed from the recently section
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items.count, 0)
    }
    
    func test_removing_muliple_items_cleans_up_catalog() {
        // Given we have selected multiple items with the same item id
        var currentListContent = listContentLoader.createDefaultListContent()
        uuidGenerator.generatedUuid = "item_0_uuid"
        let itemToSelect1 = createItem(itemId: "item_0", name: "item 0", uuidGenerator: uuidGenerator)
        uuidGenerator.generatedUuid = "item_1_uuid"
        let itemToSelect2 = createItem(itemId: "item_0", name: "item 1", uuidGenerator: uuidGenerator)
        
        currentListContent = select(items: [itemToSelect1, itemToSelect2], in: currentListContent)
        XCTAssertEqual(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0").count, 2)
        
        // when we perform a `BringListContentRemoveMultipleItemsAction` on the recently section with both items
        let action = BringListContentRemoveMultipleItemsAction(items: ["item_0_uuid", "item_1_uuid"], section: BringListContent.recentlySectionKey)
        currentListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the catalog is cleanup up after removing both items
        XCTAssertEqual(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0").count, 1)
    }
    
    private func select(items: [BringItem], in listContent: BringListContent) -> BringListContent {
        var newListsContent = listContent
        for item in items {
            let action = BringListContentSelectAction(item: item)
            newListsContent = listContentReducer.reduce(newListsContent, action: action)
        }
        for item in items {
            let action = BringListContentSelectAction(item: item)
            newListsContent = listContentReducer.reduce(newListsContent, action: action)
        }
        return newListsContent
    }

}
