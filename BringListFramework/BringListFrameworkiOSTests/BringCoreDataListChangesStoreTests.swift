//
//  BringCoreDataListChangesStoreTests.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import XCTest
import CoreData
import CoreLocation

@testable import BringListFrameworkiOS

class BringCoreDataListChangesStoreTests: XCTestCase {
    
    var listChangesStore: BringCoreDataListChangesStore!

    override func setUp() {
        listChangesStore = BringCoreDataListChangesStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
    }

    func testSavingListChange() {
        
        let exp = expectation(description: "Save should succeed")
        
        let listChange = BringListChange(
            uuid: "uuid",
            listUuid: "listUuid",
            itemId: "itemId",
            spec: "spec",
            location: nil,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        listChangesStore.add(listChange, isWaiting: false) { (result) in
            switch result {
            case .success:
                exp.fulfill()
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func testSavingListChangeAndRetreivingResultsBackAgain() {
        
        let exp = expectation(description: "We should be able to retreive the list change again.")
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 1.0, longitude: 2.0), altitude: 3.0, horizontalAccuracy: 4.0, verticalAccuracy: 5.0, timestamp: Date())
        
        let listChange = BringListChange(
            uuid: "uuid",
            listUuid: "listUuid",
            itemId: "itemId",
            spec: "spec",
            location: location,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        listChangesStore.add(listChange, isWaiting: false) { (result) in
            switch result {
            case .success:
                self.listChangesStore.allChanges(forListUuid: "listUuid", isWaiting: false, sortDescriptors: [], callback: { (result) in
                    switch result {
                    case .success(let changes):
                        XCTAssertEqual(changes[0].uuid, "uuid")
                        XCTAssertEqual(changes[0].listUuid, "listUuid")
                        XCTAssertEqual(changes[0].itemId, "itemId")
                        XCTAssertEqual(changes[0].spec, "spec")
                        XCTAssertEqual(changes[0].operation, .toPurchase)
                        XCTAssertEqual(changes[0].location?.coordinate.latitude, 1.0)
                        XCTAssertEqual(changes[0].location?.coordinate.longitude, 2.0)
                        XCTAssertEqual(changes[0].location?.altitude, 3.0)
                        XCTAssertEqual(changes[0].location?.horizontalAccuracy, 4.0)
                        XCTAssertEqual(changes[0].location?.verticalAccuracy, 5.0)
                        XCTAssertEqual(changes.count, 1)
                        exp.fulfill()
                    case .failure:
                        break
                    }
                })
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func testSavingMultipleListChangeAndRetreivingResultsBackAgain() {
        
        let exp = expectation(description: "We should be able to retreive the list change again.")
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 1.0, longitude: 2.0), altitude: 3.0, horizontalAccuracy: 4.0, verticalAccuracy: 5.0, timestamp: Date())
        
        let listChange = BringListChange(
            uuid: "uuid",
            listUuid: "listUuid",
            itemId: "itemId",
            spec: "spec",
            location: location,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        let location2 = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 1.0, longitude: 2.0), altitude: 3.0, horizontalAccuracy: 4.0, verticalAccuracy: 5.0, timestamp: Date())
        let listChange2 = BringListChange(
            uuid: "uuid2",
            listUuid: "listUuid",
            itemId: "itemId",
            spec: "spec",
            location: location2,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        listChangesStore.add([listChange, listChange2]) { (result) in
            switch result {
            case .success:
                self.listChangesStore.allChanges(forListUuid: "listUuid", isWaiting: false, sortDescriptors: [], callback: { (result) in
                    switch result {
                    case .success(let changes):
                        XCTAssertEqual(changes.count, 2)
                        exp.fulfill()
                    case .failure:
                        break
                    }
                })
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func testDeletingListChange() {
        let exp = expectation(description: "After the delete we should get empty changes.")
        
        let listChange = BringListChange(
            uuid: "uuid",
            listUuid: "listUuid",
            itemId: "itemId",
            spec: "spec",
            location: nil,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        listChangesStore.add(listChange, isWaiting: false) { (result) in
            switch result {
            case .success:
                self.listChangesStore.deleteChanges(forListUuid: "listUuid", syncDate: Date(), isWaiting: false, callback: { (result) in
                    switch result {
                    case .success:
                        self.listChangesStore.allChanges(forListUuid: "listUuid", isWaiting: false, sortDescriptors: [], callback: { (result) in
                            switch result {
                            case .success(let changes):
                                XCTAssertEqual(changes.count, 0)
                                exp.fulfill()
                            case .failure:
                                break
                            }
                        })
                    case .failure:
                        break
                    }
                })
            case .failure:
                break
            }
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func testDeletingMoreThanOneListChange() {
        let exp = expectation(description: "After the delete we should get empty changes.")
        
        let listUuid = "listUuid"
        
        let listChange = BringListChange(
            uuid: "uuid",
            listUuid: listUuid,
            itemId: "itemId",
            spec: "spec",
            location: nil,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        let listChangeTwo = BringListChange(
            uuid: "uuid2",
            listUuid: listUuid,
            itemId: "itemId2",
            spec: "spec",
            location: nil,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        listChangesStore.add(listChangeTwo, isWaiting: true) { _ in }
        listChangesStore.add(listChange, isWaiting: true) { _ in }
        self.listChangesStore.deleteChanges(forListUuid: listUuid, syncDate: Date(), isWaiting: true, callback: { _ in })
        
        self.listChangesStore.allChanges(forListUuid: listUuid, isWaiting: false, sortDescriptors: [], callback: { (result) in
            switch result {
            case .success(let changes):
                XCTAssertEqual(changes.count, 0)
                exp.fulfill()
            case .failure:
                break
            }
        })
        
        wait(for: [exp], timeout: 5.0)
    }
    
}
