//
//  BringListManagerListContentUpdateTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 29.01.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListManagerListContentUpdateTests: XCTestCase, BringListContentManagerTestUtils {
    
    var listManager: BringListContentManager!
    var sectionLoader: TestSectionLoader!
    var listSyncManager: MockListChangeSyncer!
    var userItemStore: MockedUserItemStore!
    var catalogLoader: BringCatalogLoader!
    var listSettings: BringListSettings!
    var settingsProvider: BringListContentSettingsProviderMock!
    
    override func setUp() {
        sectionLoader = TestSectionLoader()
        listSyncManager = MockListChangeSyncer()
        userItemStore = MockedUserItemStore()
        listSettings = BringListSettings(suiteName: "BringListManagerListContentUpdateTestsSuite")
        settingsProvider = BringListContentSettingsProviderMock()
        catalogLoader = BringCatalogLoader(dataSources: [], baseDataSource: sectionLoader)
        let expt = expectation(description: "Should have loaded data sources before running the tests")
        BringListContentManager.setup(with: catalogLoader, settingsProvider: settingsProvider, maxRecently: 12)
        listManager = BringListContentManager(locationService: MockLocationService(), listSyncManager: listSyncManager, userItemStore: userItemStore, listSettings: listSettings)
        listManager.initializeListContent {
            expt.fulfill()
        }
        wait(for: [expt], timeout: 5.0)
    }

    override func tearDown() {
    }

    func testContentUpdateNoItemInToBePurchasedAndAddingOneItem() {
        // Given we have an empty to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        
        // when we update the list content with a single item to the to-be-purchased section
        let toBePurchasedItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_0", modificationDate: Date())
        let listState = BringListState(purchase: [toBePurchasedItem], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then we have one item in the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().items(forItemId: "item_0").count, 1)
    }
    
    func testContentUpdateWithTwoItemsWithSameItemIdButDifferentUuid() {
        
        // Given we have an empty to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        
        // when we update the to-be-purchased section with two items having bothe the same itemId but different UUIDs
        let toBePurchasedItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_0", modificationDate: Date())
        let toBePurchasedItemTwo = BringListStateItem(uuid: "uuid2", specification: "", itemId: "item_0", modificationDate: Date())
        let listState = BringListState(purchase: [toBePurchasedItem, toBePurchasedItemTwo], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then we have the two items in the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 2)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
    }
    
    func testContentUpdateWithItemThatIsAlreadyInToBePurchasedSection() {
        
        // Given that we already have one item in the to-be-purchased section
        let alreadySelectedItem = BringItem(uuid: "uuid", itemId: "item_1")
        listManager.select(alreadySelectedItem)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
        
        // when we update the list content with the same item
        let toBePurchasedItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_1", modificationDate: Date())
        let listState = BringListState(purchase: [toBePurchasedItem], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then the item stays in the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
    }
    
    func testContentUpdateWithItemThatIsLocallyInToBePurchasedAndInRecentlyFromServer() {
        
        // Given we have item_3 already selected
        let alreadySelectedItem = BringItem(uuid: "uuid", itemId: "item_3")
        listManager.select(alreadySelectedItem)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        
        // when the list content manager receives a content update from the server with item_3 in the recently section
        let recentlyItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_3", modificationDate: Date())
        let listState = BringListState(purchase: [], recently: [recentlyItem], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then item_3 is moved to the recently section and removed from the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items[0].itemId, "item_3")
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
    }
    
    func testContentUpdateWithItemThatIsLocallyInToRecentlySectionAndInTheToBePurchasedFromServer() {
        
        // Given we have item_3 in the recently section
        let itemInRecentlySection = BringItem(uuid: "uuid", itemId: "item_3")
        listManager.select(itemInRecentlySection)
        listManager.select(itemInRecentlySection)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
        
        // when the list content manager receives a content update from the server with item_3 in the to-be-purchased section
        let purchasedItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_3", modificationDate: Date())
        let listState = BringListState(purchase: [purchasedItem], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date().addingTimeInterval(100))
        
        // then item_3 is moved to the recently section and removed from the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items[0].itemId, "item_3")
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
    }
    
    func testContentUpdateNoItemInRecentlySectionAndAddingOneItem() {
        
        // Given that we have an empty recently section
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        
        // when the list content manager receives a content update from the server with an item in the recently section
        let recentlyItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_3", modificationDate: Date())
        let listState = BringListState(purchase: [], recently: [recentlyItem], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then we have one item in the recently section
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
    }
    
    func testContentUpdateWithItemThatIsAlreadyInRecentlySection() {
        
        // Given we have an item_1 in the recently section
        let alreadySelectedItem = BringItem(uuid: "uuid", itemId: "item_1")
        listManager.select(alreadySelectedItem)
        listManager.select(alreadySelectedItem)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
        
        // when the list content manager receives a content update from the server with an item_1 in the recently section
        let recentlyItem = BringListStateItem(uuid: "uuid", specification: "", itemId: "item_1", modificationDate: Date())
        let listState = BringListState(purchase: [], recently: [recentlyItem], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then there is still only one item in the recently section
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
    }
    
    func testCreateUserItemWhenItemDoesNotExistInTheModel() {
        
        // Given we have an empty to-be-purchased, recently and user-item section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.section(bySectionId: BringCatalogProvider.USER_SECTION_KEY)?.items.count, 0)
        
        // when the list content manager receives a content update from the server with an item that does not exist in the catalog
        let recentlyItem = BringListStateItem(uuid: "not-existing", specification: "", itemId: "not-existing", modificationDate: Date())
        let listState = BringListState(purchase: [], recently: [recentlyItem], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then the UserItemStore is called to create a new item and the item is move to the user section
        XCTAssertTrue(userItemStore.didCallCreate)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.section(bySectionId: BringCatalogProvider.USER_SECTION_KEY)?.items.count, 1)
    }
    
    func test_specification_change_over_update() {
        // Given we have item_3 already selected
        let alreadySelectedItem = BringItem(uuid: "uuid", itemId: "item_3")
        listManager.select(alreadySelectedItem)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        
        // when the list content manager receives a content update from the server with item_3 in the purchase section with a specification change
        let purchaseItem = BringListStateItem(uuid: "uuid", specification: "1", itemId: "item_3", modificationDate: Date())
        let listState = BringListState(purchase: [purchaseItem], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then item_3 is moved to the recently section and removed from the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items[0].itemId, "item_3")
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items[0].specification, "1")
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 0)
        
        let itemsInCatalog = listManager.getCurrentModel().items(forItemId: "item_3")
        
        XCTAssertEqual(itemsInCatalog.count, 1)
        XCTAssertEqual(itemsInCatalog[0].specification, "1")
    }
    
    func testContentUpdateItemIsNotMarkedAsNew() {
        // Given we have item_1 in the to-be-purchased section with a older date than the upcoming sync date
        let alreadySelectedItem = BringItem(uuid: "uuid", itemId: "item_1")
        listManager.select(alreadySelectedItem)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
        listSettings.setLastSyncDate(Date(timeIntervalSinceNow: 10.0))
        
        // when we receive an update without item_1 in the to-be-purchased section and a newer sync date than item_1s last modification date
        let listState = BringListState(purchase: [], recently: [], status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: listSettings.lastSyncDate()!)
        
        // then item_1 gets removed from the to-be-purchased section
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 0)
    }

    func testListContentUpdateWhenASharedListUserSelectedTheLastItemFromTheRecentlySection() {
        // Given That we have 12 items in the recently section
        var selectedItems = [BringItem]()
        let uuidGenerator = BringUuidGeneratorMock()
        for i in 0 ..< 10 {
            uuidGenerator.generatedUuid =  "\(i)"
            let item = createItem(uuid: nil, name: "Item \(i)", key: "item_\(i)", uuidGenerator: uuidGenerator)
            selectedItems.append(item)
            listManager.select(item)
        }
        
        for i in 0 ..< 2 {
            uuidGenerator.generatedUuid =  "\(i)_section_2"
            let item = createItem(uuid: nil, name: "Meat \(i)", key: "item_\(i)_section_2", uuidGenerator: uuidGenerator)
            selectedItems.append(item)
            listManager.select(item)
        }
        
        // When we deselect all of them
        for i in 0 ..< 10 {
            uuidGenerator.generatedUuid =  "\(i)"
            let item = createItem(uuid: nil, name: "Item \(i)", key: "item_\(i)", uuidGenerator: uuidGenerator)
            listManager.select(item)
        }
        
        for i in 0 ..< 2 {
            uuidGenerator.generatedUuid =  "\(i)_section_2"
            let item = createItem(uuid: nil, name: "Meat \(i)", key: "item_\(i)_section_2", uuidGenerator: uuidGenerator)
            listManager.select(item)
        }
        
        XCTAssertEqual(listManager.getListContent().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getListContent().getRecentlyPurchasedSection().items.count, listManager.getCurrentModel().maxItemsInRecently)
        
        // When a contentUpdate is performed with the last item in the recently section beeing moved to the to-be-purchased section
        var recently = selectedItems.compactMap { item -> BringListStateItem? in
            
            guard let uuid = item.uuid else {
                return nil
            }
            
            return BringListStateItem(uuid: uuid, specification: item.specification, itemId: item.itemId, modificationDate: Date())
        }
        
        let toBePurchased = recently.filter { $0.uuid == "1_section_2" }
        recently.removeLast()
        let listState = BringListState(purchase: toBePurchased, recently: recently, status: BringListStatus.unregistered.rawValue, uuid: "uuid")
        listManager.updateListContent(listState, lastSyncDate: Date())
        
        // then the purchased section will have one item and the recently section 11 items
        XCTAssertEqual(listManager.getListContent().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getListContent().getRecentlyPurchasedSection().items.count, listManager.getCurrentModel().maxItemsInRecently - 1)
    }
    
    // MARK: Private methods
}
