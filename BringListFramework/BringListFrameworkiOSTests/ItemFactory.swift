//
//  ItemFactory.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 23.02.19.
//

import Foundation
@testable import BringListFrameworkiOS

protocol BringItemFactory {}

protocol BringSectionFactory {}

extension BringItemFactory {
    func createItem(uuid: String? = nil, itemId: String = "item_0", name: String = "Item 0", specification: String = "", isUserItem: Bool = false, isNewItem: Bool = false, uuidGenerator: UuidGenerator = UUIDWrapper(), shouldAssignUuid: Bool = true) -> BringItem {
        let item = BringItem(uuid: uuid, itemId: itemId, name: name, specification: specification, userItem: isUserItem, newItem: isNewItem, uuidGenerator: uuidGenerator)
        if shouldAssignUuid {
            item.assignUuidIfNeeded()
        }
        return item
    }
}

extension BringSectionFactory {
    func createSection(name: String, key: String, items: [BringItem] = []) -> BringSection {
        return BringSection(sectionId: key, name: name, items: items)
    }
}

protocol BringListContentReducerTestUtils: BringItemFactory, BringSectionFactory {
    var listContentReducer: BringListContentReducer { get }
}

extension BringListContentReducerTestUtils {
    func select(numberOfItems: Int, fromTierbedarfSectionOfListContent listContent: BringListContent) -> BringListContent {
        var listContent = listContent
        let uuidGenerator = BringUuidGeneratorMock()
        
        for i in 0..<numberOfItems {
            uuidGenerator.generatedUuid = "\(i)"
            let item = createItem(itemId: "item_\(i)", name: "Item \(i)", uuidGenerator: uuidGenerator)
            let action = BringListContentSelectAction(item: item)
            listContent = listContentReducer.reduce(listContent, action: action)
        }
        
        return listContent
    }
    
    func select(numberOfItems: Int, fromMeatSection listContent: BringListContent) -> BringListContent {
        var listContent = listContent
        let uuidGenerator = BringUuidGeneratorMock()
        
        for i in 0..<numberOfItems {
            uuidGenerator.generatedUuid = "\(i)_section_2"
            let item = createItem(itemId: "item_\(i)_section_2", name: "Meat \(i)", uuidGenerator: uuidGenerator)
            let action = BringListContentSelectAction(item: item)
            listContent = listContentReducer.reduce(listContent, action: action)
        }
        
        return listContent
    }
}
