//
//  BringListContentReducerTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 23.02.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentReducerSelectActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let sectionLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testSelectAction() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        
        // when we select an item
        let itemToSelect = createItem()
        let action = BringListContentSelectAction(item: itemToSelect)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then this item will be in the to-be-pruchased section
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items.count, 0)
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[0].itemId, "item_0")
    }
    
    func testSelectingANewItemUpdatesTheNewFlag() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        currentListContent.items(forItemId: "item_0")[0].newItem = true
        XCTAssertTrue(currentListContent.items(forItemId: "item_0")[0].newItem)
        
        // when we select an item
        let itemToSelect = createItem()
        let action = BringListContentSelectAction(item: itemToSelect)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then this item will be in the to-be-pruchased section
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items.count, 0)
        XCTAssertFalse(newListContent.getToBePurchasedSection().items[0].newItem)
    }
    
    func testSelectActionOnSameItem() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        
        // when we select and afterwards deselect an item
        let itemToSelect = createItem()
        let action = BringListContentSelectAction(item: itemToSelect)
        
        let newListContent = listContentReducer.reduce(
            listContentReducer.reduce(currentListContent, action: action),
            action: action
        )
        
        // then the to-be-purchased section has 0 items and the recently section has one item.
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items[0].itemId, "item_0")
    }
    
    func testSelectingAnItemThatIsNotInTheCatalogYetInsertsANewItem() {
        // Given we have already item_0 in the purchase (uuid is assigned)
        var currentListContent = createDefaultListContent()
        let uuidGenerator = BringUuidGeneratorMock()
        uuidGenerator.generatedUuid = "0"
        let itemToSelect = createItem(itemId: "item_0", name: "Item 0", uuidGenerator: uuidGenerator)
        let action = BringListContentSelectAction(item: itemToSelect)
        currentListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // when we select item_0 with a different uuid (most likly from search)
        uuidGenerator.generatedUuid = "1"
        let itemTwoToSelect = createItem(itemId: "item_0", name: "Item 0", uuidGenerator: uuidGenerator)
        let actionSecondSelect = BringListContentSelectAction(item: itemTwoToSelect)
        let newListContent = listContentReducer.reduce(currentListContent, action: actionSecondSelect)
        
        // then catalog contains both items with the different uuids
        let section = newListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertEqual(section?.items(withItemId: "item_0").count, 2)
        XCTAssertNotEqual(section?.items(withItemId: "item_0")[0].uuid, section?.items(withItemId: "item_0")[1].uuid)
    }
    
    func testSelectingAnItem() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        
        // when we select and item with uuid == "0"
        let uuidGenerator = BringUuidGeneratorMock()
        uuidGenerator.generatedUuid = "0"
        let itemToSelect = createItem(itemId: "item_0", name: "Item 0", uuidGenerator: uuidGenerator)
        let action = BringListContentSelectAction(item: itemToSelect)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the corresponding item in the catalog updates its uuid
        let section = newListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertEqual(section?.items(withItemId: "item_0").count, 1)
        XCTAssertEqual(section?.items(withItemId: "item_0")[0].uuid, "0")
    }
    
    func test_when_slecting_user_item_it_stays_a_user_item() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        
        // when we select and item with uuid == "0"
        let uuidGenerator = BringUuidGeneratorMock()
        uuidGenerator.generatedUuid = "0"
        let itemToSelect = createItem(itemId: "item_10", name: "Item 0", isUserItem: true, uuidGenerator: uuidGenerator)
        let action = BringListContentSelectAction(item: itemToSelect)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the corresponding item remains a user item
        let section = newListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertTrue(section?.items(withItemId: "item_10")[0].userItem ?? false)
        
        let toBePurchased = newListContent.getToBePurchasedSection()
        XCTAssertTrue(toBePurchased.items(withItemId: "item_10")[0].userItem)
    }
    
    func test_when_slecting_user_item_multiple_times_it_stays_a_user_item() {
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        
        // when we select and item with uuid == "0"
        let uuidGenerator = BringUuidGeneratorMock()
        uuidGenerator.generatedUuid = "0"
        let itemToSelect = createItem(itemId: "item_10", name: "User Item", isUserItem: true, uuidGenerator: uuidGenerator)
        let action = BringListContentSelectAction(item: itemToSelect)
        var newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        uuidGenerator.generatedUuid = "1"
        let itemToSelect2 = createItem(itemId: "item_10", name: "User Item", specification: "1", isUserItem: true, uuidGenerator: uuidGenerator)
        let action2 = BringListContentSelectAction(item: itemToSelect2)
        newListContent = listContentReducer.reduce(newListContent, action: action2)
        
        
        // then the corresponding item in the catalog updates its uuid
        let section = newListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        let toBePurchased = newListContent.getToBePurchasedSection()
        
        XCTAssertEqual(section?.items(withItemId: "item_10").count, 2)
        XCTAssertTrue(section?.items(withItemId: "item_10")[0].userItem ?? false)
        XCTAssertTrue(toBePurchased.items(withItemId: "item_10")[0].userItem)
        XCTAssertTrue(section?.items(withItemId: "item_10")[1].userItem ?? false)
        XCTAssertTrue(toBePurchased.items(withItemId: "item_10")[1].userItem)
    }
    
    func testSelectingItemFromRecenlySectionRemovesItemFromRecentlySection() {
        
        // Given we have an empty list content
        let currentListContent = createDefaultListContent()
        let itemToSelect = createItem(itemId: "item_4", name: "Item 4")
        
        let action1 = BringListContentSelectAction(item: itemToSelect)
        
        // when we select the item, deselect it and select it again
        let newListContent = listContentReducer.reduce(
            listContentReducer.reduce(
                listContentReducer.reduce(
                    currentListContent,
                    action: action1),
                action: action1),
            action: action1
        )
        
        // then the recently section has no items anymore and the to-be-purchased section has one item
        XCTAssert(newListContent.getRecentlyPurchasedSection().items.count == 0)
        XCTAssert(newListContent.getToBePurchasedSection().items.count == 1)
    }
    
    func testSelectingItemSortsTheToBePurchasedSectionAccoringToSectionAndName() {
        
        // Given the section Tierbedarf is before the Fleisch & Fisch section in the model
        let currentListContent = createDefaultListContent()
        
        let firstItemToSelect = createItem(itemId: "item_3", name: "Item 3")
        let secondItemToSelect = createItem(itemId: "item_4", name: "Item 4")
        let thirdItemToSelect = createItem(itemId: "Fleisch", name: "item_1_section_2 ")
        
        // When we select three items (two from one section and one from another section)
        let action1 = BringListContentSelectAction(item: firstItemToSelect)
        let action2 = BringListContentSelectAction(item: secondItemToSelect)
        let action3 = BringListContentSelectAction(item: thirdItemToSelect)
        
        let newListContent = listContentReducer.reduce(
            listContentReducer.reduce(
                listContentReducer.reduce(
                    currentListContent,
                    action: action1),
                action: action2),
            action: action3
        )
        
        // The two Tierbedarf items should be before the Fleisch & Fisch section in toBePurchased section and the items within the individual sections are sorted by name
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[0].itemId, "item_3")
        XCTAssertEqual(newListContent.getToBePurchasedSection().items[1].itemId, "item_4")
    }

    func testDeselectingItemRemovesOnlyTheDeselectedItemFromTheToBePurchasedSection() {
        // Given the section Tierbedarf is before the Fleisch & Fisch section in the model
        let currentListContent = createDefaultListContent()
        
        let firstItemToSelect = createItem(itemId: "item_3", name: "Item 3")
        let secondItemToSelect = createItem(itemId: "item_4", name: "Item 4")
        
        // when we select both of them but only deselect one
        let action1 = BringListContentSelectAction(item: firstItemToSelect)
        let action2 = BringListContentSelectAction(item: secondItemToSelect)
        let action3 = BringListContentSelectAction(item: secondItemToSelect)
        
        let newListContent = listContentReducer.reduce(
            listContentReducer.reduce(
                listContentReducer.reduce(
                    currentListContent,
                    action: action1),
                action: action2),
            action: action3
        )

        // then only one of them moves to the recently section and one stays in the to-be-purchased section
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items.count, 1)
    }
    
    func testDeselectingItemRemovesOnlyTheDeselectedItemFromTheToBePurchasedSectionEvenWithSameItemId() {
        // Given the section Tierbedarf is before the Fleisch & Fisch section in the model
        let currentListContent = createDefaultListContent()
        
        let firstItemToSelect = createItem(itemId: "item_4", name: "Item 3")
        let secondItemToSelect = createItem(itemId: "item_4", name: "Item 4")
        
        // when we select both of them but only deselect one
        let action1 = BringListContentSelectAction(item: firstItemToSelect)
        let action2 = BringListContentSelectAction(item: secondItemToSelect)
        let action3 = BringListContentSelectAction(item: secondItemToSelect)
        
        let newListContent = listContentReducer.reduce(
            listContentReducer.reduce(
                listContentReducer.reduce(
                    currentListContent,
                    action: action1),
                action: action2),
            action: action3
        )
        
        // then only one of them moves to the recently section and one stays in the to-be-purchased section
        XCTAssertEqual(newListContent.getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(newListContent.getRecentlyPurchasedSection().items.count, 1)
    }
    
    func testDeselectingMoreItemsThanMaxRecentlyDoesNotExeedRecentlySection() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        var currentListContent = createDefaultListContent()
        
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // When we deselect all of them
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // The recently section only contains 12 items
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items.count, currentListContent.maxItemsInRecently)
    }
    
    func testDeselectingMoreItemsThanMaxRecentlyDoesAddTheLatestElementAtFirstPostion() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        var currentListContent = createDefaultListContent()
        
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // When we deselect all of them
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)

        // The recently sections first element is the latest
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items[0].itemId, "item_2_section_2")
    }

    func testDeselectingMoreItemsThanMaxRecentlyDoesRemoveTheOldestItemAddedToTheRecentlySection() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        var currentListContent = createDefaultListContent()
        
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // When we deselect all of them
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)

        // The recently sections last element is the second added element
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items.count, currentListContent.maxItemsInRecently)
        XCTAssertEqual(currentListContent.getRecentlyPurchasedSection().items[currentListContent.maxItemsInRecently - 1].itemId, "item_1")
    }
    
    func testItemResetsTheUuidWhenRemovedFromRecently() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        var currentListContent = createDefaultListContent()
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // When we deselect all of them
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 3, fromMeatSection: currentListContent)
        
        // The uuid of the item beeing removed from the recently section is reset to nil
        let section = currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertNil(section?.items(withItemId: "item_0")[0].uuid)
    }
    
    func testItemIsRemovedFromTheCatalogWhenRemovedFromRecentlyAndTheCatalogAlreadyContainsItemWithNilUuid() {
        // Given That we selected two more items than recently can have (12 is max currently, so 14 are selected) and the first two items beeing selected have the same itemId with different uuids
        var currentListContent = createDefaultListContent()
        
        let uuidGenerator = BringUuidGeneratorMock()
        uuidGenerator.generatedUuid = "0_first"
        
        let item = createItem(itemId: "item_0", name: "Item 0", uuidGenerator: uuidGenerator)
        let action = BringListContentSelectAction(item: item)
        currentListContent = listContentReducer.reduce(currentListContent, action: action)
        
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 4, fromMeatSection: currentListContent)
        
        // when we deselect all of them so that item "item_0" with uuid "0_first" and item "item_0" with uuid "0" are selected first
        uuidGenerator.generatedUuid = "0_first"
        
        let itemToDeselect = createItem(itemId: "item_0", name: "Item 0", uuidGenerator: uuidGenerator)
        let actionDeselect = BringListContentSelectAction(item: itemToDeselect)
        currentListContent = listContentReducer.reduce(currentListContent, action: actionDeselect)
        
        let currentTierbedarfSection = currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertEqual(currentTierbedarfSection?.items(withItemId: "item_0").count, 2)
        XCTAssertEqual(currentTierbedarfSection?.items(withItemId: "item_0")[0].uuid, "0")
        XCTAssertEqual(currentTierbedarfSection?.items(withItemId: "item_0")[1].uuid, "0_first")
        
        currentListContent = select(numberOfItems: 10, fromTierbedarfSectionOfListContent: currentListContent)
        currentListContent = select(numberOfItems: 4, fromMeatSection: currentListContent)
        
        // then after deselecting 14 items the catalog contains one "item_0" with uuid nil and one "item_0" item iwth uuid "0"
        let section = currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")
        XCTAssertEqual(currentListContent.getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(section?.items(withItemId: "item_0").count, 1)
        XCTAssertEqual(section?.items(withItemId: "item_0")[0].uuid, nil)
    }

    private func createDefaultListContent() -> BringListContent {
        return BringListContent(
            sections: sectionLoader.loadSections(),
            recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(),
            recommendedSection: BringListContent.createRecommendedSection(),
            toBeBurchasedSection: BringListContent.createToBePurchasedSection(),
            listStatus: .unregistered,
            listUuid: "listUuid",
            maxItemsInRecently: 12
        )
    }
    
}
