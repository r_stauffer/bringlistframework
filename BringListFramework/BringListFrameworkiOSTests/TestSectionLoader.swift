//
//  File.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 29.01.19.
//

import Foundation
@testable import BringListFrameworkiOS

class BringUuidGeneratorMock: UuidGenerator {
    
    var didGenerateUuid = false
    var generatedUuid = "uuid"
    
    var uuidString: String {
        didGenerateUuid = true
        return generatedUuid
    }
}

public class TestSectionLoader: BaseCatalogDataSource {
    
    public var shouldReorderSection = false
    
    public init() {}
    
    public func loadSections(callback: @escaping ([BringSection]) -> Void) {
        // Create first section
        let section1 = createSection(name: "Tierbedarf", key: "Tierbedarf")
        let item0 = createItem(uuid: nil, name: "Item 0", key: "item_0")
        let item1 = createItem(uuid: nil, name: "Item 1", key: "item_1")
        let item2 = createItem(uuid: nil, name: "Item 2", key: "item_2")
        let item3 = createItem(uuid: nil, name: "Item 3", key: "item_3")
        let item4 = createItem(uuid: nil, name: "Item 4", key: "item_4")
        let item5 = createItem(uuid: nil, name: "Item 5", key: "item_5")
        let item6 = createItem(uuid: nil, name: "Item 6", key: "item_6")
        let item7 = createItem(uuid: nil, name: "Item 7", key: "item_7")
        let item8 = createItem(uuid: nil, name: "Item 8", key: "item_8")
        let item9 = createItem(uuid: nil, name: "Item 9", key: "item_9")
        let item10 = createItem(uuid: nil, name: "user_item", key: "item_10", userItem: true)
        section1.add(item: item0)
        section1.add(item: item1)
        section1.add(item: item2)
        section1.add(item: item3)
        section1.add(item: item4)
        section1.add(item: item5)
        section1.add(item: item6)
        section1.add(item: item7)
        section1.add(item: item8)
        section1.add(item: item9)
        section1.add(item: item10)
        
        // Create second section
        let section2 = createSection(name: "Meat", key: "meat")
        let item0Section2 = createItem(uuid: nil, name: "Meat 0", key: "item_0_section_2")
        let item1Section2 = createItem(uuid: nil, name: "Meat 1", key: "item_1_section_2")
        let item2Section2 = createItem(uuid: nil, name: "Meat 2", key: "item_2_section_2")
        let item3Section2 = createItem(uuid: nil, name: "Meat 3", key: "item_3_section_2")
        let item4Section2 = createItem(uuid: nil, name: "Meat 4", key: "item_4_section_2")
        let item5Section2 = createItem(uuid: nil, name: "Meat 5", key: "item_5_section_2")
        section2.add(item: item0Section2)
        section2.add(item: item1Section2)
        section2.add(item: item2Section2)
        section2.add(item: item3Section2)
        section2.add(item: item4Section2)
        section2.add(item: item5Section2)
        
        let userItemSection = createSection(name: "Recently", key: BringCatalogProvider.USER_SECTION_KEY)
        
        callback(shouldReorderSection ? [section2, section1] : [section1, section2, userItemSection])
    }
    
    public func loadSections() -> [BringSection] {
        // Create first section
        let section1 = createSection(name: "Tierbedarf", key: "Tierbedarf")
        let item0 = createItem(uuid: nil, name: "Item 0", key: "item_0")
        let item1 = createItem(uuid: nil, name: "Item 1", key: "item_1")
        let item2 = createItem(uuid: nil, name: "Item 2", key: "item_2")
        let item3 = createItem(uuid: nil, name: "Item 3", key: "item_3")
        let item4 = createItem(uuid: nil, name: "Item 4", key: "item_4")
        let item5 = createItem(uuid: nil, name: "Item 5", key: "item_5")
        let item6 = createItem(uuid: nil, name: "Item 6", key: "item_6")
        let item7 = createItem(uuid: nil, name: "Item 7", key: "item_7")
        let item8 = createItem(uuid: nil, name: "Item 8", key: "item_8")
        let item9 = createItem(uuid: nil, name: "Item 9", key: "item_9")
        let item10 = createItem(uuid: nil, name: "user_item", key: "item_10", userItem: true)
        section1.add(item: item0)
        section1.add(item: item1)
        section1.add(item: item2)
        section1.add(item: item3)
        section1.add(item: item4)
        section1.add(item: item5)
        section1.add(item: item6)
        section1.add(item: item7)
        section1.add(item: item8)
        section1.add(item: item9)
        section1.add(item: item10)
        
        // Create second section
        let section2 = createSection(name: "Meat", key: "meat")
        let item0Section2 = createItem(uuid: nil, name: "Meat 0", key: "item_0_section_2")
        let item1Section2 = createItem(uuid: nil, name: "Meat 1", key: "item_1_section_2")
        let item2Section2 = createItem(uuid: nil, name: "Meat 2", key: "item_2_section_2")
        let item3Section2 = createItem(uuid: nil, name: "Meat 3", key: "item_3_section_2")
        let item4Section2 = createItem(uuid: nil, name: "Meat 4", key: "item_4_section_2")
        let item5Section2 = createItem(uuid: nil, name: "Meat 5", key: "item_5_section_2")
        section2.add(item: item0Section2)
        section2.add(item: item1Section2)
        section2.add(item: item2Section2)
        section2.add(item: item3Section2)
        section2.add(item: item4Section2)
        section2.add(item: item5Section2)
        
        let userItemSection = createSection(name: "Recently", key: BringCatalogProvider.USER_SECTION_KEY)
        
        return shouldReorderSection ? [section2, section1] : [section1, section2, userItemSection]
    }
    
    public func loadItems(callback: @escaping ([String: [BringItem]]) -> Void) {
        callback([String: [BringItem]]())
    }
    
    public var isDefaultCatalogDataSource: Bool {
        return true
    }
    
    public func createDefaultListContent() -> BringListContent {
        return BringListContent(
            sections: self.loadSections(),
            recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(),
            recommendedSection: BringListContent.createRecommendedSection(),
            toBeBurchasedSection: BringListContent.createToBePurchasedSection(),
            listStatus: .unregistered,
            listUuid: "listUuid",
            maxItemsInRecently: 12
        )
    }
    
    func createSection(name: String, key: String, items: [BringItem] = []) -> BringSection {
        return BringSection(sectionId: key, name: name, items: items, isDefaultCatalogSection: true)
    }
    
    func createItem(uuid: String?, name: String, key: String, userItem: Bool = false) -> BringItem {
        return BringItem(uuid: uuid, itemId: key, name: name, userItem: userItem)
    }
}
