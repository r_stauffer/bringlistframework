//
//  BringLocalListItemDetailStoreTests.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import XCTest
@testable import BringListFrameworkiOS
import CoreData

class BringCoreDataListItemDetailStoreTests: XCTestCase {

    var store: BringCoreDataListItemDetailStore!
    
    override func setUp() {
        store = BringCoreDataListItemDetailStore(stack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_add_item_detail() {
        let detail = createItemDetail()
        store.addBringListItemDetail(detail)
        let storedDetail = store.get(with: nil)
        XCTAssertEqual(storedDetail[0].uuid, "uuid")
        XCTAssertEqual(storedDetail[0].itemKey, "itemKey")
        XCTAssertEqual(storedDetail[0].listUuid, "listUuid")
        XCTAssertEqual(storedDetail[0].userIconItemId, "userIconItemId")
        XCTAssertEqual(storedDetail[0].userSectionId, "userSectionId")
        XCTAssertEqual(storedDetail[0].assignedTo, "assignedTo")
        XCTAssertEqual(storedDetail[0].localImageUrl, "localImageUrl")
        XCTAssertEqual(storedDetail[0].remoteImageUrl, "remoteImageUrl")
    }
    
    func test_create_multiple_item_details() {
        let detail = createItemDetail()
        let detail2 = createItemDetail(uuid: "uuid2", itemKey: "custom")
        store.create([detail, detail2])
        let storedDetails = store.get(with: nil)
        let ids = storedDetails.map { $0.uuid }
        XCTAssertTrue(ids.contains("uuid"))
        XCTAssertTrue(ids.contains("uuid2"))
        XCTAssertEqual(storedDetails.count, 2)
    }
    
    func test_delete() {
        let detail = createItemDetail()
        store.addBringListItemDetail(detail)
        store.delete(with: detail.uniqueItemDetailPredicate)
        let storedDetail = store.get(with: nil)
        XCTAssertNil(storedDetail.first)
    }
    
    func test_delete_only_unique_detail_different_listUuid() {
        let detail = createItemDetail()
        let detail2 = createItemDetail(uuid: "uuid2", itemKey: "itemKey", listUuid: "listUuid2")
        
        store.addBringListItemDetail(detail)
        store.addBringListItemDetail(detail2)
        
        store.delete(with: detail.uniqueItemDetailPredicate)
        
        let storedDetail = store.get(with: detail2.uniqueItemDetailPredicate)
        XCTAssertEqual(storedDetail.count, 1)
        XCTAssertEqual(storedDetail[0].uuid, "uuid2")
        XCTAssertEqual(storedDetail[0].itemKey, "itemKey")
        XCTAssertEqual(storedDetail[0].listUuid, "listUuid2")
        XCTAssertEqual(storedDetail[0].userIconItemId, "userIconItemId")
        XCTAssertEqual(storedDetail[0].userSectionId, "userSectionId")
        XCTAssertEqual(storedDetail[0].assignedTo, "assignedTo")
        XCTAssertEqual(storedDetail[0].localImageUrl, "localImageUrl")
        XCTAssertEqual(storedDetail[0].remoteImageUrl, "remoteImageUrl")
    }
    
    func test_delete_only_unique_detail_different_itemKey() {
        let detail = createItemDetail()
        let detail2 = createItemDetail(uuid: "uuid2", itemKey: "itemKey2", listUuid: "listUuid")
        
        store.addBringListItemDetail(detail)
        store.addBringListItemDetail(detail2)
        
        store.delete(with: detail.uniqueItemDetailPredicate)
        
        let storedDetail = store.get(with: detail2.uniqueItemDetailPredicate)
        XCTAssertEqual(storedDetail.count, 1)
        XCTAssertEqual(storedDetail[0].uuid, "uuid2")
        XCTAssertEqual(storedDetail[0].itemKey, "itemKey2")
        XCTAssertEqual(storedDetail[0].listUuid, "listUuid")
        XCTAssertEqual(storedDetail[0].userIconItemId, "userIconItemId")
        XCTAssertEqual(storedDetail[0].userSectionId, "userSectionId")
        XCTAssertEqual(storedDetail[0].assignedTo, "assignedTo")
        XCTAssertEqual(storedDetail[0].localImageUrl, "localImageUrl")
        XCTAssertEqual(storedDetail[0].remoteImageUrl, "remoteImageUrl")
    }
    
    func test_update() {
        let detail = createItemDetail()
        
        store.addBringListItemDetail(detail)

        let updatedDetail = createItemDetail(
            userIconItemId: "userIconItemIdUpdated",
            userSectionId: "userSectionIdUpdated",
            assignedTo: "assignedToUpdated",
            localImageUrl: "localImageUrlUpdated",
            remoteImageUrl: "remoteImageUrlUpdated"
        )
        
        store.update(updatedDetail)
        
        let storedDetail = store.get(with: updatedDetail.uniqueItemDetailPredicate)
        
        XCTAssertEqual(storedDetail[0].uuid, "uuid")
        XCTAssertEqual(storedDetail[0].itemKey, "itemKey")
        XCTAssertEqual(storedDetail[0].listUuid, "listUuid")
        XCTAssertEqual(storedDetail[0].userIconItemId, "userIconItemIdUpdated")
        XCTAssertEqual(storedDetail[0].userSectionId, "userSectionIdUpdated")
        XCTAssertEqual(storedDetail[0].assignedTo, "assignedToUpdated")
        XCTAssertEqual(storedDetail[0].localImageUrl, "localImageUrlUpdated")
        XCTAssertEqual(storedDetail[0].remoteImageUrl, "remoteImageUrlUpdated")
    }
    
    private func createItemDetail(
        uuid: String = "uuid",
        itemKey: String = "itemKey",
        listUuid: String = "listUuid",
        userIconItemId: String = "userIconItemId",
        userSectionId: String = "userSectionId",
        assignedTo: String = "assignedTo",
        localImageUrl: String = "localImageUrl",
        remoteImageUrl: String = "remoteImageUrl"
    ) -> BringListItemDetail {
        return BringListItemDetail(
            uuid: uuid,
            itemKey: itemKey,
            listUuid: listUuid,
            userIconItemId: userIconItemId,
            userSectionId: userSectionId,
            assignedTo: assignedTo,
            localImageUrl: localImageUrl,
            remoteImageUrl: remoteImageUrl
        )
    }

}
