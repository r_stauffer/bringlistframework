//
//  BringLocalListUpdateOperationTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 02.02.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

class BringLocalListUpdateOperationTests: XCTestCase {

    var listStatusStore: BringCoreDataListStateStore!
    var listChangesStore: BringCoreDataListChangesStore!
    let queue = OperationQueue()
    let listUuid = "listUuid"
    let privateQueue = OperationQueue()
    
    override func setUp() {
        queue.maxConcurrentOperationCount = 1
        privateQueue.maxConcurrentOperationCount = 1
        listStatusStore = BringCoreDataListStateStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
        listChangesStore = BringCoreDataListChangesStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testListChangeGetsAddedToTheLocalListChangesStore() {
        
        let expt = expectation(description: "We should get back all list changes for the specified list uuid")
        
        // Given we have a list change
        let listChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "itemId", spec: "spec", location: nil, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            // when we perform a BringLocalListUpdateOperation
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // the list change gets stored.
            self.listChangesStore.allChanges(forListUuid: self.listUuid, isWaiting: false, sortDescriptors: [], callback: { (result) in
                switch result {
                case .success(let changes):
                    XCTAssertEqual(changes.count, 1)
                    XCTAssertEqual(changes[0].uuid, "uuid")
                    XCTAssertEqual(changes[0].listUuid, self.listUuid)
                    XCTAssertEqual(changes[0].itemId, "itemId")
                    XCTAssertEqual(changes[0].spec, "spec")
                    XCTAssertNil(changes[0].location)
                    XCTAssertEqual(changes[0].operation, .remove)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListStatesPurchaseGetsUpdated() {
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have a list change
        let listChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "itemId", spec: "", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            // when we perform a BringLocalListUpdateOperation
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
        
                // the local list state gets upated.
                self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { result in
                    switch result {
                    case .success(let listState):
                        XCTAssertEqual(listState?.purchase.count, 1)
                        expt.fulfill()
                    case .failure:
                        break
                    }
                })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    
    func testListStatesPurchaseGetsUpdatedWhenListChangeHasARemoveOperation() {
        let expt = expectation(description: "We should get back the current list state.")
        
        // Given we have a list change stored before with an toPurchase operation
        let oldListChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "litemId", spec: "", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let firstOperation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: oldListChange)
        
        // when we perform a remove operation the list change with the same uuid
        let listChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "litemId", spec: "", location: nil, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            self.queue.addOperation(firstOperation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { result in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 1)
                    
                    self.privateQueue.addOperation {
                        
                        self.queue.addOperation(operation)
                        self.queue.waitUntilAllOperationsAreFinished()
                        
                        // then the old item is removed from the purchase of the list state
                        self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { result in
                            switch result {
                            case .success(let newListState):
                                XCTAssertEqual(newListState?.purchase.count, 0)
                                expt.fulfill()
                            case .failure:
                                break
                            }
                        })
                        
                    }
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListStatesRecentlyGetsUpdatedWhenListChangeHasARemoveOperation() {
        let expt = expectation(description: "We should get back the current list state.")
        
        // Given we have a list change stored before with an toRecently operation
        let oldListChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "litemId", spec: "", location: nil, operation: .toRecently, timeStamp: Date().timeIntervalSince1970)
        let firstOperation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: oldListChange)
        
        // when we perform a remove operation the list change with the same listUuid
        let listChange = BringListChange(uuid: "uuid", listUuid: listUuid, itemId: "litemId", spec: "", location: nil, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            self.queue.addOperation(firstOperation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { result in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.recently.count, 1)
                    
                    self.privateQueue.addOperation {
                        
                        self.queue.addOperation(operation)
                        self.queue.waitUntilAllOperationsAreFinished()
                        
                        // then the old item is removed from the recently of the list state
                        self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { result in
                            switch result {
                            case .success(let newListState):
                                XCTAssertEqual(newListState?.recently.count, 0)
                                expt.fulfill()
                            case .failure:
                                break
                            }
                        })
                        
                    }
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 50.0)
    }
    
    func testListChangeToPurchaseFromRecently() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in purchase and one in recently saved in the local list state
        let recently = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let purchase = [BringListStateItem(uuid: "uuid2", specification: "spec", itemId: "1", modificationDate: Date())]
        let currentListState = BringListState(purchase: purchase, recently: recently, status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we move the item from recently to the to-be-pruchased section
        let listChange = BringListChange(uuid: "uuid1", listUuid: listUuid, itemId: "0", spec: "", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then the list state has now two items in the to-be-prchased section and zero items in recently
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 2)
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListChangeToPurchaseFromCatalog() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in purchase and one in recently saved in the local list state
        let recently = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let purchase = [BringListStateItem(uuid: "uuid2", specification: "spec", itemId: "1", modificationDate: Date())]
        let currentListState = BringListState(purchase: purchase, recently: recently, status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we move another item from the catalog to the to-be-pruchased section
        let listChange = BringListChange(uuid: "uuid3", listUuid: listUuid, itemId: "2", spec: "", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then we have to items in the to-be-modelprchased section and one in recently
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 2)
                    XCTAssertEqual(listState?.recently.count, 1)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListChangeToRecentlyFromPurchase() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in purchase and one in recently saved in the local list state
        let recently = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let purchase = [BringListStateItem(uuid: "uuid2", specification: "", itemId: "1", modificationDate: Date())]
        let currentListState = BringListState(purchase: purchase, recently: recently, status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we move an item from the to-be-pruchased section to recently
        let listChange = BringListChange(uuid: "uuid2", listUuid: listUuid, itemId: "1", spec: "", location: nil, operation: .toRecently, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then we have two items in the recently section and zero in the to-be-purchased section
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 0)
                    XCTAssertEqual(listState?.recently.count, 2)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListChangeRemoveFromRecently() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in purchase and one in recently saved in the local list state
        let recently = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let purchase = [BringListStateItem(uuid: "uuid2", specification: "spec", itemId: "1", modificationDate: Date())]
        let currentListState = BringListState(purchase: purchase, recently: recently, status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we remove an item from the recently section back to the catalog
        let listChange = BringListChange(uuid: "uuid1", listUuid: listUuid, itemId: "0", spec: "", location: nil, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then we have one item left in the to-be-purchased section and zero in the recently section
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testListChangeToPurchaseFromRecentlyWithSpecChange() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in the recently setion with an empty specification
        let recently = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let currentListState = BringListState(purchase: [], recently: recently, status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we move the item to purchase with a different specification
        let listChange = BringListChange(uuid: "uuid1", listUuid: listUuid, itemId: "0", spec: "new", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then we have one item left in the to-be-purchased section and zero in the recently section
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].specification, "new")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testSpecificationChangeInPurchase() {
        
        let expt = expectation(description: "We should get back the current list state")
        
        // Given we have one item in the purchase setion with an empty specification
        let purchase = [BringListStateItem(uuid: "uuid1", specification: "", itemId: "0", modificationDate: Date())]
        let currentListState = BringListState(purchase: purchase, recently: [], status: "", uuid: listUuid)
        
        self.listStatusStore.save(currentListState, isWaiting: true) { _ in }
        
        // when we move the item to purchase with a different specification
        let listChange = BringListChange(uuid: "uuid1", listUuid: listUuid, itemId: "0", spec: "new", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let operation = BringLocalListUpdateOperation(listStateStore: listStatusStore, listChangesStore: listChangesStore, listChange: listChange)
        
        privateQueue.addOperation {
            
            self.queue.addOperation(operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // Then we have one item left in the to-be-purchased section and zero in the recently section
            self.listStatusStore.getListState(forUuid: self.listUuid, isWaiting: false, callback: { (result) in
                switch result {
                case .success(let listState):
                    XCTAssertEqual(listState?.purchase.count, 1)
                    XCTAssertEqual(listState?.purchase[0].specification, "new")
                    XCTAssertEqual(listState?.recently.count, 0)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
}
