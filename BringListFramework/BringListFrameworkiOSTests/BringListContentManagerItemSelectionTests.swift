//
//  BringListContentManagerItemSelectionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 08.02.19.
//

import XCTest
@testable import BringListFrameworkiOS

protocol BringListContentManagerTestUtils {
    var listManager: BringListContentManager! { get }
}

extension BringListContentManagerTestUtils {
    func selectTierbedarfSectionItems(numberOfItems: Int) {
        let uuidGenerator = BringUuidGeneratorMock()
        for i in 0 ..< numberOfItems {
            uuidGenerator.generatedUuid = "\(i)"
            let item = createItem(uuid: nil, name: "Item \(i)", key: "item_\(i)", uuidGenerator: uuidGenerator)
            listManager.select(item)
        }
    }
    
    func selectMeatSectionItems(numberOfItems: Int) {
        let uuidGenerator = BringUuidGeneratorMock()
        for i in 0 ..< numberOfItems {
            uuidGenerator.generatedUuid = "\(i)_section_2"
            let item = createItem(uuid: nil, name: "Meat \(i)", key: "item_\(i)_section_2", uuidGenerator: uuidGenerator)
            listManager.select(item)
        }
    }
    
    func createSection(name: String, key: String, items: [BringItem] = []) -> BringSection {
        return BringSection(sectionId: key, name: name, items: items)
    }
    
    func createItem(uuid: String?, name: String, key: String, uuidGenerator: UuidGenerator = UUIDWrapper()) -> BringItem {
        return BringItem(uuid: uuid, itemId: key, name: name, uuidGenerator: uuidGenerator)
    }
}

class BringListContentManagerItemSelectionTests: XCTestCase, BringListContentManagerTestUtils {

    var listManager: BringListContentManager!
    var sectionLoader: TestSectionLoader!
    var listSyncManager: MockListChangeSyncer!
    var listSettings: BringListSettings!
    
    override func setUp() {
        sectionLoader = TestSectionLoader()
        listSyncManager = MockListChangeSyncer()
        let catalogLoader = BringCatalogLoader(dataSources: [], baseDataSource: sectionLoader)
        BringListContentManager.setup(with: catalogLoader, settingsProvider: BringListContentSettingsProviderMock(), maxRecently: 12)
        listSettings = BringListSettings(suiteName: "BringListContentManagerItemSelectionTestsSuite")
        listManager = BringListContentManager(locationService: MockLocationService(), listSyncManager: listSyncManager, listSettings: listSettings)
        let expt = expectation(description: "Should have loaded data sources before running the tests")
        
        listManager.initializeListContent {
            expt.fulfill()
        }
        
        wait(for: [expt], timeout: 5.0)
    }

    override func tearDown() {}
    
    func testSelectingItem() {
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 1)
    }
    
    func testItemHasUuidAfterSelection() {
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        XCTAssertNotNil(listManager.getCurrentModel().getToBePurchasedSection().items[0].uuid)
    }
    
    func testItemHasSameUuidAfterDeselection() {
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        let uuidAfterSelection = listManager.getCurrentModel().getToBePurchasedSection().items[0].uuid
        listManager.select(itemToSelect)
        let uuidAfterDeselection = listManager.getCurrentModel().getRecentlyPurchasedSection().items[0].uuid
        XCTAssertEqual(uuidAfterSelection, uuidAfterDeselection)
    }
    
    func testSelectingItemSortsTheToBePurchasedSectionAccoringToSectionAndName() {
        
        // Given the section Tierbedarf is before the Fleisch & Fisch section in the model
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        
        let firstItemToSelect = createItem(uuid: nil, name: "Item 3", key: "item_3")
        let secondItemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        let thirdItemToSelect = createItem(uuid: nil, name: "Fleisch", key: "item_1_section_2")
        // When we select three items (two from one section and one from another section)
        listManager.select(firstItemToSelect)
        listManager.select(secondItemToSelect)
        listManager.select(thirdItemToSelect)
        
        // The two Tierbedarf items should be before the Fleisch & Fisch section in toBePurchased section and the items within the individual sections are sorted by name
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items[0].itemId, "item_3")
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items[1].itemId, "item_4")
    }
    
    func testSelectingItemFromRecenlySectionRemovesItemFromRecentlySection() {
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: "uuid", name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        listManager.select(itemToSelect)
        listManager.select(itemToSelect)
        XCTAssert(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
    }
    
    func testDeselectingItemRemovesItFromTheToBePurchasedSection() {
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        listManager.select(itemToSelect)
        XCTAssertTrue(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
    }
    
    func testDeselectingItemRemovesOnlyTheDeselectedItemFromTheToBePurchasedSection() {
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        let itemToSelectTwo = createItem(uuid: nil, name: "Item 3", key: "item_3")
        listManager.select(itemToSelect)
        listManager.select(itemToSelectTwo)
        listManager.select(itemToSelect)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
    }
    
    func testDeselectingItemAddsItToTheRecentlyPurchasedSection() {
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        listManager.select(itemToSelect)
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items[0].itemId, "item_4")
    }
    
    func testDeselectingMoreItemsThanMaxRecentlyDoesNotExeedRecentlySection() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // When we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // The recently section only contains 12 items
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, listManager.getCurrentModel().maxItemsInRecently)
    }
    
    func testRemoveOperationIsPerformedWhenDeselectingMoreThanItemsAllowedInRecently() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // When we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // all operations including the last remove operation are sent to the syncer
        XCTAssertEqual(listSyncManager.syncedListChanges[listSyncManager.syncedListChanges.count - 1].operation, BringListContentOperation.remove)
        XCTAssertEqual(listSyncManager.syncedListChanges.count, 27)
    }
    
    func testNoRemoveOperationIsPerformedWhenDeselectingExactlyTheNumberOfAllowedItemsInRecently() {
        // Given That we have 12 items in the recently section
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 2)
        
        // When we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 2)
        
        // then all operations sent to the syncer are toRecently operations.
        for change in listSyncManager.syncedListChanges {
            XCTAssertTrue((BringListContentOperation.toRecently == change.operation) || (change.operation == BringListContentOperation.toPurchase))
        }
    }
    
    func testDeselectingMoreItemsThanMaxRecentlyDoesAddTheLatestElementAtFirstPostion() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // When we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // The recently sections first element is the latest
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items[0].itemId, "item_2_section_2")
    }
    
    func testDeselectingMoreItemsThanMaxRecentlyDoesRemoveTheOldestItemAddedToTheRecentlySection() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // When we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // The recently sections last element is the second added element
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, listManager.getCurrentModel().maxItemsInRecently)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items[listManager.getCurrentModel().maxItemsInRecently - 1].itemId, "item_1")
    }
    
    func testAfterRemovingItemFromRecentlySetsTheUUIDBackToNil() {
        // Given That we selected more items than recently can have (12 is max currently, so 13 are selected)...
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // when we deselect all of them
        selectTierbedarfSectionItems(numberOfItems: 10)
        selectMeatSectionItems(numberOfItems: 3)
        
        // then the item removed from the recently has a uuid == nil
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, listManager.getCurrentModel().maxItemsInRecently)
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items[listManager.getCurrentModel().maxItemsInRecently - 1].itemId, "item_1")
        XCTAssertNil(listManager.getCurrentModel().catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0")[0].uuid)
    }
    
    func testSelectingAnItemTriggersAToBePurchasedOperation() {
        // Given that we have an empty to-be-purchased list
        XCTAssert(listManager.getCurrentModel().getToBePurchasedSection().items.count == 0)
        
        // When selecting an item
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        
        // A toPurchase operation is sent to the syncer
        XCTAssertEqual(listSyncManager.syncedListChanges.last?.operation, BringListContentOperation.toPurchase)
        XCTAssertEqual(listSyncManager.syncedListChanges.count, 1)
    }
    
    func testDeselectingAnItemTriggersAToRecentlyOperation() {
        // Given that we have one item on the to-be-purchased list
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        
        // When deselecting the item
        listManager.select(itemToSelect)
        
        // A toRecently operation is sent to the syncer
        XCTAssertEqual(listSyncManager.syncedListChanges.last?.operation, BringListContentOperation.toRecently)
        XCTAssertEqual(listSyncManager.syncedListChanges.count, 2)
    }

}
