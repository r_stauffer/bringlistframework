//
//  MockedListContentService.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 23.01.19.
//

import Foundation
import Alamofire
@testable import BringListFrameworkiOS

final class MockedListContentService: ListContentService {
    
    
    var didCallGetItems = false
    var getItemsResponse: BringListState?
    var getItemsShouldFail = false
    
    func getItems(forListUuid uuid: String, syncStartDate syncDate: Date, callback: @escaping (Result<BringListState>) -> Void) {
        didCallGetItems = true
        if !getItemsShouldFail {
            callback(Result.success(getItemsResponse!))
        } else {
            callback(Result.failure(NSError()))
        }
    }
    
    var didCallBatchUpdate = false
    var batchUpdateShouldFail = false
    func batchUpdate(_ request: BringBatchUpdateRequest, listUuid: String, callback: @escaping (Result<Void>) -> Void) {
        didCallBatchUpdate = true
        if !batchUpdateShouldFail {
            callback(Result.success(()))
        } else {
            callback(Result.failure(NSError()))
        }
    }
}
