//
//  BringListContentResetNewItemFlagsActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 01.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentResetNewItemFlagsActionTests: XCTestCase, BringListContentReducerTestUtils  {
    
    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testFlagReset() {
        // Given there is an item with item id "item_0" in the to-be-purchased section and has the isNew flag set to true.
        var currentListContent = listContentLoader.createDefaultListContent()
        let newItem = createItem(uuid: "random", itemId: "newItem", name: "New Item", isNewItem: true, shouldAssignUuid: false)
        currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.add(item: newItem)
        XCTAssertTrue(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.containsItems(withItemId: "newItem") ?? false)
        XCTAssertTrue(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.item(byUuid: "newItem", orItemId: "newItem")?.newItem ?? false)
        
        let selectAction = BringListContentSelectAction(item: newItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        XCTAssertTrue(currentListContent.getToBePurchasedSection().containsItems(withItemId: "newItem"))
        
        // when we reset the isNew flags
        let action = BringListContentResetNewItemFlagsAction()
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // The items
        XCTAssertFalse(newListContent.getToBePurchasedSection().item(byUuid: "newItem", orItemId: "newItem")?.newItem ?? true)
        XCTAssertFalse(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.item(byUuid: "newItem", orItemId: "newItem")?.newItem ?? true)
    }

}
