//
//  BringListContentUpdateSpecificationActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 02.03.19.
//

import XCTest
@testable import BringListFrameworkiOS
class BringListContentUpdateSpecificationActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testSpecUpdateOfItemInPurchaseSection() {
        // Given there is an item with item id "item_0" in the to-be-purchased section and has an empty specification.
        var currentListContent = listContentLoader.createDefaultListContent()
        let selectedItem = createItem(uuid: "random", itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        
        let selectAction = BringListContentSelectAction(item: selectedItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        XCTAssertTrue(currentListContent.getToBePurchasedSection().items.contains(selectedItem))
        
        // when we update the sepcification of that item
        let updateSpecificationAction = BringListContentUpdateSpecificationAction(uniqueIdentifier: selectedItem.uniqueIdentifier, specification: "S")
        currentListContent = listContentReducer.reduce(currentListContent, action: updateSpecificationAction)
        
        // then the item in the to-be-purchased section and the corresponding item in the catalog have the new specification
        XCTAssertEqual(currentListContent.getToBePurchasedSection().item(byUuid: "random", orItemId: "item_0")?.specification, "S")
        XCTAssertEqual(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.item(byUuid: "random", orItemId: "item_0")?.specification, "S")
    }
    
    func testSpecUpdateOfItemInCatalog() {
        // Given there is an item with item id "item_0" in the catalog and has an empty specification.
        var currentListContent = listContentLoader.createDefaultListContent()
        let selectedItem = createItem(uuid: "random", itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        
        let selectAction = BringListContentSelectAction(item: selectedItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)
        XCTAssertTrue(currentListContent.getToBePurchasedSection().items.contains(selectedItem))
        
        // when we update the sepcification of that item
        let updateSpecificationAction = BringListContentUpdateSpecificationAction(uniqueIdentifier: selectedItem.uniqueIdentifier, specification: "S")
        currentListContent = listContentReducer.reduce(currentListContent, action: updateSpecificationAction)
        
        // then the item in the to-be-purchased section and the corresponding item in the catalog have the new specification
        XCTAssertEqual(currentListContent.getToBePurchasedSection().item(byUuid: "random", orItemId: "item_0")?.specification, "S")
        XCTAssertEqual(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.item(byUuid: "random", orItemId: "item_0")?.specification, "S")
    }

}
