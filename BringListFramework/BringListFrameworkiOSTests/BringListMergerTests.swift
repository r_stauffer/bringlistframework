//
//  BringListMergerTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 15.10.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListMergerTests: XCTestCase {
    
    override func setUp() {}

    override func tearDown() {}
    
    // MARK: Item replacement

    func test_moving_an_item_to_purchase_while_syncing_and_backend_returns_older_item_in_recently() {
        
        // Given we have item with uuid "uuid_0" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we fetch item with uuid_0 from the server with it beeing in recently but having an older sync date
        let newRecentlyItem2 = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [],
            recently: [newRecentlyItem2],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the item with uuid "uuid_0" stays in the purchase section
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.purchase[0].uuid, "uuid_0")
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_moving_an_item_to_purchase_while_syncing_and_backend_returns_newer_item_in_recently() {
        
        // Given we have a saved list state with uuid_0 in purchase before the fetch is happening
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we fetch item with uuid uuid_0 and a newer modification date from the server
        let newRecentlyItem2 = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        let remoteListState = BringListState(
            purchase: [],
            recently: [newRecentlyItem2],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the item with uuid "uuid_0" is moved to the purchase section
        XCTAssertEqual(mergedListState.purchase.count, 0)
        XCTAssertEqual(mergedListState.recently[0].uuid, "uuid_0")
        XCTAssertEqual(mergedListState.recently.count, 1)
    }
    
    func test_moving_an_item_to_recently_while_syncing_and_backend_returns_newer_item_in_purchase() {
        // Given we have item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with a newer sync date, an empty recently but a new item in the purchase section
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the new purchase item is in the purchase section and the recently item is remove from the recently section
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.purchase[0].uuid, "uuid_0")
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_moving_an_item_to_recently_while_syncing_and_backend_returns_older_item_in_purchase() {
        // Given we have item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with an older sync date, with the item with uuid "uuid_0" being moved to purchase
        let purchaseItem = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [purchaseItem],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        // then the newer local item is considered and therefore stays in the recently section instead of the purchase section
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        XCTAssertEqual(mergedListState.purchase.count, 0)
        XCTAssertEqual(mergedListState.recently[0].uuid, "uuid_0")
        XCTAssertEqual(mergedListState.recently.count, 1)
    }
    
    // MARK: Spec update
    
    func test_spec_update_for_item_in_purchase_section() {
        // Given we have item with uuid "uuid_0" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date())
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with a newer sync date and the same item with uuid "uuid_0" but a different specification in the purchase section
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        // then the specification of item with uuid "uuid_0" gets updated
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.purchase[0].specification, "2")
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_spec_update_for_item_in_purchase_section_is_not_happening() {
        // Given we have item with uuid "uuid_0" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with an older sync date and the same item with uuid "uuid_0" but a different specification in the purchase section
        let newPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [newPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        // then the specification of item with uuid "uuid_0" gets not updated, because the sync date is older than the local date
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.purchase[0].specification, "")
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_newer_remote_item_is_saved_when_local_and_remote_item_are_in_recently() {
        // Given we have item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with a newer sync date and the same item with uuid "uuid_0" but a different specification in the recently section
        let newRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [],
            recently: [newRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the specification of item with uuid "uuid_0" gets updated
        XCTAssertEqual(mergedListState.recently.count, 1)
        XCTAssertEqual(mergedListState.recently[0].specification, "2")
        XCTAssertEqual(mergedListState.purchase.count, 0)
    }
    
    func test_newer_local_item_is_saved_when_local_and_remote_item_are_in_recently() {
        // Given we have item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000))
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when we receive an update from the backend with an older sync date and the same item with uuid "uuid_0" but a different specification in the recently section
        let newRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "2",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let remoteListState = BringListState(
            purchase: [],
            recently: [newRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the specification of item with uuid "uuid_0" gets not updated, because the sync date is older than the local date
        XCTAssertEqual(mergedListState.recently.count, 1)
        XCTAssertEqual(mergedListState.recently[0].specification, "")
        XCTAssertEqual(mergedListState.purchase.count, 0)
    }
    
    // MARK: Empty sections
    
    func test_empty_but_newer_recently_comes_from_the_remote() {
        // Given we have the item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when an update comes from the backend with a newer sync date and an empty recently section
        let remoteListState = BringListState(
            purchase: [],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the recently section gets emptied
        XCTAssertEqual(mergedListState.recently.count, 0)
        XCTAssertEqual(mergedListState.purchase.count, 0)
    }
    
    func test_empty_but_newer_purchase_comes_from_the_remote() {
        // Given we have the item with uuid "uuid_0" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when an update comes from the backend with a newer sync date and an empty purchase section
        let remoteListState = BringListState(
            purchase: [],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the purchase section gets emptied
        XCTAssertEqual(mergedListState.purchase.count, 0)
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_empty_older_recently_comes_from_the_remote() {
        // Given we have the item with uuid "uuid_0" in the recently section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when an update comes from the backend with a newer sync date and an empty recently section
        let remoteListState = BringListState(
            purchase: [],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the recently section gets emptied
        XCTAssertEqual(mergedListState.recently.count, 1)
        XCTAssertEqual(mergedListState.purchase.count, 0)
    }
    
    func test_empty_older_purchase_comes_from_the_remote() {
        // Given we have the item with uuid "uuid_0" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date(timeIntervalSinceNow: 1000)
        )
        
        let currentListState = BringListState(
            purchase: [currentPurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        // when an update comes from the backend with a newer sync date and an empty purchase section
        let remoteListState = BringListState(
            purchase: [],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the purchase section gets emptied
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.recently.count, 0)
    }
    
    func test_delete_only_one_item_from_purchase() {
        // Given we have the item with uuid "uuid_0" and item with uuid "uuid_1" in the purchase section
        let currentPurchase = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentPurchase2 = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [currentPurchase, currentPurchase2],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        let remotePurchase = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        // when delete the item with uuid "uuid_0" from the purchase and from the recently (via edit button in the recently section)
        let remoteListState = BringListState(
            purchase: [remotePurchase],
            recently: [],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the purchase section contains only the item with uuid "uuid_1"
        XCTAssertEqual(mergedListState.purchase.count, 1)
        XCTAssertEqual(mergedListState.recently.count, 0)
        XCTAssertEqual(mergedListState.purchase[0].uuid, "uuid_1")
    }
    
    func test_delete_only_one_item_from_recently() {
        // Given we have the item with uuid "uuid_0" and item with uuid "uuid_1" in the purchase section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentRecently2 = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [],
            recently: [currentRecently, currentRecently2],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        let remoteRecently = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        // when delete the item with uuid "uuid_0" from the purchase and from the recently (via edit button in the recently section)
        let remoteListState = BringListState(
            purchase: [],
            recently: [remoteRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the purchase section contains only the item with uuid "uuid_1"
        XCTAssertEqual(mergedListState.purchase.count, 0)
        XCTAssertEqual(mergedListState.recently.count, 1)
        XCTAssertEqual(mergedListState.recently[0].uuid, "uuid_1")
    }
    
    func test_multiple_item_movements() {
        // Given we have the item with uuid "uuid_0" and item with uuid "uuid_1" in the purchase section
        let currentRecently = BringListStateItem(
            uuid: "uuid_0",
            specification: "",
            itemId: "item_0",
            modificationDate: Date()
        )
        
        let currentRecently1 = BringListStateItem(
            uuid: "uuid_2",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let currentRecently2 = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let currentPurchase = BringListStateItem(
            uuid: "uuid_6",
            specification: "3",
            itemId: "item_6",
            modificationDate: Date()
        )
        
        let currentPurchase2 = BringListStateItem(
            uuid: "uuid_7",
            specification: "",
            itemId: "item_7",
            modificationDate: Date()
        )
        
        let currentListState = BringListState(
            purchase: [currentPurchase, currentPurchase2],
            recently: [currentRecently, currentRecently2, currentRecently1],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid")
        
        let remoteRecently = BringListStateItem(
            uuid: "uuid_1",
            specification: "",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let remoteRecently1 = BringListStateItem(
            uuid: "uuid_2",
            specification: "3",
            itemId: "item_1",
            modificationDate: Date()
        )
        
        let remotePurchase = BringListStateItem(
            uuid: "uuid_3",
            specification: "3",
            itemId: "item_2",
            modificationDate: Date()
        )
        
        let remotePurchaseNewInRecently = BringListStateItem(
            uuid: "uuid_6",
            specification: "3",
            itemId: "item_6",
            modificationDate: Date()
        )
        
        let remotePurchaseStay = BringListStateItem(
            uuid: "uuid_7",
            specification: "",
            itemId: "item_7",
            modificationDate: Date()
        )
        
        // when delete the item with uuid "uuid_0" from the purchase and from the recently (via edit button in the recently section)
        let remoteListState = BringListState(
            purchase: [remotePurchase, remotePurchaseStay],
            recently: [remoteRecently, remoteRecently1, remotePurchaseNewInRecently],
            status: BringListStatus.unregistered.rawValue,
            uuid: "listUuid"
        )
        
        let merger = BringDefaultListStateMerger(syncStartDate: Date())
        let mergedListState = merger.merge(remoteListState, withOther: currentListState)
        
        // then the purchase section contains only the item with uuid "uuid_1"
        XCTAssertEqual(mergedListState.purchase.count, 2)
        XCTAssertEqual(mergedListState.recently.count, 3)
    }

}
