//
//  BringListContentResponseTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 23.01.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentResponseTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {}

    func testParsingResponse() {
        let json = loadResource(res: "ListContentResponse", ext: "json")!
        let listContentResponse = try! BringListContentDecoder().decode(BringListContentResponse.self, from: json)
        XCTAssertEqual(listContentResponse.uuid, "f5310454-adee-4b94-8413-f4149faf7be0")
        XCTAssertEqual(listContentResponse.status, .registered)
        
        XCTAssertEqual(listContentResponse.items.purchase.count, 1)
        XCTAssertEqual(listContentResponse.items.purchase[0].uuid, "07fa3322-14fd-45e5-ae7f-1f12065a5249")
        XCTAssertEqual(listContentResponse.items.purchase[0].itemId, "buttermilch")
        XCTAssertEqual(listContentResponse.items.purchase[0].specification, "")
        
        XCTAssertEqual(listContentResponse.items.recently.count, 2)
        XCTAssertEqual(listContentResponse.items.recently[0].uuid, "54c8590e-6a83-474d-bfb9-3f237df9fc30")
        XCTAssertEqual(listContentResponse.items.recently[0].itemId, "Peperoni")
        XCTAssertEqual(listContentResponse.items.recently[0].specification, "")
    }
    
    func testParsingResponseWithEmptyToBePurchasedSection() {
        let json = loadResource(res: "ListContentResponseEmptyToBePurchasedSection", ext: "json")!
        let listContentResponse = try! BringListContentDecoder().decode(BringListContentResponse.self, from: json)
        XCTAssertEqual(listContentResponse.uuid, "f5310454-adee-4b94-8413-f4149faf7be0")
        XCTAssertEqual(listContentResponse.status, .registered)
        XCTAssertEqual(listContentResponse.items.purchase.count, 0)
        XCTAssertEqual(listContentResponse.items.recently.count, 2)
    }
    
    func testParsingResponseWithEmptyRecentlySection() {
        let json = loadResource(res: "ListContentResponseWithoutRecently", ext: "json")!
        let listContentResponse = try! BringListContentDecoder().decode(BringListContentResponse.self, from: json)
        XCTAssertEqual(listContentResponse.uuid, "f5310454-adee-4b94-8413-f4149faf7be0")
        XCTAssertEqual(listContentResponse.status, .registered)
        XCTAssertEqual(listContentResponse.items.purchase.count, 1)
        XCTAssertEqual(listContentResponse.items.recently.count, 0)
    }

}
