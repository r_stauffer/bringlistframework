//
//  BringListManagerTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 19.01.19.
//

import XCTest
@testable import BringListFrameworkiOS


class BringListManagerTests: XCTestCase {

    var listManager: BringListContentManager!
    var sectionLoader: TestSectionLoader!
    var listSyncManager: MockListChangeSyncer!
    var catalogLoader: BringCatalogLoader!
    var listSettings: BringListSettings!
    
    override func setUp() {
        sectionLoader = TestSectionLoader()
        listSyncManager = MockListChangeSyncer()
        let expt = expectation(description: "Should have loaded data sources before running the tests")
        catalogLoader = BringCatalogLoader(dataSources: [], baseDataSource: sectionLoader)
        BringListContentManager.setup(with: catalogLoader, settingsProvider: BringListContentSettingsProviderMock(), maxRecently: 12)
        listSettings = BringListSettings(suiteName: "BringListManagerTestsSuite")
        listManager = BringListContentManager(locationService: MockLocationService(), listSyncManager: listSyncManager, listSettings: listSettings)
        listManager.initializeListContent {
            expt.fulfill()
        }
        wait(for: [expt], timeout: 5.0)
    }

    override func tearDown() {}
    
    // MARK: Recommended section update
    
    func testUpdateRecommendedSectionWithItemThatIsAlreadyInToBePurchased() {
        // Given item_4 is aleardy in the to-be-purchased section
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        
        let item4 = BringItem(uuid: nil, itemId: "item_4", name: "Item 4")
        let item5 = BringItem(uuid: nil, itemId: "item_5", name: "Item 5")
        
        // When updating the recommended section with item_4 among other items
        listManager.updateRecommendedSection(sectionName: "Name", items: [item4,item5])
        
        // Only item_5 is added to the recommended section.
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items[0].itemId, "item_5")
    }
    
    func testUpdateRecommendedSectionAndToBePurchasedSectionWhenSelectingItems() {
        
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        
        let item4 = BringItem(uuid: nil, itemId: "item_4", name: "Item 4")
        let item5 = BringItem(uuid: nil, itemId: "item_5", name: "Item 5")
        
        // Given the recommended section has been updated
        listManager.updateRecommendedSection(sectionName: "Name", items: [item4, item5])
        
        // When an item gets selected
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        
        // then the to-be-purchased and recommended section get updated correctly
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 1)
        
        // When the item gets deselected again
        listManager.select(itemToSelect)
        
        // then the to-be-purchased and recommended section get updated correctly
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 2)

        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
    }
    
    func testUpdateRecommendedSection() {
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        
        // When updating the recommended section
        let item4 = BringItem(uuid: nil, itemId: "item_4", name: "Item 4")
        let item5 = BringItem(uuid: nil, itemId: "item_5", name: "Item 5")
        
        listManager.updateRecommendedSection(sectionName: "Name", items: [item4, item5])
        
        // Then the items are present in the recommended section
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 2)
        XCTAssertEqual(listManager.getCurrentModel().initialRecommenedItemKeys[0], "item_4")
        XCTAssertEqual(listManager.getCurrentModel().initialRecommenedItemKeys[1], "item_5")
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().name, "Name")
    }
    
    func testDeselectItemAfterUpdatingRecommendedSection() {
        // Given item_4 is aleardy in the to-be-purchased section
        XCTAssertTrue(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count == 0)
        let itemToSelect = createItem(uuid: nil, name: "Item 4", key: "item_4")
        listManager.select(itemToSelect)
        
        let item4 = BringItem(uuid: nil, itemId: "item_4", name: "Item 4")
        let item5 = BringItem(uuid: nil, itemId: "item_5", name: "Item 5")
        
        // When updating the recommended section with item_4 among other items
        listManager.updateRecommendedSection(sectionName: "Name", items: [item4,item5])
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 1)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 1)
        
        // After the recommended section update, item_4 goes into the recommended section again.
        listManager.select(itemToSelect)
        XCTAssertEqual(listManager.getCurrentModel().getRecommendedSection().items.count, 2)
        XCTAssertEqual(listManager.getCurrentModel().getToBePurchasedSection().items.count, 0)
        
        XCTAssertEqual(listManager.getCurrentModel().getRecentlyPurchasedSection().items.count, 1)
    }
    
    func testReordersections() {
        XCTAssertEqual(listManager.getCurrentModel().getSections()[0].sectionId, "Tierbedarf")
        sectionLoader.shouldReorderSection = true
        
        let expt = expectation(description: "Should have loaded data sources before running the tests")
        listManager.initializeListContent {
            expt.fulfill()
        }
        wait(for: [expt], timeout: 5.0)
        listManager.reorderSections()
        XCTAssertEqual(listManager.getCurrentModel().getSections()[0].sectionId, "meat")
    }
    
    func testMoveItemToNewSection() {
        // Given we have item_4 in section Tierbedarf
        let item4 = BringItem(uuid: nil, itemId: "item_4", name: "Item 4")
        
        let sectionTierBedarfBeforeMove = listManager.getCurrentModel().getSectionsCopy().filter({ (section) -> Bool in
            section.sectionId == "Tierbedarf"
        }).first
        
        XCTAssertEqual(sectionTierBedarfBeforeMove?.items(withItemId: "item_4").count, 1)
        
        let sectionMeatBeforeMove = listManager.getCurrentModel().getSectionsCopy().filter({ (section) -> Bool in
            section.sectionId == "meat"
        }).first
        
        XCTAssertEqual(sectionMeatBeforeMove?.items(withItemId: "item_4").count, 0)
        
        // wehen we move the item to the meat section
        let section = BringSection(sectionId: "meat", name: "Meat")
        
        listManager.move(catalogItem: item4, toSection: section)
        
        // then item_4 is gone from the Tierbedarf section and now in the meat section
        let sectionTierBedarf = listManager.getCurrentModel().getSectionsCopy().filter({ (section) -> Bool in
            section.sectionId == "Tierbedarf"
        }).first
        
        XCTAssertEqual(sectionTierBedarf?.items(withItemId: "item_4").count, 0)
        
        let sectionMeat = listManager.getCurrentModel().getSectionsCopy().filter({ (section) -> Bool in
            section.sectionId == "meat"
        }).first
        
        XCTAssertEqual(sectionMeat?.items(withItemId: "item_4").count, 1)
    }
    
    // MARK: MISC
    
    private func createSection(name: String, key: String, items: [BringItem] = []) -> BringSection {
        return BringSection(sectionId: key, name: name, items: items)
    }
    
    private func createItem(uuid: String?, name: String, key: String) -> BringItem {
        return BringItem(uuid: uuid, itemId: key, name: name)
    }
}
