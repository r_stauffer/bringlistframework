//
//  MockLanguageProvider.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import Foundation
@testable import BringListFrameworkiOS

class MockLanguageProvider: LanguageProvider {
    
    
    var didCallGetLanguage = false
    var getLanguageCodeResult = "de-CH"
    
    func getLanguageCode() -> String {
        didCallGetLanguage = true
        return getLanguageCodeResult
        
    }
    
    var didCallGetCountryCode = false
    var getCountryCodeResult = "CH"
    
    func getCountryCode() -> String {
        didCallGetCountryCode = true
        return getCountryCodeResult
    }
    
    var didCallGetArticleLanguage = false
    var getArticleLanguageResult = "de-CH"
    
    func getArticleLanguage() -> String {
        didCallGetArticleLanguage = true
        return getArticleLanguageResult
    }
}
