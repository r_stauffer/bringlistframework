//
//  BringListContentUpdateRecommendedActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 03.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentUpdateRecommendedActionTests: XCTestCase, BringListContentReducerTestUtils {

    let listContentReducer = BringListContentReducer()
    let sectionLoader = TestSectionLoader()
    
    override func setUp() {
    }

    override func tearDown() {
    }
    
    func testRecommendedSectionUpdateUpdatesInitialKeys() {
        // Given we have an empty recommended section
        let currentListContent = sectionLoader.createDefaultListContent()
        XCTAssertTrue(currentListContent.initialRecommenedItemKeys.isEmpty)
        
        // when we update the recommended section with item "item_0"
        let item = createItem(uuid: nil, itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        let action = BringListContentUpdateRecommendedAction(sectionName: "Name", items: [item])
        
        let newListContent = listContentReducer.reduce(currentListContent, action: action)

        // then the initial recommended keys get updated.
        XCTAssertEqual(newListContent.initialRecommenedItemKeys[0], item.itemId)
    }
    
    func testRecommendedSectionUpdateSectionNameGetsUpdated() {
        // Given we have an empty recommended section with no name
        let currentListContent = sectionLoader.createDefaultListContent()
        XCTAssertEqual(currentListContent.getRecommendedSection().name, "")
        
        // when we update the recommended section with the new name "Name"
        let item = createItem(uuid: nil, itemId: "item_0", name: "Item 0", shouldAssignUuid: false)
        let action = BringListContentUpdateRecommendedAction(sectionName: "Name", items: [item])
        
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        
        // then the recommended section has the name "Name"
        XCTAssertEqual(newListContent.getRecommendedSection().name, "Name")
    }

}
