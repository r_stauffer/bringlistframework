//
//  BringBatchUpdateRequestEncodingTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 30.01.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringBatchUpdateRequestEncodingTests: XCTestCase {
    
    override func setUp() {
    }

    override func tearDown() {
    }

    func testEncoding() {
        let listChange = BringListChange(uuid: UUID().uuidString, listUuid: "listUuid", itemId: "itemId", spec: "", location: nil, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        let request = BringBatchUpdateRequest(sender: "sender", listChanges: [listChange])
        let encoded = try? JSONEncoder().encode(request)
        XCTAssertNotNil(encoded)
    }

}
