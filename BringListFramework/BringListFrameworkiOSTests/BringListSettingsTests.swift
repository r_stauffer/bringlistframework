//
//  BringListSettingsTests.swift
//  BringListFrameworkiOSTests
//
//  Created by Renato Stauffer on 29.11.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListSettingsTests: XCTestCase {
    
    var settings: BringListSettings!
    var languageProvider: MockLanguageProvider!
    
    override func setUp() {
        languageProvider = MockLanguageProvider()
        settings = BringListSettings(suiteName: "BringListSettingsTestsSuite", languageProvider: languageProvider)
        settings.reset()
    }

    func test_setLastSyncDate() {
        let date = settings.lastSyncDate()
        XCTAssertNil(date)
        
        let dateToStore = Date()
        settings.setLastSyncDate(dateToStore)
        
        let storedDate = settings.lastSyncDate()
        XCTAssertEqual(storedDate?.timeIntervalSinceReferenceDate, dateToStore.timeIntervalSinceReferenceDate)
    }
    
    func test_did_migrate_user_settings() {
        XCTAssertFalse(settings.didMigrateUserSettings())
        settings.setDidMigrateUserSettings(true)
        XCTAssertTrue(settings.didMigrateUserSettings())
    }
    
    func test_did_migrate_user_lists() {
        XCTAssertFalse(settings.didMigrateUserLists())
        settings.setDidMigrateUserLists(true)
        XCTAssertTrue(settings.didMigrateUserLists())
    }
    
    func test_did_migrate_list_changes() {
        XCTAssertFalse(settings.didMigrateListChanges())
        settings.setDidMigrateListChanges(true)
        XCTAssertTrue(settings.didMigrateListChanges())
    }
    
    func test_did_migrate_user_items() {
        XCTAssertFalse(settings.didMigrateUserItems())
        settings.setDidMigrateUserItems(true)
        XCTAssertTrue(settings.didMigrateUserItems())
    }
    
    func test_did_migrate_user_item_details() {
        XCTAssertFalse(settings.didMigrateListItemDetails())
        settings.setDidMigrateListItemDetails(true)
        XCTAssertTrue(settings.didMigrateListItemDetails())
    }
    
    func test_did_migrate_list_state() {
        XCTAssertFalse(settings.didMigrateListStates())
        settings.setDidMigrateListStates(true)
        XCTAssertTrue(settings.didMigrateListStates())
    }
}
