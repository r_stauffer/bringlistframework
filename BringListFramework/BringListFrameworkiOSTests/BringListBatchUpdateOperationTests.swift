//
//  BringListBatchUpdateOperationTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 02.02.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

class BringListBatchUpdateOperationTests: XCTestCase {
    
    var operation: BringListBatchUpdateOperation!
    var listContentService: MockedListContentService!
    var listChangesStore: BringCoreDataListChangesStore!
    let queue = OperationQueue()
    let privateQueue = OperationQueue()
    
    let listUuid = "listUuid"
    let token = ""
    
    override func setUp() {
        queue.maxConcurrentOperationCount = 1
        privateQueue.maxConcurrentOperationCount = 1
        listContentService = MockedListContentService()
        listChangesStore = BringCoreDataListChangesStore(coreDataStack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
        operation = BringListBatchUpdateOperation(
            listContentService: listContentService,
            listChangesStore: listChangesStore,
            listUuid: listUuid,
            apnsToken: token, syncStartDate: Date())
    }

    override func tearDown() {
    }

    func testAllListChangesGetDeletedAfterTheBatchupdateSucceeded() {
        
        let expt = expectation(description: "Operation should delete all stored list changes")
        
        // Given we have stored a list change with uuid "listUuid"
        let listChange = BringListChange(uuid: "itemUuid", listUuid: "listUuid", itemId: "itemid", spec: "spec", location: nil, operation: .toPurchase, timeStamp: Date(timeInterval: -1000, since: Date()).timeIntervalSince1970)
        listChangesStore.add(listChange, isWaiting: false) { _ in
            self.listChangesStore.allChanges(forListUuid: self.listUuid, isWaiting: false, sortDescriptors: [], callback: { (result) in
                switch result {
                case .success(let changes):
                    XCTAssertEqual(changes.count, 1)
                    // When we do a batch update.
                    self.performBatchupdate(expectation: expt)
                case .failure:
                    break
                }
            })
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testOnlyTheListChangesOfTheSpecifiedListUuidGetDeleted() {
        let expt = expectation(description: "Operation should delete all stored list changes")
        
        // Given we have stored list changes from different lists
        let listChange = BringListChange(uuid: "itemUuid", listUuid: "listUuid", itemId: "itemid", spec: "spec", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        let otherListChange = BringListChange(uuid: "itemUuid", listUuid: "notDeleted", itemId: "itemid", spec: "spec", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        listChangesStore.add(otherListChange, isWaiting: true) { _ in }
        listChangesStore.add(listChange, isWaiting: true) { _ in }
        
        privateQueue.addOperation {
            self.queue.addOperation(self.operation)
            self.queue.waitUntilAllOperationsAreFinished()
            
            // When we do the batch update
            self.listChangesStore.allChanges(forListUuid: "notDeleted", isWaiting: false, sortDescriptors: [], callback: { (result) in
                switch result {
                case .success(let changes):
                    // Then only the changes of the list with uuid "listUuid" get deleted and not the changes with uuid "notDeleted"
                    XCTAssertEqual(changes.count, 1)
                    expt.fulfill()
                case .failure:
                    break
                }
            })
        }
        
        
        wait(for: [expt], timeout: 5.0)
    }
    
    func testSendingListChangesToTheServer() {
        let expt = expectation(description: "Operation should delete all stored list changes")
        
        // Given we have stored a list change with uuid "listUuid"
        let listChange = BringListChange(uuid: "itemUuid", listUuid: "listUuid", itemId: "itemid", spec: "spec", location: nil, operation: .toPurchase, timeStamp: Date().timeIntervalSince1970)
        listChangesStore.add(listChange, isWaiting: false) { _ in }
        
        privateQueue.addOperation {
            // When we perform a batchupdate
            self.queue.addOperation(self.operation)
            self.queue.waitUntilAllOperationsAreFinished()
            // Then the list changes are sent to the server
            XCTAssertTrue(self.listContentService.didCallBatchUpdate)
            expt.fulfill()
        }
        
        wait(for: [expt], timeout: 5.0)
    }
    
    private func performBatchupdate(expectation: XCTestExpectation) {
        privateQueue.addOperation {
            self.queue.addOperation(self.operation)
            self.queue.waitUntilAllOperationsAreFinished()
            self.listChangesStore.allChanges(forListUuid: self.listUuid, isWaiting: false, sortDescriptors: [], callback: { (result) in
                switch result {
                case .success(let changes):
                    // Then all the changes with uuid "listUuid" are deleted after the batchupdate.
                    XCTAssertEqual(changes.count, 0)
                    expectation.fulfill()
                case .failure:
                    break
                }
            })
        }
    }

}
