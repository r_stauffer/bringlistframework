//
//  BringListContentEnrichmentActionTests.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 01.03.19.
//

import XCTest
@testable import BringListFrameworkiOS

class BringListContentEnrichmentActionTests: XCTestCase, BringListContentReducerTestUtils {
    
    let listContentReducer = BringListContentReducer()
    let listContentLoader = TestSectionLoader()
    
    override func setUp() {}

    override func tearDown() {}
    
    func testEnrichmentAddNewSection() {
        // Given we have a list content without a section with sectionId "NewSection"
        let currentListContent = listContentLoader.createDefaultListContent()

        // when we enrich the list content with a section with sectionId "NewSection"
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [
                BringCatalogChange.SectionToAdd(
                position: 0,
                sectionId: "NewSection",
                name: "New Section",
                itemIds: ["item_0"])
            ],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )
        
        // then the list content does add this section with the corresponding items to the catalog
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertEqual(newListContent.getSections().count, currentListContent.getSections().count + 1)
        
        let addedSection = newListContent.catalogSection(bySectionId: "NewSection")
        XCTAssertNotNil(addedSection)
        XCTAssertEqual(addedSection?.items.count, 1)
    }

    func testEnrichmentAddSectionThatAlreadyExists() {
        // Given we have a list content with a section with sectionId "Tierbedarf"
        let currentListContent = listContentLoader.createDefaultListContent()

        // when we enrich the list content with a section with sectionId "Tierbedarf"
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [
                BringCatalogChange.SectionToAdd(
                    position: 0,
                    sectionId: "Tierbedarf",
                    name: "Tierbedarf",
                    itemIds: []
                )
            ],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )

        // then the list content does not change
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertEqual(newListContent.getSections().count, currentListContent.getSections().count)
    }

    func testAddItem() {
        // Given we have a list content with a section with sectionId "Tierbedarf"
        let currentListContent = listContentLoader.createDefaultListContent()

        // when we receive a catalog change with an new item item "newItem"
        let newItem = BringItem(uuid: nil, itemId: "Bier")
        
        let catalogChange = BringCatalogChange(
            itemsToAdd: [
                BringCatalogChange.ItemToAdd(
                    item: newItem,
                    sectionId: "Tierbedarf"
                )
            ],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )
        
        // then the section Tierbedarf contains the new item
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertTrue(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items.contains(newItem) ?? false)
    }

    func testAddItemThatIsAlreadyInSection() {
        // Given we have a list content with a section with sectionId "Tierbedarf"
        let currentListContent = listContentLoader.createDefaultListContent()

        // when we enrich the list content with a item that already exists in the section
        let newItem = BringItem(uuid: nil, itemId: "item_0")
        
        let catalogChange = BringCatalogChange(
            itemsToAdd: [
                BringCatalogChange.ItemToAdd(
                    item: newItem,
                    sectionId: "Tierbedarf"
                )
            ],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )

        // then the section Tierbedarf is not altered
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertEqual(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items.filter { $0.itemId == "item_0" }.count, 1)
    }

    func testRemoveItemRemovesItemFromTheCatalog() {
        // Given we have a list content with a section with sectionId "Tierbedarf" and item "item_0" in it
        let currentListContent = listContentLoader.createDefaultListContent()

        // when we remove the item "item_0" from the section "Tierbedarf"
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: ["item_0"]
        )

        // then the item is removed from the catalog
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertEqual(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items(withItemId: "item_0").count, 0)
    }

    func testRemoveItemRemovesItemNotFromPurchaseSection() {
        // Given we have item "item_0" in the to-be-purchased section
        var currentListContent = listContentLoader.createDefaultListContent()

        let newItem = self.createItem(uuid: "uuid", itemId: "item_0", name: "New Item", shouldAssignUuid: false)
        let selectAction = BringListContentSelectAction(item: newItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)

        XCTAssertTrue(currentListContent.getToBePurchasedSection().items.contains(newItem))

        // when we remove the item "item_0" from the section "Tierbedarf"
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: ["item_0"]
        )

        // then the item is not removed from the purchase section
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertTrue(newListContent.getToBePurchasedSection().items.contains(newItem))
    }

    func testRemoveItemRemovesItemNotFromRecentlySection() {
        // Given we have item "item_0" in the recently section
        var currentListContent = listContentLoader.createDefaultListContent()

        let newItem = self.createItem(uuid: "uuid", itemId: "item_0", name: "New Item", shouldAssignUuid: false)
        let selectAction = BringListContentSelectAction(item: newItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: selectAction)

        XCTAssertTrue(currentListContent.getToBePurchasedSection().items.contains(newItem))

        let toRecentlyAction = BringListContentSelectAction(item: newItem)
        currentListContent = listContentReducer.reduce(currentListContent, action: toRecentlyAction)

        XCTAssertTrue(currentListContent.getRecentlyPurchasedSection().items.contains(newItem))

        // when we remove the item "item_0" from the section "Tierbedarf"
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: ["item_0"]
        )

        // then the item is not removed from the recently section
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertTrue(newListContent.getRecentlyPurchasedSection().items.contains(newItem))
    }

    func testMoveItemToNewSection() {
        // Given we have a list content with item item_0_section_2 in section meat as a user item
        let currentListContent = listContentLoader.createDefaultListContent()
        currentListContent.item(forItemId: "item_0_section_2")?.userItem = true

        let newItem = self.createItem(uuid: nil, itemId: "item_0_section_2", name: "Meat 0", isUserItem: true, shouldAssignUuid: false)
        XCTAssertTrue(currentListContent.catalogSectionReference(bySectionId: "meat")?.items.contains(newItem) ?? false)
        XCTAssertFalse(currentListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items.contains(newItem) ?? true)

        // when we move the item to the section Tierbedarf
        let catalogChange = BringCatalogChange(
            itemsToAdd: [
                BringCatalogChange.ItemToAdd(
                    item: newItem,
                    sectionId: "Tierbedarf"
                )
            ],
            sectionsToAdd: [],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )

        // then the section Tierbedarf contains the item item_0_section_2
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertFalse(newListContent.catalogSectionReference(bySectionId: "meat")?.items.contains(newItem) ?? true)
        XCTAssertTrue(newListContent.catalogSectionReference(bySectionId: "Tierbedarf")?.items.contains(newItem) ?? false)
    }
    
    func test_adding_section_takes_position_into_account() {
        // Given we have a list content without a section with sectionId "NewSection"
        let currentListContent = listContentLoader.createDefaultListContent()
        
        // when we enrich the list content with a section with sectionId "NewSection" at position 0 and "NewSection2" at position 2
        let catalogChange = BringCatalogChange(
            itemsToAdd: [],
            sectionsToAdd: [
                BringCatalogChange.SectionToAdd(
                    position: 0,
                    sectionId: "NewSection",
                    name: "New Section",
                    itemIds: []),
                BringCatalogChange.SectionToAdd(
                    position: 2,
                    sectionId: "NewSection2",
                    name: "New Section",
                    itemIds: [])
            ],
            translationsToAdd: [],
            iconsToAdd: [],
            itemsToRemove: []
        )
        
        // then the sections are at their correct position
        let action = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        let newListContent = listContentReducer.reduce(currentListContent, action: action)
        XCTAssertEqual(newListContent.getSections().count, currentListContent.getSections().count + 2)
        
        XCTAssertEqual(newListContent.getSectionsCopy()[0].sectionId, "NewSection")
        XCTAssertEqual(newListContent.getSectionsCopy()[2].sectionId, "NewSection2")
    }

}
