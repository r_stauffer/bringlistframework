//
//  BringCatalogLoaderTest.swift
//  BringUnitTests
//
//  Created by Renato Stauffer on 13.02.19.
//

import XCTest
import CoreData
@testable import BringListFrameworkiOS

fileprivate class BringTestBaseDataSource: BaseCatalogDataSource {
    
    var didCallLoadSections = false
    var sectionsToReturn = [BringSection]()
    
    func loadSections(callback: @escaping ([BringSection]) -> Void) {
        didCallLoadSections = true
        callback(sectionsToReturn)
    }
    
}

fileprivate class BringTestDataSource: CatalogDataSource {
    
    var didCallLoadChange = false
    var changeToReturn = BringCatalogChange.empty()
    
    func loadCatalogChange(callback: @escaping (BringCatalogChange) -> Void) {
        didCallLoadChange = true
        callback(changeToReturn)
    }
    
    var isDefaultCatalogDataSource: Bool {
        return true
    }
}



class BringCatalogLoaderTest: XCTestCase {

    var catalogLoader: BringCatalogLoader!
    fileprivate var dataSource: BringTestBaseDataSource!
    var itemDetailStore: BringCoreDataListItemDetailStore!
    var listSettings: BringListSettings!
    var settingsProvider: BringListContentSettingsProviderMock!
    
    override func setUp() {
        dataSource = BringTestBaseDataSource()
        itemDetailStore = BringCoreDataListItemDetailStore(stack: BringCoreDataListsStackFactory().makeCoreDataStack(storeType: NSInMemoryStoreType))
        listSettings = BringListSettings(suiteName: "BringCatalogLoaderTestSuite")
        settingsProvider = BringListContentSettingsProviderMock()
        catalogLoader = BringCatalogLoader(dataSources: [], baseDataSource: dataSource, itemDetailStore: itemDetailStore)
    }

    override func tearDown() {}
    
    func test_callback_is_called() {
        // Given we do not have data sources set
        catalogLoader = BringCatalogLoader(dataSources: [], baseDataSource: dataSource)
        
        // when we load the catalog
        let expt = expectation(description: "Should have loaded data sources before running the tests")
        catalogLoader.loadCatalog(ForList: "uuid") { (_, _) in
            expt.fulfill()
        }
    
        //then the callback of the loadDataSources method is called
        wait(for: [expt], timeout: 5.0)
    }
    
    func test_the_correct_number_of_sections_is_loaded() {
        // Given we have two dataSources set with 2 sections and 1 section
        let exp = expectation(description: "callback gets called")
        let dataSource1 = BringTestBaseDataSource()
        dataSource1.sectionsToReturn = [BringSection(sectionId: "id", name: "name"), BringSection(sectionId: "id2", name: "name2")]
        let dataSource2 = BringTestDataSource()
        
        dataSource2.changeToReturn =
            BringCatalogChange(
                itemsToAdd: [],
                sectionsToAdd: [BringCatalogChange.SectionToAdd(position: 0, sectionId: "id3", name: "name", itemIds: [])],
                translationsToAdd: [],
                iconsToAdd: [],
                itemsToRemove: []
            )
        
        let settingsProvider = BringListContentSettingsProviderMock()
        
        catalogLoader = BringCatalogLoader(dataSources: [dataSource2], baseDataSource: dataSource1)
        
        // when we load the sections
        catalogLoader.loadCatalog(ForList: settingsProvider.seletectedList() ?? "") { (catalog, extensions) in
            // then the number of sections to add is 1
            XCTAssertEqual(extensions.sectionsToAdd.count, 1)
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func test_own_item_with_item_detail_icon_is_loaded() {
        let exp = expectation(description: "callback gets called")
        let dataSource1 = BringTestBaseDataSource()
        
        dataSource1.sectionsToReturn = [
            BringSection(
                sectionId: "Drinks",
                name: "Drinks",
                items: [
                    BringItem(uuid: nil, itemId: "Beer", name: "Beer"),
                    BringItem(uuid: nil, itemId: "Wine", name: "Wine")
                ]),
            BringSection(
                sectionId: "Meat",
                name: "Meat",
                items: [
                    BringItem(uuid: nil, itemId: "Beef", name: "Beef"),
                ]
            )
        ]
        
        let dataSource2 = BringTestDataSource()
        
        let itemDetail = BringListItemDetail(itemKey: "Chicken", listUuid: "chicken-list", userIconItemId: "Beer")
        
        // Given we have an item detail with a user section (Drinks) assigned to the item "Chicken"
        self.settingsProvider.seletectedListReturnValue = "chicken-list"
        
        itemDetailStore.addBringListItemDetail(itemDetail)
        
        dataSource2.changeToReturn =
            BringCatalogChange(
                itemsToAdd: [
                    BringCatalogChange.ItemToAdd(item: BringItem(uuid: nil, itemId: "Chicken", name: "Chicken"), sectionId: "Meat"),
                    BringCatalogChange.ItemToAdd(item: BringItem(uuid: nil, itemId: "Pork", name: "Pork"), sectionId: "Meat")
                ],
                sectionsToAdd: [],
                translationsToAdd: [],
                iconsToAdd: [],
                itemsToRemove: []
        )
        
        catalogLoader = BringCatalogLoader(dataSources: [dataSource2], baseDataSource: dataSource1, itemDetailStore: itemDetailStore)
        
        // when we load the sections
        catalogLoader.loadCatalog(ForList: "chicken-list") { (catalog, extensions) in
            // the Chicken item is in the Drinks section.
            XCTAssertEqual(extensions.itemsToAdd[0].item.itemId, "Chicken")
            XCTAssertEqual(extensions.itemsToAdd[0].sectionId, "Eigene Artikel")
            XCTAssertEqual(extensions.itemsToAdd[1].item.itemId, "Pork")
            XCTAssertEqual(extensions.itemsToAdd[1].sectionId, "Meat")
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5.0)
    }
    
    func test_item_detail_is_considered_for_items_to_add_in_a_catalog_change() {
        let exp = expectation(description: "callback gets called")
        let dataSource1 = BringTestBaseDataSource()
        
        dataSource1.sectionsToReturn = [
            BringSection(
                sectionId: "Drinks",
                name: "Drinks",
                items: [
                    BringItem(uuid: nil, itemId: "Beer", name: "Beer"),
                    BringItem(uuid: nil, itemId: "Wine", name: "Wine")
                ]),
            BringSection(
                sectionId: "Meat",
                name: "Meat",
                items: [
                    BringItem(uuid: nil, itemId: "Beef", name: "Beef"),
                ]
            )
        ]
        
        let dataSource2 = BringTestDataSource()
        
        let itemDetail = BringListItemDetail(itemKey: "Chicken", listUuid: "chicken-list", userSectionId: "Drinks")
        
        // Given we have an item detail with a user section (Drinks) assigned to the item "Chicken"
        self.settingsProvider.seletectedListReturnValue = "chicken-list"
        
        itemDetailStore.addBringListItemDetail(itemDetail)
        
        dataSource2.changeToReturn =
            BringCatalogChange(
                itemsToAdd: [
                    BringCatalogChange.ItemToAdd(item: BringItem(uuid: nil, itemId: "Chicken", name: "Chicken"), sectionId: "Meat"),
                    BringCatalogChange.ItemToAdd(item: BringItem(uuid: nil, itemId: "Pork", name: "Pork"), sectionId: "Meat")
                ],
                sectionsToAdd: [],
                translationsToAdd: [],
                iconsToAdd: [],
                itemsToRemove: []
        )
        
        catalogLoader = BringCatalogLoader(dataSources: [dataSource2], baseDataSource: dataSource1, itemDetailStore: itemDetailStore)
        
        // when we load the sections
        catalogLoader.loadCatalog(ForList: "chicken-list") { (catalog, extensions) in
            // the Chicken item is in the Drinks section.
            XCTAssertEqual(extensions.itemsToAdd[0].item.itemId, "Chicken")
            XCTAssertEqual(extensions.itemsToAdd[0].sectionId, "Drinks")
            XCTAssertEqual(extensions.itemsToAdd[1].item.itemId, "Pork")
            XCTAssertEqual(extensions.itemsToAdd[1].sectionId, "Meat")
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5.0)
    }

}
