//
//  BringListFrameworkwatchOS.h
//  BringListFrameworkwatchOS
//
//  Created by Renato Stauffer on 05.11.19.
//

#import "UIKit/UIKit.h"

//! Project version number for BringListFrameworkwatchOS.
FOUNDATION_EXPORT double BringListFrameworkwatchOSVersionNumber;

//! Project version string for BringListFrameworkwatchOS.
FOUNDATION_EXPORT const unsigned char BringListFrameworkwatchOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BringListFrameworkwatchOS/PublicHeader.h>

#import "BringEnvironment.h"
