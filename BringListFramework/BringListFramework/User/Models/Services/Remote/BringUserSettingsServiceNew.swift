//
//  BringUserSettingsServiceNew.swift
//  Bring
//
//  Created by Sascha Thöni on 03.01.17.
//
//

import Alamofire
import ObjectMapper

public protocol BringUserSettingsServiceNewProtocol {
    func setSetting(userUuid: String, key: String, value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->())
    func loadSettings(userUuid : String, onSuccess: @escaping (BringUserSettingsData) -> (), onFailure: @escaping ()->()) -> URLRequest?
    func setListSetting(userUuid: String, listUuid: String, key: String, value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->())
}

public class BringUserSettingsServiceNew: NSObject, BringUserSettingsServiceNewProtocol {
    
    public func loadSettings(userUuid : String, onSuccess: @escaping (BringUserSettingsData) -> (), onFailure: @escaping ()->()) -> URLRequest? {
        return BringAlamofireManager.sharedInstance.defaultManager.request(Router.getSettingsForUser(userUuid)).validate()
            .responseObject(queue: nil, keyPath: "", context: nil) { (response: DataResponse<BringUserSettingsResponse>) in
                
                if response.result.isSuccess, let userSettingsReponse = response.result.value {
                    
                    let userSettings = self.map(userSettingsReponse.userSettings)
                    
                    for userListSettingsResponse in userSettingsReponse.userListSettings {
                        userSettings.userListSettings.append(self.map(userListSettingsResponse))
                    }
                    
                    onSuccess(userSettings)
                    
                } else {
                    onFailure()
                }
                
            }.request
    }
    
    public func setSetting(userUuid: String, key: String, value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.postSettingForUser(userUuid, key, value)).validate()
            .response(completionHandler: { (response) in
            if response.error == nil{
                onSuccess()
            }else {
                onFailure()
            }
        })
    }
    
    public func setListSetting(userUuid: String, listUuid: String, key: String, value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.postSettingForUserList(userUuid, listUuid, key, value)).validate().response(completionHandler: { (response) in
            if response.error == nil{
                onSuccess()
            }else {
                onFailure()
            }
        })
    }
    
    func map(_ userSettingsReponseEntries : [BringUserSettingsResponseEntry]) -> BringUserSettingsData {
        let userSettings = BringUserSettingsData()
        for userSettingsResponseEntry in userSettingsReponseEntries {
            if let userSettingsKey = userSettingsResponseEntry.key {
                switch (userSettingsKey) {
                case "autoPush":
                    if let autoPushString = userSettingsResponseEntry.value {
                        if autoPushString == "ON" {
                            userSettings.autoPushEnabled = true
                        } else if autoPushString == "OFF" {
                            userSettings.autoPushEnabled = false
                        }
                    }
                case "pushChannelEducation":
                    if let pushChannelEducationString = userSettingsResponseEntry.value {
                        if pushChannelEducationString == "ON" {
                            userSettings.pushChannelEducationEnabled = true
                        } else if pushChannelEducationString == "OFF" {
                            userSettings.pushChannelEducationEnabled = false
                        }
                    }
                case "pushChannelRecipes":
                    if let pushChannelRecipesString = userSettingsResponseEntry.value {
                        if pushChannelRecipesString == "ON" {
                            userSettings.pushChannelRecipesEnabled = true
                        } else if pushChannelRecipesString == "OFF" {
                            userSettings.pushChannelRecipesEnabled = false
                        }
                    }
                case "pushChannelOffers":
                    if let pushChannelOffersString = userSettingsResponseEntry.value {
                        if pushChannelOffersString == "ON" {
                            userSettings.pushChannelOffersEnabled = true
                        } else if pushChannelOffersString == "OFF" {
                            userSettings.pushChannelOffersEnabled = false
                        }
                    }
                case "pushChannelShopping":
                    if let pushChannelShoppingString = userSettingsResponseEntry.value {
                        if pushChannelShoppingString == "ON" {
                            userSettings.pushChannelShoppingEnabled = true
                        } else if pushChannelShoppingString == "OFF" {
                            userSettings.pushChannelShoppingEnabled = false
                        }
                    }
                case "suggestedSpecifications":
                    if let suggestedSpecifications = userSettingsResponseEntry.value {
                        if suggestedSpecifications == "ON" {
                            userSettings.suggestedSpecifications = true
                        } else if suggestedSpecifications == "OFF" {
                            userSettings.suggestedSpecifications = false
                        }
                    }
                case "badgeMode":
                    if let badgeModeString = userSettingsResponseEntry.value {
                        userSettings.badgeMode = badgeModeString
                    }
                case "defaultListUUID":
                    if let defaultListUUID = userSettingsResponseEntry.value {
                        userSettings.defaultListUUID = defaultListUUID
                    } else {
                        userSettings.defaultListUUID = nil
                    }
                case "defaultZip":
                    if let zip = userSettingsResponseEntry.value {
                        userSettings.defaultZip = zip
                    } else {
                        userSettings.defaultZip = nil
                    }
                case "userPostalCode":
                    if let value = userSettingsResponseEntry.value {
                        userSettings.userPostalCode = value
                    } else {
                        userSettings.userPostalCode = nil
                    }
                case "connectOptionDisabled":
                    if let connectOptionDisabled = userSettingsResponseEntry.value {
                        
                        if connectOptionDisabled == "ON" {
                            userSettings.connectOptionDisabled = true
                        } else {
                            userSettings.connectOptionDisabled = false
                        }
                    } else {
                        userSettings.connectOptionDisabled = false
                    }
                    
                case "connectRecommendationsEnabled":
                    if let connectRecommendationsEnabled = userSettingsResponseEntry.value {
                        
                        if connectRecommendationsEnabled == "ON" {
                            userSettings.connectOptionRecommendations = true
                        } else if connectRecommendationsEnabled == "OFF" {
                            userSettings.connectOptionRecommendations = false
                        } else {
                            userSettings.connectOptionRecommendations = true
                        }
                        
                    } else {
                        userSettings.connectOptionRecommendations = true
                    }
                    
                case "acquisitionChannel":
                    if let acquisitionChannel = userSettingsResponseEntry.value {
                        userSettings.acquisitionChannel = acquisitionChannel
                    } else {
                        userSettings.acquisitionChannel = nil
                    }
                case "experiments":
                    userSettings.experiments = self.map(userSettingsResponseEntry.value)
                case "assistantNewListsAutoSync":
                    if let assistantNewListsAutoSync = userSettingsResponseEntry.value {
                        if assistantNewListsAutoSync == "ON" {
                            userSettings.assistantNewListsAutoSync = true
                        } else if assistantNewListsAutoSync == "OFF" {
                            userSettings.assistantNewListsAutoSync = false
                        }
                    }
                case "listOrder":
                    if let listSectionOrder = userSettingsResponseEntry.value {
                        if let data = listSectionOrder.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                            if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) {
                                if let listOrderArray = jsonObject as? [String] {
                                    userSettings.listOrder = listOrderArray
                                }
                            }
                        }
                    }
                default:
                    break
                }
            }
        }
        return userSettings
    }
    
    func map(_ userListSettingsReponse : BringUserListSettingsResponse) -> BringUserListSettingsData {
        let listSettings = BringUserListSettingsData()
        listSettings.listUuid = userListSettingsReponse.listUuid
        for userSettingsResponseEntry in userListSettingsReponse.userSettings {
            if let userSettingsKey = userSettingsResponseEntry.key {
                switch (userSettingsKey) {
                case "listArticleLanguage":
                    if let listArticleLocale = userSettingsResponseEntry.value {
                        listSettings.listArticleLocale = listArticleLocale
                    }
                    
                case "listSectionOrder":
                    if let listSectionOrder = userSettingsResponseEntry.value {
                        if let data = listSectionOrder.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                            if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) {
                                if let listSectionOrderArray = jsonObject as? [String] {
                                      listSettings.listSectionOrder = listSectionOrderArray
                                }
                            }
                        }
                    }
                    
                case "hiddenSections":
                    if let listSectionOrder = userSettingsResponseEntry.value {
                        if let data = listSectionOrder.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                            if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) {
                                if let listSectionOrderArray = jsonObject as? [String] {
                                    listSettings.hiddenSections = listSectionOrderArray
                                }
                            }
                        }
                    }
                    
                default: break
                    
                }
            }
        }
        return listSettings
    }
    
    
    
    func map(_ experimentsJsonString : String?) -> [ExperimentData] {
        var experiments = [ExperimentData]();
        
        guard let jsonString = experimentsJsonString else {
            return experiments
        }
        
        if let experimentsResponse = Mapper<ExperimentsResponse>().map(JSONString: jsonString) {
            for experimentResponse in experimentsResponse.experiments {
                experiments.append(self.map(experimentResponse))
            }
        }
        
        return experiments
    }
    
    func map(_ response : ExperimentResponse) -> ExperimentData {
        let data = ExperimentData()
        data.name = response.name != nil ? response.name! : ""
        data.selectedAlternative = response.selectedAlternative != nil ? response.selectedAlternative! : ""
        return data
    }
    
    enum Router: URLRequestConvertible {
        
        case getSettingsForUser(String)
        case postSettingForUser(String, String, String)
        case postSettingForUserList(String, String, String, String)
        
        var method: HTTPMethod {
            switch self {
            case .getSettingsForUser:
                return .get
            case .postSettingForUser:
                return .post
            case .postSettingForUserList:
                return .post
            }
        }
        
        var path: String {
            switch self {
            case .getSettingsForUser(let uuid):
                return "/bringusersettings/\(uuid)"
            case .postSettingForUser(let uuid, let key ,_):
                return "/bringusersettings/\(uuid)/\(key)"
            case .postSettingForUserList(let userUuid, let listUuid, let key, _):
                return "/bringusersettings/\(userUuid)/\(listUuid)/\(key)"
            }
        }
        
        // MARK: URLRequestConvertible
        
        func asURLRequest() throws -> URLRequest {
            let URL = Foundation.URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue
            
            switch self {
                case .postSettingForUser( _, _, let value):
                    let encodingUrl = URLEncoding.default
                    try mutableURLRequest = encodingUrl.encode(mutableURLRequest, with: ["value":value])
                    return mutableURLRequest
                case .postSettingForUserList(_, _, _, let value):
                    let encodingUrl = URLEncoding.default
                    try mutableURLRequest = encodingUrl.encode(mutableURLRequest, with: ["value":value])
                    return mutableURLRequest
                default:
                    return mutableURLRequest
            }
        }
    }
}
