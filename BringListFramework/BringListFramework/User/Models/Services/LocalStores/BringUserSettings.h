//
//  BringUserSettings.h
//  Bring
//
//  Created by Sandro Strebel on 16.01.13.
//
//

#import <Foundation/Foundation.h>
@class BringGeoLocation;
@class AppInvitationLinkUuid;

@protocol BringUserSettingsProtocol <NSObject>
- (NSInteger) getRunOfDismissedMessage;
- (NSInteger) getUseCount;
- (void) setCurrentRunOfDismissedMessage;
- (NSString * _Nullable)getBringUserUUID;
- (void) setBringUserUUID:(NSString * _Nonnull) uuid;
- (NSString * _Nullable) getBringListUUID;
- (NSString * _Nullable) getListArticleLanguage:(NSString * _Nonnull) listUuid;
- (NSArray<NSString*>* _Nonnull) getDismissedPostUuids;
- (NSArray<NSString*>* _Nonnull) getTemplateUuidsOfLastStreamVisit;
- (void) setTemplateUuidsOfLastStreamVisit:(NSArray<NSString*>* _Nonnull) templateUuids;
- (void) setDismissedPostUuids:(NSArray<NSString*>* _Nonnull) postUuids;
- (void) setBringListUUID:(NSString * _Nullable) uuid;
-(NSString* _Nullable)getArticleLanguageForCurrentUserList;
- (NSString * _Nullable) apnsToken;
-(NSString * _Nullable)getBringUserPublicUUID;
- (void)setYearInReview:(NSString * _Nullable)yearInReview;
- (NSString * _Nullable)getYearInReview;
- (BOOL)is18OrOlder;
- (void)setIs18OrOlder:(BOOL)isOlder;
- (void)setLinkedPartners:(NSArray<NSString*>* _Nonnull)linkedPartners;
- (NSArray<NSString*>* _Nullable)getLinkedPartners;
- (void)setLastSync:(NSDate * _Nonnull) lastSync;
- (NSDate * _Nullable) getLastSync;
- (NSArray<NSString*>* _Nonnull)getListOrder;
- (void)setListOrder:(NSArray<NSString*>* _Nonnull)listOrder;
- (id _Nullable)objectForKey:(NSString * _Nonnull)defaultName;
- (void)showedForcedEngagementAction:(NSString* _Nonnull)itemKey;
- (NSMutableArray* _Nonnull) shownForcedEngagementActions;
- (void)setLastSpecificationSyncDate:(NSDate * _Nonnull) lastSync ForArticleLanguage: (NSString* _Nonnull) articleLanguage;
- (NSDate * _Nullable) getLastSpecificationSyncForArticleLanguage:(NSString* _Nonnull) articleLanguage;
-(void)resetForcedEngagementForItems:(NSArray<NSString*>* _Nonnull)items;
-(void)setActivationLevel:(NSInteger)activationLevel;
-(NSInteger)getActivationLevel;
- (void)setDefaultsForKey:(NSString * _Nonnull)key dictionary:(NSDictionary* _Nullable)dictionary;
- (NSDictionary* _Nullable)getDefaultsForKey:(NSString * _Nonnull)key;
- (void)clearDefaultsForKey:(NSString * _Nonnull)key;
- (BOOL)hasSuggestedSpecificationsEnabled;
- (void)setSuggestedSpecifications:(BOOL)enabled;
- (NSArray<NSString*>* _Nonnull)getMessagesSortOrder;
- (void)setMessagesSortOrder:(NSArray<NSString*>* _Nonnull)messagesSortOrder;
- (NSSet<NSString*>* _Nonnull)getAllArticleLanguages;
@end

NS_ASSUME_NONNULL_BEGIN
static NSString *BADGE_MODE = @"badgeMode";
static NSString *EXPERIMENTS = @"experiments";
static NSString *AUTO_PUSH = @"autoPush";
static NSString *USE_COUNT = @"kBringCoachUseCount";
NS_ASSUME_NONNULL_END

extern NSString * _Nonnull const WIDGET_MODEL_CHANGED_KEY;

@interface BringUserSettings : NSObject <BringUserSettingsProtocol>

+ (BringUserSettings * _Nonnull) standardUserSettings;

- (id _Nonnull )initWithSuiteName:(NSString* _Nonnull)suitename;

- (BOOL) migrateToAppGroup;

- (void) synchronize;
- (void)setObject:(id _Nonnull)value forKey:(NSString * _Nonnull)defaultName;
- (id _Nullable)objectForKey:(NSString * _Nonnull)defaultName;
- (NSInteger)integerForKey:(NSString * _Nonnull)defaultName;
- (void)setInteger:(NSInteger)value forKey:(NSString * _Nonnull)defaultName;
- (void)removeObjectForKey:(NSString * _Nonnull)defaultName;
- (BOOL)boolForKey:(NSString * _Nonnull)defaultName;
- (void)setBool:(BOOL)value forKey:(NSString * _Nonnull)defaultName;

- (void) setShownReleaseNotesVersion:(NSString * _Nonnull) version;
- (NSString * _Nullable) getShownReleaseNotesVersion;

- (void) setEmail:(NSString * _Nonnull) email;
- (NSString * _Nullable) getEmail;

- (void) setBringListUUID:(NSString * _Nullable) uuid;
- (NSString * _Nullable) getBringListUUID;
- (BOOL)hasBringListUUID;

- (void) setDefaultListUUID:(NSString * _Nullable) uuid;
- (NSString * _Nullable) getDefaultListUUID;

- (void) setLastSync:(NSDate * _Nonnull) lastSync;
- (NSDate * _Nullable) getLastSync;

- (void) setListSectionOrder:(NSString * _Nonnull) listUuid order:(NSArray * _Nonnull) sectionOrder;
- (NSArray<NSString*> * _Nullable) getListSectionOrder:(NSString * _Nonnull) listUuid;

- (void) setHiddenSectionsForList:(NSString * _Nonnull)listUuid sections:(NSArray * _Nonnull) sections;
- (NSArray<NSString*> * _Nullable) getHiddenSections:(NSString * _Nonnull) listUuid;


- (void) setListArticleLanguage:(NSString * _Nonnull) listUuid language:(NSString * _Nonnull) language;
- (NSString * _Nullable) getListArticleLanguage:(NSString * _Nonnull) listUuid;
- (NSString * _Nullable) currentArticleLanguage;

- (void) setDefaultArticleLanguage:(NSString * _Nonnull) language;
- (NSString * _Nullable) getDefaultArticleLanguage;

- (void)setMigratedLanguages:(NSArray<NSString*>* _Nonnull)languages;
- (NSArray<NSString*>* _Nullable)getMigratedLanguages;

- (void) setBadgeMode:(NSString * _Nullable) mode;
- (NSString * _Nullable) badgeMode;

- (void) setAutoPushEnabled:(BOOL) enabled;
- (BOOL) autoPushEnabled;

- (void) setDefaultZip:(NSString * _Nullable) zip;
- (NSString * _Nullable) getDefaultZip;

- (void) setUserPostalCode:(NSString * _Nullable) value;
- (NSString * _Nullable) getUserPostalCode;

- (void) setUserLocationZip:(NSString * _Nullable) zip;
- (NSString * _Nullable) getUserLocationZip;

- (void)setConnectDisabled:(BOOL)disabled;
- (BOOL)isConnectDisabled;
    
- (void)setConnectRecommendations:(BOOL)enabled;
- (BOOL)isConnectRecommendationsEnabled;

- (void) setAcquisitionChannel:(NSString * _Nullable) channel;
- (NSString * _Nullable) getAcquisitionChannel;

- (void) setPocketLockEnabled:(BOOL) enabled;
- (BOOL) pocketLockEnabled;

- (void) setUserLikesBring:(BOOL)answer;
- (BOOL) userLikesBring;

- (void) setRatingActivatorAction:(NSString* _Nonnull) action;

- (void)setRatingProperties:(NSDictionary * _Nonnull)dictionary;
- (NSDictionary* _Nullable)getRatingProperties;
- (void)clearRatingProperties;

- (void)setRecommendAFriendProperties:(NSDictionary * _Nonnull)properties;
- (NSDictionary* _Nullable)getRecommendAFriendProperties;
- (void)clearRecommendAFriendProperties;


- (void) setApnsToken:(NSString * _Nonnull) token;
- (NSString * _Nullable) apnsToken;

- (void) setBringUserUUID:(NSString * _Nullable) uuid;
- (NSString * _Nullable) getBringUserUUID;
- (BOOL) hasBringUserUUID;

- (void) setBringUserPublicUUID:(NSString * _Nonnull) publicUuid;
- (NSString * _Nullable) getBringUserPublicUUID;

- (void) addShownCoachMark:(NSString * _Nonnull) coachMarkIdentifier;
- (NSArray * _Nullable) getShownCoachMarks;

- (void) setCurrentWatchApplicationContext:(NSDictionary * _Nonnull) applicationContext;
- (NSDictionary * _Nullable) currentWatchApplicationContext;

- (void) setCurrentWatchComplicationContext:(NSDictionary * _Nonnull) complicationContext;
- (NSDictionary * _Nullable) currentWatchComplicationContext;

- (void) resetUserSettings;
- (void) defaultInit;

- (BOOL) isFirstRun;
- (void) setFirstRunCompleted;

- (NSInteger) getUseCount;
- (void) incrementUseCount;

- (NSInteger) getRunOfDismissedMessage;
- (void) setCurrentRunOfDismissedMessage;

- (BOOL) isUserLoggedIn;

- (void) setIntroAdPresented:(NSString* _Nonnull) itemKey;
- (BOOL) isIntroAdAlreadyPresented:(NSString* _Nonnull) itemKey;

- (void) setPantryIntroAlreadySeen;
- (BOOL) getPantryIntroAlreadySeen;

- (void) setOffersLocationActivatorLargeDismissed;
- (BOOL) getOffersLocationActivatorLargeDismissed;

- (NSArray<NSString*>* _Nonnull) getTemplateUuidsOfLastStreamVisit;
- (void) setTemplateUuidsOfLastStreamVisit:(NSArray<NSString*>* _Nonnull) templateUuids;

- (NSArray<NSNumber*>* _Nonnull) getBrochureIdsOfLastVisit;
- (void) setBrochureIdsOfLastVisit:(NSArray<NSNumber*>* _Nonnull) brochureIds;

- (NSArray<NSNumber*>* _Nonnull) getCurrentBrochureIds;
- (void) setCurrentBrochureIds:(NSArray<NSNumber*>* _Nonnull) brochureIds;

- (NSArray<NSString*>* _Nonnull) getDismissedPostUuids;
- (void) setDismissedPostUuids:(NSArray<NSString*>* _Nonnull) postUuids;

- (void) setPushChannelEducationEnabled:(BOOL) enabled;
- (BOOL) pushChannelEducationEnabled;

- (void) setPushChannelOffersEnabled:(BOOL) enabled;
- (BOOL) pushChannelOffersEnabled;

- (void) setPushChannelRecipesEnabled:(BOOL) enabled;
- (BOOL) pushChannelRecipesEnabled;

- (void) setPushChannelShoppingEnabled:(BOOL) enabled;
- (BOOL) pushChannelShoppingEnabled;

- (void) setPremiumOption:(NSString* _Nonnull)key disabled:(BOOL)disabled;
- (BOOL) getPremiumOption:(NSString* _Nonnull)key;

- (void) setPremiumDebug:(BOOL)enabled;
- (BOOL) getPremiumDebug;

-(void)setLastOffersSync:(NSDate * _Nonnull)lastSync;
-(NSDate * _Nullable)getLastOffersSync;

- (void) setOffersIntroAlreadySeen;
- (BOOL) getOffersIntroAlreadySeen;

- (void) setConnectIntroAlreadySeen;
- (BOOL) getConnectIntroAlreadySeen;

- (void) setWalletIntroAlreadySeen;
- (BOOL) getWalletIntroAlreadySeen;

- (void)setConnectBrackIntroAlreadySeen;
- (BOOL)getConnectBrackIntroAlreadySeen;
    
- (void)setConnectBringmeisterIntroAlreadySeen;
- (BOOL)getConnectBringmeisterIntroAlreadySeen;

- (void)setConnectSupermarketIntroAlreadySeen;
- (BOOL)getConnectSupermarketIntroAlreadySeen;

- (void)setLastGeoLookupLocation:(BringGeoLocation* _Nonnull)location timestamp:(NSDate* _Nonnull)timestamp;
- (BringGeoLocation* _Nullable)getLastGeoLookupLocation;
- (NSDate* _Nullable)getLastGeoLookupTimestamp;

- (void)setConnectDefaults:(NSString * _Nonnull)partner dictionary:(NSDictionary * _Nonnull)dictionary;
- (NSDictionary* _Nullable)getConnectDefaults:(NSString * _Nonnull)partner;
- (void)clearConnectDefaults:(NSString * _Nonnull)partner;
    
- (void)setConnectBadges:(NSString * _Nonnull)partner dictionary:(NSDictionary * _Nonnull)dictionary;
- (NSDictionary* _Nullable)getConnectBadges:(NSString * _Nonnull)partner;
- (void)clearConnectBadges:(NSString * _Nonnull)partner;

- (void)setLifecycleDefaults:(NSDictionary * _Nonnull)dictionary;
- (NSDictionary* _Nullable)getLifecycleDefaults;
- (void)clearLifecycleDefaults;

- (void)setMockCountry:(NSString * _Nullable)mockCountry;
- (NSString * _Nullable)getMockCountry;

- (void)setMockLocation:(NSString * _Nullable)mockLocation;
- (NSString * _Nullable)getMockLocation;

- (void)clear:(NSString* _Nonnull)key;

-(BOOL)userMarkedForRecommendAFriend;

-(void)setUserMarkedForRecommendAFriend:(BOOL)enabled;

- (void)setYearInReview:(NSString * _Nullable)yearInReview;
- (NSString * _Nullable)getYearInReview;
    
- (BOOL)is18OrOlder;
- (void)setIs18OrOlder:(BOOL)isOlder;

-(NSString* _Nullable)getArticleLanguageForCurrentUserList;
- (NSArray<NSString*>* _Nonnull)getListOrder;
- (void)setListOrder:(NSArray<NSString*>* _Nonnull)listOrder;

- (BOOL)isAutoSyncDisabledForAssistant;
- (void)setIsAutoSyncDisabledForAssistant:(BOOL)isDisabled;

- (void)setLinkedPartners:(NSArray<NSString*>* _Nonnull)linkedPartners;
- (NSArray<NSString*>* _Nullable)getLinkedPartners;

-(void) resetUseCount;
-(void) resetRunOfLastDismissedMessage;

-(void)resetForcedEngagementForItems:(NSArray<NSString*>* _Nonnull)items;

-(void)setActivationLevel:(NSInteger)activationLevel;
-(NSInteger)getActivationLevel;

- (void)setDefaultsForKey:(NSString* _Nonnull)key dictionary:(NSDictionary * _Nullable)dictionary;
- (NSDictionary* _Nullable)getDefaultsForKey:(NSString* _Nonnull)key;
- (void)clearDefaultsForKey:(NSString* _Nonnull)key;

- (void)setLastSpecificationSyncDate:(NSDate * _Nonnull) lastSync ForArticleLanguage: (NSString* _Nonnull) articleLanguage;
- (NSDate * _Nullable) getLastSpecificationSyncForArticleLanguage:(NSString* _Nonnull) articleLanguage;

- (void)setCatalogExtensionSyncDate:(NSDate * _Nullable) lastSync ForArticleLanguage: (NSString* _Nonnull) articleLanguage;
- (NSDate * _Nullable) getLastCatalogExtensionSyncDate:(NSString* _Nonnull) articleLanguage;

- (BOOL)hasSuggestedSpecificationsEnabled;
- (void)setSuggestedSpecifications:(BOOL)enabled;

-(BOOL)didMigrateToCoreDataUserItemStore;
- (void)setDidMigrateToCoreDataUserItemStore:(BOOL)migrated;

- (NSArray<NSString*>* _Nonnull)getMessagesSortOrder;
- (void)setMessagesSortOrder:(NSArray<NSString*>* _Nonnull)messagesSortOrder;

- (NSDate* _Nullable)getLastTrackingDate;
- (void)setLastTrackingDate:(NSDate * _Nullable)trackingDate;

- (NSSet<NSString*>* _Nonnull)getClosedSections;
- (void)setClosedSections:(NSSet<NSString*>* _Nonnull)closedSections;

- (NSSet<NSString*>* _Nullable)getOpenSections;
- (void)setOpenSections:(NSSet<NSString*>* _Nonnull)openSections;

- (NSSet<AppInvitationLinkUuid*>* _Nonnull)getRewardsToCollect;
- (void)setRewardToCollect:(AppInvitationLinkUuid* _Nonnull)rewardToCollect;
- (void)removeRewardToCollect:(AppInvitationLinkUuid* _Nonnull)rewardToRemove;

- (NSSet<NSString*>* _Nonnull)getAllArticleLanguages;

- (BOOL)didWriteAppleWatchUserSettingToServer;
- (void)setDidWriteAppleWatchUserSettingToServer:(BOOL)didWriteToServer;

-(void)setDeeplink:(NSString* _Nullable)deeplink;
-(NSString* _Nullable)getDeeplink;

-(void)setMockAcceptLanaguage:(NSString* _Nullable)acceptLanguage;
-(NSString* _Nullable)getMockAcceptLanguage;

-(BOOL)didDismissupdateMessage;
- (void)setDidDismissupdateMessage:(BOOL)didDismiss;

- (NSDate* _Nullable)getLastUpdateUserLocationDate;
- (void)setLastUpdateUserLocationDate:(NSDate * _Nullable)date;
@end
