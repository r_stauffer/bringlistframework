//
//  BringUserSettings.m
//  Bring
//
//  Created by Sandro Strebel on 16.01.13.
//
//

#import "BringUserSettings.h"
#import <Foundation/Foundation.h>

#if TARGET_OS_IOS
    #import "BringListFrameworkiOS/BringListFrameworkiOS-Swift.h"
    #import "BringListFrameworkiOS/NSUserDefaults+SetAdditions.h"
#elif TARGET_OS_WATCH
    #import "BringListFrameworkwatchOS/BringListFrameworkwatchOS-Swift.h"
    #import "BringListFrameworkwatchOS/NSUserDefaults+SetAdditions.h"
#endif



static NSString *SHOWN_RELEASE_NOTES_VERSION_KEY = @"ShownReleaseNotesVersion";
static NSString *EMAIL_KEY = @"email";
static NSString *BRING_LIST_UUID_KEY = @"bringListUUID";
static NSString *DEFAULT_LIST_UUID_KEY = @"defaultListUUID";
static NSString *LAST_SYNC_KEY = @"lastSync";
static NSString *SECTION_ORDER_KEY = @"sectionOrder";
static NSString *DEFAULT_SECTION_ORDER_KEY = @"defaultSectionOrder";
static NSString *ARTICLE_LANGUAGE_KEY = @"ArticleLanguage";
static NSString *MIGRATED_LANGUAGES_KEY = @"MigratedLanguages";
static NSString *APNS_TOKEN = @"ApnsToken";
static NSString *BRING_USER_UUID_KEY = @"bringUserUUID";
static NSString *BRING_USER_PUBLIC_UUID_KEY = @"bringUserPublicUUID";
static NSString *POCKET_LOCK_ENABLED = @"pocketLockEnabled";
static NSString *AUTO_PUSH_ENABLED = @"autoPushEnabled";
static NSString *DEFAULT_ZIP = @"defaultZip";
static NSString *USER_POSTAL_CODE = @"userPostalCode";
static NSString *USER_LOCATION_ZIP = @"userLocationZip";
static NSString *CONNECT_OPTION_DISABLED = @"connectOptionDisabled";
static NSString *CONNECT_OPTION_RECOMMENDATIONS = @"connectOptionRecommendations";
static NSString *ACQUISITION_CHANNEL = @"acquisitionChannel";
static NSString *SHOWN_COACH_MARKS_KEY = @"shownCoachMarks";
static NSString *EXPIRY_REMINDER_1_SHOWN = @"expiryReminder1Shown";
static NSString *EXPIRY_REMINDER_2_SHOWN = @"expiryReminder2Shown";
static NSString *LIST_SECTION_ORDER_KEY = @"listSectionOrder";
static NSString *LIST_HIDDEN_SECTION_KEY = @"hidenSectionsKey";
static NSString *LIST_ARTICLE_LANGUAGE_KEY = @"ListArticleLanguage";
static NSString *CURRENT_WATCH_APPLICATION_CONTEXT_KEY = @"CurrentWatchApplicationContext";
static NSString *CURRENT_WATCH_COMPLICATION_CONTEXT_KEY = @"CurrentWatchComplicationContext";
static NSString *const didMigratedToAppGroupsKey = @"migratedToAppGroups";
static NSString *FIRST_RUN = @"firstRun";
static NSString *INTRO_AD_ALREADY_SEEN_KEY = @"introAdAlreadySeen";
static NSString *PANTRY_INTRO_ALREADY_SEEN_KEY = @"pantryIntroAlreadySeen";
static NSString *RATING_ACTIVATOR_USER_LIKES_BRING = @"ratingActivatorUserLikesBring";
static NSString *RATING_ACTIVATOR_USER_ACTION = @"ratingActivatorUserAction";
static NSString *TEMPLATE_UUIDS_OF_LAST_STREAM_VISIT_KEY = @"templateUuidsOfLastStreamVisit";
static NSString *BROCHURE_IDS_OF_LAST_VISIT_KEY = @"brochureIdsOfLastVisit";
static NSString *CURRENT_BROCHURE_IDS_KEY = @"currentBrochureIds";
static NSString *OFFERS_LOCATION_ACTIVATOR_LARGE_DISMISSED = @"offersLocationActivatorLargeDismissed";
static NSString *RUN_OF_DISMISSED_MESSAGE = @"runOfDismissedMessage";
static NSString *DISMISSED_POST_UUIDS_KEY = @"dismissedPostUuids";
static NSString *PUSH_CHANNEL_EDUCATION_ENABLED_KEY = @"pushChannelEducationEnabled";
static NSString *PUSH_CHANNEL_OFFERS_ENABLED_KEY = @"pushChannelOffersEnabled";
static NSString *PUSH_CHANNEL_RECIPES_ENABLED_KEY = @"pushChannelRecipesEnabled";
static NSString *PUSH_CHANNEL_SHOPPING_ENABLED_KEY = @"pushChannelShoppingEnabled";
static NSString *LAST_OFFERS_SYNC_KEY = @"lastOffersSync";
static NSString *OFFERS_INTRO_ALREADY_SEEN_KEY = @"offersIntroAlreadySeen";
static NSString *CONNECT_INTRO_ALREADY_SEEN_KEY = @"connectIntroAlreadySeen";
static NSString *WALLET_INTRO_ALREADY_SEEN_KEY = @"walletIntroAlreadySeen";
static NSString *CONNECT_BRACK_INTRO_ALREADY_SEEN_KEY = @"connectBrackIntroAlreadySeen";
static NSString *CONNECT_BRINGMEISTER_INTRO_ALREADY_SEEN_KEY = @"connectBringmeisterIntroAlreadySeen";
static NSString *CONNECT_SUPERMARKET_INTRO_ALREADY_SEEN_KEY = @"connectSupermarketIntroAlreadySeen";
static NSString *LAST_GEO_LOOKUP_LOCATION_KEY = @"lastGeoLookupLocation";
static NSString *LAST_GEO_LOOKUP_TIMESTAMP_KEY = @"lastGeoLookupTimestamp";
static NSString *CONNECT_DEFAULTS_KEY = @"connectDefaults";
static NSString *CONNECT_BADGES_KEY = @"connectBadges";
static NSString *LIFECYCLE_DEFAULTS_KEY = @"lifecycleDefaults";
static NSString *RATING_PROPERTIES_DEFAULTS_KEY = @"ratingPropertiesDefaults";
static NSString *RECOMMEND_A_FRIEND_PROPERTIES_DEFAULTS_KEY = @"recommendAFriendPropertiesDefaults";
static NSString *RECOMMEND_A_FRIEND_MARK_USER_KEY = @"recommendAFriendMarkUser";
static NSString *YEAR_IN_REVIEW_KEY = @"yearInReviewKey";
static NSString *USER_IS_18_KEY = @"userIs18Key";
static NSString *IS_ASSISTANT_AUTOSYNC_DISABLED_KEY = @"isAutoSyncDisabledForAssistant";
static NSString *MOCK_COUNTRY_KEY = @"mockCountry";
static NSString *MOCK_LOCATION_KEY = @"mockLocation";
static NSString *LINKED_PARTNERS_KEY = @"linkedPartnersKey";
static NSString *LIST_ORDER_KEY = @"listOrderKey";
static NSString *FORCED_ENGAGEBENT_ACTIONS_KEY = @"kBringForcedEngagementActions";
static NSString *LAST_SPECIFICATION_SYNC_KEY = @"lastSpecificationSyncKey";
static NSString *LAST_CATALOG_EXTENSION_SYNC_KEY = @"lastCatalogExtensionSyncKey";
static NSString *CONNECT_ACTIVATION_LEVEL = @"connectActivationLevel";
static NSString *DICTIONARY_SETTINGS_KEY_PRFIX = @"dictionary_settings_key_";
static NSString *SUGGESTED_SPECIFICATIONS_KEY = @"suggestedSpecificationsKey";
static NSString *DID_MIGRATE_TO_CORE_DATA_USER_ITEM_STORE_KEY = @"didMigrateToCoreDataUserItemStoreKey";
static NSString *MESSAGES_SORT_ORDER_KEY = @"messsagesSortOrder";
static NSString *LAST_STARTUP_TRACKING_DATE = @"lastStartupTrackingDateKey";
static NSString *CLOSED_SECTIONS_BY_USER_KEY = @"initiallyOpenSectionsKey";
static NSString *OPEN_SECTIONS = @"initiallyOpenNonExtensionSectionsKey";
static NSString *REWARD_COLLECT_KEY = @"rewardCollectKey";
static NSString *DID_WRITE_HAS_BRING_WATCH_INSTALLED = @"didWriteHasBringWatchInstalledKey";
static NSString *DEEPLINK_AFTER_REGISTRATION_KEY = @"deeplinkAfterRegistrationKey";

NSString * const WIDGET_MODEL_CHANGED_KEY = @"widgetModelChangedKey";

@interface BringUserSettings ()
@property (strong, nonatomic) NSUserDefaults* sharedUserDefaults;
@end

@implementation BringUserSettings

static BringUserSettings *standardUserSettings;

+ (BringUserSettings*)standardUserSettings {
    
    static BringUserSettings *standardUserSettings = nil;
    if (standardUserSettings == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
             standardUserSettings = [[BringUserSettings alloc] init];
        });
    }
    
    return standardUserSettings;
}

- (id)init {
    if (self = [super init]) {
        
        NSUserDefaults *appGroupUserDefaults = [[NSUserDefaults alloc] initWithSuiteName: BringConstants.APP_GROUP_IDENTIFIER];
        if (![appGroupUserDefaults boolForKey:didMigratedToAppGroupsKey]){
            self.sharedUserDefaults = [NSUserDefaults standardUserDefaults];
        }else {
            self.sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName: BringConstants.APP_GROUP_IDENTIFIER];
        }
        
        [self defaultInit];
        
    }
    return self;
}

- (id)initWithSuiteName:(NSString*)suitename {
    
    if (self = [super init]) {
        self.sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName: suitename];
    }
    return self;
}

- (BOOL) migrateToAppGroup {
    @try {
        
        [self migrateToAppGroup:[[NSUserDefaults alloc] initWithSuiteName: BringConstants.APP_GROUP_IDENTIFIER]];
        return YES;
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
        return NO;
    }
}

- (void) migrateToAppGroup:(NSUserDefaults*)appGroupUserDefaults {
    
    if (![appGroupUserDefaults boolForKey:didMigratedToAppGroupsKey]){
        
        NSDictionary *oldDefaults = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
        for (NSString * key in oldDefaults.allKeys) {
            id value = [[NSUserDefaults standardUserDefaults] valueForKey:key];
            [appGroupUserDefaults setValue:value forKey:key];
        }
        
        [appGroupUserDefaults setBool:YES forKey:didMigratedToAppGroupsKey];
        [appGroupUserDefaults synchronize];
        
        //migration completed, use new app group user defaults
        self.sharedUserDefaults = appGroupUserDefaults;
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:BRING_USER_UUID_KEY] != nil) {
        // remove old userdefaults
        NSString *domainName = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:domainName];
    }
}

- (NSInteger)integerForKey:(NSString *)defaultName {
    return [self.sharedUserDefaults integerForKey:defaultName];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName {
    [self.sharedUserDefaults setInteger:value forKey:defaultName];
}

- (void)removeObjectForKey:(NSString *)defaultName {
    [self.sharedUserDefaults removeObjectForKey:defaultName];
}

- (BOOL)boolForKey:(NSString *)defaultName {
    return [self.sharedUserDefaults boolForKey:defaultName];
}

- (void)setBool:(BOOL)value forKey:(NSString *)defaultName {
    [self.sharedUserDefaults setBool:value forKey:defaultName];
}

- (void) synchronize {
    [self.sharedUserDefaults synchronize];
}
    
- (void)setObject:(id)value forKey:(NSString *)defaultName {
    [self.sharedUserDefaults setObject:value forKey:defaultName];
}

- (id)objectForKey:(NSString *)defaultName {
    return [self.sharedUserDefaults objectForKey:defaultName];
}

-(void) setShownReleaseNotesVersion:(NSString *)version {
    [self.sharedUserDefaults setObject:version forKey:SHOWN_RELEASE_NOTES_VERSION_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSString *)getShownReleaseNotesVersion {
    return [self.sharedUserDefaults objectForKey:SHOWN_RELEASE_NOTES_VERSION_KEY];
}

- (void)setEmail:(NSString *)email {
    [self.sharedUserDefaults setObject:email forKey:EMAIL_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSString *)getEmail {
    return [self.sharedUserDefaults objectForKey:EMAIL_KEY];
}

- (void)setBringListUUID:(NSString *)uuid {
    
    if ([self isValidUUID:uuid]) {
        [self.sharedUserDefaults setObject:uuid forKey:BRING_LIST_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:BRING_LIST_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getBringListUUID {
    
    NSObject *obj = [self.sharedUserDefaults objectForKey:BRING_LIST_UUID_KEY];
    if (obj != nil && [obj isKindOfClass:[NSString class]]) {
        
        NSString *uuid = (NSString*) obj;
        
        if ([self isValidUUID:uuid]) {
            return uuid;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (BOOL)hasBringListUUID {
    return [self getBringListUUID] != nil;
}

- (void)setDefaultListUUID:(NSString *)uuid {
    
    if ([self isValidUUID:uuid]) {
        [self.sharedUserDefaults setObject:uuid forKey:DEFAULT_LIST_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:DEFAULT_LIST_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getDefaultListUUID {
    
    NSObject *obj = [self.sharedUserDefaults objectForKey:DEFAULT_LIST_UUID_KEY];
    if (obj != nil && [obj isKindOfClass:[NSString class]]) {
        
        NSString *uuid = (NSString*) obj;
        
        if ([self isValidUUID:uuid]) {
            return uuid;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (BOOL)isValidUUID:(NSString*)uuid {
    
    if (uuid != nil) {
        return [uuid stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0;
    } else {
        return NO;
    }
}

-(void)setLastSync:(NSDate *)lastSync {
    [self.sharedUserDefaults setObject:lastSync forKey:LAST_SYNC_KEY];
    [self.sharedUserDefaults synchronize];
}

-(NSDate *)getLastSync {
    return [self.sharedUserDefaults objectForKey:LAST_SYNC_KEY];
}

- (void)setListSectionOrder:(NSString *)listUuid order:(NSArray *)sectionOrder {
    
    NSDictionary *listSectionOrders = [self getListSectionOrderDictionary];
    NSMutableDictionary *mutableListSectionOrders = [listSectionOrders mutableCopy];
    [mutableListSectionOrders setObject:sectionOrder forKey:listUuid];
    
    [self.sharedUserDefaults setObject:mutableListSectionOrders forKey:LIST_SECTION_ORDER_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSArray<NSString*> *)getListSectionOrder:(NSString *)listUuid {
    
    NSDictionary *listSectionOrders = [self getListSectionOrderDictionary];
    
    return [listSectionOrders objectForKey:listUuid];
}

- (NSDictionary*)getListSectionOrderDictionary {
    
    NSDictionary *object = [self.sharedUserDefaults objectForKey:LIST_SECTION_ORDER_KEY];
    
    if (object != nil && [object isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary*) object;
    } else {
        return [NSDictionary new];
    }
}

- (void) setHiddenSectionsForList:(NSString * _Nonnull)listUuid sections:(NSArray * _Nonnull) sections {
    NSDictionary *listSectionOrders = [self.sharedUserDefaults objectForKey:LIST_HIDDEN_SECTION_KEY];
    if(!listSectionOrders){
        listSectionOrders = [NSDictionary new];
    }
    
    NSMutableDictionary *mutableListSectionOrders = [listSectionOrders mutableCopy];
    [mutableListSectionOrders setObject:sections forKey:listUuid];
    
    [self.sharedUserDefaults setObject:mutableListSectionOrders forKey:LIST_HIDDEN_SECTION_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSArray<NSString*> * _Nullable) getHiddenSections:(NSString * _Nonnull) listUuid {
    NSDictionary *listSectionOrders = [self.sharedUserDefaults objectForKey:LIST_HIDDEN_SECTION_KEY];
    if(!listSectionOrders){
        listSectionOrders = [NSDictionary new];
    }
    
    return [listSectionOrders objectForKey:listUuid];
}

- (void)setListArticleLanguage:(NSString *)listUuid language:(NSString *)language {
    
    NSDictionary *listArticleLanguages = [self.sharedUserDefaults objectForKey:LIST_ARTICLE_LANGUAGE_KEY];
    if(!listArticleLanguages){
        listArticleLanguages = [NSDictionary new];
    }else if([[listArticleLanguages objectForKey:listUuid] isEqualToString:language]){
        //nothing changed
        return;
    }
    
    if ([[self getBringListUUID] isEqualToString:listUuid]) {
         [self willChangeValueForKey:@"currentArticleLanguage"];
    }
    
    NSMutableDictionary *mutableArticleLanguages = [listArticleLanguages mutableCopy];
    [mutableArticleLanguages setObject:language forKey:listUuid];
    
    [self.sharedUserDefaults setObject:mutableArticleLanguages forKey:LIST_ARTICLE_LANGUAGE_KEY];
    [self.sharedUserDefaults synchronize];
    
    NSLog(@"currentArticleLanguage changed ------------");
    
    if ([[self getBringListUUID] isEqualToString:listUuid]) {
        [self didChangeValueForKey:@"currentArticleLanguage"];
    }
}

- (NSString *)getListArticleLanguage:(NSString *)listUuid {
    
    NSDictionary *listArticleLanguages = [self.sharedUserDefaults objectForKey:LIST_ARTICLE_LANGUAGE_KEY];
    if(!listArticleLanguages){
        listArticleLanguages = [NSDictionary new];
    }
    
    NSString *articleLanguage = [listArticleLanguages objectForKey:listUuid];
    if(!articleLanguage){
        articleLanguage = [self getDefaultArticleLanguage]; // return default article language
    }
    return articleLanguage;
    
}

- (NSSet<NSString*> *)getAllArticleLanguages {
    NSDictionary *listArticleLanguages = [self.sharedUserDefaults objectForKey:LIST_ARTICLE_LANGUAGE_KEY];
    NSArray *allValues = (NSArray<NSString*>*) [listArticleLanguages allValues];
    
    if(!allValues) {
        return [NSSet<NSString*> new];
    }
    return [[NSSet alloc] initWithArray:allValues];
}

- (NSString *)currentArticleLanguage{
    return [self getListArticleLanguage:[self getBringListUUID]];
}

- (void)setDefaultArticleLanguage:(NSString *)language {
    [self.sharedUserDefaults setObject:language forKey:ARTICLE_LANGUAGE_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSString *)getDefaultArticleLanguage {
    return [self.sharedUserDefaults objectForKey:ARTICLE_LANGUAGE_KEY];
}

- (void)setMigratedLanguages:(NSArray<NSString*>*)languages {
    [self.sharedUserDefaults setObject:languages forKey:MIGRATED_LANGUAGES_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSArray<NSString*>*)getMigratedLanguages {
    
    NSObject *languages = [self.sharedUserDefaults objectForKey:MIGRATED_LANGUAGES_KEY];
    if (languages != nil && [languages isKindOfClass:[NSArray<NSString*> class]]) {
        return (NSArray<NSString*>*) languages;
    } else {
        return nil;
    }
}

- (void)setApnsToken:(NSString *)token {
    [self.sharedUserDefaults setObject:token forKey:APNS_TOKEN];
}

- (NSString *)apnsToken {
    return [self.sharedUserDefaults objectForKey:APNS_TOKEN];
}

- (void)setBringUserUUID:(NSString *)uuid {
    
    if ([self isValidUUID:uuid]) {
        [self.sharedUserDefaults setObject:uuid forKey:BRING_USER_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:BRING_USER_UUID_KEY];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString * _Nullable)getBringUserUUID {
    
    NSObject *obj = [self.sharedUserDefaults objectForKey:BRING_USER_UUID_KEY];
    if (obj != nil && [obj isKindOfClass:[NSString class]]) {
        
        NSString *uuid = (NSString*) obj;
        
        if ([self isValidUUID:uuid]) {
            return uuid;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (BOOL)hasBringUserUUID {
    return [self getBringUserUUID] != nil;
}

- (void)setBringUserPublicUUID:(NSString *)publicUuid {
    [self.sharedUserDefaults setObject:publicUuid forKey:BRING_USER_PUBLIC_UUID_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSString *)getBringUserPublicUUID {
    return [self.sharedUserDefaults objectForKey:BRING_USER_PUBLIC_UUID_KEY];
}

- (void)setBadgeMode:(NSString *)mode {
    
    if (mode != nil) {
        [self.sharedUserDefaults setObject:mode forKey:BADGE_MODE];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:BADGE_MODE];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)badgeMode {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:BADGE_MODE];
    if (object != nil && [object isKindOfClass:[NSString class]]) {
        return (NSString*) object;
    } else {
        return nil;
    }
}

-(BOOL)autoPushEnabled{
    return [self.sharedUserDefaults boolForKey:AUTO_PUSH_ENABLED];
}

-(void)setAutoPushEnabled:(BOOL)enabled{
    [self.sharedUserDefaults setBool:enabled forKey:AUTO_PUSH_ENABLED];
    [self.sharedUserDefaults synchronize];
}

- (void)setDefaultZip:(NSString *)zip {
    
    if (zip != nil) {
        [self.sharedUserDefaults setObject:zip forKey:DEFAULT_ZIP];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:DEFAULT_ZIP];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getDefaultZip {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:DEFAULT_ZIP];
    if (object != nil && [object isKindOfClass:[NSString class]]) {
        return (NSString*) object;
    } else {
        return nil;
    }
}

- (void)setUserPostalCode:(NSString *)value {
    
    if (value != nil) {
        [self.sharedUserDefaults setObject:value forKey:USER_POSTAL_CODE];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:USER_POSTAL_CODE];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getUserPostalCode {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:USER_POSTAL_CODE];
    if (object != nil && [object isKindOfClass:[NSString class]]) {
        return (NSString*) object;
    } else {
        return nil;
    }
}

- (void)setUserLocationZip:(NSString *)zip {
    
    if (zip != nil) {
        [self.sharedUserDefaults setObject:zip forKey:USER_LOCATION_ZIP];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:USER_LOCATION_ZIP];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getUserLocationZip {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:USER_LOCATION_ZIP];
    if (object != nil && [object isKindOfClass:[NSString class]]) {
        return (NSString*) object;
    } else {
        return nil;
    }
}

- (void)setConnectDisabled:(BOOL)disabled {
    [self.sharedUserDefaults setBool:disabled forKey:CONNECT_OPTION_DISABLED];
    [self.sharedUserDefaults synchronize];
}

- (BOOL)isConnectDisabled {
    return [self.sharedUserDefaults boolForKey:CONNECT_OPTION_DISABLED];
}
    
- (void)setConnectRecommendations:(BOOL)enabled {
    [self.sharedUserDefaults setObject:[NSNumber numberWithBool:enabled] forKey:CONNECT_OPTION_RECOMMENDATIONS];
    [self.sharedUserDefaults synchronize];
}
    
- (BOOL)isConnectRecommendationsEnabled {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:CONNECT_OPTION_RECOMMENDATIONS];
    if (object != nil && [object isKindOfClass:[NSNumber class]]) {
        return [((NSNumber*) object) boolValue];
    } else {
        return YES;
    }
}

- (void)setAcquisitionChannel:(NSString *)channel {
    
    if (channel != nil) {
        [self.sharedUserDefaults setObject:channel forKey:ACQUISITION_CHANNEL];
        [self.sharedUserDefaults synchronize];
    } else {
        [self.sharedUserDefaults removeObjectForKey:ACQUISITION_CHANNEL];
        [self.sharedUserDefaults synchronize];
    }
}

- (NSString *)getAcquisitionChannel {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:ACQUISITION_CHANNEL];
    if (object != nil && [object isKindOfClass:[NSString class]]) {
        return (NSString*) object;
    } else {
        return nil;
    }
}

-(BOOL)userMarkedForRecommendAFriend {
    return [self.sharedUserDefaults boolForKey:RECOMMEND_A_FRIEND_MARK_USER_KEY];
}

-(void)setUserMarkedForRecommendAFriend:(BOOL)enabled{
    [self.sharedUserDefaults setBool:enabled forKey:RECOMMEND_A_FRIEND_MARK_USER_KEY];
    [self.sharedUserDefaults synchronize];
}


-(BOOL)pocketLockEnabled{
    return [self.sharedUserDefaults boolForKey:POCKET_LOCK_ENABLED];
}

-(void)setPocketLockEnabled:(BOOL)enabled{
    [self.sharedUserDefaults setBool:enabled forKey:POCKET_LOCK_ENABLED];
    [self.sharedUserDefaults synchronize];
}

- (void) setUserLikesBring:(BOOL)answer {
    [self.sharedUserDefaults setBool:answer forKey:RATING_ACTIVATOR_USER_LIKES_BRING];
}

- (BOOL) userLikesBring {
    return [self.sharedUserDefaults boolForKey:RATING_ACTIVATOR_USER_LIKES_BRING];
}

- (void) setRatingActivatorAction:(NSString*)action {
    [self.sharedUserDefaults setObject:action forKey:RATING_ACTIVATOR_USER_ACTION];
}

- (void)setRatingProperties:(NSDictionary *)dictionary {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    [self.sharedUserDefaults setObject:data forKey:RATING_PROPERTIES_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary*)getRatingProperties {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:RATING_PROPERTIES_DEFAULTS_KEY];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (void)clearRatingProperties {
    
    [self.sharedUserDefaults removeObjectForKey:RATING_PROPERTIES_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (void)setRecommendAFriendProperties:(NSDictionary * _Nonnull)properties {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:properties];
    
    [self.sharedUserDefaults setObject:data forKey:RECOMMEND_A_FRIEND_PROPERTIES_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary* _Nullable)getRecommendAFriendProperties {
    NSObject *object = [self.sharedUserDefaults objectForKey:RECOMMEND_A_FRIEND_PROPERTIES_DEFAULTS_KEY];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (void)clearRecommendAFriendProperties {
    [self.sharedUserDefaults removeObjectForKey:RECOMMEND_A_FRIEND_PROPERTIES_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}


- (void) setPantryIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:PANTRY_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL) getPantryIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:PANTRY_INTRO_ALREADY_SEEN_KEY];
}

-(void)addShownCoachMark:(NSString *)coachMarkIdentifier {
    NSMutableArray *shownCoachMarks = [NSMutableArray arrayWithArray:[self getShownCoachMarks]];
    if(![shownCoachMarks containsObject:coachMarkIdentifier]){
        [shownCoachMarks addObject:coachMarkIdentifier];
    }
    [self.sharedUserDefaults setObject:shownCoachMarks forKey:SHOWN_COACH_MARKS_KEY];
    [self.sharedUserDefaults synchronize];
}

-(NSArray *)getShownCoachMarks {
    return [self.sharedUserDefaults arrayForKey:SHOWN_COACH_MARKS_KEY];
}

-(BOOL)expiryReminder1Shown{
    return [self.sharedUserDefaults boolForKey:EXPIRY_REMINDER_1_SHOWN];
}

-(void)setExpiryReminder1Shown:(BOOL)shown{
    [self.sharedUserDefaults setBool:shown forKey:EXPIRY_REMINDER_1_SHOWN];
    [self.sharedUserDefaults synchronize];
}

-(BOOL)expiryReminder2Shown{
    return [self.sharedUserDefaults boolForKey:EXPIRY_REMINDER_2_SHOWN];
}

-(void)setExpiryReminder2Shown:(BOOL)shown{
    [self.sharedUserDefaults setBool:shown forKey:EXPIRY_REMINDER_2_SHOWN];
    [self.sharedUserDefaults synchronize];
}

- (void)setCurrentWatchApplicationContext:(NSDictionary *)applicationContext {
    [self.sharedUserDefaults setObject:applicationContext forKey:CURRENT_WATCH_APPLICATION_CONTEXT_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary *)currentWatchApplicationContext {
    return [self.sharedUserDefaults objectForKey:CURRENT_WATCH_APPLICATION_CONTEXT_KEY];
}

- (void)setCurrentWatchComplicationContext:(NSDictionary *)complicationContext {
    [self.sharedUserDefaults setObject:complicationContext forKey:CURRENT_WATCH_COMPLICATION_CONTEXT_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary *)currentWatchComplicationContext {
    return [self.sharedUserDefaults objectForKey:CURRENT_WATCH_COMPLICATION_CONTEXT_KEY];
}

-(void) resetUserSettings{
    [self.sharedUserDefaults removePersistentDomainForName:BringConstants.APP_GROUP_IDENTIFIER];
    [self synchronize];
}

-(void)defaultInit{
    if (![self.sharedUserDefaults objectForKey:POCKET_LOCK_ENABLED]) {
        [self setPocketLockEnabled:NO];
    }
}

- (BOOL) isFirstRun {
    if ([self.sharedUserDefaults objectForKey:FIRST_RUN] == nil) {
        return YES;
    } else {
        return [self.sharedUserDefaults boolForKey:FIRST_RUN];
    }
}

- (void) setFirstRunCompleted {
    [self.sharedUserDefaults setBool:NO forKey:FIRST_RUN];
}

- (NSInteger) getUseCount {
    return [self.sharedUserDefaults integerForKey:USE_COUNT];
}

- (void) incrementUseCount {
    NSInteger useCount = [self getUseCount];
    useCount++;
    [self.sharedUserDefaults setInteger:useCount forKey:USE_COUNT];
}

-(void) resetUseCount {
    [self.sharedUserDefaults setInteger:0 forKey:USE_COUNT];
}

-(void) resetRunOfLastDismissedMessage {
    [self.sharedUserDefaults setInteger:0 forKey:RUN_OF_DISMISSED_MESSAGE];
}

- (NSInteger) getRunOfDismissedMessage {
    return [self.sharedUserDefaults integerForKey:RUN_OF_DISMISSED_MESSAGE];
}
- (void) setCurrentRunOfDismissedMessage {
    return [self.sharedUserDefaults setInteger:[self getUseCount] forKey:RUN_OF_DISMISSED_MESSAGE];
}

- (BOOL) isUserLoggedIn {
    return [self getBringUserUUID] != nil && ![[self getBringUserUUID] isEqualToString:@""] && [self getBringListUUID] != nil && ![[self getBringListUUID] isEqualToString:@""];
}

- (void) setIntroAdPresented:(NSString*) adIdentifier {
    [self.sharedUserDefaults setBool:YES forKey:adIdentifier];
}

- (BOOL) isIntroAdAlreadyPresented:(NSString*) adIdentifier {
    return [self.sharedUserDefaults boolForKey:adIdentifier];
}

- (void) setOffersLocationActivatorLargeDismissed {
    [self.sharedUserDefaults setBool:YES forKey:OFFERS_LOCATION_ACTIVATOR_LARGE_DISMISSED];
    
}
- (BOOL) getOffersLocationActivatorLargeDismissed {
    return [self.sharedUserDefaults boolForKey:OFFERS_LOCATION_ACTIVATOR_LARGE_DISMISSED];
}

- (NSArray<NSString*>*) getTemplateUuidsOfLastStreamVisit {
    NSArray<NSString*>* uuids = [self.sharedUserDefaults objectForKey:TEMPLATE_UUIDS_OF_LAST_STREAM_VISIT_KEY];
    if(uuids) {
        return uuids;
    } else {
        return [[NSArray<NSString*> alloc] init];
    }
}

- (void) setTemplateUuidsOfLastStreamVisit:(NSArray<NSString*>*) templateUuids {
    [self.sharedUserDefaults setObject:templateUuids forKey:TEMPLATE_UUIDS_OF_LAST_STREAM_VISIT_KEY];
}

- (NSArray<NSNumber*>* _Nonnull) getBrochureIdsOfLastVisit {
    NSArray<NSNumber*>* uuids = [self.sharedUserDefaults objectForKey:BROCHURE_IDS_OF_LAST_VISIT_KEY];
    if(uuids) {
        return uuids;
    } else {
        return [[NSArray<NSNumber*> alloc] init];
    }
}
    
- (void) setBrochureIdsOfLastVisit:(NSArray<NSNumber*>* _Nonnull) brochureIds {
     [self.sharedUserDefaults setObject:brochureIds forKey:BROCHURE_IDS_OF_LAST_VISIT_KEY];
}

- (NSArray<NSNumber*>* _Nonnull) getCurrentBrochureIds {
    NSArray<NSNumber*>* uuids = [self.sharedUserDefaults objectForKey:CURRENT_BROCHURE_IDS_KEY];
    if(uuids) {
        return uuids;
    } else {
        return [[NSArray<NSNumber*> alloc] init];
    }
}

- (void) setCurrentBrochureIds:(NSArray<NSNumber*>* _Nonnull) brochureIds {
    [self.sharedUserDefaults setObject:brochureIds forKey:CURRENT_BROCHURE_IDS_KEY];
}

- (NSArray<NSNumber*>* _Nonnull) getDismissedPostUuids {
    NSArray<NSNumber*>* uuids = [self.sharedUserDefaults objectForKey:DISMISSED_POST_UUIDS_KEY];
    if(uuids) {
        return uuids;
    } else {
        return [[NSArray<NSNumber*> alloc] init];
    }
}

- (void) setDismissedPostUuids:(NSArray<NSString *> *)postUuids {
    [self.sharedUserDefaults setObject:postUuids forKey:DISMISSED_POST_UUIDS_KEY];
}

- (void) setPushChannelEducationEnabled:(BOOL) enabled {
    [self.sharedUserDefaults setBool:enabled forKey:PUSH_CHANNEL_EDUCATION_ENABLED_KEY];
    [self.sharedUserDefaults synchronize];
}

- (BOOL) pushChannelEducationEnabled {
    if([self.sharedUserDefaults objectForKey:PUSH_CHANNEL_EDUCATION_ENABLED_KEY] == nil){
        return YES;
    }
    return [self.sharedUserDefaults boolForKey:PUSH_CHANNEL_EDUCATION_ENABLED_KEY];
}

- (void)setPushChannelOffersEnabled:(BOOL)enabled {
    [self.sharedUserDefaults setBool:enabled forKey:PUSH_CHANNEL_OFFERS_ENABLED_KEY];
    [self.sharedUserDefaults synchronize];
}

- (BOOL)pushChannelOffersEnabled {
    if([self.sharedUserDefaults objectForKey:PUSH_CHANNEL_OFFERS_ENABLED_KEY] == nil){
        return YES;
    }
    return [self.sharedUserDefaults boolForKey:PUSH_CHANNEL_OFFERS_ENABLED_KEY];
}

- (BOOL)pushChannelRecipesEnabled {
    if([self.sharedUserDefaults objectForKey:PUSH_CHANNEL_RECIPES_ENABLED_KEY] == nil){
        return YES;
    }
    return [self.sharedUserDefaults boolForKey:PUSH_CHANNEL_RECIPES_ENABLED_KEY];
}

- (void)setPushChannelRecipesEnabled:(BOOL)enabled {
    [self.sharedUserDefaults setBool:enabled forKey:PUSH_CHANNEL_RECIPES_ENABLED_KEY];
    [self.sharedUserDefaults synchronize];
}

- (BOOL)pushChannelShoppingEnabled {
    if([self.sharedUserDefaults objectForKey:PUSH_CHANNEL_SHOPPING_ENABLED_KEY] == nil){
        return YES;
    } else {
        return [self.sharedUserDefaults boolForKey:PUSH_CHANNEL_SHOPPING_ENABLED_KEY];
    }
}

- (void)setPushChannelShoppingEnabled:(BOOL)enabled {
    [self.sharedUserDefaults setBool:enabled forKey:PUSH_CHANNEL_SHOPPING_ENABLED_KEY];
    [self.sharedUserDefaults synchronize];
}

-(void)setLastOffersSync:(NSDate *)lastSync {
    [self.sharedUserDefaults setObject:lastSync forKey:LAST_OFFERS_SYNC_KEY];
    [self.sharedUserDefaults synchronize];
}

-(NSDate *)getLastOffersSync {
    return [self.sharedUserDefaults objectForKey:LAST_OFFERS_SYNC_KEY];
}

- (void) setOffersIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:OFFERS_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL) getOffersIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:OFFERS_INTRO_ALREADY_SEEN_KEY];
}

- (void) setConnectIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:CONNECT_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL) getConnectIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:CONNECT_INTRO_ALREADY_SEEN_KEY];
}

- (void)setConnectBrackIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:CONNECT_BRACK_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL)getConnectBrackIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:CONNECT_BRACK_INTRO_ALREADY_SEEN_KEY];
}

- (void)setConnectBringmeisterIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:CONNECT_BRINGMEISTER_INTRO_ALREADY_SEEN_KEY];
}
    
- (BOOL)getConnectBringmeisterIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:CONNECT_BRINGMEISTER_INTRO_ALREADY_SEEN_KEY];
}

- (void)setConnectSupermarketIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:CONNECT_SUPERMARKET_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL)getConnectSupermarketIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:CONNECT_SUPERMARKET_INTRO_ALREADY_SEEN_KEY];
}

- (void) setWalletIntroAlreadySeen {
    [self.sharedUserDefaults setBool:YES forKey:WALLET_INTRO_ALREADY_SEEN_KEY];
}

- (BOOL) getWalletIntroAlreadySeen {
    return [self.sharedUserDefaults boolForKey:WALLET_INTRO_ALREADY_SEEN_KEY];
}

- (void)setLastGeoLookupLocation:(BringGeoLocation*)location timestamp:(NSDate*)timestamp {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:location];
    [self.sharedUserDefaults setObject:data forKey:LAST_GEO_LOOKUP_LOCATION_KEY];
    [self.sharedUserDefaults setObject:timestamp forKey:LAST_GEO_LOOKUP_TIMESTAMP_KEY];
    [self.sharedUserDefaults synchronize];
}

- (BringGeoLocation*)getLastGeoLookupLocation {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:LAST_GEO_LOOKUP_LOCATION_KEY];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[BringGeoLocation class]]) {
            return (BringGeoLocation*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (NSDate*)getLastGeoLookupTimestamp {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:LAST_GEO_LOOKUP_TIMESTAMP_KEY];
    
    if (object != nil && [object isKindOfClass:[NSDate class]]) {
        return (NSDate*) object;
    } else {
        return nil;
    }
}

- (void)setDefaultsForKey:(NSString* _Nonnull)key dictionary:(NSDictionary * _Nullable)dictionary {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    NSString *compoundKey = [self getDefaultsCompoundKeyForKey:key];
    [self.sharedUserDefaults setObject:data forKey:compoundKey];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary* _Nullable)getDefaultsForKey:(NSString* _Nonnull)key {
    
    NSString *compoundKey = [self getDefaultsCompoundKeyForKey:key];
    NSObject *object = [self.sharedUserDefaults objectForKey:compoundKey];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (void)clearDefaultsForKey:(NSString * _Nonnull)key {
    
    NSString *compoundKey = [self getConnectDefaultsKey:key];
    [self.sharedUserDefaults removeObjectForKey:compoundKey];
    [self.sharedUserDefaults synchronize];
}

- (NSString*)getDefaultsCompoundKeyForKey:(NSString*)key {
    return [NSString stringWithFormat:@"%@_%@", DICTIONARY_SETTINGS_KEY_PRFIX, key];
}

- (void)setConnectDefaults:(NSString *)partner dictionary:(NSDictionary *)dictionary {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    NSString *key = [self getConnectDefaultsKey:partner];
    
    [self.sharedUserDefaults setObject:data forKey:key];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary*)getConnectDefaults:(NSString *)partner {
    
    NSString *key = [self getConnectDefaultsKey:partner];
    NSObject *object = [self.sharedUserDefaults objectForKey:key];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (void)clearConnectDefaults:(NSString *)partner {
    
    NSString *key = [self getConnectDefaultsKey:partner];
    [self.sharedUserDefaults removeObjectForKey:key];
    [self.sharedUserDefaults synchronize];
}
    
- (NSString*)getConnectDefaultsKey:(NSString*)partner {
    return [NSString stringWithFormat:@"%@_%@", CONNECT_DEFAULTS_KEY, partner];
}

- (void)setConnectBadges:(NSString *)partner dictionary:(NSDictionary *)dictionary {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    NSString *key = [self getConnectBadgesKey:partner];
    
    [self.sharedUserDefaults setObject:data forKey:key];
    [self.sharedUserDefaults synchronize];
}
    
- (NSDictionary*)getConnectBadges:(NSString *)partner {
    
    NSString *key = [self getConnectBadgesKey:partner];
    NSObject *object = [self.sharedUserDefaults objectForKey:key];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}
    
- (void)clearConnectBadges:(NSString *)partner {
    
    NSString *key = [self getConnectBadgesKey:partner];
    [self.sharedUserDefaults removeObjectForKey:key];
    [self.sharedUserDefaults synchronize];
}

- (NSString*)getConnectBadgesKey:(NSString*)partner {
    return [NSString stringWithFormat:@"%@_%@", CONNECT_BADGES_KEY, partner];
}

- (void)setLifecycleDefaults:(NSDictionary * _Nonnull)dictionary {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    [self.sharedUserDefaults setObject:data forKey:LIFECYCLE_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDictionary* _Nullable)getLifecycleDefaults {
    
    NSObject *object = [self.sharedUserDefaults objectForKey:LIFECYCLE_DEFAULTS_KEY];
    
    if (object != nil && [object isKindOfClass:[NSData class]]) {
        
        NSData *data = (NSData*) object;
        NSObject *decoded = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (decoded != nil && [decoded isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary*) decoded;
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}

- (void)clearLifecycleDefaults {
    
    [self.sharedUserDefaults removeObjectForKey:LIFECYCLE_DEFAULTS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (void)setMockCountry:(NSString *)mockCountry {
    if(!mockCountry){
        [self.sharedUserDefaults removeObjectForKey:MOCK_COUNTRY_KEY];
    }else{
        [self.sharedUserDefaults setObject:mockCountry forKey:MOCK_COUNTRY_KEY];
        
    }
    [self.sharedUserDefaults synchronize];
}

- (NSString * _Nullable)getMockCountry {
    return [self.sharedUserDefaults objectForKey:MOCK_COUNTRY_KEY];
}

- (void)setMockLocation:(NSString *)mockLocation {
    if(!mockLocation){
        [self.sharedUserDefaults removeObjectForKey:MOCK_LOCATION_KEY];
    }else{
        [self.sharedUserDefaults setObject:mockLocation forKey:MOCK_LOCATION_KEY];
    }
    [self.sharedUserDefaults synchronize];
}

- (NSString * _Nullable)getMockLocation {
    return [self.sharedUserDefaults objectForKey:MOCK_LOCATION_KEY];
}

- (void)clear:(NSString*)key {
    [self.sharedUserDefaults removeObjectForKey:key];
    [self.sharedUserDefaults synchronize];
}

-(NSString* _Nullable)getArticleLanguageForCurrentUserList {
    return [self getListArticleLanguage:[self getBringListUUID]];
}

- (void)setYearInReview:(NSString * _Nullable)yearInReview {
    [self.sharedUserDefaults setObject:yearInReview forKey:YEAR_IN_REVIEW_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSString * _Nullable)getYearInReview {
    return [self.sharedUserDefaults objectForKey:YEAR_IN_REVIEW_KEY];
}
    
- (BOOL)is18OrOlder {
    return [self.sharedUserDefaults boolForKey:USER_IS_18_KEY];
}
    
- (void)setIs18OrOlder:(BOOL)isOlder {
    [self.sharedUserDefaults setBool:isOlder forKey:USER_IS_18_KEY];
    [self.sharedUserDefaults synchronize];
}

- (void)setLinkedPartners:(NSArray<NSString*>* _Nonnull)linkedPartners {
    [self.sharedUserDefaults setObject:linkedPartners forKey:LINKED_PARTNERS_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSArray<NSString*>* _Nullable)getLinkedPartners {
    return [self.sharedUserDefaults objectForKey:LINKED_PARTNERS_KEY];
}

- (BOOL)isAutoSyncDisabledForAssistant {
    NSNumber *returnValue = (NSNumber*)[self.sharedUserDefaults objectForKey:IS_ASSISTANT_AUTOSYNC_DISABLED_KEY];
    if (returnValue) {
        return returnValue.boolValue;
    } else {
        return YES;
    }
}

- (void)setIsAutoSyncDisabledForAssistant:(BOOL)isDisabled {
    [self.sharedUserDefaults setObject:[NSNumber numberWithBool:isDisabled] forKey:IS_ASSISTANT_AUTOSYNC_DISABLED_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSArray<NSString*>* _Nonnull)getListOrder; {
    NSArray<NSString*>* returnValue = [self.sharedUserDefaults objectForKey:LIST_ORDER_KEY];
    return returnValue != nil ? returnValue : [NSArray new];
}

- (void)setListOrder:(NSArray<NSString*>* _Nonnull)listOrder {
    [self.sharedUserDefaults setObject:listOrder forKey:LIST_ORDER_KEY];
    [self.sharedUserDefaults synchronize];
}

- (void)showedForcedEngagementAction:(NSString* _Nonnull)itemKey {
    
    NSMutableArray *forcedEngagementActions = [self shownForcedEngagementActions];
    
    [forcedEngagementActions addObject:itemKey];
    [self setObject:forcedEngagementActions forKey:FORCED_ENGAGEBENT_ACTIONS_KEY];
    [self synchronize];
    
}

- (NSMutableArray * _Nonnull) shownForcedEngagementActions {
    NSMutableArray *forcedEngagementActions = [[self objectForKey:FORCED_ENGAGEBENT_ACTIONS_KEY] mutableCopy];
    if (!forcedEngagementActions) {
        forcedEngagementActions = [NSMutableArray array];
    }
    
    return forcedEngagementActions;
}

- (void)setLastSpecificationSyncDate:(NSDate * _Nonnull) lastSync ForArticleLanguage: (NSString* _Nonnull) articleLanguage{
    NSString *key = [NSString stringWithFormat: @"%@_%@", LAST_SPECIFICATION_SYNC_KEY, articleLanguage];
    [self.sharedUserDefaults setObject:lastSync forKey:key];
    [self.sharedUserDefaults synchronize];
}

- (NSDate * _Nullable) getLastSpecificationSyncForArticleLanguage:(NSString* _Nonnull) articleLanguage{
    NSString *key = [NSString stringWithFormat: @"%@_%@", LAST_SPECIFICATION_SYNC_KEY, articleLanguage];
    return [self.sharedUserDefaults objectForKey:key];
}

- (void)setCatalogExtensionSyncDate:(NSDate * _Nullable) lastSync ForArticleLanguage: (NSString* _Nonnull) articleLanguage {
    NSString *key = [NSString stringWithFormat: @"%@_%@", LAST_CATALOG_EXTENSION_SYNC_KEY, articleLanguage];
    [self.sharedUserDefaults setObject:lastSync forKey:key];
    [self.sharedUserDefaults synchronize];
}

- (NSDate * _Nullable) getLastCatalogExtensionSyncDate:(NSString* _Nonnull) articleLanguage {
    NSString *key = [NSString stringWithFormat: @"%@_%@", LAST_CATALOG_EXTENSION_SYNC_KEY, articleLanguage];
    return [self.sharedUserDefaults objectForKey:key];
}

-(void)resetForcedEngagementForItems:(NSArray<NSString*>* _Nonnull)items {
    NSMutableArray *forcedEngagementActions = [self shownForcedEngagementActions];
    for (NSString* item in items) {
        if ([forcedEngagementActions containsObject:item]) {
            [forcedEngagementActions removeObject:item];
        }
    }
    [self setObject:forcedEngagementActions forKey:FORCED_ENGAGEBENT_ACTIONS_KEY];
    [self synchronize];
}

-(void)setActivationLevel:(NSInteger)activationLevel {
    [self.sharedUserDefaults setInteger:activationLevel forKey:CONNECT_ACTIVATION_LEVEL];
}

-(NSInteger)getActivationLevel {
    return [self.sharedUserDefaults integerForKey:CONNECT_ACTIVATION_LEVEL];
}

- (BOOL)hasSuggestedSpecificationsEnabled {
    if ([self.sharedUserDefaults objectForKey:SUGGESTED_SPECIFICATIONS_KEY] == nil) {
        return YES;
    }
    return [self.sharedUserDefaults boolForKey:SUGGESTED_SPECIFICATIONS_KEY];
}

- (void)setSuggestedSpecifications:(BOOL)enabled {
    [self.sharedUserDefaults setBool:enabled forKey:SUGGESTED_SPECIFICATIONS_KEY];
    [self.sharedUserDefaults synchronize];
}

-(BOOL)didMigrateToCoreDataUserItemStore {
    return [self.sharedUserDefaults boolForKey:DID_MIGRATE_TO_CORE_DATA_USER_ITEM_STORE_KEY];
}

- (void)setDidMigrateToCoreDataUserItemStore:(BOOL)migrated {
    [self.sharedUserDefaults setBool:migrated forKey:DID_MIGRATE_TO_CORE_DATA_USER_ITEM_STORE_KEY];
}

- (NSArray<NSString*>* _Nonnull)getMessagesSortOrder {
    NSArray<NSString*>* returnValue = [self.sharedUserDefaults objectForKey:MESSAGES_SORT_ORDER_KEY];
    return returnValue != nil ? returnValue : [NSArray new];
}

- (void)setMessagesSortOrder:(NSArray<NSString*>* _Nonnull)messagesSortOrder {
    [self.sharedUserDefaults setObject:messagesSortOrder forKey:MESSAGES_SORT_ORDER_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSDate* _Nullable)getLastTrackingDate {
    return [self.sharedUserDefaults objectForKey:LAST_STARTUP_TRACKING_DATE];
}

- (void)setLastTrackingDate:(NSDate * _Nullable)trackingDate {
    [self.sharedUserDefaults setObject:trackingDate forKey:LAST_STARTUP_TRACKING_DATE];
    [self.sharedUserDefaults synchronize];
}

- (NSSet<NSString*>* _Nonnull)getClosedSections {
    NSSet<NSString*>* returnValue = [self.sharedUserDefaults setForKey: CLOSED_SECTIONS_BY_USER_KEY];
    return returnValue != nil ? returnValue : [NSSet new];
}

- (void)setClosedSections:(NSSet<NSString*>* _Nonnull)closedSections {
    [self.sharedUserDefaults setSet: closedSections forKey: CLOSED_SECTIONS_BY_USER_KEY];
    [self.sharedUserDefaults synchronize];
}

- (NSSet<NSString*>* _Nullable)getOpenSections {
    NSSet<NSString*>* returnValue = [self.sharedUserDefaults setForKey: OPEN_SECTIONS];
    return returnValue;
}

- (void)setOpenSections:(NSSet<NSString*>* _Nonnull)openSections {
    [self.sharedUserDefaults setSet: openSections forKey: OPEN_SECTIONS];
    [self.sharedUserDefaults synchronize];
}

- (NSSet<AppInvitationLinkUuid*>* _Nonnull)getRewardsToCollect {
    NSData* currentRewardsData = [self.sharedUserDefaults objectForKey:REWARD_COLLECT_KEY];
    
    if (!currentRewardsData) {
        return [NSSet new];
    }
    
    NSSet<AppInvitationLinkUuid*> *currentRewards = [NSKeyedUnarchiver unarchiveObjectWithData: currentRewardsData];
    return currentRewards != nil ? currentRewards : [NSSet new];
}

- (void)setRewardToCollect:(AppInvitationLinkUuid* _Nonnull)rewardToCollect {
    NSData* currentRewardsData = [self.sharedUserDefaults objectForKey:REWARD_COLLECT_KEY];
    
    NSSet<AppInvitationLinkUuid*> *currentRewards;
    
    if (!currentRewardsData) {
        currentRewards = [NSSet new];
    } else {
        currentRewards = [NSKeyedUnarchiver unarchiveObjectWithData: currentRewardsData];
    }
    
    NSMutableSet<AppInvitationLinkUuid*>* newMutableRewards = [currentRewards mutableCopy];
    [newMutableRewards addObject:rewardToCollect];
    
    NSData* dataToStore = [NSKeyedArchiver archivedDataWithRootObject:newMutableRewards];
    
    [self.sharedUserDefaults setObject:dataToStore forKey:REWARD_COLLECT_KEY];
    [self.sharedUserDefaults synchronize];
}

- (void)removeRewardToCollect:(AppInvitationLinkUuid* _Nonnull)rewardToRemove {
    NSData* currentRewardsData = [self.sharedUserDefaults objectForKey:REWARD_COLLECT_KEY];
    
    if (!currentRewardsData) {
        return;
    }
    
    NSSet<AppInvitationLinkUuid*> *currentRewards = [NSKeyedUnarchiver unarchiveObjectWithData: currentRewardsData];
    if (currentRewards) {
        NSMutableSet<AppInvitationLinkUuid*>* mutableSet = [currentRewards mutableCopy];
        [mutableSet removeObject: rewardToRemove];
        
        NSData* dataToStore = [NSKeyedArchiver archivedDataWithRootObject:mutableSet];
        
        [self.sharedUserDefaults setObject:dataToStore forKey:REWARD_COLLECT_KEY];
        [self.sharedUserDefaults synchronize];
    }
    
}

- (BOOL)didWriteAppleWatchUserSettingToServer {
    return [self.sharedUserDefaults boolForKey:DID_WRITE_HAS_BRING_WATCH_INSTALLED];
}

- (void)setDidWriteAppleWatchUserSettingToServer:(BOOL)didWriteToServer {
    [self.sharedUserDefaults setBool:didWriteToServer forKey:DID_WRITE_HAS_BRING_WATCH_INSTALLED];
    [self.sharedUserDefaults synchronize];
}

-(void)setDeeplink:(NSString*)deeplink {
    [self.sharedUserDefaults setObject:deeplink forKey:DEEPLINK_AFTER_REGISTRATION_KEY];
    [self.sharedUserDefaults synchronize];
}

-(NSString*)getDeeplink {
    return [self.sharedUserDefaults objectForKey:DEEPLINK_AFTER_REGISTRATION_KEY];
}

@end
