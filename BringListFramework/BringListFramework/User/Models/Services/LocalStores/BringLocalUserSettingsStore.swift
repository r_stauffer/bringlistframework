//
//  BringLocalUserSettingsStore.swift
//  Bring
//
//  Created by Sascha Thöni on 04.01.17.
//
//

import UIKit

enum BringLocalUserSettingsStoreError: Error {
    case unknown
}

extension BringLocalUserSettingsStore: Syncer {
    public func sync(callback: @escaping (Result<Void, Error>) -> Void) {
        syncUserSettingsFromRemote(onSuccess: {
            callback(Result.success(()))
        }, onFailure: {
            callback(Result.failure(BringLocalUserSettingsStoreError.unknown))
        })
    }
}

@objcMembers
public class BringLocalUserSettingsStore: NSObject {
    
    public static let currentListSectionOrderChangedNotificationName = NSNotification.Name(rawValue: "currentListSectionOrderChanged")
    
    public static let shared = BringLocalUserSettingsStore()
    
    private let service : BringUserSettingsServiceNewProtocol
    private let catalogProvider : BringCatalogProvider
    private let userListStore: LocalListStore
    
    public init(service : BringUserSettingsServiceNewProtocol = BringUserSettingsServiceNew(), catalogProvider : BringCatalogProvider = BringCatalogProvider.shared, userListStore: LocalListStore = BringCoreDataUserListStore.shared) {
        self.service = service
        self.catalogProvider = catalogProvider
        self.userListStore = userListStore
    }
    
    public func syncUserSettingsFromRemote(onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        _ = service.loadSettings(userUuid: userUuid, onSuccess: { (userSettingsData) in
            
            BringUserSettings.standard().setBadgeMode(userSettingsData.badgeMode)
            BringUserSettings.standard().setAutoPushEnabled(userSettingsData.autoPushEnabled)
            
            BringUserSettings.standard().setPushChannelEducationEnabled(userSettingsData.pushChannelEducationEnabled)
            BringUserSettings.standard().setPushChannelOffersEnabled(userSettingsData.pushChannelOffersEnabled)
            BringUserSettings.standard().setPushChannelRecipesEnabled(userSettingsData.pushChannelRecipesEnabled)
            BringUserSettings.standard().setPushChannelShoppingEnabled(userSettingsData.pushChannelShoppingEnabled)
            BringUserSettings.standard().setIsAutoSyncDisabledForAssistant(userSettingsData.assistantNewListsAutoSync)
            BringUserSettings.standard().setSuggestedSpecifications(userSettingsData.suggestedSpecifications)
            BringUserSettings.standard().setListOrder(userSettingsData.listOrder)
            
            BringUserSettings.standard().setDefaultListUUID(userSettingsData.defaultListUUID)
            
            BringUserSettings.standard().setDefaultZip(userSettingsData.defaultZip)
            BringUserSettings.standard().setConnectDisabled(userSettingsData.connectOptionDisabled)
            BringUserSettings.standard().setConnectRecommendations(userSettingsData.connectOptionRecommendations)
            
            BringUserSettings.standard().setAcquisitionChannel(userSettingsData.acquisitionChannel)
            
            for userListSettings in userSettingsData.userListSettings {
                
                guard !userListSettings.listUuid.isEmpty else {
                    continue
                }
                
                if !userListSettings.listArticleLocale.isEmpty {
                    let articleLanguage = BringLocaleHelper.sharedInstance.getArticleLanguage(forLocale: userListSettings.listArticleLocale)
                    BringUserSettings.standard().setListArticleLanguage(userListSettings.listUuid, language: articleLanguage)
                }
                
                if !userListSettings.listSectionOrder.isEmpty {
                    BringUserSettings.standard().setListSectionOrder(userListSettings.listUuid, order: userListSettings.listSectionOrder)
                }
                
                if !userListSettings.hiddenSections.isEmpty {
                    BringUserSettings.standard().setHiddenSectionsForList(userListSettings.listUuid, sections: userListSettings.hiddenSections)
                }
            }
            
            for experiment in userSettingsData.experiments {
                // @FIXME: Move this!
                //BringGoogleAnalyticsTracker.sharedInstance().trackAction("Experiment-\(experiment.name)", withLabel: experiment.selectedAlternative)
            }
            
            onSuccess()
            
        }, onFailure: {
            onFailure()
        })
    }
    
    public func pushChannelEducationEnabled() -> Bool {
        return BringUserSettings.standard().pushChannelEducationEnabled()
    }
    
    public func enablePushChannelEducation(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "pushChannelEducation", value: value, onSuccess: {
            BringUserSettings.standard().setPushChannelEducationEnabled(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func pushChannelOffersEnabled() -> Bool {
        return BringUserSettings.standard().pushChannelOffersEnabled()
    }
    
    public func enablePushChannelOffers(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "pushChannelOffers", value: value, onSuccess: {
            BringUserSettings.standard().setPushChannelOffersEnabled(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func pushChannelRecipesEnabled() -> Bool {
        return BringUserSettings.standard().pushChannelRecipesEnabled()
    }
    
    public func enablePushChannelRecipes(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "pushChannelRecipes", value: value, onSuccess: {
            BringUserSettings.standard().setPushChannelRecipesEnabled(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func enableSuggestedSpecifications(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "suggestedSpecifications", value: value, onSuccess: {
            BringUserSettings.standard().setSuggestedSpecifications(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func pushChannelShoppingEnabled() -> Bool {
        return BringUserSettings.standard().pushChannelShoppingEnabled()
    }
    
    public func enablePushChannelShopping(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "pushChannelShopping", value: value, onSuccess: {
            BringUserSettings.standard().setPushChannelShoppingEnabled(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setBadgeMode(value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        service.setSetting(userUuid: userUuid, key: "badgeMode", value: value, onSuccess: {
            BringUserSettings.standard().setBadgeMode(value)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func autoPushEnabled() -> Bool {
        return BringUserSettings.standard().autoPushEnabled()
    }
    
    public func setAutoPush(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "autoPush", value: value, onSuccess: {
            BringUserSettings.standard().setAutoPushEnabled(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setDefaultList(uuid: String?, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        service.setSetting(userUuid: userUuid, key: "defaultListUUID", value: uuid ?? "", onSuccess: {
            BringUserSettings.standard().setDefaultListUUID(uuid)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setUserListArticleLanguage(listUuid: String, articleLanguage: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        // the service has to save the locale string, but internal we are storing the language string in the usersettings
        let localeString = BringLocaleHelper.sharedInstance.getLocale(forArticleLanguage: articleLanguage)
        service.setListSetting(userUuid: userUuid, listUuid: listUuid, key: "listArticleLanguage", value: localeString, onSuccess: {
            BringUserSettings.standard().setListArticleLanguage(listUuid, language: articleLanguage)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setListOrder(_ listOrder: [String], onSuccess: @escaping () -> Void, onFailure: @escaping () -> Void) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        guard let data = try? JSONEncoder().encode(listOrder) else {
            onFailure()
            return
        }
        
        guard let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
            onFailure()
            return
        }
        
        service.setSetting(userUuid: userUuid, key: "listOrder", value: jsonString, onSuccess: {
            
            let oldListOrder = BringUserSettings.standard().getListOrder()
            if !oldListOrder.hasSameOrder(as: listOrder) {
                BringUserSettings.standard().setListOrder(listOrder)
            }
            
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func getListOrder() -> [String] {
        let savedListOrder = BringUserSettings.standard().getListOrder()
        
        let userLists = userListStore.allLists(forPredicate: nil).map { $0.uuid }
        
        var listsToAdd = [String]()
        for userList in userLists {
            if !savedListOrder.contains(userList) {
                listsToAdd.append(userList)
            }
        }
        
        var listsToRemove = [String]()
        for savedList in savedListOrder {
            if !userLists.contains(savedList) {
                listsToRemove.append(savedList)
            }
        }
        
        var composedListOrder = savedListOrder + listsToAdd
        composedListOrder.removeAll { (list) -> Bool in
            listsToRemove.contains(list)
        }
        
        return composedListOrder
    }
    
    
    public func setUserListSectionOrder(listUuid: String, sectionOrder: [String], onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: sectionOrder, options: []) else {
            onFailure()
            return
        }
        
        guard let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
            onFailure()
            return
        }
        
        service.setListSetting(userUuid: userUuid, listUuid: listUuid, key: "listSectionOrder", value: jsonString, onSuccess: {
            
            let oldSectionOrder = BringUserSettings.standard().getListSectionOrder(listUuid) ?? []
            
            if !self.isSectionOrderEqual(order1: oldSectionOrder, toOrder: sectionOrder) {
                
                BringUserSettings.standard().setListSectionOrder(listUuid, order: sectionOrder)
                
                if listUuid == BringUserSettings.standard().getBringListUUID() {
                    NotificationCenter.default.post(name: BringLocalUserSettingsStore.currentListSectionOrderChangedNotificationName, object: nil)
                }
                
            }
            
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setUserListHidden(listUuid: String, hiddeenSections: [String], onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: hiddeenSections, options: []) else {
            onFailure()
            return
        }
        
        guard let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
            onFailure()
            return
        }
        
        service.setListSetting(userUuid: userUuid, listUuid: listUuid, key: "hiddenSections", value: jsonString, onSuccess: {
            
            let oldHiddenSections = BringUserSettings.standard().getHiddenSections(listUuid) ?? []
            
            if !self.isHiddenSectionsArray(sectionsArray: oldHiddenSections, equalToOtherSectionArray: hiddeenSections) {
                BringUserSettings.standard().setHiddenSectionsForList(listUuid, sections: hiddeenSections)
                
                if listUuid == BringUserSettings.standard().getBringListUUID() {
                    NotificationCenter.default.post(name: BringLocalUserSettingsStore.currentListSectionOrderChangedNotificationName, object: nil)
                }
            }
            
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setUserPostalCode(value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        service.setSetting(userUuid: userUuid, key: "userPostalCode", value: value, onSuccess: {
            BringUserSettings.standard().setUserPostalCode(value)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func changeCatalog(forListUuid listUuid : String, toArticleLanguage articleLanguage : String, onSuccess: @escaping ()->(), onFailure:@escaping ()->()) {
        
        self.setUserListArticleLanguage(listUuid: listUuid, articleLanguage: articleLanguage, onSuccess: { [weak self] in
            
            guard let strongSelf = self else {
                onSuccess()
                return
            }
            
            let listSectionOrder = strongSelf.catalogProvider.getSectionOrder(forListUuid: listUuid)
            strongSelf.setUserListSectionOrder(listUuid: listUuid, sectionOrder: listSectionOrder, onSuccess: {
                onSuccess()
            }, onFailure: onFailure)
        }, onFailure: onFailure)
    }
    
    public func setDefaultZip(value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        service.setSetting(userUuid: userUuid, key: "defaultZip", value: value, onSuccess: {
            BringUserSettings.standard().setDefaultZip(value)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setConnectOption(disabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = disabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "connectOptionDisabled", value:value, onSuccess: {
            BringUserSettings.standard().setConnectDisabled(disabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setConnectRecommendations(enabled: Bool, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        let value = enabled ? "ON" : "OFF"
        
        service.setSetting(userUuid: userUuid, key: "connectRecommendationsEnabled", value:value, onSuccess: {
            BringUserSettings.standard().setConnectRecommendations(enabled)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    public func setAcquisitionChannel(value: String, onSuccess: @escaping () -> (), onFailure: @escaping ()->()) {
        
        guard let userUuid = BringUserSettings.standard().getBringUserUUID() else {
            onFailure()
            return
        }
        
        service.setSetting(userUuid: userUuid, key: "acquisitionChannel", value: value, onSuccess: {
            BringUserSettings.standard().setAcquisitionChannel(value)
            onSuccess()
        }, onFailure: onFailure)
    }
    
    private func isHiddenSectionsArray(sectionsArray: [String], equalToOtherSectionArray otherSectionsArray: [String]) -> Bool {
        guard sectionsArray.count == otherSectionsArray.count else {
            return false
        }
        
        for section in sectionsArray {
            if !otherSectionsArray.contains(section) {
                return false
            }
        }
        
        return true
    }
    
    private func isSectionOrderEqual(order1:[String], toOrder order2: [String]) -> Bool {
        
        guard order1.count == order2.count else {
            return false;
        }
        
        for(index, sectionName1) in order1.enumerated() {
            let sectionName2 = order2[index]
            if(sectionName1 != sectionName2){
                return false
            }
        }
        
        return true
    }

    public func storeDefaultListSettingsRemote(listUuid : String, onSuccess:  @escaping () -> (), onFailure: @escaping () -> ()) {
        // Save list language and list section order on backend
        if let listArticleLanguage = BringUserSettings.standard().getListArticleLanguage(listUuid) {
            self.setUserListArticleLanguage(listUuid: listUuid, articleLanguage: listArticleLanguage, onSuccess: { [weak self] in
                
                guard let strongSelf = self else {
                    onSuccess()
                    return
                }
                
                strongSelf.setUserListSectionOrder(listUuid: listUuid, sectionOrder: strongSelf.catalogProvider.getDefaultSectionOrder(forLanguage: listArticleLanguage), onSuccess: { onSuccess() }, onFailure: { onFailure() })
            }, onFailure: { onFailure() })
        }
    }
}
