//
//  File.swift
//  
//
//  Created by Renato Stauffer on 05.08.19.
//

import Foundation
import CoreData
import CoreLocation

@objc public final class BringCoreDataListsStackFactory: NSObject {
    
    @objc public override init() {}
    
    @objc public func makeCoreDataStack(storeType: String = NSSQLiteStoreType) -> BringCoreDataListsStack {
        if BringCoreDataListsStackFactory.isRunningTests() {
            let coreDataStack = BringCoreDataListsStack()
            coreDataStack.configure(storeType: NSInMemoryStoreType)
            return coreDataStack
        } else {
            let coreDataStack = BringCoreDataListsStack.shared
            coreDataStack.configure(storeType: storeType)
            return coreDataStack
        }
    }
    
    fileprivate static func isRunningTests() -> Bool {
        #if DEBUG
            return ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
        #else
            return false
        #endif
    }
}

@available(iOS 12.0, *)
public class BringCLLocationSecureValueFransformer: NSSecureUnarchiveFromDataTransformer {
    
    public override class var allowedTopLevelClasses: [AnyClass] {
        return [CLLocation.self]
    }
}

@objc public final class BringCoreDataListsStack: NSObject {
    
    private let modelName = "BringCoreDataModel"
    private let sqliteFileName: String = "BringCoreDataModel.sqlite"
    private let modelDirectory = "BringModel"
    private let modelExtension = "momd"
    private var storeType: String?
    
    fileprivate static let shared = BringCoreDataListsStack()
    
    @objc override init() {
        if #available(iOS 12, *) {
            let transformer = BringCLLocationSecureValueFransformer()
            ValueTransformer.setValueTransformer(transformer, forName: NSValueTransformerName(rawValue: "BringCLLocationSecureValueFransformer"))
            print(ValueTransformer.valueTransformerNames())
        }
    }
    
    public lazy var mainContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    public func createBackgroundContext() -> NSManagedObjectContext{
        return self.storeContainer.newBackgroundContext()
    }
    
    public func save(_ context: NSManagedObjectContext) -> Void {
        
        context.performAndWait {
            guard context.hasChanges else { return }
            
            do {
                try context.obtainPermanentIDs(for: Array(context.insertedObjects))
                try context.save()
            } catch {
                Log.w("Failed to get permanent ids")
            }
        }
    }
    
    // MARK: Model / container loading
    
    public lazy var managedObjectModel: NSManagedObjectModel = {
        let bundle = Bundle(for: type(of: self))
        
        if let modelURL = bundle.url(forResource: modelName, withExtension: modelExtension) {
            return NSManagedObjectModel(contentsOf: modelURL)!
        } else {
            return NSManagedObjectModel()
        }
    }()
    
    lazy var libraryDirectory: URL? = {
        #if os(iOS)
        if !BringCoreDataListsStackFactory.isRunningTests() {
            return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: BringConstants.APP_GROUP_IDENTIFIER)!
        } else {
            return nil
        }
        #elseif os(watchOS)
            return sqliteFileUrl!
        #endif
    }()
    
    lazy var storeContainer: NSPersistentContainer = {
        
        guard let storeType = self.storeType else {
            fatalError("Configure the storetype first!")
        }
        
        let container = NSPersistentContainer(name: self.modelName, managedObjectModel: managedObjectModel)
        
        
        let description: NSPersistentStoreDescription
        
        if let libraryDirectory = self.libraryDirectory {
            description = NSPersistentStoreDescription(url: libraryDirectory.appendingPathComponent(self.modelName + self.modelExtension))
        } else {
            description = NSPersistentStoreDescription()
        }
        
        description.type = storeType
        container.persistentStoreDescriptions = [description]
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                assert(false, "Failed to load persistent stores: \n\(error)")
            }
        })
        
        let context = container.viewContext
        context.automaticallyMergesChangesFromParent = true
        
        return container
    }()
    
    // MARK: MISC
    
    func configure(storeType: String = NSSQLiteStoreType) {
        self.storeType = storeType
    }
    
    private func getDir() -> URL? {
        
        guard let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            Log.e("Failed to get documents directory.")
            return nil
        }
        
        let directory = documents.appendingPathComponent(self.modelDirectory)
        
        if FileManager.default.fileExists(atPath: directory.path) {
            return directory
        } else {
            Log.d("Create store directory.")
            do {
                try FileManager.default.createDirectory(at: directory, withIntermediateDirectories: true, attributes: nil)
                return directory
            } catch {
                Log.e("Failed to create store directory: \n\(error)")
                return nil
            }
        }
    }
    
    private var sqliteFileUrl: URL? {
        return self.getDir()
    }
}
