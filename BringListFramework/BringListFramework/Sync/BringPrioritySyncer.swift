//
//  BringPrioritySyncer.swift
//  BringWatch
//
//  Created by Renato Stauffer on 19.09.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

public struct BringPrioritySyncer {
    public typealias SyncPriority = Int
    public static let lowPriority: SyncPriority = 100
    public static let mediumPriority: SyncPriority = 500
    public static let highPriority: SyncPriority = 1000
    
    public let priority: SyncPriority
    public let syncer: Syncer
    
    public init(priority: SyncPriority, syncer: Syncer) {
        self.priority = priority
        self.syncer = syncer
    }
}
