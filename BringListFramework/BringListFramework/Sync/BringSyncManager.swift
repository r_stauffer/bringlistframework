//
//  BringSyncManager.swift
//  Bring
//
//  Created by Renato Stauffer on 14.08.19.
//

import Foundation

public final class BringSyncManager {
    
    private let queue = OperationQueue()
    
    private let syncersArray: [[Syncer]]
    
    public init(syncers: [BringPrioritySyncer]) {
        queue.maxConcurrentOperationCount = 1
        self.syncersArray = BringSyncManager.createSyncerGroups(syncers: syncers)
    }
    
    public func sync(callback: @escaping (Result<Void, Error>) -> Void) {
        
        for syncers in syncersArray {
            let operation = BringGroupedSyncOperation(syncers: syncers)
            queue.addOperation(operation)
        }
        
        queue.addOperation {
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
        }
    }
    
    static func createSyncerGroups(syncers: [BringPrioritySyncer]) -> [[Syncer]] {
        var array = [[Syncer]]()
        
        let prioritiesSet = Set(syncers.map { $0.priority })
        let priorities = Array(prioritiesSet).sorted { $0 > $1 }
        
        for priority in priorities {
            let mappedSyncers = syncers
                .filter {
                    $0.priority == priority
                }
                .map {
                    $0.syncer
                }
            
            array.append(mappedSyncers)
        }
        
        return array
    }
}
