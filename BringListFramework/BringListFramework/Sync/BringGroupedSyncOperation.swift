//
//  BringGroupedSyncOperation.swift
//  BringWatch
//
//  Created by Renato Stauffer on 19.09.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

final class BringGroupedSyncOperation: AsynchronousOperation {
    
    private let syncers: [Syncer]
    
    public init(syncers: [Syncer]) {
        self.syncers = syncers
    }
    
    override func main() {
        if isCancelled {
            finish()
            return
        }
        
        let group = DispatchGroup()
        
        for syncer in syncers {
            group.enter()
            syncer.sync { result in
                Log.i("Start syncing \(syncer.self)")
                group.leave()
                switch result {
                case .success:
                    Log.i("Finished syncing \(syncer.self)")
                case .failure(let error):
                    Log.ed(error)
                }
            }
        }
        
        group.notify(queue: DispatchQueue.main) {
            self.finish()
        }
    }
}
