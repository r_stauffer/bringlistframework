//
//  Syncer.swift
//  BringWatch
//
//  Created by Renato Stauffer on 19.09.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

public protocol Syncer {
    func sync(callback: @escaping (Result<Void, Error>) -> Void)
}
