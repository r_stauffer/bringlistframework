//
//  BringListUserSettings.swift
//  BringListFramework
//
//  Created by Renato Stauffer on 22.11.19.
//

import Foundation

fileprivate extension UserDefaults {
    
    @discardableResult
    func set(_ value: Any?, forKey key: BringListSettings.Key) -> Bool {
        self.set(value, forKey: key.rawValue)
        return self.synchronize()
    }
    
    func object(forKey key: BringListSettings.Key) -> Any? {
        return self.object(forKey: key.rawValue)
    }
    
    @discardableResult
    func removeObject(forKey key: BringListSettings.Key) -> Bool {
        self.removeObject(forKey: key.rawValue)
        return self.synchronize()
    }
    
    func string(forKey key: BringListSettings.Key) -> String? {
        return self.string(forKey: key.rawValue)
    }
    
    func dictionary(forKey key: BringListSettings.Key) -> [String : Any]? {
        return self.dictionary(forKey: key.rawValue)
    }
    
    func array(forKey key: BringListSettings.Key) -> [Any]? {
        return self.array(forKey: key.rawValue)
    }
    
    func bool(forKey key: BringListSettings.Key) -> Bool {
        return self.bool(forKey: key.rawValue)
    }
}

public class BringListSettings {
    
    public static let standard = BringListSettings(suiteName: Constants.Project.bundleIdentifier)
    
    private let userDefaults: UserDefaults
    private let persistentDomain: String
    
    private let languageProvider: LanguageProvider
    
    fileprivate enum Key: String {
        case lastSyncDate = "ch.publisheria.bring.BringListFramework.lastSyncDate"
        case didMigrateUserSettings = "ch.publisheria.bring.BringListFramework.didMigrateUserSettings"
        case didMigrateUserLists = "ch.publisheria.bring.BringListFramework.didMigrateUserLists"
        case didMigrateListChanges = "ch.publisheria.bring.BringListFramework.didMigrateListChanges"
        case didMigrateUserItems = "ch.publisheria.bring.BringListFramework.didMigrateUserItems"
        case didMigrateListItemDetails = "ch.publisheria.bring.BringListFramework.didMigrateListItemDetails"
        case didMigrateListStates = "ch.publisheria.bring.BringListFramework.didMigrateListStates"
    }
    
    public init(suiteName name: String, languageProvider: LanguageProvider = BringLocaleHelper.sharedInstance) {
        self.persistentDomain = name
        self.userDefaults = UserDefaults(suiteName: name) ?? UserDefaults.standard
        self.languageProvider = languageProvider
    }
    
    public func reset() {
        self.userDefaults.removePersistentDomain(forName: self.persistentDomain)
        self.userDefaults.synchronize()
    }
    
    // MARK: Current List
    
    public func lastSyncDate() -> Date? {
        return userDefaults.object(forKey: .lastSyncDate) as? Date
    }
    
    public func setLastSyncDate(_ date: Date) {
        userDefaults.set(date, forKey: .lastSyncDate)
    }
    
    // MARK: BringListFramework migrations
    
    public func didMigrateUserSettings() -> Bool {
        return userDefaults.bool(forKey: .didMigrateUserSettings)
    }
    
    public func setDidMigrateUserSettings(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateUserSettings)
    }
    
    public func didMigrateUserLists() -> Bool {
        return userDefaults.bool(forKey: .didMigrateUserLists)
    }
    
    public func setDidMigrateUserLists(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateUserLists)
    }
    
    public func didMigrateListChanges() -> Bool {
        return userDefaults.bool(forKey: .didMigrateListChanges)
    }
    
    public func setDidMigrateListChanges(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateListChanges)
    }
    
    public func didMigrateUserItems() -> Bool {
        return userDefaults.bool(forKey: .didMigrateUserItems)
    }
    
    public func setDidMigrateUserItems(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateUserItems)
    }
    
    public func didMigrateListItemDetails() -> Bool {
        return userDefaults.bool(forKey: .didMigrateListItemDetails)
    }
    
    public func setDidMigrateListItemDetails(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateListItemDetails)
    }
    
    public func didMigrateListStates() -> Bool {
        return userDefaults.bool(forKey: .didMigrateListStates)
    }
    
    public func setDidMigrateListStates(_ didSet: Bool) {
        userDefaults.set(didSet, forKey: .didMigrateListStates)
    }

    
}
