//
//  BringDevice.swift
//  Bring
//
//  Created by Beat on 15.11.18.
//

import Foundation

class BringDevice {
    
    // MARK: Properties
    
    private var userUuid: String
    private var email: String?
    
    private var pushService: String
    private var deviceToken: String
    private var oldDeviceToken: String?
    
    private var appVersion: String
    
    
    // MARK: Object
    
    init(userUuid: String, email: String?, pushService: String, deviceToken: String, oldDeviceToken: String?, appVersion: String) {
        self.userUuid = userUuid
        self.email = email
        self.pushService = pushService
        self.deviceToken = deviceToken
        self.oldDeviceToken = oldDeviceToken
        self.appVersion = appVersion
    }
    
    
    // MARK: Accessors
    
    func getUserUuid() -> String {
        return self.userUuid
    }
    
    func getEmail() -> String? {
        return self.email
    }
    
    func getPushService() -> String {
        return self.pushService
    }
    
    func getDeviceToken() -> String {
        return self.deviceToken
    }
    
    func getOldDeviceToken() -> String? {
        return self.oldDeviceToken
    }
    
    func getAppVersion() -> String {
        return self.appVersion
    }
    
}
