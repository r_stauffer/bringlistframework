//
//  BringDeviceManager.swift
//  Bring
//
//  Created by Beat on 15.11.18.
//

import Foundation

struct BringDeviceConstants {
    #if os(iOS)
    static let apns: String = "APNS"
    #elseif os(watchOS)
    static let apns: String = "APNS_WATCH"
    #endif
    
    static let geoCacheDuration: TimeInterval = 14 * 24 * 60 * 60.0
}

public protocol BringDeviceManagerDataSource: class {
    func version() -> String
}

@objc
public class BringDeviceManager: NSObject {
    
    // MARK: Properties
    
    @objc
    public static let shared = BringDeviceManager()
    
    private var userSettings: BringUserSettings
    private var deviceService: BringDeviceService
    public weak var dataSource: BringDeviceManagerDataSource?
    
    
    // MARK: Object
    
    init(settings: BringUserSettings, service: BringDeviceService) {
        self.userSettings = settings
        self.deviceService = service
    }
    
    convenience override init() {
        
        let settings = BringUserSettings.standard()
        
        let rest = BringDeviceRestEndpoint()
        let service = BringDeviceService(rest: rest)
        
        self.init(settings: settings, service: service)
    }
    
    
    // MARK: Device
    @objc
    public func updateDevice(token: NSData?) {
        
        self.updateDevice(token: token, onSuccess: {
            Log.d("Device token updated")
        }) {
            Log.w("Failed to update device token.")
        }
    }
    
    func updateDevice(token: NSData?, onSuccess:@escaping() -> (), onFailure:@escaping() -> ()) {
        Log.i()
        
        guard let userUuid = userSettings.getBringUserUUID() else {
            Log.w("User is not (yet) registered.")
            onFailure()
            return
        }
        
        guard let deviceToken = self.parseDeviceToken(token) else {
            Log.e("Failed to parse device token.")
            onFailure()
            return
        }
        
        let email = userSettings.getEmail()
        let pushService = BringDeviceConstants.apns
        let oldDeviceToken = self.getOldDeviceToken(deviceToken)
        
        let appVersion = self.getAppVersion()
        
        let device = BringDevice(userUuid: userUuid, email: email, pushService: pushService, deviceToken: deviceToken, oldDeviceToken: oldDeviceToken, appVersion: appVersion)
        
        self.deviceService.postDevice(device, onSuccess: {
            
            self.userSettings.setApnsToken(deviceToken)
            Log.d("Device token updated: \(device.getDeviceToken())")
            onSuccess()
            
        }, onFailure: onFailure)
    }
    
    private func getAppVersion() -> String {
        guard let version = dataSource?.version() else {
            assertionFailure("No delegate set for BringDeviceManager")
            if let version =  Bundle(for: type(of: self)).infoDictionary?["CFBundleShortVersionString"] as? String {
                return version
            } else {
                return "0.0.0"
            }
        }
        return version
    }
    
    internal func parseDeviceToken(_ token: NSData?) -> String? {
        
        guard let token = token as Data? else {
            return nil
        }
        
        let deviceToken = token.map { String(format: "%02.2hhx", $0) }.joined()
        
        return deviceToken
    }
    
    internal func getOldDeviceToken(_ deviceToken: String) -> String? {
        
        if let currentDeviceToken = userSettings.apnsToken(), currentDeviceToken != deviceToken {
            return currentDeviceToken
        } else {
            return nil
        }
    }
    
    
    // MARK: Location
    
    public func getGeoLocation(onSuccess:@escaping(BringGeoLocation) -> (), onFailure:@escaping() -> ()) {
        Log.i()
        
        if let cached = self.getCachedLocation() {
            onSuccess(cached)
        } else {
            
            self.deviceService.getGeoLocation(onSuccess: { (location) in
                
                self.userSettings.setLastGeoLookupLocation(location, timestamp: Date())
                
                onSuccess(location)
                
            }, onFailure: onFailure)
        }
    }
    
    func getCachedLocation() -> BringGeoLocation? {
        
        guard let location = userSettings.getLastGeoLookupLocation(), let timestamp = userSettings.getLastGeoLookupTimestamp() else {
            return nil
        }
        
        if abs(timestamp.timeIntervalSinceNow) < BringDeviceConstants.geoCacheDuration {
            return location
        } else {
            return nil
        }
    }
    
    public func getCurrentIP(onSuccess:@escaping (String)->(), onFailure:@escaping ()->()) {
        Log.i()
        
        self.deviceService.getIPAddress(onSuccess: { (ip) in
            onSuccess(ip)
        }, onFailure: onFailure)
    }
    
}
