//
//  BringDeviceRestEndpoint.swift
//  Bring
//
//  Created by Beat on 15.11.18.
//

import Foundation
import Alamofire

class BringDeviceRestEndpoint: BringDeviceRestProtocol {
    
    // MARK: Properties
    
    private enum Router: URLRequestConvertible {
        case postDevice(json: [String: Any])
        case getIP
        case getGeo
        
        var method: HTTPMethod {
            switch self {
            case .postDevice:
                return .post
            case .getIP:
                return .get
            case .getGeo:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .postDevice(_):
                return "/bringdevices"
            case .getIP:
                return "/v2/bringdevices/ip"
            case .getGeo:
                return "/v2/bringdevices/geo"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            
            let url = URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: url.appendingPathComponent(self.path))
            mutableURLRequest.httpMethod = self.method.rawValue
            
            switch self {
            case .postDevice(let json):
                return try URLEncoding.default.encode(mutableURLRequest, with: json)
            case .getIP:
                return mutableURLRequest
            case .getGeo:
                return mutableURLRequest
            }
        }
        
    }
    
    private let alamofire = BringAlamofireManager.sharedInstance
    
    
    // MARK: Rest
    
    func postDevice(json: [String : Any], result: @escaping (Error?) -> ()) {
        Log.v()
        
        self.alamofire.defaultManager.request(Router.postDevice(json: json)).validate().responseString { (response) in
            
            switch response.result {
            case .success:
                Log.d("success")
                result(nil)
            case .failure(let error):
                Log.e("failure: error = \(error)")
                result(error)
            }
        }
    }
    
    func getGeo(result: @escaping ([String: Any]?, Error?) -> ()) {
        Log.v()
        
        self.alamofire.defaultManager.request(Router.getGeo).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let json):

                if let json = json as? [String: Any] {
                    result(json, nil)
                } else {
                    Log.e("Invalid response json.")
                    result(nil, NSError(domain: "BringDeviceService", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey: "Invalid response json."]))
                }
                
            case .failure(let error):
                Log.e("Failed: error = \(error)")
                result(nil, error)
            }
        })
    }
    
    func getIP(result: @escaping ([String: Any]?, Error?) -> ()) {
        Log.v()
        
        self.alamofire.defaultManager.request(Router.getIP).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let json):
                
                if let json = json as? [String: Any] {
                    result(json, nil)
                } else {
                    Log.e("Invalid response json.")
                    result(nil, NSError(domain: "BringDeviceService", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey: "Invalid response json."]))
                }
                
            case .failure(let error):
                Log.e("Failed: error = \(error)")
                result(nil, error)
            }
        })
    }
    
}
