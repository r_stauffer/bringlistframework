//
//  BringDeviceService.swift
//  Bring
//
//  Created by Beat on 15.11.18.
//

import Foundation

class BringDeviceService {
    
    // MARK: Properties
    
    private var rest: BringDeviceRestProtocol
    
    
    // MARK: Object
    
    init(rest: BringDeviceRestProtocol) {
        self.rest = rest
    }
    
    
    // MARK: Service
    
    public func postDevice(_ device: BringDevice, onSuccess:@escaping() -> (), onFailure:@escaping() -> ()) {
        Log.v()
        
        let json = self.json(device: device)
        
        self.rest.postDevice(json: json) { (error) in
            
            if error == nil {
                onSuccess()
            } else {
                onFailure()
            }
        }
    }
    
    public func getGeoLocation(onSuccess:@escaping(BringGeoLocation) -> (), onFailure:@escaping() -> ()) {
        Log.v()
        
        self.rest.getGeo { (json, error) in
            
            if error == nil, let json = json {
                
                if let latitude = json["latitude"] as? Double, let longitude = json["longitude"] as? Double {
                    
                    let accuracy = json["accuracyRadius"] as? Double
                    let location = BringGeoLocation(latitude: latitude, longitude: longitude, altitude: nil, accuracy: accuracy)
                    
                    onSuccess(location)
                    
                } else {
                    Log.e("Invalid json response.")
                    onFailure()
                }
                
            } else {
                onFailure()
            }
        }
    }
    
    public func getIPAddress(onSuccess:@escaping(String) -> (), onFailure:@escaping() -> ()) {
        Log.v()
        
        self.rest.getIP { (json, error) in
            
            if error == nil, let json = json {
                
                if let ip = json["ip"] as? String {
                    onSuccess(ip)
                } else {
                    Log.e("Invalid json response.")
                    onFailure()
                }
                
            } else {
                onFailure()
            }
        }
    }
    
    
    // MARK: JSON
    
    private func json(device: BringDevice) -> [String: Any] {
        
        var json = [String: Any]()
        
        json["userUuid"] = device.getUserUuid()
        
        if let email = device.getEmail() {
            json["email"] = email
        }
        
        json["pushService"] = device.getPushService()
        json["deviceToken"] = device.getDeviceToken()
        
        if let oldDeviceToken = device.getOldDeviceToken() {
            json["oldDeviceToken"] = oldDeviceToken
        }
        
        json["appVersion"] = device.getAppVersion()
        
        return json
    }
    
}
