//
//  BringConstants.swift
//  Bring
//
//  Created by Sascha Thöni on 21.10.16.
//
//

import Foundation

public class BringConstants: NSObject {
    
    #if DEBUG
        @objc public static let APP_GROUP_IDENTIFIER = "group.ch.publisheria.bring.dev"
    #else
        @objc public static let APP_GROUP_IDENTIFIER = "group.ch.publisheria.bring"
    #endif
    
    @objc public static let PERSISTENT_STORE_NAME = "Bring"
    @objc public static let PERSISTENT_STORE_FILE_EXTENSION = "sqlite"
    
    @objc public static let PERSISTENT_STORE_FILE_NAME = "\(PERSISTENT_STORE_NAME).\(PERSISTENT_STORE_FILE_EXTENSION)"
    @objc public static let MANAGED_OBJECT_MODEL_NAME = "Bring"
    @objc public static let MANAGED_OBJECT_MODEL_FILE_EXTENSION = "momd"
    @objc public static let MANAGED_OBJECT_MODEL_FILE_NAME = "\(MANAGED_OBJECT_MODEL_NAME).\(MANAGED_OBJECT_MODEL_FILE_EXTENSION)"
    
}

