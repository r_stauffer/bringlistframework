//
//  BringListItemDetail.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

@objc public class BringListItemDetail: NSObject {
    public let uuid: String
    public let itemKey: String
    public let listUuid: String
    public let userIconItemId: String
    public let userSectionId: String
    public let assignedTo: String
    public let localImageUrl: String
    @objc public let remoteImageUrl: String
    
    public init(uuid: String = "", itemKey: String = "", listUuid: String = "", userIconItemId: String = "", userSectionId: String = "", assignedTo: String = "", localImageUrl: String = "", remoteImageUrl: String = "") {
        self.uuid = uuid
        self.itemKey = itemKey
        self.listUuid = listUuid
        self.userIconItemId = userIconItemId
        self.userSectionId = userSectionId
        self.assignedTo = assignedTo
        self.localImageUrl = localImageUrl
        self.remoteImageUrl = remoteImageUrl
    }
    
    public func createItemDetail(newUuid: String? = nil, itemKey: String? = nil, listUuid: String? = nil, userIconItemId: String? = nil, userSectionId: String? = nil, assignedTo: String? = nil, localImageUrl: String? = nil, remoteImageUrl: String? = nil) -> BringListItemDetail {
        let uuid = newUuid ?? self.uuid
        let userIconItemId = userIconItemId ?? self.userIconItemId
        let userSectionId = userSectionId ?? self.userSectionId
        let assignedTo = assignedTo ?? self.assignedTo
        let localImageUrl = localImageUrl ?? self.localImageUrl
        let remoteImageUrl = remoteImageUrl ?? self.remoteImageUrl
        let itemKey = itemKey ?? self.itemKey
        let listUuid = listUuid ?? self.listUuid
        
        return BringListItemDetail(uuid: uuid, itemKey: itemKey, listUuid: listUuid, userIconItemId: userIconItemId, userSectionId: userSectionId, assignedTo: assignedTo, localImageUrl: localImageUrl, remoteImageUrl: remoteImageUrl)
    }
    
    public var imageUploadFailed: Bool = false
    
    public func isSynced() -> Bool {
        return !self.uuid.isEmpty
    }
    
    @objc
    public func hasImage() -> Bool {
        return !self.localImageUrl.isEmpty
    }
}

public extension BringListItemDetail {
    
    static func predicate(withListUuid uuid: String, andItemKey itemKey: String) -> NSPredicate {
        return NSPredicate(format: "listUuid = %@ AND itemKey = %@", uuid, itemKey)
    }
    
    var uniqueItemDetailPredicate: NSPredicate {
        return NSPredicate(format: "listUuid = %@ AND itemKey = %@", listUuid, itemKey)
    }
    
    static func predicate(withListUuid uuid: String) -> NSPredicate {
        NSPredicate(format: "listUuid = %@", uuid)
    }
    
    var uuidPredicate: NSPredicate {
        return NSPredicate(format: "uuid = %@", uuid)
    }
    
    static func predicate(withUuid uuid: String) -> NSPredicate {
        return NSPredicate(format: "uuid = %@", uuid)
    }
}

extension BringListItemDetail {
    public convenience init?(entity: BringListItemDetailEntity) {
        guard let uuid = entity.uuid else {
            // TODO: Log here!
            return nil
        }
        
        guard let itemKey = entity.itemKey else {
            // TODO: Log here!
            return nil
        }
        
        guard let listUuid = entity.listUuid else {
            // TODO: Log here!
            return nil
        }
        
        guard let userIconItemId = entity.userIconItemId else {
            // TODO: Log here!
            return nil
        }
        
        guard let userSectionId = entity.userSectionId else {
            // TODO: Log here!
            return nil
        }
        
        guard let assignedTo = entity.assignedTo else {
            // TODO: Log here!
            return nil
        }
        
        guard let localImageUrl = entity.localImageUrl else {
            // TODO: Log here!
            return nil
        }
        
        guard let remoteImageUrl = entity.remoteImageUrl else {
            // TODO: Log here!
            return nil
        }
        
        self.init(uuid: uuid, itemKey: itemKey, listUuid: listUuid, userIconItemId: userIconItemId, userSectionId: userSectionId, assignedTo: assignedTo, localImageUrl: localImageUrl, remoteImageUrl: remoteImageUrl)
    }
}
