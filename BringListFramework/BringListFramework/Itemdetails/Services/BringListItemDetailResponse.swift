//
//  BringItemDetailResponse.swift
//  Bring
//
//  Created by Sandro Strebel on 24/06/16.
//
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper

class BringListItemDetailResponse: Mappable {
    
    var uuid: String?
    var itemId: String?
    var listUuid: String?
    var userIconItemId: String?
    var userSectionId: String?
    var assignedTo: String?
    var imageUrl: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        uuid <- map["uuid"]
        itemId <- map["itemId"]
        listUuid <- map["listUuid"]
        userIconItemId <- map["userIconItemId"]
        userSectionId <- map["userSectionId"]
        assignedTo <- map["assignedTo"]
        imageUrl <- map["imageUrl"]
        
    }
}
