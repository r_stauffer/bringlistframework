//
//  BringCoreDataListItemDetailStore.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation
import CoreData

public protocol ListItemDetailStoreDao {
    func addBringListItemDetail(_ itemDetail: BringListItemDetail)
    func delete(with predicate: NSPredicate?)
    func update(_ itemDetail: BringListItemDetail)
    func get(with predicate: NSPredicate?) -> [BringListItemDetail]
    func create(_ itemDetails: [BringListItemDetail])
}

// @TODO: All is currently done on the main thread. Move all to a serial background queue
public class BringCoreDataListItemDetailStore: ListItemDetailStoreDao {
    
    private let stack: BringCoreDataListsStack
    private let serialQueue = OperationQueue()
    
    public static let shared = BringCoreDataListItemDetailStore()
    
    public init(stack: BringCoreDataListsStack = BringCoreDataListsStackFactory().makeCoreDataStack()) {
        self.stack = stack
    }
    
    public func delete(with predicate: NSPredicate?) {
        let context = self.stack.mainContext
        
        context.performAndWait {
            do {
                let fetchreqeust: NSFetchRequest<BringListItemDetailEntity> = NSFetchRequest<BringListItemDetailEntity>(entityName: "BringListItemDetailEntity")
                fetchreqeust.predicate = predicate
                let result = try context.fetch(fetchreqeust)
                
                for itemDetail in result {
                    context.delete(itemDetail)
                }
                
                self.stack.save(context)
                
                return
            } catch let error {
                assertionFailure("error: \(error.localizedDescription)")
            }
        }
    }
    
    public func update(_ itemDetail: BringListItemDetail) {
        let context = self.stack.mainContext
        
        context.performAndWait {
            do {
                let fetchreqeust: NSFetchRequest<BringListItemDetailEntity> = NSFetchRequest<BringListItemDetailEntity>(entityName: "BringListItemDetailEntity")
                fetchreqeust.predicate = itemDetail.uniqueItemDetailPredicate
                let result = try context.fetch(fetchreqeust)
                
                for entity in result {
                    entity.assignedTo = itemDetail.assignedTo
                    entity.itemKey = itemDetail.itemKey
                    entity.listUuid = itemDetail.listUuid
                    entity.localImageUrl = itemDetail.localImageUrl
                    entity.userIconItemId = itemDetail.userIconItemId
                    entity.userSectionId = itemDetail.userSectionId
                    entity.uuid = itemDetail.uuid
                    entity.remoteImageUrl = itemDetail.remoteImageUrl
                }
                
                self.stack.save(context)
                
                return
            } catch let error {
                assertionFailure("error: \(error.localizedDescription)")
            }
        }
    }
    
    public func get(with predicate: NSPredicate?) -> [BringListItemDetail] {
        let context = self.stack.mainContext
        var returnValue = [BringListItemDetail]()
        
        context.performAndWait {
            do {
                let fetchreqeust: NSFetchRequest<BringListItemDetailEntity> = NSFetchRequest<BringListItemDetailEntity>(entityName: "BringListItemDetailEntity")
                fetchreqeust.predicate = predicate
                let result = try context.fetch(fetchreqeust)
                let itemDetails = result.compactMap { BringListItemDetail(entity: $0) }
                returnValue = itemDetails
            } catch let error {
                assertionFailure("\(error.localizedDescription)")
            }
        }
        
        return returnValue
    }
    
    public func create(_ itemDetails: [BringListItemDetail]) {
        let context = self.stack.mainContext
        
        context.performAndWait {
            do {
                
                for itemDetail in itemDetails {
                    let fetchreqeust: NSFetchRequest<BringListItemDetailEntity> = NSFetchRequest<BringListItemDetailEntity>(entityName: "BringListItemDetailEntity")
                    fetchreqeust.predicate = NSPredicate(format: "uuid == %@", itemDetail.uuid)
                    let result = try context.fetch(fetchreqeust)
                    
                    if result.count == 0 {
                        
                        guard let iistItemDetailEntity = NSEntityDescription.insertNewObject(forEntityName: "BringListItemDetailEntity", into: context) as? BringListItemDetailEntity else {
                            assertionFailure("Entity unknown!")
                            return
                        }
                        
                        self.map(itemDetail, toEntity: iistItemDetailEntity)
                    }
                }
                
                self.stack.save(context)
                
            } catch let error {
                assertionFailure("\(error.localizedDescription)")
                return
            }
        }
    }
    
    public func addBringListItemDetail(_ itemDetail: BringListItemDetail) {
        
        let context = self.stack.mainContext
        
        context.performAndWait {
            do {
                let fetchreqeust: NSFetchRequest<BringListItemDetailEntity> = NSFetchRequest<BringListItemDetailEntity>(entityName: "BringListItemDetailEntity")
                fetchreqeust.predicate = NSPredicate(format: "uuid == %@", itemDetail.uuid)
                let result = try context.fetch(fetchreqeust)
                
                if result.count == 0 {
                    
                    guard let iistItemDetailEntity = NSEntityDescription.insertNewObject(forEntityName: "BringListItemDetailEntity", into: context) as? BringListItemDetailEntity else {
                        assertionFailure("Entity unknown!")
                        return
                    }
                    
                    self.map(itemDetail, toEntity: iistItemDetailEntity)
                    
                    self.stack.save(context)
                    
                    return
                } else {
                    return
                }
                
            } catch let error {
                assertionFailure("Error while fetching item detail for itemKey = \(itemDetail.itemKey) of list = \(itemDetail.listUuid) error = \(error.localizedDescription)")
                return
            }
        }
    }
    
    private func map(_ itemDetail: BringListItemDetail, toEntity entity: BringListItemDetailEntity) {
        entity.uuid = itemDetail.uuid
        entity.itemKey = itemDetail.itemKey
        entity.listUuid = itemDetail.listUuid
        entity.userIconItemId = itemDetail.userIconItemId
        entity.userSectionId = itemDetail.userSectionId
        entity.assignedTo = itemDetail.assignedTo
        entity.localImageUrl = itemDetail.localImageUrl
        entity.remoteImageUrl = itemDetail.remoteImageUrl
    }
    
    
}
