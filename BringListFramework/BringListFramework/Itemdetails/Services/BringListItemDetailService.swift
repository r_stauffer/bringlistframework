//
//  BringListItemDetailService.swift
//  Bring
//
//  Created by Sandro Strebel on 24/06/16.
//
//

import Foundation
import Alamofire

public protocol ItemDetailService {
    func createItemDetailsForListUUIDAndBringItem(listUuid: String, itemKey: String, userIconItemId: String?, userSectionId: String?, assignedTo: String?, senderDeviceToken: String?, predictedIcon: Bool, predictedSection: Bool, onSuccess: @escaping (( BringListItemDetail) -> ()), onFailure: @escaping (() -> ()))
}

public class BringListItemDetailService: ItemDetailService {
    
    public static let sharedInstance = BringListItemDetailService()
    
    public func getAllItemDetailsForList(listUuid: String, onSuccess: @escaping ([BringListItemDetail]) -> (), onFailure: @escaping (() -> ())) -> URLRequest? {
        
        guard !listUuid.isEmpty else {
            onFailure()
            return nil
        }
        
        return BringAlamofireManager.sharedInstance.defaultManager.request(Router.getAllBringListItemDetailsForList(listUuid))
            .validate()
            .responseArray { (response: DataResponse<[BringListItemDetailResponse]>) in
                
                switch response.result {
                case .success:
                    
                    let itemDetailsArray = response.result.value
                    var result:[BringListItemDetail] = []
                    
                    if let itemDetailsArray = itemDetailsArray {
                        for itemDetail in itemDetailsArray {
                            result.append(self.map(listItemDetailResponse: itemDetail))
                        }
                    }
                    
                    onSuccess(result)
                    
                case .failure(_):
                    onFailure()
                }  
            }.request
    }
    
    public func getItemDetail(itemDetailUuid: String, onSuccess: @escaping ((BringListItemDetail) -> Void), onFailure: @escaping ((Int) -> Void)) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.readBringListItemDetail(itemDetailUuid))
            .validate()
            .responseObject { (response: DataResponse<BringListItemDetailResponse>) in
                
                switch response.result {
                case .success:
                    
                    let bringListItemDetailResponse = response.result.value
                    onSuccess(self.map(listItemDetailResponse: bringListItemDetailResponse!))
                    
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    if(response.response != nil){
                        onFailure((response.response?.statusCode)!)
                    }else{
                        onFailure(0)
                    }
                    
                }
        }
        
    }
    
    public func searchItemDetail(listUuid: String, itemKey: String, onSuccess: @escaping ((BringListItemDetail) -> Void), onNotFound: @escaping (() -> ()), onFailure: @escaping (() -> ())) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.searchBringListItemDetail(listUuid, itemKey))
            .validate()
            .responseObject { (response: DataResponse<BringListItemDetailResponse>) in
                
                if(204 == response.response?.statusCode){
                    onNotFound()
                }else {
                    
                    switch response.result {
                    case .success:
                        
                        let bringListItemDetailResponse = response.result.value
                        onSuccess(self.map(listItemDetailResponse: bringListItemDetailResponse!))
                        
                    case .failure(let error):
                        Log.e(error.localizedDescription)
                        onFailure()
                    }
                }
        }
        
    }
    
    
    public func createItemDetailsForListUUIDAndBringItem(listUuid: String, itemKey: String, userIconItemId: String?, userSectionId: String?, assignedTo: String?, senderDeviceToken: String?, predictedIcon: Bool = false, predictedSection: Bool = false, onSuccess: @escaping (( BringListItemDetail) -> ()), onFailure: @escaping (() -> ())) {
        
        BringAlamofireManager.sharedInstance.defaultManager.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(listUuid.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "listUuid")
            multipartFormData.append(itemKey.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "itemId")
            
            if(userIconItemId != nil && !userIconItemId!.isEmpty){
                multipartFormData.append(userIconItemId!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "userIconItemId")
            }
            
            if (predictedIcon) {
                if let hasIcon = predictedIcon.description.data(using: .utf8) {
                    multipartFormData.append(hasIcon, withName: "predictedIcon")
                }
            }
            
            if (predictedSection) {
                if let hasIcon = predictedSection.description.data(using: .utf8) {
                    multipartFormData.append(hasIcon, withName: "predictedSection")
                }
            }
            
            if(userSectionId != nil && !userSectionId!.isEmpty){
                multipartFormData.append(userSectionId!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "userSectionId")
            }
            if(assignedTo != nil && !assignedTo!.isEmpty){
                multipartFormData.append(assignedTo!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "assignedTo")
            }
            if(senderDeviceToken != nil){
                multipartFormData.append(senderDeviceToken!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "sender")
            }
        }
            , with: Router.createBringListItemDetail, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseObject { (response: DataResponse<BringListItemDetailResponse>) in
                        
                        switch response.result {
                        case .success:
                            let bringListItemDetailResponse = response.result.value
                            onSuccess(self.map(listItemDetailResponse: bringListItemDetailResponse!))
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            onFailure()
                        }
                    }
                    
                case .failure(let encodingError):
                    Log.e(encodingError.localizedDescription)
                    onFailure()
                }
        }
            
        )
    }
    
    public func updateItemDetailImage(_ itemDetailUuid: String, senderDeviceToken: String?, image: UIImage, onSuccess: @escaping ((String) -> ()), onFailure: @escaping (() -> ())) {
        BringAlamofireManager.sharedInstance.defaultManager.upload(multipartFormData:
            
            { multipartFormData in
                if  let imageData = image.jpegData(compressionQuality: 0.6) {
                    
                    multipartFormData.append(imageData, withName: "imageData", fileName: "image.jpg", mimeType: "image/jpeg")
                }
                if(senderDeviceToken != nil){
                    multipartFormData.append(senderDeviceToken!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "sender")
                }
        }
            
            , with: Router.updateBringListItemDetailImage(itemDetailUuid),
              
              
              encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                        case .success:
                            
                            guard let value = response.result.value as? [String: AnyObject],
                                let imageUrl = value["imageUrl"] as? String else {
                                    Log.e("Malformed data received from service")
                                    onFailure()
                                    return
                            }
                            
                            onSuccess(imageUrl)
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            onFailure()
                        }
                        
                    }
                case .failure(let encodingError):
                    Log.e(encodingError.localizedDescription)
                    onFailure()
                }
        }
        )
        
    }
    
    public func removeItemDetailImage(itemDetailUuid: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.removeBringListItemDetailImage(itemDetailUuid, senderDeviceToken))
            .validate()
            .responseData { response in
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
                
                
        }
    }
    
    public func updateItemDetailUserSection(itemDetailUuid: String, sectionKey: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.updateBringListItemDetailUserSection(itemDetailUuid, sectionKey, senderDeviceToken))
            .validate()
            .responseData { response in
                
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    public func removeItemDetailUserSection(itemDetailUuid: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.removeBringListItemDetailUserSection(itemDetailUuid, senderDeviceToken))
            .validate()
            .responseData { response in
                
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    public func updateItemDetailUserIcon(itemDetailUuid: String, iconKey: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.updateBringListItemDetailUserIcon(itemDetailUuid, iconKey, senderDeviceToken))
            .validate()
            .responseData { response in
                
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    public func removeItemDetailUserIcon(itemDetailUuid: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.removeBringListItemDetailUserIcon(itemDetailUuid, senderDeviceToken))
            .validate()
            .responseData { response in
                
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    public func removeItemDetail(for itemDetailUuid: String, senderDeviceToken: String?, onSuccess: @escaping (() -> ()), onFailure: @escaping (() -> ())) -> Void {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.deleteBringListItemDetail(itemDetailUuid, senderDeviceToken))
            .validate()
            .responseData { response in
                switch response.result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    
    func map(listItemDetailResponse: BringListItemDetailResponse) -> BringListItemDetail {
        
        let itemDetail = BringListItemDetail(
            uuid: listItemDetailResponse.uuid!,
            itemKey: listItemDetailResponse.itemId!,
            listUuid: listItemDetailResponse.listUuid!,
            userIconItemId: listItemDetailResponse.userIconItemId!,
            userSectionId: listItemDetailResponse.userSectionId!,
            assignedTo: listItemDetailResponse.assignedTo!,
            remoteImageUrl: listItemDetailResponse.imageUrl!
        )
        
        return itemDetail
        
    }
    
    enum Router: URLRequestConvertible {
        
        case getAllBringListItemDetailsForList(String)
        case readBringListItemDetail(String)
        case searchBringListItemDetail(String, String)
        case createBringListItemDetail
        case updateBringListItemDetailImage(String)
        case updateBringListItemDetailUserIcon(String, String, String?)
        case updateBringListItemDetailUserSection(String, String, String?)
        case updateBringListItemDetailAssignedTo(String, String, String?)
        case removeBringListItemDetailImage(String, String?)
        case removeBringListItemDetailUserIcon(String, String?)
        case removeBringListItemDetailUserSection(String, String?)
        case removeBringListItemDetailAssignedTo(String, String?)
        case deleteBringListItemDetail(String, String?)
        
        
        var method: HTTPMethod {
            switch self {
            case .getAllBringListItemDetailsForList:
                return .get
            case .readBringListItemDetail:
                return .get
            case .searchBringListItemDetail:
                return .get
            case .createBringListItemDetail:
                return .post
            case .updateBringListItemDetailImage:
                return .put
            case .updateBringListItemDetailUserIcon:
                return .put
            case .updateBringListItemDetailUserSection:
                return .put
            case .updateBringListItemDetailAssignedTo:
                return .put
            case .removeBringListItemDetailImage:
                return .delete
            case .removeBringListItemDetailUserIcon:
                return .delete
            case .removeBringListItemDetailUserSection:
                return .delete
            case .removeBringListItemDetailAssignedTo:
                return .delete
            case .deleteBringListItemDetail:
                return .delete
            }
        }
        
        var path: String {
            switch self {
            case .getAllBringListItemDetailsForList(let listUuid):
                return "/bringlists/\(listUuid)/details"
            case .readBringListItemDetail(let uuid):
                return "/bringlistitemdetails/\(uuid)"
            case .searchBringListItemDetail(_,_):
                return "/bringlistitemdetails"
            case .createBringListItemDetail:
                return "/bringlistitemdetails"
            case .updateBringListItemDetailImage(let uuid):
                return "/bringlistitemdetails/\(uuid)/image"
            case .updateBringListItemDetailUserIcon(let uuid, _, _):
                return "/bringlistitemdetails/\(uuid)/usericon"
            case .updateBringListItemDetailUserSection(let uuid, _, _):
                return "/bringlistitemdetails/\(uuid)/usersection"
            case .updateBringListItemDetailAssignedTo(let uuid, _, _):
                return "/bringlistitemdetails/\(uuid)/assignedto"
            case .removeBringListItemDetailImage(let uuid, _):
                return "/bringlistitemdetails/\(uuid)/image"
            case .removeBringListItemDetailUserIcon(let uuid, _):
                return "/bringlistitemdetails/\(uuid)/usericon"
            case .removeBringListItemDetailUserSection(let uuid, _):
                return "/bringlistitemdetails/\(uuid)/usersection"
            case .removeBringListItemDetailAssignedTo(let uuid, _):
                return "/bringlistitemdetails/\(uuid)/assignedto"
            case .deleteBringListItemDetail(let uuid, _):
                return "/bringlistitemdetails/\(uuid)"
            }
        }
        
        // MARK: URLRequestConvertible
        
        func asURLRequest() throws -> URLRequest {
            let url = URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: url.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue
            
            let encoding = URLEncoding.default
            
            switch self {
            case .searchBringListItemDetail(let listUuid, let itemKey):
                return try encoding.encode(mutableURLRequest, with: ["listUuid": listUuid, "itemId": itemKey])
            case .updateBringListItemDetailUserSection(_, let sectionKey, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: ["userSectionId": sectionKey], sender: sender))
            case .updateBringListItemDetailUserIcon(_, let iconKey, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: ["userIconItemId": iconKey], sender: sender))
            case .updateBringListItemDetailAssignedTo(_, let assignedTo, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: ["assignedTo": assignedTo], sender: sender))
            case .removeBringListItemDetailImage(_, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: [:], sender: sender))
            case .removeBringListItemDetailUserIcon(_, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: [:], sender: sender))
            case .removeBringListItemDetailUserSection(_, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: [:], sender: sender))
            case .removeBringListItemDetailAssignedTo(_, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: [:], sender: sender))
            case .deleteBringListItemDetail(_, let sender):
                return try encoding.encode(mutableURLRequest, with: parametersWithSender(parameters: [:], sender: sender))
            default:
                
                return mutableURLRequest
            }
        }
        
        func parametersWithSender(parameters: [String: String], sender: String?) -> [String: String] {
            
            var newParameters = parameters
            
            if(sender != nil){
                newParameters["sender"] = sender
            }
            
            return newParameters
            
        }
        
    }
    
}

