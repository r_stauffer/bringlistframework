//
//  BringEnvironment.h
//  Bring
//
//  Created by Sandro Strebel on 30.07.14.
//
//

#import <Foundation/Foundation.h>

typedef enum BringEndpoint {
    BringEndpointCore,
    BringEndpointOffers,
    BringEndpointConnect,
    BringEndpointEventTracking,
    BringEndpointPrediction,
    BringEndpointPartners,
} BringEndpoint;

@interface BringEnvironment : NSObject {
    
}

+ (BringEnvironment * _Nonnull)sharedInstance;

@property (nonatomic, strong, readonly) NSDictionary * _Nonnull endpoint;
@property (readonly) BOOL logEnabled;
@property (readonly) BOOL uiTest;
@property (nonatomic, strong, readonly) NSString * _Nullable gantId;
@property (nonatomic, strong, readonly) NSString * _Nullable gantIdAdvertising;
@property (nonatomic, strong, readonly) NSString * _Nullable gantIdInsightsRepo;
@property (nonatomic, strong, readonly) NSString *  _Nullable appStoreId;
@property (nonatomic, strong, readonly) NSString  * _Nullable firebaseDynamicLinkDomain;
@property (nonatomic, strong, readonly) NSString  * _Nullable androidPackageName;
@property (nonatomic, strong, readonly) NSString  * _Nullable firebaseWebApiKey;
@property (nonatomic, readonly) int remoteConfigExpirationDuration;
@property (nonatomic, strong, readonly) NSString  * _Nullable appsFlyerApiKey;

- (NSString*_Nonnull)getEndpoint:(BringEndpoint)endpoint;

@end
