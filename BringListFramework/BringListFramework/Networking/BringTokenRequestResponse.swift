//
//  BringUserLoginResponse.swift
//  Bring
//
//  Created by Sascha Thöni on 29.12.16.
//
//

import UIKit
import ObjectMapper

class BringTokenRequestResponse: Mappable {

    var accessToken : String?
    var refreshToken : String?
    
    var email: String?
    var name: String?
    var photoPath: String?
    var publicUuid: String?
    var privateUuid: String?
    var currenListUuid: String?
    
    required public init?(map: Map){
        
    }
    
    open func mapping(map: Map) {
        accessToken <- map["access_token"]
        refreshToken <- map["refresh_token"]
        
        email <- map["email"]
        name <- map["name"]
        photoPath <- map["photoPath"]
        publicUuid <- map["publicUuid"]
        privateUuid <- map["uuid"]
        currenListUuid <- map["bringListUUID"]
    }
}
