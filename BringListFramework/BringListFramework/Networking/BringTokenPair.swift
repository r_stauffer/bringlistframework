//
//  BringUserLoginData.swift
//  Bring
//
//  Created by Sascha Thöni on 29.12.16.
//
//

import UIKit

// @TODO Probably this only needs the properties refreshToken and accessToken
public class BringTokenPair: NSObject {
    
    public var refreshToken = ""
    public var accessToken = ""
    
    public var email = ""
    public var name = ""
    public var photoPath = ""
    public var publicUuid = ""
    public var privateUuid = ""
    public var currenListUuid: String?
    
    public var statusCode: Int = 0
    
    public func isStatusCreated() -> Bool {
        return self.statusCode == 201
    }
    
}
