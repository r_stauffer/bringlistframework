//
//  BringAlamofireTokenRequester.swift
//  BringListFramework
//
//  Created by Renato Stauffer on 26.12.19.
//

import Foundation
import Alamofire

public protocol TokenRequester {
    func requestTokens(forPrivateUserUuid uuid : String, onSuccess: @escaping (BringTokenPair) -> (), onFailure: @escaping () -> ())
}

public class BringAlamofireTokenRequester: TokenRequester {
    
    public init() {}
    
    enum Router: URLRequestConvertible {
        
        case requestTokens(String)
        
        
        var method: HTTPMethod {
            switch self {
                case .requestTokens:
                    return .post
            }
        }
        
        var path: String {
            switch self {
            case .requestTokens(_):
                return "v2/bringauth/migrate"
            }
        }
        
        // MARK: URLRequestConvertible
        
        func asURLRequest() throws -> URLRequest {
            let URL = Foundation.URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue
            
            switch self {
            case .requestTokens(let uuid):
                var parameters = [String:Any]()
                parameters["userUuid"] = uuid
                
                try mutableURLRequest = URLEncoding.default.encode(mutableURLRequest, with: parameters)
                
                return mutableURLRequest
            }
        }
    }
    
    public func requestTokens(forPrivateUserUuid uuid : String, onSuccess: @escaping (BringTokenPair) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.requestTokens(uuid)).validate().responseObject {(response:DataResponse<BringTokenRequestResponse>) in
            if response.result.isSuccess {
                guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                    onFailure()
                    return
                }
                
                onSuccess(loginData)
            } else {
                onFailure()
            }
        }
    }
    
    private func map(authResponse : BringTokenRequestResponse?, status: Int?) -> BringTokenPair? {
        
        guard let response = authResponse else {
            return nil
        }
        
        let loginData = BringTokenPair()
        
        loginData.accessToken = response.accessToken != nil ? response.accessToken! : ""
        loginData.refreshToken = response.refreshToken != nil ? response.refreshToken! : ""
        
        loginData.email = response.email != nil ? response.email! : ""
        loginData.name = response.name != nil ? response.name! : ""
        loginData.photoPath = response.photoPath != nil ? response.photoPath! : ""
        loginData.publicUuid = response.publicUuid != nil ? response.publicUuid! : ""
        loginData.privateUuid = response.privateUuid != nil ? response.privateUuid! : ""
        loginData.currenListUuid = response.currenListUuid
        
        loginData.statusCode = status ?? 0
        
        return loginData
    }
}
