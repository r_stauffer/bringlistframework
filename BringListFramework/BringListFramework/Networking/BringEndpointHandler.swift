//
//  BringEndpointHandler.swift
//  BringExtensionKit
//
//  Created by Beat on 20.02.19.
//

import Foundation

public protocol BringEndpointHandler: class {
    func adapt(_ request: inout URLRequest)
    func refreshAccessToken(url: URL, onSuccess: @escaping ()->(), onFailure:@escaping ()->())
}
