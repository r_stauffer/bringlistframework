//
//  BringAlamofireAuthHandler.swift
//  Bring
//
//  Created by Sascha Thöni on 22.12.16.
//
//

import UIKit
import Alamofire
import KeychainSwift

class BringAlamofireInterceptor: RequestAdapter, RequestRetrier {
    
    private let ACCESS_TOKEN_KEY = "accessToken";
    private let REFRESH_TOKEN_KEY = "refreshToken";
    private var baseURLString: String
    private var allowedURLs: [String]
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    private var accessToken: String
    private var refreshToken: String
    private var currentLocation : BringGeoLocation?
    weak var delegate: ListContentSettingsProvider?
    private var connect: (endpoint: String, handler: BringEndpointHandler)?
    
    private let keychain = KeychainSwift()
    private typealias RefreshCompletion = (_ state: RefreshAccessTokenResultState, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private lazy var tokenRequester = {
        return BringAlamofireTokenRequester()
    }()
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        return SessionManager(configuration: configuration)
    }()
    
   
    // MARK: - Initialization
    public init(baseURLString: String, allowedURLs: [String]) {
        self.baseURLString = baseURLString
        self.allowedURLs = allowedURLs
        
        self.keychain.synchronizable = false
        self.accessToken = keychain.get(ACCESS_TOKEN_KEY) ?? ""
        self.refreshToken = keychain.get(REFRESH_TOKEN_KEY) ?? ""
    }
    
    public func setLocation(location : BringGeoLocation) {
        self.currentLocation = location
    }
    
    public func setAccessToken(token : String) {
        self.accessToken = token
        if !token.isEmpty {
            self.keychain.set(token, forKey: ACCESS_TOKEN_KEY)
        } else {
            self.keychain.delete(ACCESS_TOKEN_KEY)
        }
    }
    
    public func setRefreshToken(token : String) {
        self.refreshToken = token
        if !token.isEmpty {
            self.keychain.set(token, forKey: REFRESH_TOKEN_KEY)
        } else {
            self.keychain.delete(REFRESH_TOKEN_KEY)
        }
    }
    
    public func setConnectHandler(_ handler: BringEndpointHandler?, endpoint: String? = nil) {
        
        if let handler = handler, let endpoint = endpoint {
            self.connect = (handler: handler, endpoint: endpoint)
        } else {
            self.connect = nil
        }
    }
    
    // MARK: - RequestAdapter
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var mutatingRequest = urlRequest
        
        if !accessToken.isEmpty, let url = urlRequest.url?.absoluteString  {
            
            for allowedURL in self.allowedURLs {
                
                if url.hasPrefix(allowedURL) {
                    mutatingRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
                }
            }
        }
        
        if let location = self.currentLocation {
            mutatingRequest.setValue(String(location.latitude), forHTTPHeaderField: "X-BRING-LATITUDE")
            mutatingRequest.setValue(String(location.longitude), forHTTPHeaderField: "X-BRING-LONGITUDE")
            if let accuracy = location.accuracy {
                mutatingRequest.setValue(String(accuracy), forHTTPHeaderField: "X-BRING-ACCURACY")
            }
            
            if let altitude = location.altitude {
                mutatingRequest.setValue(String(altitude), forHTTPHeaderField: "X-BRING-ALTITUDE")
            }
        }
        
        if let connect = self.connect, let url = urlRequest.url?.absoluteString, url.hasPrefix(connect.endpoint) {
            connect.handler.adapt(&mutatingRequest)
        }
        
        return mutatingRequest
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        // Info: Could only happen when refreshing the accessToken
        guard !isRequestFailedDueToInvalidRefreshToken(request: request) else {
            reset()
            completion(false, 0.0)
            return
        }
        
        if isRequestFailedDueToInvalidAccessToken(request: request) {
            // if there is no refresh token, try to fetch a new refresh token (like on migration)
            guard !self.refreshToken.isEmpty else {
                self.requestRefreshToken(onSuccess: {
                    completion(true, 0.0)
                }, onFailure: {
                    self.reset()
                    completion(false, 0.0)
                })
                return
            }
            
            requestsToRetry.append(completion)

            if !isRefreshing {
                self.refreshAccessTokens(completion: { [weak self] state, accessToken, refreshToken in
                    
                    guard let strongSelf = self else { return }
                    
                    if state == .notAuthorized {
                        strongSelf.reset()
                        completion(false, 0.0)
                        return
                    }
                    
                    if let refreshToken = refreshToken, refreshToken.isEmpty {
                        strongSelf.setRefreshToken(token: refreshToken)
                    }
                    
                    if let accessToken = accessToken, !accessToken.isEmpty {
                        strongSelf.setAccessToken(token: accessToken)
                    }
                    
                    let shouldRetry = state == .success
                    strongSelf.requestsToRetry.forEach { $0(shouldRetry, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                })
            }
        } else if isRequestFailedDueToInvalidPartnerToken(request: request) {
            
            self.refreshPartnerToken(request, onSuccess: {
                completion(true, 0.0)
            }, onFailure: {
                completion(false, 0.0)
            })
            
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshAccessTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else {
            return
        }
        
        guard !refreshToken.isEmpty else {
            completion(.failed, nil, nil)
            return
        }
        
        isRefreshing = true
        
        let urlString = "\(baseURLString)/v2/bringauth/token"
        let parameters: [String: Any] = ["refresh_token": refreshToken]
        
        Log.i("Info: Refreshing access token")
        
        sessionManager.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON {
        response in
            if response.result.isSuccess, let json = response.result.value as? [String: Any], let accessToken = json["access_token"] as? String, let refreshToken = json["refresh_token"] as? String {
                completion(.success, accessToken, refreshToken)
            } else {
                if response.response?.statusCode == 401 {
                    print ("Refresh accessToken failed due to error 401")
                    completion(.notAuthorized, nil, nil)
                } else {
                    completion(.failed, nil, nil)
                }
            }
            self.isRefreshing = false
        }
    }
    
    private func isRequestFailedDueToInvalidRefreshToken(request : Request) -> Bool {
        
        let jsonString = getJSONStringResponseFromRequest(request: request)
        
        guard !jsonString.isEmpty else {
            return false
        }
        
        let response = BringTokenErrorReponse(JSONString: jsonString)

        return response?.errorCode == BringTokenErrorReponse.AuthErrorCode.INVALID_REFRESH_TOKEN
    }
    
    
    private func isRequestFailedDueToInvalidAccessToken(request : Request) -> Bool {
       
        let jsonString = getJSONStringResponseFromRequest(request: request)
        
        guard !jsonString.isEmpty else {
            return false
        }
        
        let response = BringTokenErrorReponse(JSONString: jsonString)
        
        return response?.errorCode == BringTokenErrorReponse.AuthErrorCode.INVALID_ACCESS_TOKEN
    }
    
    private func isRequestFailedDueToInvalidPartnerToken(request : Request) -> Bool {
        
        let jsonString = getJSONStringResponseFromRequest(request: request)
        
        guard !jsonString.isEmpty else {
            return false
        }
        
        let response = BringTokenErrorReponse(JSONString: jsonString)
        
        return response?.errorCode == BringTokenErrorReponse.AuthErrorCode.INVALID_PARTNER_TOKEN
    }
    
    
    private func getJSONStringResponseFromRequest(request : Request) -> String {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 || response.statusCode == 403 else {
            return ""
        }
        
        guard let data = request.delegate.data else {
            return ""
        }
        
        guard let jsonString = String.init(data: data, encoding: .utf8) else {
            return ""
        }
        
        return jsonString
    }

    private func requestRefreshToken(onSuccess: @escaping ()->(), onFailure:@escaping ()->()) {
        guard let privateUserUuid = delegate?.userUuid() else {
            onFailure()
            return
        }
        
        tokenRequester.requestTokens(forPrivateUserUuid: privateUserUuid, onSuccess: { (loginData) in
            BringAlamofireManager.sharedInstance.setupTokens(refreshToken: loginData.refreshToken, accessToken: loginData.accessToken)
            onSuccess()
        }) {
            onFailure()
        }
    }
    
    private func refreshPartnerToken(_ resource: Request, onSuccess: @escaping ()->(), onFailure:@escaping ()->()) {
        
        guard let url = resource.request?.url  else {
            onFailure()
            return
        }
        
        if let connect = self.connect, url.absoluteString.hasPrefix(connect.endpoint) {
            connect.handler.refreshAccessToken(url: url, onSuccess: onSuccess, onFailure: onFailure)
        } else {
            onFailure()
        }
    }
    
    public func reset() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ACCESS_TOKEN_REFRESH_FAILED"), object: self, userInfo: [:])
        self.setAccessToken(token: "")
        self.setRefreshToken(token: "")
    }
    
    private enum RefreshAccessTokenResultState  {
        case success
        case failed
        case notAuthorized
    }
}
