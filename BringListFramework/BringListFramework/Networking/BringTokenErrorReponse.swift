//
//  BringAuthErrorReponse.swift
//  Bring
//
//  Created by Sascha Thöni on 11.05.17.
//
//

import UIKit
import ObjectMapper

class BringTokenErrorReponse: Mappable {
    
    enum AuthErrorCode : Int {
        case UNAUTHORIZED = 100
        case LOGIN_FAILED = 200
        case INVALID_ACCESS_TOKEN = 201
        case INVALID_REFRESH_TOKEN = 202
        case INVALID_PARTNER_TOKEN = 203
        case INVALID_API_KEY = 300
    }
    
    var message : String = ""
    var errorCode : AuthErrorCode?
    
    required public init?(map: Map){
        
    }
    
    open func mapping(map: Map) {
        message <- map["message"]
        var errorCodeNumber : Int = 0
        errorCodeNumber <- map["errorcode"]
        errorCode = AuthErrorCode.init(rawValue: errorCodeNumber)
    }
}
