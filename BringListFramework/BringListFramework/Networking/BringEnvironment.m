//
//  BringEnvironment.m
//  Bring
//
//  Created by Sandro Strebel on 30.07.14.
//
//

#import "BringEnvironment.h"

@implementation BringEnvironment

+ (BringEnvironment*)sharedInstance {
    static BringEnvironment *environment = nil;
    if (environment == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            environment = [[BringEnvironment alloc] init];
        });
    }
    
    return environment;
}

- (id)init {
    
    if ((self = [super init])) {
        
        #if DEBUG
        NSString *configuration = @"Debug";
        #else
        NSString *configuration = @"Release";
        #endif
        
        NSBundle* bundle = [NSBundle bundleForClass:[self class]];
        NSString* envsPListPath = [bundle pathForResource:@"Environments" ofType:@"plist"];
        NSDictionary* environments = [[NSDictionary alloc] initWithContentsOfFile:envsPListPath];
        NSDictionary* environment = [environments objectForKey:configuration];
        
        _endpoint = [environment objectForKey:@"endpoint"];
        _gantId = [environment valueForKey:@"gantId"];
        _gantIdAdvertising = [environment valueForKey:@"gantIdAdvertising"];
        _logEnabled = [[environment valueForKey:@"logEnabled"] boolValue];
        _uiTest = [[[NSProcessInfo processInfo] arguments] containsObject:@"UI_TEST"];
        _gantIdInsightsRepo =  [environment valueForKey:@"gantIdInsightsRepo"];
        _appStoreId =  [environment valueForKey:@"appStoreId"];
        _firebaseDynamicLinkDomain =  [environment valueForKey:@"firebaseDynamicLinkDomain"];
        _androidPackageName =  [environment valueForKey:@"androidPackageName"];
        _firebaseWebApiKey = [environment valueForKey:@"firebaseWebApiKey"];
        _remoteConfigExpirationDuration = [(NSString*) [environment valueForKey:@"remoteConfigExpirationDurationInSeconds"] intValue];
        _appsFlyerApiKey = [environment valueForKey:@"appsFlyerApiKey"];
        
        //DEBUG overrides
        //_endpoint = @"http://localhost:8080/rest";
        //_endpoint = @"http://192.168.1.8:8080/rest";
    }
    
    return self;
}

- (NSString*)getEndpoint:(BringEndpoint)endpoint {
    switch (endpoint) {
        case BringEndpointCore:
            return self.endpoint[@"core"];
        case BringEndpointOffers:
        return self.endpoint[@"offers"];
        case BringEndpointConnect:
            return self.endpoint[@"connect"];
        case BringEndpointEventTracking:
            return self.endpoint[@"tracking"];
        case BringEndpointPrediction:
            return self.endpoint[@"prediction"];
        case BringEndpointPartners:
            return self.endpoint[@"partners"];
    }
}

@end
