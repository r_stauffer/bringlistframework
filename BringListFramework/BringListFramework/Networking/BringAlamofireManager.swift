//
//  BringAlamofireManager.swift
//  Bring
//
//  Created by Sandro Strebel on 16/06/16.
//
//

import Foundation
import Alamofire

@objc
public class BringAlamofireManager : NSObject {
    
    @objc
    public static let sharedInstance = BringAlamofireManager()
    
    private var advertisingId: String?
    private var advertisingTrackingEnabled: Bool?
    
    private var internalManager: SessionManager?
    private var interceptor: BringAlamofireInterceptor?
    
    public weak var delegate: ListContentSettingsProvider? {
        didSet {
            interceptor?.delegate = self.delegate
        }
    }
    
    public var defaultManager: SessionManager! {
        get {
            if(internalManager != nil){
                return internalManager
            }else {
                setup(advertisingId: nil, userUuid: nil, advertisingTrackingEnabled: nil)
                return internalManager;
            }
        }
    }
    
    public override init() {
        super.init()
        
        let environment = BringEnvironment.sharedInstance()
        
        let endpointCore = environment.getEndpoint(BringEndpointCore)
        let endpointOffers = environment.getEndpoint(BringEndpointOffers)
        let endpointConnect = environment.getEndpoint(BringEndpointConnect)
        let endpointTracking = environment.getEndpoint(BringEndpointEventTracking)
        let endpointPrediction = environment.getEndpoint(BringEndpointPrediction)
        let endpointPartners = environment.getEndpoint(BringEndpointPartners)
        
        self.interceptor = BringAlamofireInterceptor(baseURLString: endpointCore, allowedURLs: [endpointCore, endpointOffers, endpointConnect, endpointTracking, endpointPrediction, endpointPartners])
    }
    
    public func setup() {
        self.setup(userUuid: BringListContentSettingsProvider.shared.userUuid())
    }
    
    private func setup(userUuid : String?) {
        setup(advertisingId: self.advertisingId, userUuid:  userUuid, advertisingTrackingEnabled: self.advertisingTrackingEnabled)
    }
    
    @objc
    public func setup(advertisingId: String?, advertisingTrackingEnabled: Bool) {
        setup(advertisingId: advertisingId, userUuid: delegate?.userUuid(), advertisingTrackingEnabled: advertisingTrackingEnabled)
    }
    
    public func setup(advertisingId: String?, userUuid: String?, advertisingTrackingEnabled: Bool?){
        
        var defaultHeaders = Alamofire.SessionManager.default.session.configuration.httpAdditionalHeaders ?? [:]
        defaultHeaders["X-BRING-API-KEY"] = "cof4Nc6D8saplXjE3h3HXqHH8m7VU2i1Gs0g85Sp"
        defaultHeaders["X-BRING-COUNTRY"] = BringLocaleHelper.sharedInstance.getCountryCode()
        defaultHeaders["Accept-Language"] = self.getAcceptLanguage()
        defaultHeaders["X-BRING-CLIENT-TIMEZONE"] = TimeZone.current.identifier

        if let adId = advertisingId {
            self.advertisingId = adId
            defaultHeaders["X-BRING-USER-AD-ID"] = adId
        }
        
        if let adTrackingEnabled = advertisingTrackingEnabled {
            self.advertisingTrackingEnabled = adTrackingEnabled
            defaultHeaders["X-BRING-USER-AD-TRACKING-ENABLED"] = adTrackingEnabled ? "true" : "false";
        }
        
        if let uuid = userUuid, !uuid.isEmpty {
            defaultHeaders["X-BRING-USER-UUID"] = uuid
        }else if(delegate?.userUuid() != nil){
            defaultHeaders["X-BRING-USER-UUID"] = delegate?.userUuid()
        }
        
        if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString"){
            defaultHeaders["X-BRING-VERSION"] = version as! String
        }

        #if os(iOS)
            defaultHeaders["X-BRING-CLIENT"] = "iOS"
        #elseif os(watchOS)
            defaultHeaders["X-BRING-CLIENT"] = "watchOS"
        #endif
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = defaultHeaders
        configuration.urlCache?.diskCapacity = 75000000
        
        self.internalManager = Alamofire.SessionManager(configuration: configuration)
        
        if let interceptor = self.interceptor {
            self.internalManager?.adapter = interceptor
            self.internalManager?.retrier = interceptor
        } else {
            assert(false, "Interceptor not available.")
        }
    }
    
    public func getAcceptLanguage() -> String {
        return "\(BringLocaleHelper.sharedInstance.getLanguageCodeFromLocale())-\(BringLocaleHelper.sharedInstance.getCountryCodeFromLocale() ?? BringLocaleHelper.FALLBACK_COUNTRY_CODE)"
    }
    
    public func setupTokens(refreshToken : String, accessToken : String) {
        setRefreshToken(token: refreshToken)
        setAccessToken(token: accessToken)
    }
    
    @objc 
    public func setupNewUser(userUuid : String) {
        setup(advertisingId: self.advertisingId, userUuid: userUuid, advertisingTrackingEnabled: self.advertisingTrackingEnabled)
    }
    
    open func endpoint(_ resource: String) -> String {
        return BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore) + resource;
    }
    
    public func setLocation(location : BringGeoLocation) {
        self.interceptor?.setLocation(location: location)
    }
    
    func setAccessToken(token : String) {
        if !token.isEmpty {
            self.interceptor?.setAccessToken(token: token)
        }
    }

    private func setRefreshToken(token : String) {
        if !token.isEmpty {
            self.interceptor?.setRefreshToken(token: token)
        }
    }
    
    public func setConnectHandler(_ handler: BringEndpointHandler?) {
        
        if let handler = handler {
            let endpoint = BringEnvironment.sharedInstance().getEndpoint(BringEndpointConnect)
            self.interceptor?.setConnectHandler(handler, endpoint: endpoint)
        } else {
            self.interceptor?.setConnectHandler(nil)
        }
    }
    
    @objc
    public func reset() {
        self.interceptor?.reset()
        self.advertisingId = ""
    }
    
    public func clearCache() {
        URLCache.shared.removeAllCachedResponses()
    }
}
