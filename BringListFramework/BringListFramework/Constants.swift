//
//  Constants.swift
//  Bring
//
//  Created by Renato Stauffer on 29.08.19.
//

import Foundation

struct Constants {
    struct Project {
        #if os(iOS)
        static let bundleIdentifier = "ch.publisheria.bring.BringListFrameworkiOS"
        #elseif os(watchOS)
        static let bundleIdentifier = "ch.publisheria.bring.BringSharedwatchOS"
        #endif
    }
}
