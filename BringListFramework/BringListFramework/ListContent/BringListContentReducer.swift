//
//  BringListContentReducer.swift
//  Bring
//
//  Created by Renato Stauffer on 22.02.19.
//

import Foundation

// We do a lot of copy() ! <<Type>>. So it makes snese to disable this here.
// swiftlint:disable force_cast

public class BringListContentReducer {
    
    public init() {}
    
    public func reduce(_ listContent: BringListContent, action: BringListContentAction) -> BringListContent {
        let listContent = listContent.copy() as! BringListContent
        switch action {
        case let action as BringListContentSelectAction:
            return select(action.item, currentListContent: listContent)
        case let action as BringListContentUpdateAction:
            return updateListContent(newListstate: action.currentListState, userItemsToAdd: action.userItemsToAdd, listContent: listContent, syncDate: action.syncStartDate, sectionOrder: action.sectionOrder, translations: action.translations)
        case let action as BringListContentMoveCatalogItemAction:
            return moveCatalogItems(withItemId: action.itemToMove.itemId, toSectionWithSectionId: action.sectionToMoveTo.sectionId, inListContent: listContent)
        case let action as BringListContentUpdateRecommendedAction:
            return updateRecommendedSection(sectionName: action.sectionName, items: action.items, listContent: listContent)
        case let action as BringListContentReorderSectionAction:
            return reorderSections(listContent: listContent, sectionOrder: action.sectionOrder)
        case let action as BringListContentUpdateSpecificationAction:
            return updateBringItemSpecification(forUniqueIdentifier: action.uniqueIdentifier, specification: action.specification, listContent: listContent)
        case _ as BringListContentResetNewItemFlagsAction:
            return resetNewItemFlags(listContent: listContent)
        case let action as BringListContentAddItemAction:
            return add(action.item, toSection: action.sectionId, listContent: listContent)
        case let action as BringListContentRemoveItemAction:
            return removeItem(withItemId: action.itemId, listContent: listContent)
        case let action as BringListContentCatalogChangeAction:
            return enrich(listContent, enrichment: action.catalogChange)
        case let action as BringListContentRemoveMultipleItemsAction:
            return remove(action.items, from: action.section, listContent: listContent)
        case let action as BringListContentAddItemToPurchaseAction:
            return add(action.item, toPurchaseOf: listContent)
        default:
            assertionFailure("An action is not handled yet!")
            return listContent
        }
    }
    
    // MARK: BringListContentMoveCatalogItemAction
    
    private func moveCatalogItems(withItemId itemId: String, toSectionWithSectionId sectionId: String, inListContent listContent: BringListContent) -> BringListContent {
        let items = listContent.itemsFromDefaultSections(forItemId: itemId)
        remove(items, withItemId: itemId, fromOldSectionInListContent: listContent)
        add(items, toSectionWithSectionId: sectionId, inListContent: listContent)
        sortToBePruchasedSection(of: listContent)
        return listContent
    }
    
    private func remove(_ items: [BringItem], withItemId itemId: String, fromOldSectionInListContent listContent: BringListContent) {
        guard let oldSection = self.section(ofItemWithItemId: itemId, inListContent: listContent) else {
            return
        }
        
        for item in items {
            oldSection.remove(item: item)
        }
    }
    
    private func add(_ items: [BringItem], toSectionWithSectionId sectionId: String, inListContent listContent: BringListContent) {
        guard let newSection = self.section(by: sectionId, ofListContent: listContent) else {
            return
        }
        
        for item in items {
            item.assignUuidIfNeeded()
            newSection.add(item: item)
        }
        
        newSection.sort()
    }
    
    private func section(ofItemWithItemId itemId: String, inListContent listContent: BringListContent) -> BringSection? {
        return listContent.getSections().filter { (section) -> Bool in
            for item in section.items where item.itemId == itemId && section.isDefaultCatalogSection {
                return true
            }
            return false
            }.first
    }
    
    // MARK: BringListContentAddItemToPurchaseAction
    
    private func add(_ item: BringItem, toPurchaseOf listContent: BringListContent) -> BringListContent {
        assert(item.uuid != nil, "When an item gets added to purchase it should always have a uuid.")
        let newItem = item.copy() as! BringItem
        let currentSectionOrder = listContent.getSectionsCopy().map { $0.sectionId }
        
        // 1. Add item to purchase
        let purchaseSection = listContent.getToBePurchasedSection()
        let newPurchaseSection = purchaseSection.add(newItem)
        
        // 2. Create new catalog content
        let newCatalog = createNewCatalog(with: newItem, fromCatalogOf: listContent)
        let sortedCatalog = newCatalog.sorted { (first, second) -> Bool in
            return (currentSectionOrder.index(of: first.sectionId) ?? 0) < (currentSectionOrder.index(of: second.sectionId) ?? 1)
        }
        
        // 3. Put together new list content
        let newListContent = listContent.createNewModel(
            catalogSections: sortedCatalog,
            toBePurchasedSection: newPurchaseSection
        )
        
        sortToBePruchasedSection(of: newListContent)
        
        return newListContent
    }
    
    private func createNewCatalog(with item: BringItem, fromCatalogOf listContent: BringListContent) -> [BringSection] {
        
        var newItemSection: BringSection
        var newCatalogSections = listContent.getSectionsCopy()
        
        if let section = self.section(ofItemWithItemId: item.itemId, inListContent: listContent) {
            
            newItemSection = section.createVanillaSection(withNewItem: item)
            
            newCatalogSections.removeAll { (section) -> Bool in
                section.sectionId == newItemSection.sectionId
            }
            
        } else {
            // The item needs is not in the catalog. So we move it to the "Own articles" section.
            guard let ownArticlesSection = listContent.getOwnArticlesSectionCopy() else {
                return listContent.getSectionsCopy()
            }
            
            newItemSection = ownArticlesSection.createVanillaSection(withNewItem: item)
            
            newCatalogSections.removeAll { (section) -> Bool in
                section.sectionId == BringCatalogProvider.USER_SECTION_KEY
            }
        }
        
        newCatalogSections.append(newItemSection)

        return newCatalogSections
    }
    
    // MARK: BringListContentSelectAction
    
    private func select(_ item: BringItem, currentListContent listContent: BringListContent) -> BringListContent {
        assert(item.uuid != nil, "When an item gets selected it should always have a uuid.")
        let newItem = item.copy() as! BringItem
        updateCurrentModelIfNeeded(newItem, inListContent: listContent)
        return update(listContent: listContent, withItem: newItem)
    }
    
    /// Updates an item in the `listContent` with `newItem` or inserts `newItem` at the correct index in the `listContent` if needed.
    ///
    /// - Parameters:
    ///   - newItem: The item to update or insert
    ///   - listContent: The list content to uddate
    private func updateCurrentModelIfNeeded(_ newItem: BringItem, inListContent listContent: BringListContent) {
        let catalogSections = listContent.getSections()

        for section in catalogSections {
            var needsNewItem = false
            if let index = (section.items.firstIndex { (sectionItem) -> Bool in
                if newItem.itemId == sectionItem.itemId && sectionItem.uuid == nil {
                    // The item has never been selected before, so we need to give it a uuid
                    return true
                } else if newItem.itemId == sectionItem.itemId && sectionItem.uuid != nil {
                    // The item has been selected before.
                    if sectionItem.uuid != newItem.uuid && !section.items.contains(newItem) {
                        // A new item needs to be created, since this does not exist in the list content yet.
                        needsNewItem = true
                        return true
                    }
                }
                return false
            }) {
                if needsNewItem {
                    section.insert(items: [newItem], at: index)
                } else {
                    section.replace(itemAtIndex: index, withItem: newItem)
                }
            }
        }
    }
    
    private func update(listContent: BringListContent, withItem item: BringItem) -> BringListContent {
        guard let itemToSelect = listContent.itemFor(uuid: item.uuid, orItemId: item.itemId) else {
            Log.w("Item you want to select does not exist.")
            return listContent
        }
        
        itemToSelect.specification = item.specification
        itemToSelect.newItem = false
        itemToSelect.lastModification = Date()
        
        assert(itemToSelect.uuid != nil, "At this point, the item always needs a UUID. Otherwise the consistantcy of the list content is not given anymore.")
        listContent.isInToBePurchasedSection(item: itemToSelect) ? move(itemToSelect, toRecentlyInListContent: listContent) : move(itemToSelect, toToBePurchasedInListContent: listContent)
        
        let recommendedSection = listContent.getRecommendedSectionByReference()
        if listContent.isInitialRecommendedItem(item: itemToSelect) {
            if listContent.isRecommendedItem(item: itemToSelect) {
                recommendedSection.removeItems(withItemId: itemToSelect.itemId)
            } else {
                if let indexToAddItem = getIndexToInsert(itemToSelect, inListContent: listContent) {
                    recommendedSection.add(item: itemToSelect, at: indexToAddItem)
                }
            }
        }
        
        sortToBePruchasedSection(of: listContent)
        
        return listContent
    }
    
    private func getIndexToInsert(_ item: BringItem, inListContent listContent: BringListContent) -> Int? {
        guard let indexOfItemToAdd = listContent.getIndex(ofRecommendedItem: item) else { return nil }
        
        let newRecommendedSectionItems = listContent.getRecommendedSection().items
        if newRecommendedSectionItems.count == 0 {
            return 0
        }
        
        for (index, recommendedItem) in newRecommendedSectionItems.enumerated() {
            if let indexOfCurrentItem = listContent.getIndex(ofRecommendedItem: recommendedItem) {
                if indexOfItemToAdd < indexOfCurrentItem {
                    return index
                }
            }
        }
        
        return newRecommendedSectionItems.count
    }
    
    // MARK: BringListContentUpdateAction
    
    private func updateListContent(newListstate: BringListState, userItemsToAdd: [BringItem], listContent: BringListContent, syncDate: Date, sectionOrder: [String], translations: [String: String]) -> BringListContent {
        return update(listContent, remoteListState: newListstate, updateState: newListstate.status, syncDate: syncDate, sectionOrder: sectionOrder, translations: translations)
    }
    
    private func update(_ listContent: BringListContent, remoteListState: BringListState, updateState: String, syncDate: Date, sectionOrder: [String], translations: [String: String]) -> BringListContent {
        let mergedListState = self.createListStateFrom(listContent: listContent, remoteListState: remoteListState, syncDate: syncDate)
        var newListContent = updateToBePurchasedItems(inListContent: listContent, withNewToBePurchasedItems: mergedListState.purchase, translations: translations)
        newListContent = updateRecentlyItems(inListContent: newListContent, withNewRecentlyItems: mergedListState.recently, translations: translations)
        cleanupCatalog(of: newListContent, purchase: mergedListState.purchase, recently: mergedListState.recently)
        newListContent.listStatus = self.getListStatus(updateState, listUuid: newListContent.listUuid)
        newListContent.didFetchListContentForCurrentList = true
        return reorderSections(listContent: newListContent, sectionOrder: sectionOrder)
    }
    
    private func createListStateFrom(listContent: BringListContent, remoteListState: BringListState, syncDate: Date) -> BringListState {
        let purchase = listContent.getToBePurchasedSection().items.compactMap { item -> BringListStateItem? in
            
            guard let modificationDate = item.lastModification else {
                assertionFailure("We should have a modification date")
                return nil
            }
            
            guard let uuid = item.uuid else {
                assertionFailure("We should have a uuid")
                return nil
            }
            
            return BringListStateItem(uuid: uuid, specification: item.specification, itemId: item.itemId, modificationDate: modificationDate)
        }
        
        let recently = listContent.getRecentlyPurchasedSection().items.compactMap { item -> BringListStateItem? in
            guard let modificationDate = item.lastModification else {
                return nil
            }
            
            guard let uuid = item.uuid else {
                return nil
            }
            
            return BringListStateItem(uuid: uuid, specification: item.specification, itemId: item.itemId, modificationDate: modificationDate)
        }
        
        let merger = BringDefaultListStateMerger(syncStartDate: syncDate)
        
        let currentListState = BringListState(purchase: purchase, recently: recently, status: listContent.listStatus.rawValue, uuid: listContent.listUuid)
        
        let mergedState =  merger.merge(remoteListState, withOther: currentListState)
        return mergedState
    }
    
    /// Removes items from the catalog that are already in the to-be-purchased or recently section from the given `listContent`
    ///
    /// - Parameters:
    ///   - listContent: The list content to update
    ///   - purchase: The purchase section
    ///   - recently: The recently section
    private func cleanupCatalog(of listContent: BringListContent, purchase: [BringListStateItem], recently: [BringListStateItem]) {
        let items = purchase + recently
        for section in listContent.getSections() {
            for item in items {
                let foundItems = section.items(withItemId: item.itemId)
                if foundItems.count > 1 {
                    section.removeItems {
                        ($0.uuid == nil && $0.itemId == item.itemId)
                    }
                }
            }
        }
    }
    
    private func map(_ listStateItem: BringListStateItem, toBringItemWithName name: String, isUserItem: Bool, isNewItem: Bool) -> BringItem {
        let item =  BringItem(
            uuid: listStateItem.uuid,
            itemId: listStateItem.itemId,
            name: name, specification:
            listStateItem.specification,
            userItem: isUserItem,
            newItem: isNewItem
        )
        
        item.lastModification = listStateItem.modificationDate
        return item
    }
    
    private func updateToBePurchasedItems(inListContent listContent: BringListContent, withNewToBePurchasedItems purchase: [BringListStateItem], translations: [String: String]) -> BringListContent {
        
        let mappedItems = purchase.map { item -> BringItem in
            
            let name: String
            
            let wasItemNewBefore = listContent.getToBePurchasedSection().item(byUuid: item.uuid)?.newItem ?? false
            
            let isNewItem = listContent.didFetchListContentForCurrentList && (!listContent.isInToBePurchasedSection(uniqueIdentifier: item.uuid) || wasItemNewBefore)
            if translations[item.itemId] != nil {
                name = translations[item.itemId] ?? ""
            } else {
                name = BringLocalizationSystem.shared.localizedString(forKey: item.itemId)
            }
            
            return self.map(item, toBringItemWithName: name, isUserItem: false, isNewItem: isNewItem)
        }
        
        let newListContent = listContent.createNewModel(toBePurchasedSection: BringListContent.createToBePurchasedSection(items: mappedItems))
        
        for state in purchase {
            if let item = newListContent.item(for: state.uuid) {
                item.specification = state.specification
                
                // remove item from recommendSection if its in purchase
                let recommendedSection = newListContent.getRecommendedSection()
                for recommendedItem in recommendedSection.items where recommendedItem.itemId == item.itemId {
                    recommendedSection.remove(item: recommendedItem)
                }
            } else {
                // The item is not in the catalog yet. Sa we add the item to the catalog (or user item section) and move it to the to-be-purchased section.
                let newItem = enrich(newListContent, withMissingItem: state, translations: translations)
                if listContent.didFetchListContentForCurrentList && !listContent.isInToBePurchasedSection(item: newItem) {
                    newItem.newItem = true
                }
                newItem.touch()
            }
        }
        
        return newListContent
    }
    
    private func updateRecentlyItems(inListContent listContent: BringListContent, withNewRecentlyItems recently: [BringListStateItem], translations: [String: String]) -> BringListContent {
        
        let mappedItems = recently.map { item -> BringItem in
            
            let name: String
            let isNewItem = listContent.didFetchListContentForCurrentList && !listContent.isInToBePurchasedSection(uniqueIdentifier: item.uuid)
            if translations[item.itemId] != nil {
                name = translations[item.itemId] ?? ""
            } else {
                name = BringLocalizationSystem.shared.localizedString(forKey: item.itemId)
            }
            
            return self.map(item, toBringItemWithName: name, isUserItem: isNewItem, isNewItem: false)
        }
        
        let recentlyInBounds = Array(mappedItems.dropLast(mappedItems.count - listContent.maxItemsInRecently < 0 ? 0 : mappedItems.count - listContent.maxItemsInRecently))
        
        if mappedItems.count > listContent.maxItemsInRecently {
            let outOfRecently = Array(mappedItems.dropFirst(listContent.maxItemsInRecently))
            cleanCatalog(forRemovedItemsFromRecently: outOfRecently, in: listContent)
        }
        
        let newListContent = listContent.createNewModel(recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(items: recentlyInBounds))
        
        for state in recently {
            if let item = newListContent.item(for: state.uuid) {
                
                // add item to recommended section if it goes to recently and is not already in the recommended
                var isRecommendedItem = false
                for recommendedItemKey in newListContent.initialRecommenedItemKeys where recommendedItemKey == item.itemId {
                    isRecommendedItem = true
                    break
                }
                
                let recommendedSection = newListContent.getRecommendedSection()
                if isRecommendedItem && !newListContent.isRecommendedItem(item: item) {
                    if let indexToAddItem = getIndex(ofRecommendedItem: item, inListContent: newListContent) {
                        if indexToAddItem.intValue < recommendedSection.items.count {
                            recommendedSection.add(item: item, at: indexToAddItem.intValue)
                        } else {
                            recommendedSection.add(item: item)
                        }
                    }
                }
            } else {
                let newItem = enrich(newListContent, withMissingItem: state, translations: translations)
            }
        }
        
        return newListContent
    }
    
    private func enrich(_ listContent: BringListContent, withMissingItem state: BringListStateItem, translations: [String: String]) -> BringItem {
        
        let name: String
        if translations[state.itemId] != nil {
            name = translations[state.itemId] ?? ""
        } else {
            name = BringLocalizationSystem.shared.localizedString(forKey: state.itemId)
        }
        
        let newItem = BringItem(uuid: state.uuid, itemId: state.itemId, name: name, specification: state.specification)
        
        var itemFound = false
        
        for section in listContent.getSections() {
            let itemsInSection = section.items(withItemId: state.itemId)
            if itemsInSection.count > 0 {
                section.add(item: newItem)
                section.sort()
                if section.sectionId == BringCatalogProvider.USER_SECTION_KEY || itemsInSection[0].userItem {
                    newItem.userItem = true
                }
                itemFound = true
            }
        }

        if !itemFound {
            newItem.userItem = true
            self.add([newItem], toSection: BringCatalogProvider.USER_SECTION_KEY, listContent: listContent)
        }
        
        return newItem
    }
    
    private func add(_ items: [BringItem], toSection sectionId: String, listContent: BringListContent) {
        guard let section = listContent.catalogSectionReference(bySectionId: sectionId) else {
            return
        }
        for item in items {
            section.add(item: item)
        }
    }
    
    private func getIndex(ofRecommendedItem item: BringItem, inListContent listContent: BringListContent) -> NSNumber? {
        for (index, itemKey) in listContent.initialRecommenedItemKeys.enumerated() where itemKey == item.itemId {
            return NSNumber(value: index)
        }
        return nil
    }
    
    // MARK: MISC
    
    private func sortToBePruchasedSection(of listContent: BringListContent) {
        
        let toBePurchasedSection = listContent.getToBePurchasedSectionByReference()
        
        var sortedToBePurchasedItems = [BringItem]()
        for section in listContent.getSections() {
            
            var itemsPerSection = [BringItem]()
            
            for item in toBePurchasedSection.items {
                if section.items.contains(where: { (itemInSection) -> Bool in
                    itemInSection.itemId == item.itemId && section.isDefaultCatalogSection
                }) {
                    itemsPerSection.append(item)
                }
            }
            
            if itemsPerSection.count > 0 {
                let sortedItemsPerSection = itemsPerSection.sorted { $0.name < $1.name }
                for sortedItem in sortedItemsPerSection {
                    sortedToBePurchasedItems.append(sortedItem)
                }
            }
        }
        toBePurchasedSection.replaceItems(with: Array(NSOrderedSet(array: sortedToBePurchasedItems)) as! [BringItem])
    }
    
    private func move(_ item: BringItem, toRecentlyInListContent listContent: BringListContent) {
        assert(item.uuid != nil, "When an item is moved to the recently section it should always have a uuid.")
        let recentlySection = listContent.getRecentlyPurchasedSectionByReference()
        if !listContent.isInRecentlyPurchased(item: item) {
            if listContent.isInToBePurchasedSection(item: item) {
                listContent.getToBePurchasedSectionByReference().remove(item: item)
            }
            listContent.getRecentlyPurchasedSectionByReference().add(item: item)
            
            if recentlySection.items.count > listContent.maxItemsInRecently {
                let removedItem = recentlySection.removeLast()
                cleanCatalog(forRemovedItemsFromRecently: [removedItem], in: listContent)
            }
        }
    }
    
    private func cleanCatalog(forRemovedItemsFromRecently removedItems: [BringItem?], in listContent: BringListContent) {
        for removedItem in removedItems {
            if let (item, section) = listContent.itemAndSectionByReference(for: removedItem?.uuid) {
                let itemsInSectionWithSameItemId = section.items(withItemId: item.itemId)
                if itemsInSectionWithSameItemId.count > 1 {
                    section.remove(item: item)
                } else {
                    item.resetUuid()
                }
            }
        }
    }
    
    private func move(_ item: BringItem, toToBePurchasedInListContent listContent: BringListContent) {
        assert(item.uuid != nil, "When an item is moved to the to-be-purchased section it should always have a uuid.")
        if !listContent.isInToBePurchasedSection(item: item) {
            if listContent.isInRecentlyPurchased(item: item) {
                listContent.getRecentlyPurchasedSectionByReference().remove(item: item)
            }
            listContent.getToBePurchasedSectionByReference().insert(items: [item], at: 0)
        }
    }
    
    private func getListStatus(_ status: String, listUuid: String) -> BringListStatus {
        if let listStatus = BringListStatus(rawValue: status) {
            return listStatus
        } else {
            assert(false, "Invalid list status: uuid = \(listUuid), status = \(status)")
            // FIXME: Replace with new logger!
//            BringCrashlytics.log(cls: "BringListContentReducer", method: "updateListContent", description: "Invalid list status.", issue: "uuid = \(listUuid), status = \(status)")
            return .unregistered
        }
    }
    
    private func section(by sectionId: String, ofListContent listContent: BringListContent) -> BringSection? {
        for section in listContent.getSections() where sectionId == section.sectionId {
            return section
        }
        return nil
    }
    
    // MARK: BringListContentUpdateRecommendedAction
    
    public func updateRecommendedSection(sectionName: String, items: [BringItem], listContent: BringListContent) -> BringListContent {
        guard items.count > 0 else {
            return listContent.createNewModel(initialRecommenedItemKeys: listContent.initialRecommenedItemKeys, recommendedSection: BringListContent.createRecommendedSection(items: [], name: sectionName))
        }
        
        let sections = listContent.getSections()
        var bringItems = [BringItem]()
        
        for item in items {
            if let bringItem = ((sections.flatMap { $0.items }).filter { $0.itemId == item.itemId }).first {
                bringItems.append(bringItem)
            }
        }
        
        var notYetExistingItems = [BringItem]()
        
        for item in bringItems {
            var itemAlreadyExistsInPurchase = false
            for purchaseItem in listContent.getToBePurchasedSection().items where item.itemId == purchaseItem.itemId {
                itemAlreadyExistsInPurchase = true
            }
            
            if !itemAlreadyExistsInPurchase {
                notYetExistingItems.append(item)
            }
        }
        
        let recommendedSection = BringListContent.createRecommendedSection(items: notYetExistingItems, name: sectionName)
        
        return updateRecommendedSection(recommendedItems: notYetExistingItems, recommendedSection: recommendedSection, initialRecommenedItemKeys: items.compactMap { $0.itemId }, listContent: listContent)
    }
    
    private func updateRecommendedSection(recommendedItems: [BringItem], recommendedSection: BringSection, initialRecommenedItemKeys: [String], listContent: BringListContent) -> BringListContent {
        return listContent.createNewModel(initialRecommenedItemKeys: initialRecommenedItemKeys, recommendedSection: recommendedSection)
    }
    
    // MARK: BringListContentReorderSectionAction
    
    private func reorderSections(listContent: BringListContent, sectionOrder: [String]) -> BringListContent {
        var currentSections = listContent.getSections()
        currentSections.sort { (section1, section2) -> Bool in
            guard let sectionIndex1 = sectionOrder.firstIndex(of: section1.sectionId) else { return false }
            guard let sectionIndex2 = sectionOrder.firstIndex(of: section2.sectionId) else { return false }
            return  sectionIndex1 < sectionIndex2
        }
        let newListContent = listContent.createNewModel(catalogSections: currentSections)
        sortToBePruchasedSection(of: newListContent)
        return newListContent
    }
    
    // MARK: BringListContentUpdateSpecificationAction
    
    private func updateBringItemSpecification(forUniqueIdentifier identifier: String, specification: String, listContent: BringListContent) -> BringListContent {
        let toBePurchasedItem = listContent.getToBePurchasedSectionByReference().items.filter { (toBePurchasedItem) -> Bool in
            toBePurchasedItem.uniqueIdentifier == identifier
            }.first
        
        toBePurchasedItem?.specification = specification
        
        let catalogItem = listContent.allItems.filter { (catalogItem) -> Bool in
            catalogItem.uniqueIdentifier == identifier
            }.first
        
        catalogItem?.specification = specification
        
        return listContent
    }
    
    // MARK: BringListContentResetNewItemFlagsAction
    
    private func resetNewItemFlags(listContent: BringListContent) -> BringListContent {
        for item in listContent.getToBePurchasedSectionByReference().items {
            item.newItem = false
        }
        return listContent
    }
    
    // MARK: BringListContentAddItemAction
    
    private func add(_ item: BringItem, toSection sectionId: String, listContent: BringListContent) -> BringListContent {
        assert(item.uuid != nil, "The uuid should always be non nil here.")
        guard let section = self.section(by: sectionId, ofListContent: listContent) else {
            return listContent
        }
        
        section.add(item: item)
        return listContent
    }
    
    // MARK: BringListContentCatalogChangeAction
    
    func enrich(_ listContent: BringListContent, enrichment: BringCatalogChange) -> BringListContent {
        var itemsToAdd = [String: [BringItem]]()
        var itemsToRemove = [BringItem]()
        var itemsToMove = [String: [BringItem]]()
        
        self.prepareItemsToRemove(itemIds: enrichment.itemsToRemove, itemsToAdd: &itemsToAdd, itemsToRemove: &itemsToRemove, listContent: listContent)
        self.prepareItemsToAdd(potentialItemsToAdd: enrichment.itemsToAdd, itemsToAdd: &itemsToAdd, itemsToMove: &itemsToMove, listContent: listContent)
        
        var newListContent = listContent
        
        // First remove all unwanted items.
        newListContent = newListContent.removeItemsBasedOnItemIds(itemsToRemove)

        // add all new items.
        for (sectionId, items) in itemsToAdd {
            guard let section = (newListContent.getSections().filter { $0.sectionId == sectionId }.first) else {
                continue
            }
            for item in items {
                if !section.containsItems(withItemId: item.itemId) {
                    section.add(item: item)
                }
            }
            section.sort()
        }

        // move items
        for (sectionId, items) in itemsToMove {
            let newSection = newListContent.getSections().filter { $0.sectionId == sectionId }.first
            for item in items {
                let oldSection = self.section(of: item, inListContent: newListContent)
                oldSection?.remove(item: item)
                newSection?.add(item: item)
            }
        }
        
        // Ad new sections
        var sections = newListContent.getSections().filter { $0.isDefaultCatalogSection }
        
        for section in enrichment.sectionsToAdd {
            guard newListContent.catalogSectionReference(bySectionId: section.sectionId) == nil else {
                continue
            }
            
            let items = section.itemIds.compactMap { (itemId: String) -> BringItem? in
                let itemCopy = newListContent.item(forItemId: itemId)?.copy() as! BringItem?
                itemCopy?.name = BringLocalizationSystem.shared.localizedString(forKey: itemId)
                return itemCopy
            }
            
            let newSection = BringSection(sectionId: section.sectionId, name: section.name, items: items)
            
            if sections.count > section.position {
                sections.insert(
                    newSection,
                    at: section.position
                )
            } else {
                sections.append(newSection)
            }
        }
        
        newListContent = newListContent.createNewModel(catalogSections: sections)
        
        sortToBePruchasedSection(of: newListContent)
        
        return newListContent
    }
    
    private func prepareItemsToAdd(potentialItemsToAdd: [BringCatalogChange.ItemToAdd], itemsToAdd: inout [String: [BringItem]], itemsToMove: inout [String: [BringItem]], listContent: BringListContent) {
        
        for potentialItemToAdd in potentialItemsToAdd {
            if let listContentItem = listContent.item(forItemId: potentialItemToAdd.item.itemId)?.copy() as! BringItem?, listContentItem.userItem {
                if !listContent.isInToBePurchasedSection(item: potentialItemToAdd.item) && !listContent.isInRecentlyPurchased(item: potentialItemToAdd.item) {
                    
                    if !potentialItemToAdd.item.specification.isEmpty {
                        listContentItem.specification = potentialItemToAdd.item.specification
                    }
                    
                    itemsToMove = self.add(item: listContentItem, toDictionary: itemsToMove, ofSection: potentialItemToAdd.sectionId)
                }
            } else {
                itemsToAdd = add(item: potentialItemToAdd.item, toDictionary: itemsToAdd, ofSection: potentialItemToAdd.sectionId)
            }
        }
    }
    
    private func prepareItemsToRemove(itemIds: [String], itemsToAdd: inout [String: [BringItem]], itemsToRemove: inout [BringItem], listContent: BringListContent) {
        for itemId in itemIds {
            if let item = listContent.item(forItemId: itemId) {
                if !listContent.isInRecentlyPurchased(item: item) && !listContent.isInToBePurchasedSection(item: item) {
                    itemsToRemove.append(item)
                } else {
                    item.userItem = true
                    itemsToAdd = add(item: item, toDictionary: itemsToAdd, ofSection: BringCatalogProvider.USER_SECTION_KEY)
                }
            }
        }
    }
    
    private func add(item: BringItem, toDictionary dictionary: [String: [BringItem]], ofSection sectionId: String) -> [String: [BringItem]] {
        var dictionary = dictionary
        if dictionary[sectionId] == nil {
            dictionary[sectionId] = [item]
        } else {
            dictionary[sectionId]?.append(item)
        }
        return dictionary
    }
    
    func section(of item: BringItem, inListContent listContent: BringListContent) -> BringSection? {
        return listContent.getSections().filter { (section) -> Bool in
            return section.items.contains(item)
            }.first
    }

    // MARK: BringListContentRemoveItemAction
    
    func removeItem(withItemId itemId: String, listContent: BringListContent) -> BringListContent {
        
        let recentlySection = listContent.getRecentlyPurchasedSectionByReference()
        let toBePurchasedSection = listContent.getToBePurchasedSectionByReference()
        let catalogSections = listContent.getSections()

        recentlySection.removeItems(withItemId: itemId)
        toBePurchasedSection.removeItems(withItemId: itemId)
        
        for section in catalogSections {
            section.removeItems(withItemId: itemId)
        }
        
        return listContent
    }
    
    // MARK: BringListContentRemoveMultipleItemsAction
    
    func remove(_ items: [String], from section: String, listContent: BringListContent) -> BringListContent {
        // Currently we do only support removing items from the recently section.
        if section == BringListContent.recentlySectionKey {
            let recently = listContent.getRecentlyPurchasedSectionByReference()
            var removedItems = [BringItem]()
            recently.removeItems { (recentlyItem) -> Bool in
                if items.contains(recentlyItem.uniqueIdentifier) {
                    removedItems.append(recentlyItem)
                    return true
                } else {
                    return false
                }
            }
            cleanCatalog(forRemovedItemsFromRecently: removedItems, in: listContent)
        }
        return listContent
    }
    
    /// Removes items from the catalog that are already in the to-be-purchased or recently section from the given `listContent`
    ///
    /// - Parameters:
    ///   - listContent: The list content to update
    ///   - purchase: The purchase section
    ///   - recently: The recently section
    private func cleanupRecently(of listContent: BringListContent, purchase: [BringListStateItem], recently: [BringListStateItem]) {
        let items = purchase + recently
        for section in listContent.getSections() {
            for item in items {
                let foundItems = section.items(withItemId: item.itemId)
                if foundItems.count > 1 {
                    section.removeItems {
                        ($0.uuid == nil && $0.itemId == item.itemId)
                    }
                }
            }
        }
    }
}

// swiftlint:enable force_cast
