//
//  BringItemHelper.swift
//  Bring
//
//  Created by Sascha Thöni on 06.03.17.
//
//

import Foundation

public class BringItemHelper: NSObject {
    
    public struct ExtractResult {
        public var name = ""
        public var specification = ""
        
        init(name : String, specification : String) {
            self.name = name
            self.specification = specification
        }
    }
    
    public class func extractItemAndSpecificationFromSearch(forSearchText searchText : String, withItemNames itemNames: [String]) -> [ExtractResult]{

        var extractResults = [ExtractResult]()
        
        var searchWithoutQuantity = searchText
        
        // first extract quantity from searchText
        let quantity = BringItemUtils.extractQuantity(name: searchWithoutQuantity)
        
        
        // if quantity is not empty, remove it from searchText
        
        if !quantity.isEmpty {
            searchWithoutQuantity = searchWithoutQuantity.replacingOccurrences(of: quantity, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        var searchTokens = searchWithoutQuantity.components(separatedBy: " ")
        
        // find the token which contains an itemName. if multiple tokes are contained in an itemname, take the last one
        var tokenThatMatchesItem = ""
        
        for token in searchTokens {
            if !tokenThatMatchesItem.isEmpty {
                break
            }
            for itemName in itemNames {
                if itemName.lowercased() == token.lowercased() {
                    tokenThatMatchesItem = token
                    break
                }
            }
        }
        
        var tokenToSearch : String?

        if !tokenThatMatchesItem.isEmpty {
            searchTokens.remove(at: searchTokens.index(of: tokenThatMatchesItem)!)
            tokenToSearch = tokenThatMatchesItem
        } else {
            tokenToSearch = searchTokens.last
            if tokenToSearch != nil {
                searchTokens.remove(at: searchTokens.index(of: tokenToSearch!)!)
            }
        }

        // get last token (item name)
        if let tokenToSearch = tokenToSearch {
            
            let predicate = NSPredicate(format: "self CONTAINS[cd] %@", tokenToSearch)
            let filteredItems: [String] = self.sortFilteredItems(items: itemNames.filter { predicate.evaluate(with: $0) }, search: tokenToSearch)
            
            for itemName in filteredItems {
                var specification = ""
                // create specification
                if !quantity.isEmpty {
                    specification = "\(quantity) \(searchTokens.joined(separator: " "))"
                } else {
                    specification = searchTokens.joined(separator: " ")
                }
                
                let extractResult = ExtractResult(name: itemName, specification: specification)
                extractResults.append(extractResult)
            }

        }
        
        if searchWithoutQuantity != tokenThatMatchesItem {
            extractResults.append(ExtractResult(name: searchWithoutQuantity, specification: quantity))
        }
        
        return extractResults;
    }
    
    private class func sortFilteredItems(items: [String], search: String) -> [String] {
        let predicateBeginsWith = NSPredicate(format: "(self BEGINSWITH[cd] %@)", search)
        let predicateNotBeginsWith = NSCompoundPredicate(notPredicateWithSubpredicate: predicateBeginsWith)
        
        let beginsWithItems: [String] = items.filter { predicateBeginsWith.evaluate(with: $0) }
        let containsItems: [String] = items.filter { predicateNotBeginsWith.evaluate(with: $0) }
        
        return beginsWithItems.sorted() + containsItems.sorted()
    }

    class func extractChainedStrings(fromTokens tokens : [String]) -> [String] {
        var result : [String] = [] + tokens
        
        for startPos in 0 ..< tokens.count-1 {
            for lenght in 1..<tokens.count-startPos {
                
                var token = tokens[startPos];
                for index in 1..<lenght+1 {
                    token = token + " \(tokens[startPos + index])"
                }
                
                
                token = token.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                result.append(token)
            }
        }
        return result
    }
}
