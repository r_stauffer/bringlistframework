//
//  BringListSyncManager.swift
//  Bring
//
//  Created by Renato Stauffer on 23.01.19.
//

import Foundation

enum BringListSyncManagerError: Error {
    /// There is no list existing.
    case noExistingList
    
    /// There is no apns token stored for this user.
    case missingApnsToken
    
    case noConnection
}

public protocol BringAPNSTokenProvider: class {
    func getAPNSToken() -> String?
}

public protocol ListChangeSyncer: class {
    func sync(_ listChange: BringListChange, shouldSyncimmediately: Bool)
    func getCurrentListState(forList listUuid: String, callback: @escaping (Result<BringListState, Error>) -> Void)
    func getCurrentListState(forList listuuid: String) -> BringListState?
    func syncLists(mode: BringListSyncManager.BringListSyncMode, callback: @escaping (Result<Void, Error>) -> Void)
    var delegate: ListChangeSyncerDelegate? { get set }
}

public protocol ListChangeSyncerDelegate: class {
    func didFinishFetchingCurrentListContent(listState: BringListState, startSyncDate: Date)
}

/// This class is responsible to synchronize local pending list changes with the current state of the lists on the backend.
@objc public final class BringListSyncManager: NSObject, ListChangeSyncer {
    
    @objc
    public static let shared = BringListSyncManager()
    fileprivate let listContentService: ListContentService
    fileprivate let listChangesStore: ListChangesStore
    private let listSettings: BringListSettings
    fileprivate let localListStore: LocalListStore
    private let connectivityManager: ConnectionTestable
    fileprivate let listStateStore: ListStateStore
    private var timer: Timer?
    public weak var provider: BringAPNSTokenProvider?
    
    /// Background task identifier for the sync.
//    private var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    /// A queue, where all syncs should happen.
    private let syncQueue = OperationQueue()
    
    /// A serial queue, which handles list changes
    private let listChangesUpdateQueue = OperationQueue()
    
    /// Concurrent queue for synchronizing access to `isSyncing`.
    private let syncStateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! + ".sync.state.queue", attributes: .concurrent)
    
    private var _syncState = false
    
    /// The state of the sync
    @objc private var isSyncing: Bool {
        get { return syncStateQueue.sync { _syncState } }
        set { syncStateQueue.sync(flags: .barrier) { _syncState = newValue } }
    }
    
    /// The interval for batch updates.
    private let batchUpdateInterval: Double
    
    /// Uuids with pending batch updates
    private var listUuidsForPendingBatchOperations = Set<String>()
    
    public weak var delegate: ListChangeSyncerDelegate?
    
    /// Callbacks to be called when the sync finished.
    var callbacks = [(Result<Void, Error>) -> Void]()
    
    public enum BringListSyncMode {
        case all
        case specific([String])
    }
    
    
    // MARK: Init & deinit
    init(listContentService: ListContentService = BringAlamofireListContentService.shared, listChangesStore: ListChangesStore = BringCoreDataListChangesStore.shared, listSettings: BringListSettings = BringListSettings.standard, localListStore: LocalListStore = BringCoreDataUserListStore.shared, connectivityManager: ConnectionTestable = BringConnectivity(), listStateStore: ListStateStore = BringCoreDataListStateStore.shared, batchUpdateInterval: Double = 5.0) {
        self.listContentService = listContentService
        self.listChangesStore = listChangesStore
        self.localListStore = localListStore
        self.connectivityManager = connectivityManager
        self.batchUpdateInterval = batchUpdateInterval
        self.listStateStore = listStateStore
        self.listSettings = BringListSettings.standard
        listChangesUpdateQueue.maxConcurrentOperationCount = 1
    }
    
    public convenience init(listContentService: ListContentService = BringAlamofireListContentService.shared, listChangesStore: ListChangesStore = BringCoreDataListChangesStore.shared, localListStore: LocalListStore = BringCoreDataUserListStore.shared, connectivityManager: ConnectionTestable = BringConnectivity(), listStateStore: ListStateStore = BringCoreDataListStateStore.shared, batchUpdateInterval: Double = 5.0) {
        self.init(listContentService: listContentService, listChangesStore: listChangesStore, listSettings: BringListSettings.standard, localListStore: localListStore, connectivityManager: connectivityManager, listStateStore: listStateStore, batchUpdateInterval: batchUpdateInterval)
    }
    
    deinit {
        resetTimer()
    }
    
    // MARK: Public API
    
    /// Sends pending changes for the specified lists and fetches the their new state. The new list state of the currently selected list will be forwarded to the `delegate` (`ListChangeSyncerDelegate`).
    ///
    /// - Parameters:
    ///   - mode: Tells the sync which lists to sync.
    ///   - callback: Callback with success / failure
    public func syncLists(mode: BringListSyncMode, callback: @escaping (Result<Void, Error>) -> Void) {
        assert(Thread.isMainThread, "This should be called from the main thread only")
        
        callbacks.append(callback)
        
        if isSyncing {
            return
        }
        
        guard connectivityManager.hasConnection else {
            for cachedCallback in self.callbacks {
                cachedCallback(Result.failure(BringListSyncManagerError.noConnection))
            }
            self.callbacks.removeAll()
            return
        }
        
        let lastSyncDate = Date()
        self.listSettings.setLastSyncDate(lastSyncDate)
        
        isSyncing = true
        resetTimer()
        
        let fetchOperationFinishOperation = createListFetchFinishOperation(callback: callback)
        let fetchOperationGroup = createFetchListOperationGroup(mode: mode, syncDate: lastSyncDate)
        self.performBatchupdate(forLists: self.localListStore.allLists(forPredicate: nil).map { $0.uuid }, afterBatchUpdateGroup: fetchOperationGroup, finishOperation: fetchOperationFinishOperation, syncStartdate: lastSyncDate)
    }
    
    /// Sends pending changes for all lists and fetches the new state of the current selected list from the server afterwards.
    @objc public func syncAllListsAndFetch(list: String, block: @escaping () -> Void) {
        self.syncLists(mode: .specific([list])) { _ in
            DispatchQueue.main.async {
                block()
            }
        }
    }
    
    @objc public func syncListChangesIfNeeded(block: @escaping () -> Void) {
        guard !listUuidsForPendingBatchOperations.isEmpty else {
            block()
            return
        }
        
        self.syncLists(mode: .specific([])) { _ in
            DispatchQueue.main.async {
                block()
            }
        }
    }
    
    /// Schedules a batch snyc for the given list change.
    ///
    /// - Parameter listChange: The list change to sync.
    /// - Parameter shouldSyncimmediately: If true, the batch sync will be performed immediatly for all pending list changes.
    @objc
    public func sync(_ listChange: BringListChange, shouldSyncimmediately: Bool = false) {
        Log.i("listUuid = \(listChange.listUuid)")
        let operation = BringLocalListUpdateOperation(listStateStore: self.listStateStore, listChangesStore: self.listChangesStore, listChange: listChange)
        operation.queuePriority = .veryHigh
        self.listChangesUpdateQueue.addOperation(operation)
        
        listUuidsForPendingBatchOperations.insert(listChange.listUuid)
        
        if !isSyncing {
            if shouldSyncimmediately {
                performBatchUpdateForPendingOperations()
            } else {
                startTimerForPendingBatchUpdateOperationsToBePerformed()
            }
        }
    }
    
    /// Gets the current list state from the local store.
    ///
    /// - Parameters:
    ///   - listUuid: The uuid of the list to load
    ///   - callback: Returns the list state of the list with the given uuid or an error if something went wrong.
    public func getCurrentListState(forList listUuid: String, callback: @escaping (Result<BringListState, Error>) -> Void) {
        self.listStateStore.getListState(forUuid: listUuid, isWaiting: true, callback: { (result) in
            switch result {
            case .success(let state):
                DispatchQueue.main.async {
                    if let state = state {
                        callback(Result.success(state))
                    } else {
                        let emptyListState = BringListState(purchase: [], recently: [], status: BringListStatus.unregistered.rawValue, uuid: listUuid)
                        callback(Result.success(emptyListState))
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    callback(Result.failure(error))
                }
            }
        })
    }
    
    @objc public func deleteListStateObjc(for listUuid: String, callback: @escaping (Bool) -> Void) -> Void {
        self.deleteListState(for: listUuid) { (result) in
            switch result {
            case .success:
                callback(true)
            case .failure:
                callback(false)
            }
        }
    }
    
    public func deleteListState(for listUuid: String, callback: @escaping (Result<Void, Error>) -> Void) -> Void {
        self.listStateStore.delete(withPredicate: NSPredicate(format: "uuid = %@", listUuid), callback: callback)
    }
    
    public func createEmptyListState(withListUuid listUuid: String, status: BringListStatus, callback: @escaping (Result<Void, Error>) -> Void) {
        let liststate = BringListState(purchase: [], recently: [], status: status.rawValue, uuid: listUuid)
        self.listStateStore.save(liststate, isWaiting: false, callback: callback)
    }
    
    public func getCurrentListState(forList listuuid: String) -> BringListState? {
        return self.listStateStore.get(listuuid)
    }
    
    @objc public func getCurrentListStateObjc(forList listUuid: String, callback: @escaping (BringListState?) -> Void) {
        self.getCurrentListState(forList: listUuid) { (result) in
            switch result {
            case .success(let listState):
                callback(listState)
            case .failure:
                callback(nil)
            }
        }
    }
    
    @objc public func getAllListStates(callback: @escaping ([BringListState]) -> Void) {
        self.listStateStore.getAllListStates { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let listStates):
                    callback(listStates)
                case .failure(let error):
                    assertionFailure("This should not happen: \(error.localizedDescription)")
                    callback([])
                }
            }
        }
    }
    
    @objc public func objcFetchListState(forListUuid uuid: String, callback: @escaping () -> Void) {
        self.syncLists(mode: .specific([uuid])) { _ in
            callback()
        }
    }
    
    // MARK: Private methods
    
    // MARK: Operation creation for main list sync
    
    private func createFetchListOperationGroup(mode: BringListSyncMode, syncDate: Date) -> BringListSyncOperationGroup {
        let listsToFetch = self.getListsToBeSynced(for: mode)
        
        var operations = [ListSyncOperation]()
        
        for listUuid in listsToFetch {
            let fetchOperation = BringListFetchOperation(listContentService: self.listContentService, listStateStore: self.listStateStore, listUuid: listUuid, syncStartDate: syncDate)
            fetchOperation.delegate = self
            operations.append(fetchOperation)
        }
        
        let fetchOperationGroup = BringListSyncOperationGroup(operations: operations, priority: 2)
        return fetchOperationGroup
    }
    
    private func createListFetchFinishOperation(callback: @escaping (Result<Void, Error>) -> Void) -> Operation {
        let fetchOperationFinishOperation = BlockOperation {
            self.cleanUpSync()
        }
        
        return fetchOperationFinishOperation
    }
    
    private func getListsToBeSynced(for mode: BringListSyncMode) -> [String] {
        switch mode {
        case .all:
            return self.localListStore.allLists(forPredicate: nil).map { $0.uuid }
        case .specific(let lists):
            return lists
        }
    }
    
    // MARK: Batch update
    
    @objc private func performBatchUpdateForPendingOperations() {
        
        guard connectivityManager.hasConnection else {
            startTimerForPendingBatchUpdateOperationsToBePerformed()
            return
        }
        
        if !isSyncing {
            let listUuidsForPendingBatchOperationsCopy = self.listUuidsForPendingBatchOperations
            listUuidsForPendingBatchOperations.removeAll()
            
            let finishOperation = BlockOperation {
                DispatchQueue.main.async {
                    Log.i("Finished Batch update")
                }
            }
            performBatchupdate(forLists: Array(listUuidsForPendingBatchOperationsCopy), afterBatchUpdateGroup: nil, finishOperation: finishOperation, syncStartdate: Date())
        } else {
            startTimerForPendingBatchUpdateOperationsToBePerformed()
            return
        }
    }
    
    private func performBatchupdate(forLists listUuids: [String], afterBatchUpdateGroup: BringListSyncOperationGroup?, finishOperation: Operation, syncStartdate: Date) {
        resetTimer()
        
        if listUuids.count > 0 {
            let batchUpdateGroup = createBatchUpdateOperationGroup(forLists: listUuids, syncStartdate: syncStartdate)
            let finalOperationsToPerform = BringListSyncOperationsDependencyBuilder.buildDependencies(for: [batchUpdateGroup, afterBatchUpdateGroup].compactMap { $0 }, finishOperation: finishOperation)
            syncQueue.addOperations(finalOperationsToPerform, waitUntilFinished: false)
        } else {
            let finalOperationsToPerform = BringListSyncOperationsDependencyBuilder.buildDependencies(for: [afterBatchUpdateGroup].compactMap { $0 }, finishOperation: finishOperation)
            syncQueue.addOperations(finalOperationsToPerform, waitUntilFinished: false)
        }
    }
    
    private func createBatchUpdateOperationGroup(forLists listUuids: [String], syncStartdate: Date) -> BringListSyncOperationGroup {
        var operations = [ListSyncOperation]()
        
        for listUuid in listUuids {
            let operation = BringListBatchUpdateOperation(listContentService: self.listContentService, listChangesStore: self.listChangesStore, listUuid: listUuid, apnsToken: provider?.getAPNSToken(), syncStartDate: syncStartdate)
            operation.delegate = self
            operations.append(operation)
        }
        
        let batchUpdateGroup = BringListSyncOperationGroup(operations: operations, priority: 0)
        
        return batchUpdateGroup
    }
    
    private func startTimerForPendingBatchUpdateOperationsToBePerformed() {
        resetTimer()
        timer = Timer.scheduledTimer(timeInterval: batchUpdateInterval, target: self, selector: #selector(performBatchUpdateForPendingOperations), userInfo: nil, repeats: false)
    }
    
    private func resetTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    private func cleanUpSync() {
        DispatchQueue.main.async {
            for cachedCallback in self.callbacks {
                cachedCallback(Result.success(()))
            }
            
            self.callbacks.removeAll()
            self.isSyncing = false
        }
    }
}

extension BringListSyncManager: BringListFetchOperationDelegate {
    
    func didFetch(_ listState: BringListState, startSyncDate: Date) {
        Log.i("[ListSync]: Did fetch new list state for list: \(listState.uuid)")
        DispatchQueue.main.async {
            self.delegate?.didFinishFetchingCurrentListContent(listState: listState, startSyncDate: startSyncDate)
        }
    }
    
    func didFinish(with error: Error) {
        self.syncQueue.cancelAllOperations()
        cleanUpSync()
    }
}

extension BringListSyncManager: BringListBatchUpdateOperationDelegate {
    func didFinishBatchupdate(with error: BringListBatchUpdateOperationError, forListWithUuid listUuid: String) {
        
        switch error.reason {
        case .batchUpdateService:
            break
        case .listChangeDeletion, .listChangesFetch:
            BringRemoteLoggingManager.shared.log(cls: "BringListSyncManager", method: "didFinishBatchupdate", description: "Batch update did fail because of an unexpected error", issue: "listUuid == \(listUuid)", error: error.underlyingError, shouldLog: true)
        }
        
        self.syncQueue.cancelAllOperations()
        cleanUpSync()
    }
    
    func didFinishBatchupdate(forListWithUuid listUuid: String) {
        Log.i("[ListSync]: Finished batch update for list: \(listUuid)")
    }
    
    func didFinishBatchupdate(with error: Error) {
        self.syncQueue.cancelAllOperations()
        cleanUpSync()
    }
}
