//
//  ListChangesStore.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation

public protocol ListChangesStore {
    func allChanges(forListUuid uuid: String, isWaiting: Bool, sortDescriptors: [NSSortDescriptor], callback: @escaping (Result<[BringListChange], Error>) -> Void)
    func add(_ listChange: BringListChange, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void)
    func deleteChanges(forListUuid uuid: String, syncDate: Date, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void)
}
