//
//  BringListStateStore.swift
//  Bring
//
//  Created by Renato Stauffer on 30.01.19.
//

import Foundation
import CoreData

enum BringItemState: String {
    case purchase
    case recently
}

public protocol ListStateMerger: class {
    func merge(_ listState: BringListState, withOther otherListState: BringListState) -> BringListState
}

public protocol ListChangeMerger: class {
    
    var listChange: BringListChange { get }
    
    func mergeListChange(into currentListState: BringListState?) -> BringListState
}

public enum BringCoreDataListStateStoreError: Error {
    case unknown
}

@objc public class BringCoreDataListStateStore: NSObject, ListStateStore {
    
    private let coreDataStack: BringCoreDataListsStack
    private let serialQueue = OperationQueue()
    
    public static let listStateDidChangeNotification = Notification.Name("bring.com.listStateDidChangeNotification")
    @objc public static let shared = BringCoreDataListStateStore()
    
    @objc public init(coreDataStack: BringCoreDataListsStack = BringCoreDataListsStackFactory().makeCoreDataStack()) {
        self.coreDataStack = coreDataStack
        self.serialQueue.maxConcurrentOperationCount = 1
        super.init()
    }
    
    public func save(_ state: BringListState, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void) {
        self.serialQueue.addOperation { [weak self] in
            self?.saveInternal(state, isWaiting: isWaiting, callback: callback)
        }
    }
    
    private func saveInternal(_ state: BringListState, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void) {
        let backgroundContext = self.coreDataStack.createBackgroundContext()
        
        backgroundContext.performAndWait { [weak self] in
            
            self?.deleteListStateInternal(onContext: backgroundContext, forListUuid: state.uuid)
            
            let purchaseItems = state.purchase
            let recentlyItems = state.recently
            
            let listEntity = BringListStateEntity(usedContext: backgroundContext)
            listEntity.status = state.status
            listEntity.uuid = state.uuid
            
            let allIEntities = NSMutableOrderedSet()
            self?.add(purchaseItems, with: BringItemState.purchase, to: listEntity, in: backgroundContext, usingSet: allIEntities)
            self?.add(recentlyItems, with: BringItemState.recently, to: listEntity, in: backgroundContext, usingSet: allIEntities)
            
            listEntity.addToItems(allIEntities)
        }
        
        self.coreDataStack.save(backgroundContext)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: BringCoreDataListStateStore.listStateDidChangeNotification, object: nil, userInfo: ["newListState": state])
            callback(Result.success(()))
        }
    }
    
    public func update(listState uuid: String, withListChangeUsing listStateMerger: ListChangeMerger, callback: @escaping (Result<Void, Error>) -> Void) {
        self.serialQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else {
                DispatchQueue.main.async {
                    callback(Result.success(()))
                }
                return
            }
            
            let backgroundContext = strongSelf.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
                fetchreqeust.predicate = NSPredicate(format: "uuid = %@", uuid)
                
                do {
                    
                    let storedListState: BringListState?
                    
                    if let result = try backgroundContext.fetch(fetchreqeust).first {
                        storedListState = BringListState(entity: result)
                    } else {
                        storedListState = nil
                    }
                    
                    let newListState = listStateMerger.mergeListChange(into: storedListState)
                    strongSelf.saveInternal(newListState, isWaiting: false, callback: callback)
                } catch let error {
                    Log.ed(error)
                }
            }
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
            
        }
    }
    
    public func update(_ listState: BringListState, listStateMerger: ListStateMerger, callback: @escaping (Result<Void, Error>) -> Void) {
        self.serialQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else {
                DispatchQueue.main.async {
                    callback(Result.success(()))
                }
                return
            }
            
            let backgroundContext = strongSelf.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
                fetchreqeust.predicate = NSPredicate(format: "uuid = %@", listState.uuid)
                
                do {
                    guard let result = try backgroundContext.fetch(fetchreqeust).first else {
                        strongSelf.saveInternal(listState, isWaiting: false, callback: callback)
                        return
                    }
                    
                    guard let storedListState = BringListState(entity: result) else {
                        strongSelf.saveInternal(listState, isWaiting: false, callback: callback)
                        return
                    }
                    
                    let newListState = listStateMerger.merge(listState, withOther: storedListState)
                    strongSelf.saveInternal(newListState, isWaiting: false, callback: callback)
                } catch let error {
                    Log.ed(error)
                }
            }
        }
    }
    
    private func deleteListStateInternal(onContext context: NSManagedObjectContext, forListUuid uuid: String) {
        do {
            let fetchreqeust = NSFetchRequest<BringListStateEntity>(entityName: "BringListStateEntity")
            fetchreqeust.predicate = NSPredicate(format: "uuid = %@", uuid)
            let result = try context.fetch(fetchreqeust)
            for entity in result {
                context.delete(entity)
            }
        } catch let error {
            Log.e(error.localizedDescription)
        }
    }
    
    private func add(_ items: [BringListStateItem], with state: BringItemState, to listEntity: BringListStateEntity, in context: NSManagedObjectContext, usingSet entitiesToAdd: NSMutableOrderedSet) {
        for item in items {            
            let itemEntity = BringItemEntity(usedContext: context)
            itemEntity.list = listEntity
            listEntity.addToItems(itemEntity)
            itemEntity.itemId = item.itemId
            itemEntity.modificationDate = item.modificationDate
            itemEntity.specification = item.specification
            itemEntity.status = state.rawValue
            itemEntity.uuid = item.uuid
            entitiesToAdd.add(itemEntity)
        }
    }
    
    public func getListState(forUuid uuid: String, isWaiting: Bool, callback: @escaping (Result<BringListState?, Error>) -> Void) {
        
        self.serialQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else {
                DispatchQueue.main.async {
                    callback(Result.success(nil))
                }
                return
            }
            
            var returnValue: BringListState?
            
            let viewContext = strongSelf.coreDataStack.createBackgroundContext()
            
            viewContext.performAndWait {
                let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
                fetchreqeust.predicate = NSPredicate(format: "uuid = %@", uuid)
                
                do {
                    guard let result = try viewContext.fetch(fetchreqeust).first else {
                        return
                    }
                    
                    let listState = BringListState(entity: result)
                    returnValue = listState
                } catch let error {
                    Log.ed(error)
                }
            }
            
            DispatchQueue.main.async {
                callback(Result.success(returnValue))
            }
        }
    }
    
    public func delete(withPredicate predicate: NSPredicate?, callback: @escaping (Result<Void, Error>) -> Void) {
        self.serialQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else {
                DispatchQueue.main.async {
                    callback(Result.success(()))
                }
                return
            }
            
            let backgroundContext = strongSelf.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                do {
                    let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
                    fetchreqeust.predicate = predicate
                    let result = try backgroundContext.fetch(fetchreqeust)
                    for entity in result {
                        backgroundContext.delete(entity)
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
            
            strongSelf.coreDataStack.save(backgroundContext)
            
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
        }
    }
    
    public func get(_ uuid: String) -> BringListState? {
        
        var returnValue: BringListState?
        
        let viewContext = self.coreDataStack.mainContext
        
        viewContext.performAndWait {
            let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
            fetchreqeust.predicate = NSPredicate(format: "uuid = %@", uuid)
            
            do {
                guard let result = try viewContext.fetch(fetchreqeust).first else {
                    return
                }
                
                let listState = BringListState(entity: result)
                returnValue = listState
            } catch let error {
                Log.ed(error)
            }
        }
        
        return returnValue
    }
    
    public func getAllListStates(callback: @escaping (Result<[BringListState], Error>) -> Void) {
        self.serialQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else {
                DispatchQueue.main.async {
                    callback(Result.success(([])))
                }
                return
            }
            
            var returnValue: [BringListState]? = nil
            
            let backgroundContext = strongSelf.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                let fetchreqeust: NSFetchRequest<BringListStateEntity> = NSFetchRequest(entityName: "BringListStateEntity")
                
                do {
                    let result = try backgroundContext.fetch(fetchreqeust)
                    let listStates = result.compactMap { BringListState(entity: $0) }
                    returnValue = listStates
                } catch let error {
                    Log.ed(error)
                }
            }
            
            DispatchQueue.main.async {
                if let returnValue = returnValue {
                    callback(Result.success(returnValue))
                } else {
                    callback(Result.failure(BringCoreDataListStateStoreError.unknown))
                }
            }
        }
    }
}

private extension BringListState {
    convenience init?(entity: BringListStateEntity?) {
        // @FIXME: Add logging here!!!
        guard let entity = entity else { return nil }
        guard let uuid = entity.uuid else { return nil }
        guard let serverListStatus = entity.status else { return nil }
        
    
        guard let items = entity.items else {
            self.init(purchase: [], recently: [], status: serverListStatus, uuid: uuid)
            return
        }
        
        let purchase = BringListState.map(items, forStatus: BringItemState.purchase)
        let recently = BringListState.map(items, forStatus: BringItemState.recently)
        
        self.init(purchase: purchase, recently: recently, status: serverListStatus, uuid: uuid)
    }
    
    private static func map(_ items: NSOrderedSet, forStatus status: BringItemState) -> [BringListStateItem] {
        let mappedItems = items
            .compactMap({ (item) -> BringItemEntity? in
                item as? BringItemEntity
            })
            .filter({ (item) -> Bool in
                return item.status == status.rawValue
            })
            .compactMap { (entity) -> BringListStateItem? in
                return BringListStateItem(entity: entity)
            }
        
        return mappedItems
    }
}

private extension BringListStateItem {
    convenience init?(entity: BringItemEntity) {
        // @FIXME: Add logging here!!!  
        guard let itemId = entity.itemId else {
            return nil
        }
        // @FIXME: Add logging here!!!
        guard let uuid = entity.uuid else {
            return nil
        }
        // @FIXME: Add logging here!!!
        guard let modificationDate = entity.modificationDate else {
            // @TODO: Make this throwable to expose it outside the framework so we can log it to firebase
            assertionFailure("This should not happen! There should always be a modification date")
            return nil
        }
        
        self.init(uuid: uuid, specification: entity.specification ?? "", itemId: itemId, modificationDate: modificationDate)
    }
}
