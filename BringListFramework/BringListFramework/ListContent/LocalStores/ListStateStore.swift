//
//  ListStateStore.swift
//  BringShared
//
//  Created by Renato Stauffer on 08.08.19.
//

import Foundation

public protocol ListStateStore {
    func save(_ state: BringListState, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void)
    func getListState(forUuid uuid: String, isWaiting: Bool, callback: @escaping (Result<BringListState?, Error>) -> Void)
    func getAllListStates(callback: @escaping (Result<[BringListState], Error>) -> Void)
    func delete(withPredicate predicate: NSPredicate?, callback: @escaping (Result<Void, Error>) -> Void)
    func update(_ listState: BringListState, listStateMerger: ListStateMerger, callback: @escaping (Result<Void, Error>) -> Void)
    func update(listState uuid: String, withListChangeUsing listStateMerger: ListChangeMerger, callback: @escaping (Result<Void, Error>) -> Void)
    func get(_ uuid: String) -> BringListState?
}
