//
//  ListChangesStore.swift
//  Bring
//
//  Created by Renato Stauffer on 23.01.19.
//

import Foundation
import CoreData
import CoreLocation

@objc public class BringCoreDataListChangesStore: NSObject, ListChangesStore {
    
    private let coreDataStack: BringCoreDataListsStack
    private let serialQueue = DispatchQueue(label: "BringCoreDataListChangesStoreQueue")
    
    public static let shared = BringCoreDataListChangesStore()
    
    public init(coreDataStack: BringCoreDataListsStack = BringCoreDataListsStackFactory().makeCoreDataStack()) {
        self.coreDataStack = coreDataStack
    }
    
    public func allChanges(forListUuid uuid: String, isWaiting: Bool, sortDescriptors: [NSSortDescriptor], callback: @escaping (Result<[BringListChange], Error>) -> Void) {
        
        serialQueue.async {
            let backgroundContext = self.coreDataStack.createBackgroundContext()
            
            var changes = [BringListChange]()
            
            var error: Error? = nil
            
            backgroundContext.performAndWait {
                let fetchrequest: NSFetchRequest<BringListChangeEntity> = NSFetchRequest<BringListChangeEntity>(entityName: "BringListChangeEntity")
                fetchrequest.predicate = NSPredicate(format: "uuid == %@", uuid)
                fetchrequest.sortDescriptors = sortDescriptors
                
                do {
                    let result = try backgroundContext.fetch(fetchrequest)
                    changes = self.createListChanges(from: result, context: backgroundContext)
                } catch let caughtError {
                    error = caughtError
                }
            }
            
            if let error = error {
                callback(Result.failure(error))
            } else {
             callback(Result.success(changes))
            }
        }
    }
    
    @available(*, deprecated, message: "This is only used for old Objective-C code")
    @objc public func allChanges(forListUuid uuid: String, isWaiting: Bool, callback: @escaping ([BringListChange]) -> Void) {
        
        serialQueue.async {
            let backgroundContext = self.coreDataStack.createBackgroundContext()
            
            let fetchrequest: NSFetchRequest<BringListChangeEntity> = NSFetchRequest<BringListChangeEntity>(entityName: "BringListChangeEntity")
            fetchrequest.predicate = NSPredicate(format: "uuid == %@", uuid)
            
            backgroundContext.performAndWait {
                do {
                    let result = try backgroundContext.fetch(fetchrequest)
                    let changes = self.createListChanges(from: result, context: backgroundContext)
                    callback(changes)
                } catch {
                    callback([])
                }
            }
        }
    }
    
    public func add(_ listChange: BringListChange, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void) {
        
        serialQueue.async {
            let backgroundContext = self.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                let serverRequest = BringListChangeEntity(usedContext: backgroundContext)
                serverRequest.location = listChange.location
                serverRequest.operation = listChange.operation.rawValue
                serverRequest.itemUuid = listChange.uuid
                serverRequest.itemId = listChange.itemId
                serverRequest.specification = listChange.spec
                serverRequest.uuid = listChange.listUuid
                serverRequest.timestamp = listChange.timeStamp
                
                self.coreDataStack.save(backgroundContext)
            }
            
            callback(Result.success(()))
        }
    }
    
    public func add(_ listChanges: [BringListChange], callback: @escaping (Result<Void, Error>) -> Void) {
        
        serialQueue.async {
            let backgroundContext = self.coreDataStack.createBackgroundContext()
            
            backgroundContext.performAndWait {
                
                for listChange in listChanges {
                    let serverRequest = BringListChangeEntity(usedContext: backgroundContext)
                    serverRequest.location = listChange.location
                    serverRequest.operation = listChange.operation.rawValue
                    serverRequest.itemUuid = listChange.uuid
                    serverRequest.itemId = listChange.itemId
                    serverRequest.specification = listChange.spec
                    serverRequest.uuid = listChange.listUuid
                    serverRequest.timestamp = listChange.timeStamp
                }
                
                self.coreDataStack.save(backgroundContext)
            }
            
            callback(Result.success(()))
        }
        
    }
    
    public func deleteChanges(forListUuid uuid: String, syncDate: Date, isWaiting: Bool, callback: @escaping (Result<Void, Error>) -> Void) {
        serialQueue.async {
            let backgroundContext = self.coreDataStack.createBackgroundContext()
            let fetchrequest: NSFetchRequest<BringListChangeEntity> = NSFetchRequest<BringListChangeEntity>(entityName: "BringListChangeEntity")
            fetchrequest.predicate = NSPredicate(format: "uuid == %@ AND timestamp <= %lf", uuid, syncDate.timeIntervalSince1970)
            
            backgroundContext.performAndWait {
                do {
                    let result = try backgroundContext.fetch(fetchrequest)
                    
                    for serverRequest in result {
                        backgroundContext.delete(serverRequest)
                    }
                    
                    self.coreDataStack.save(backgroundContext)
                } catch let error {
                    callback(Result.failure(error))
                }
            }
            
            callback(Result.success(()))
        }
    }
    
    private func createListChanges(from entities: [BringListChangeEntity], context: NSManagedObjectContext) -> [BringListChange] {
        var changes = [BringListChange]()
        
        for entity in entities {
            if let change = BringListChange(serverRequest: entity) {
                changes.append(change)
            } else {
                if entity.requestId != nil {
                    context.delete(entity)
                } else {
                    if let change =  BringListChange.migrate(entity) {
                        changes.append(change)
                    }
                }
            }
        }
        return changes
    }
}

fileprivate extension BringListChange {

    /// Migrates a Bring 3.x `BringServerRequest` to a Bring 4.0 `BringListChange`.
    ///
    /// - Parameter serverRequest: The request to migrate
    /// - Returns: A migrated `BringListChange` or `nil` if the migration failed.
    static func migrate(_ serverRequest: BringListChangeEntity) -> BringListChange? {

        var operation: BringListContentOperation?
        var itemId: String?

        if serverRequest.purchase != nil {
            operation = BringListContentOperation.toPurchase
            itemId = serverRequest.purchase
        } else if serverRequest.recently != nil {
            operation = BringListContentOperation.toRecently
            itemId = serverRequest.recently
        } else if serverRequest.remove != nil {
            operation = BringListContentOperation.remove
            itemId = serverRequest.remove
        }

        guard let validOperation = operation else {
            return nil
        }

        guard let validItemId = itemId else {
            return nil
        }

        guard let listUuid = serverRequest.uuid else {
            return nil
        }

        return BringListChange(
            uuid: UUID().uuidString,
            listUuid: listUuid,
            itemId: validItemId,
            spec: serverRequest.specification ?? "",
            location: serverRequest.location as? CLLocation,
            operation: validOperation,
            timeStamp: serverRequest.timestamp)
    }
}

extension BringListChange {
    @objc convenience init?(serverRequest: BringListChangeEntity) {
        
        guard let operation = BringListContentOperation(rawValue: serverRequest.operation ?? "") else {
            return nil
        }
        guard let itemId = serverRequest.itemId else {
            return nil
        }
        guard let listUuid = serverRequest.uuid else {
            return nil
        }
        
        guard let itemUuid = serverRequest.itemUuid else {
            return nil
        }
        
        
        self.init(uuid: itemUuid, listUuid: listUuid, itemId: itemId, spec: serverRequest.specification ?? "", location: serverRequest.location as? CLLocation, operation: operation, timeStamp: serverRequest.timestamp)
    }
}
