//
//  BringLocaleHelper.swift
//  Bring
//
//  Created by Sascha Thöni on 05.12.16.
//
//

import Foundation
#if os(iOS)
    import CoreTelephony
#endif

public protocol LanguageProvider {
    func getLanguageCode() -> String
    func getCountryCode() -> String
    func getArticleLanguage() -> String
}

// TODO: Move this class to a core framework!
@objcMembers
public class BringLocaleHelper: NSObject, LanguageProvider {
    
    public static let FALLBACK_LANGUAGE_CODE = "en"
    public static let FALLBACK_COUNTRY_CODE = "US"
    public static let FALLBACK_LOCALE = "\(FALLBACK_LANGUAGE_CODE)-\(FALLBACK_COUNTRY_CODE)"
    
    public static let sharedInstance = BringLocaleHelper()
    
    #if os(iOS)
        private var networkInfo = CTTelephonyNetworkInfo()
    #endif
    
    private override init() {
        super.init()
    }
    
    public func getCountryCodeFromCarrier() -> String? {
        
        #if os(iOS)
            
            guard let carrier = networkInfo.subscriberCellularProvider else {
                return nil
            }
            guard let countryCode = carrier.isoCountryCode else {
                return nil
            }
            
            return countryCode.uppercased()

        #else
            return nil
        #endif
    }
    
    public func getCountryCodeFromLocale() -> String? {
        
        guard let countryCode = NSLocale.current.regionCode else {
            return nil
        }
        
        return countryCode.uppercased()
    }

    public func getCountryCode() -> String {
        // FIXME: We removed the mock here. Add this later back again!
        if let countryCode = getCountryCodeFromCarrier() {
            return countryCode
        }else if let countryCode = getCountryCodeFromLocale() {
            return countryCode
        } else {
            return BringLocaleHelper.FALLBACK_COUNTRY_CODE
        }
    }
    
    public func getLanguageCodeFromLocale() -> String {
        if let language = NSLocale.current.languageCode {
            return language.lowercased()
        } else {
            return BringLocaleHelper.FALLBACK_LANGUAGE_CODE
        }
    }
    
    public func getLanguageCode() -> String {
        let localeIdentifier = NSLocale.preferredLanguages[0]
        let localeComponents = NSLocale.components(fromLocaleIdentifier: localeIdentifier)
        
        // get preferred language if available
        if let language = localeComponents["kCFLocaleLanguageCodeKey"] {
            return language.lowercased()
        }
        // get language from currently set region/locale
        else {
            return getLanguageCodeFromLocale()
        }
    }
    
    public func getLocale(forArticleLanguage articleLanguage : String) -> String {
        switch (articleLanguage) {
        case "de":
            return "de-DE"
        case "pt":
            return "pt-BR"
        case "es":
            return "es-ES"
        case "it":
            return "it-IT"
        case "fr":
            return "fr-FR"
        case "nl":
            return "nl-NL"
        case "ru":
            return "ru-RU"
        case "tr":
            return "tr-TR"
        case "hu":
            return "hu-HU"
        case "sv":
            return "sv-SE"
        case "nb", "nn", "no":
            return "nb-NO"
        case "pl":
            return "pl-PL"
        case "de-CH", "de-AT", "en-US", "en-CA", "en-AU", "en-GB", "it-CH", "fr-CH" :
            return articleLanguage
        default:
            return BringLocaleHelper.FALLBACK_LOCALE
        }
    }
    
    public func getLocaleKey(forArticleLanguage articleLanguage : String) -> String {
        
        let locale = self.getLocale(forArticleLanguage: articleLanguage)
        return locale.replacingOccurrences(of: "-", with: "_").uppercased()
    }
    
    public func getArticleLanguage(forLocale localeString: String) -> String {
        var languageCode = BringLocaleHelper.FALLBACK_LANGUAGE_CODE // default
        var countryCode = BringLocaleHelper.FALLBACK_COUNTRY_CODE // default
        
        if localeString.contains("-") {
            let components = localeString.components(separatedBy: "-")
            if components.count == 2 && !components[0].isEmpty && !components[1].isEmpty{
                languageCode = components[0]
                countryCode = components[1]
            }
        }
    
        return getArticleLanguage(languageCode: languageCode, countryCode: countryCode)
    }
    
    public func getLanguageCountryCode() -> String {
        return String.init(format: "%@-%@", self.getLanguageCode(), self.getCountryCode())
    }
    
    public func getArticleLanguage(languageCode : String, countryCode : String) -> String {
        let languageCountryCode = String.init(format: "%@-%@", languageCode.lowercased(), countryCode.uppercased())
        
        switch (languageCountryCode) {
        case "de-CH", "de-AT", "en-US", "en-CA", "en-AU", "en-GB", "it-CH", "fr-CH" :
            return languageCountryCode
        default:
            switch languageCode {
            case "gsw":
                return "de-CH"
            case "de", "pt", "es", "it", "fr", "nl", "ru", "tr", "hu", "sv", "nb", "nn", "no", "pl":
                return languageCode
            default:
                return BringLocaleHelper.FALLBACK_LOCALE
            }
        }
    }
    
    public func getArticleLanguage() -> String {
        let languageCode = self.getLanguageCode()
        if let countryCode = self.getCountryCodeFromLocale(){
            return getArticleLanguage(languageCode: languageCode, countryCode: countryCode)
        }
        return BringLocaleHelper.FALLBACK_LOCALE
    }
    
    /// Returns the language code of the set cataloge language
    public func getArticleLanguageCode(from catalogLanguage: String) -> String {
        var languageCode = BringLocaleHelper.FALLBACK_LANGUAGE_CODE
        
        if catalogLanguage.contains("-") {
            let components = catalogLanguage.components(separatedBy: "-")
            if components.count >= 1 && !components[0].isEmpty {
                languageCode = components[0]
            }
        } else if catalogLanguage.count == 2 {
            if catalogLanguage.lowercased() == catalogLanguage {
                return catalogLanguage
            }
        }
        
        return languageCode
    }
}
