//
//  BringCatalogLoader.swift
//  Bring
//
//  Created by Renato Stauffer on 13.02.19.
//

import Foundation

/// The BringCatalogLoader can loads sections and items from different data sources and merge these together. `BringCatalogLoaderDelegate`s can listen to these events. The data sources load their content according to the order they have been injected to the constructor of the `BringCatalogLoader`.
public final class BringCatalogLoader {
    
    private var dataSources = [CatalogDataSource]()
    private let baseDataSource: BaseCatalogDataSource
    private let itemDetailStore: ListItemDetailStoreDao
    
    /// Initializes the BringCatalogLoader
    ///
    /// - Parameter dataSources: The data sources which load sections and items. Sections and items get loaded according to the order in the `dataSources` array.
    public init(dataSources: [CatalogDataSource], baseDataSource: BaseCatalogDataSource, itemDetailStore: ListItemDetailStoreDao = BringCoreDataListItemDetailStore.shared) {
        self.dataSources = dataSources
        self.itemDetailStore = itemDetailStore
        self.baseDataSource = baseDataSource
    }
    
    // MARK: Public API
    
    public func loadCatalog(ForList listUuid: String, callback: @escaping ([BringSection], BringCatalogChange) -> Void) {
        
        let itemDetails = self.itemDetailStore.get(with: BringListItemDetail.predicate(withListUuid: listUuid))
        
        self.loadBaseDataSource(baseDataSource) { sections in
            var catalogChanges = [BringCatalogChange]()
            let dispatchGroup = DispatchGroup()
            
            self.dataSources.forEach({ (dataSource) in
                
                dispatchGroup.enter()
                
                dataSource.loadCatalogChange(callback: { (catalogChange) in
                    catalogChanges.append(catalogChange)
                    dispatchGroup.leave()
                })
            })
            
            dispatchGroup.notify(queue: .main) {
                let catalogChange = self.flatten(catalogChanges)
                let enchancedCatalogChange = self.enhance(catalogChange, withItemDetails: itemDetails)
                callback(sections, enchancedCatalogChange)
            }
        }
        
    }
    
    // MARK: Private methods
    
    private func enhance(_ catalogChange: BringCatalogChange, withItemDetails itemDetails: [BringListItemDetail]) -> BringCatalogChange {
        
        var itemsToAdd = [BringCatalogChange.ItemToAdd]()
        
        for itemToAdd in catalogChange.itemsToAdd {
            
            guard let itemDetail = findItemDetail(for: itemToAdd, itemDetails: itemDetails) else {
                itemsToAdd.append(itemToAdd)
                continue
            }
            
            let userSection = itemDetail.userSectionId.isEmpty ? BringCatalogProvider.USER_SECTION_KEY : itemDetail.userSectionId
            itemsToAdd.append(BringCatalogChange.ItemToAdd(item: itemToAdd.item, sectionId: userSection))
        }
        
        return BringCatalogChange(itemsToAdd: itemsToAdd, sectionsToAdd: catalogChange.sectionsToAdd, translationsToAdd: catalogChange.translationsToAdd, iconsToAdd: catalogChange.iconsToAdd, itemsToRemove: catalogChange.itemsToRemove)
    }
    
    private func findItemDetail(for itemToAdd: BringCatalogChange.ItemToAdd, itemDetails: [BringListItemDetail]) -> BringListItemDetail? {
        return itemDetails.first { (itemDetail) -> Bool in
            itemDetail.itemKey == itemToAdd.item.itemId
        }
    }
    
    private func flatten(_ catalogChanges: [BringCatalogChange]) -> BringCatalogChange {
        let allItemsToAdd = catalogChanges
            .map { $0.itemsToAdd }
            .flatMap { $0 }
        
        let allSectionsToAdd = catalogChanges
            .map { $0.sectionsToAdd }
            .flatMap { $0 }
        
        let allTranslationsToAdd = catalogChanges
            .map { $0.translationsToAdd }
            .flatMap { $0 }
        
        let allIconsToAdd = catalogChanges
            .map { $0.iconsToAdd }
            .flatMap { $0 }
        
        let allItemsToRemove = catalogChanges
            .map { $0.itemsToRemove }
            .flatMap { $0 }
        
        return BringCatalogChange(
            itemsToAdd: allItemsToAdd,
            sectionsToAdd: allSectionsToAdd,
            translationsToAdd: allTranslationsToAdd,
            iconsToAdd: allIconsToAdd,
            itemsToRemove: allItemsToRemove
        )
    }
    
    private func loadBaseDataSource(_ dataSource: BaseCatalogDataSource, callback: @escaping ([BringSection]) -> Void) {
        var sections = [BringSection]()
        
        dataSource.loadSections { loadedSections in
            sections.append(contentsOf: loadedSections)
            callback(sections)
        }
    }
}


/// Class to cast messages to multiple delegates
public final class MulticastDelegate<ProtocolType> {
    
    /// Wrapper class to be able to store weak references of `ProtocolType` delegates
    private class DelegateWrapper {
        
        weak var delegate: AnyObject?
        
        init(_ delegate: AnyObject) {
            self.delegate = delegate
        }
    }
    
    private var delegateWrappers: [DelegateWrapper]
    
    // MARK: - Public API
    
    public var delegates: [ProtocolType] {
        delegateWrappers = delegateWrappers
            .filter { $0.delegate != nil }
        
        // swiftlint:disable force_cast
        return delegateWrappers.map { $0.delegate! } as! [ProtocolType]
    }
    
    public init(delegates: [ProtocolType] = []) {
        delegateWrappers = delegates.map {
            DelegateWrapper($0 as AnyObject)
        }
    }
    
    public func add(_ delegate: ProtocolType) {
        let wrapper = DelegateWrapper(delegate as AnyObject)
        delegateWrappers.append(wrapper)
    }
    
    public func remove(_ delegate: ProtocolType) {
        guard let index = delegateWrappers.index(where: {
            $0.delegate === (delegate as AnyObject)
        }) else {
            Log.w("Could not find the delegate to remove")
            return
        }
        delegateWrappers.remove(at: index)
    }
    
    public func notifyDelegates(_ closure: (ProtocolType) -> ()) {
        delegates.forEach { closure($0) }
    }
    
}
