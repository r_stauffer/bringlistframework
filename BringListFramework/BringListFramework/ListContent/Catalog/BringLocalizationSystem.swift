//
//  File.swift
//  
//
//  Created by Renato Stauffer on 05.08.19.
//

import Foundation

public protocol TranslationProvider {
    func localizedString(forKey key: String) -> String
    func enrichLocalizationSystem(_ localizations: [String: String])
    func setArticleLanguage(_ articleLanguage: String)
    func getAllKeys(for language: String) -> [String]
    func defaultDeviceArticleLanguage() -> String
    func getAdditionalTranslations() -> [String: String]
}

@objc public class BringLocalizationSystem: NSObject, TranslationProvider {
    
    @objc public static let shared = BringLocalizationSystem()
    
    private var currentBundle = Bundle(identifier: Constants.Project.bundleIdentifier)!
    private var providedTranslations: [String: String]
    private var catalogName: String
    
    private override init() {
        catalogName = "Articles"
        providedTranslations = [String: String]()
        super.init()
    }
    
    public func getAdditionalTranslations() -> [String: String] {
        return providedTranslations
    }
    
    public func registerCatalog(withName name: String) {
        catalogName = name
    }
    
    @objc public func localizedString(forKey key: String) -> String {
        if let providedTranslation = self.providedTranslations[key] {
            return providedTranslation
        } else {
            return currentBundle.localizedString(forKey: key, value: nil, table: catalogName)
        }
    }
    
    public func enrichLocalizationSystem(_ localizations: [String: String]) {
        providedTranslations = localizations
    }
    
    @objc public func setArticleLanguage(_ articleLanguage: String) {
        guard let bundle = bundle(for: articleLanguage) else {
            reset()
            return
        }
        
        currentBundle = bundle
    }
    
    private func bundle(for language: String) -> Bundle? {
        guard let path = Bundle(for: type(of: self)).path(forResource: language, ofType: "lproj") else {
            return nil
        }
        
        guard let bundle = Bundle(path: path) else {
            return nil
        }
        
        return bundle
    }
    
    public func reset() {
        currentBundle = Bundle(for: type(of: self))
        providedTranslations = [:]
    }
    
    public func itemId(for itemName: String, in language: String) -> String? {
        let articles = self.articles(in: language)
        return articles.first { $0.value == itemName }?.key
    }
    
    public func key(forItem itemName: String, language: String) -> String? {
        let allArticles = articles(in: language)
        
        let itemName = itemName.lowercased()
        let index = itemName.index(itemName.endIndex, offsetBy: -1)
        let itemNameSingular = String(itemName[..<index])
        
        var item: String? = nil
        var singular: String? = nil
        
        for key in allArticles.keys {
            
            let localizedName = allArticles[key]?.lowercased()
            
            if localizedName == itemName {
                item = key
                break
            } else if localizedName == itemNameSingular {
                singular = key
                break
            }
        }
        
        if item != nil {
            return item
        } else if (singular != nil) {
            return singular
        } else {
            return nil
        }
    }
    
    public func articles(in language: String) -> [String: String] {
        guard let bundle = bundle(for: language), let translations = bundle.path(forResource: self.catalogName, ofType: "strings") else {
            return [:]
        }
        
        guard let dictionary = NSDictionary(contentsOfFile: translations) as? [String: String] else {
            assertionFailure("We should always be able to convert this")
            return [:]
        }
        
        return dictionary
        
    }
    
    public func getAllKeys(for language: String) -> [String] {
        var returnValue = [String]()
        
        let keys = self.providedTranslations.keys
        returnValue.append(contentsOf: keys)
        
        let currentArticles = articles(in: language)
        returnValue.append(contentsOf: currentArticles.keys)
        
        return returnValue
    }
    
    @objc public func defaultDeviceArticleLanguage() -> String {
        return BringLocaleHelper.sharedInstance.getArticleLanguage()
    }
}
