//
//  BringListContentLoader.swift
//  Bring
//
//  Created by Renato Stauffer on 29.01.19.
//

import Foundation

/// Loads sections and items from a data source.
public protocol CatalogDataSource {
    
    func loadCatalogChange(callback: @escaping (BringCatalogChange) -> Void)
    
    /// Indicates if a data source contributes to the default catalog.
    var isDefaultCatalogDataSource: Bool { get }
}

public protocol BaseCatalogDataSource {
    func loadSections(callback: @escaping ([BringSection]) -> Void)
}

public class BringCatalogChange {
    
    public typealias ItemToRemove = String
    
    public struct ItemToAdd {
        public let item: BringItem
        public let sectionId: String
        
        public init(item: BringItem, sectionId: String) {
            self.item = item
            self.sectionId = sectionId
        }
    }
    
    public struct SectionToAdd {
        public let position: Int
        public let sectionId: String
        public let name: String
        public let itemIds: [String]
        
        public init(position: Int, sectionId: String, name: String, itemIds: [String]) {
            self.position = position
            self.sectionId = sectionId
            self.name = name
            self.itemIds = itemIds
        }
    }
    
    public struct IconToAdd {
        public let itemId: String
        public let base64: String
        
        public init(itemId: String, base64: String) {
            self.itemId = itemId
            self.base64 = base64
        }
    }
    
    public struct TranslationToAdd {
        public let itemId: String
        public let itemName: String
        
        public init(itemId: String, itemName: String) {
            self.itemId = itemId
            self.itemName = itemName
        }
    }
    
    public let itemsToAdd: [ItemToAdd]
    public let sectionsToAdd: [SectionToAdd]
    public let translationsToAdd: [TranslationToAdd]
    public let iconsToAdd: [IconToAdd]
    public let itemsToRemove: [ItemToRemove]
    
    public init(itemsToAdd: [ItemToAdd] ,sectionsToAdd: [SectionToAdd], translationsToAdd: [TranslationToAdd], iconsToAdd: [IconToAdd], itemsToRemove: [ItemToRemove]) {
        self.itemsToAdd = itemsToAdd
        self.sectionsToAdd = sectionsToAdd
        self.translationsToAdd = translationsToAdd
        self.iconsToAdd = iconsToAdd
        self.itemsToRemove = itemsToRemove
    }
    
    public static func empty() -> BringCatalogChange {
        return BringCatalogChange(itemsToAdd: [], sectionsToAdd: [], translationsToAdd: [], iconsToAdd: [], itemsToRemove: [])
    }
    
    static func flatten(_ catalogChanges: [BringCatalogChange]) -> BringCatalogChange {
        let allItemsToAdd = catalogChanges
            .map { $0.itemsToAdd }
            .flatMap { $0 }
        
        let allSectionsToAdd = catalogChanges
            .map { $0.sectionsToAdd }
            .flatMap { $0 }
        
        let allTranslationsToAdd = catalogChanges
            .map { $0.translationsToAdd }
            .flatMap { $0 }
        
        let allIconsToAdd = catalogChanges
            .map { $0.iconsToAdd }
            .flatMap { $0 }
        
        let allItemsToRemove = catalogChanges
            .map { $0.itemsToRemove }
            .flatMap { $0 }
        
        return BringCatalogChange(
            itemsToAdd: allItemsToAdd,
            sectionsToAdd: allSectionsToAdd,
            translationsToAdd: allTranslationsToAdd,
            iconsToAdd: allIconsToAdd,
            itemsToRemove: allItemsToRemove
        )
    }
}
