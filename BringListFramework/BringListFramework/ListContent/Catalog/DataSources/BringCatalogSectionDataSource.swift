//
//  BringCatalogSectionDataSource.swift
//  Bring
//
//  Created by Renato Stauffer on 28.01.19.
//

import Foundation

/// Data source for the default catalog sections.
public class BringCatalogSectionDataSource: BaseCatalogDataSource {

    private let catalogProvider: BringCatalogProvider
    private let itemDetailStore: ListItemDetailStoreDao
    private let settingsProvider: ListContentSettingsProvider
    
    public init(catalogProvider: BringCatalogProvider = BringCatalogProvider.shared, itemDetailStore: ListItemDetailStoreDao = BringCoreDataListItemDetailStore.shared, settingsProvider: ListContentSettingsProvider) {
        self.catalogProvider = catalogProvider
        self.itemDetailStore = itemDetailStore
        self.settingsProvider = settingsProvider
    }
    
    // MARK: CatalogDataSource
    
    public func loadSections(callback: @escaping ([BringSection]) -> Void) {
        
        guard let listUuid = self.settingsProvider.seletectedList() else {
            callback([])
            return
        }
        
        guard let articleLanguage = settingsProvider.articleLanguage(forList: listUuid) else {
            callback([])
            return
        }
        
        let sections = self.catalogProvider.getCatalog(forLanguage: articleLanguage)
        
        for itemDetail in self.itemDetailStore.get(with: BringListItemDetail.predicate(withListUuid: listUuid)) {
            
            guard let section = self.getSection(sections, sectionId: itemDetail.userSectionId) else {
                continue
            }
            
            guard let item = self.getItem(sections, itemId: itemDetail.itemKey) else {
                continue
            }
            
            guard section.items.contains(item) == false else {
                continue
            }
            
            for current in sections {
                current.remove(item: item)
            }
            
            section.add(item: item)
            
            section.sort()
        }
        
        callback(sections)
    }
    
    private func getSection(_ sections: [BringSection], sectionId: String) -> BringSection? {
        
        return sections.first { (section) -> Bool in
            return section.sectionId == sectionId
        }
    }
    
    private func getItem(_ sections: [BringSection], itemId: String) -> BringItem? {
        
        for section in sections {
            
            guard let item = getItem(section: section, itemId: itemId) else {
                continue
            }
            
            return item
        }
        
        return nil
    }
    
    private func getItem(section: BringSection, itemId: String) -> BringItem? {
        
        return section.items.first { (item) -> Bool in
            return item.itemId == itemId
        }
    }
    
    var isDefaultCatalogDataSource: Bool {
        return true
    }
}
