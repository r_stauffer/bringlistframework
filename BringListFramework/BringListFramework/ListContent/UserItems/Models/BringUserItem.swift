//
//  BringUserItem.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation

public struct BringUserItem {
    public let itemKey: String
    
    public init(itemKey: String) {
        self.itemKey = itemKey
    }
}
