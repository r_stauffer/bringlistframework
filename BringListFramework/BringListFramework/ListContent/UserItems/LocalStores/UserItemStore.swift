//
//  UserItemStore.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation

public protocol UserItemStore {
    func create(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void)
    func remove(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void)
    func getAllUserItems(callback: @escaping (Result<[BringUserItem], Error>) -> Void)
    func create(_ itemKeys: [String], callback: @escaping (Result<Void, Error>) -> Void)
    func removeAll(callback: @escaping (Result<Void, Error>) -> Void)
}
