//
//  BringCoreDataUserItemStore.swift
//  BringStandaloneWatch WatchKit Extension
//
//  Created by Renato Stauffer on 08.08.19.
//

import Foundation
import CoreData

public enum BringCoreDataUserItemDaoError: Error {
    case unknownEntity
}

public final class BringCoreDataUserItemStore: UserItemStore {

    private let coreDataStack: BringCoreDataListsStack
    
    private let serialQueue = OperationQueue()
    
    public static let shared = BringCoreDataUserItemStore()
    
    public init(coreDataStack: BringCoreDataListsStack = BringCoreDataListsStackFactory().makeCoreDataStack()) {
        self.coreDataStack = coreDataStack
        self.serialQueue.maxConcurrentOperationCount = 1
    }

    public func create(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void) {
        serialQueue.addOperation {
            
            let context = self.coreDataStack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    let fetchreqeust: NSFetchRequest<BringUserItemEntity> = NSFetchRequest<BringUserItemEntity>(entityName: "BringUserItemEntity")
                    fetchreqeust.predicate = NSPredicate(format: "itemKey == %@", itemKey)
                    let result = try context.fetch(fetchreqeust)
                    
                    if result.count == 0 {
                        
                        guard let userItemEntity = NSEntityDescription.insertNewObject(forEntityName: "BringUserItemEntity", into: context) as? BringUserItemEntity else {
                            assertionFailure("Entity unknown!")
                            callback(Result.failure(BringCoreDataUserItemDaoError.unknownEntity))
                            return
                        }
                        
                        userItemEntity.itemKey = itemKey
                        
                        self.coreDataStack.save(context)
                        
                        DispatchQueue.main.async {
                            callback(Result.success(()))
                        }
                    } else {
                        DispatchQueue.main.async {
                            callback(Result.success(()))
                        }
                    }
                    
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
            
        }
    }
    
    public func create(_ itemKeys: [String], callback: @escaping (Result<Void, Error>) -> Void) {
        
        serialQueue.addOperation {
            
            let context = self.coreDataStack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    for itemKey in itemKeys {
                        let fetchreqeust: NSFetchRequest<BringUserItemEntity> = NSFetchRequest<BringUserItemEntity>(entityName: "BringUserItemEntity")
                        fetchreqeust.predicate = NSPredicate(format: "itemKey == %@", itemKey)
                        let result = try context.fetch(fetchreqeust)

                        if result.count == 0 {
                            guard let userItemEntity = NSEntityDescription.insertNewObject(forEntityName: "BringUserItemEntity", into: context) as? BringUserItemEntity else {
                                assertionFailure("Entity unknown!")
                                DispatchQueue.main.async {
                                    callback(Result.failure(BringCoreDataUserItemDaoError.unknownEntity))
                                }
                                return
                            }
                            userItemEntity.itemKey = itemKey
                        }
                    }

                    self.coreDataStack.save(context)
                    
                    DispatchQueue.main.async {
                        callback(Result.success(()))
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
        }
    }
    
    public func remove(_ itemKey: String, callback: @escaping (Result<Void, Error>) -> Void) {
        serialQueue.addOperation {
            
            let context = self.coreDataStack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    let fetchreqeust: NSFetchRequest<BringUserItemEntity> = NSFetchRequest<BringUserItemEntity>(entityName: "BringUserItemEntity")
                    fetchreqeust.predicate = NSPredicate(format: "itemKey == %@", itemKey)
                    let result = try context.fetch(fetchreqeust)
                    
                    for userItem in result {
                        context.delete(userItem)
                    }
                    
                    self.coreDataStack.save(context)
                    
                    DispatchQueue.main.async {
                        callback(Result.success(()))
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
        }
    }

    public func getAllUserItems(callback: @escaping (Result<[BringUserItem], Error>) -> Void) {
        serialQueue.addOperation {
            
            let context = self.coreDataStack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    let fetchreqeust: NSFetchRequest<BringUserItemEntity> = NSFetchRequest<BringUserItemEntity>(entityName: "BringUserItemEntity")
                    let result = try context.fetch(fetchreqeust)
                    let userItems = result.compactMap { BringUserItem(entity: $0) }
                    
                    DispatchQueue.main.async {
                        callback(Result.success(userItems))
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
        }
    }

    public func removeAll(callback: @escaping (Result<Void, Error>) -> Void) {
        serialQueue.addOperation {
            
            let context = self.coreDataStack.createBackgroundContext()

            context.performAndWait {
                do {
                    let fetchreqeust: NSFetchRequest<BringUserItemEntity> = NSFetchRequest<BringUserItemEntity>(entityName: "BringUserItemEntity")
                    let result = try context.fetch(fetchreqeust)

                    for object in result {
                        context.delete(object)
                    }

                    self.coreDataStack.save(context)
                    
                    DispatchQueue.main.async {
                        callback(Result.success(()))
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
        }
    }
}

fileprivate extension BringUserItem {
    init?(entity: BringUserItemEntity) {
        guard let itemKey = entity.itemKey else {
            return nil
        }
        self.init(itemKey: itemKey)
    }
}
