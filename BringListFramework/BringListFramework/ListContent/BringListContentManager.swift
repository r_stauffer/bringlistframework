//
//  BringListManager.swift
//  Bring
//
//  Created by Renato Stauffer on 16.01.19.
//

import Foundation

@objc public protocol ListContentSettingsProvider: class {
    func articleLanguage(forList listUuid: String) -> String?
    func userSectionOrder(forList listUuid: String) -> [String]?
    func seletectedList() -> String?
    func userUuid() -> String?
    func defaultList() -> String?
}

class BringListContentSettingsProvider: ListContentSettingsProvider {
    
    static let shared = BringListContentSettingsProvider()
    fileprivate var wrappedValue: ListContentSettingsProvider?
    
    func articleLanguage(forList listUuid: String) -> String? {
        return wrappedValue?.articleLanguage(forList: listUuid)
    }
    
    func userSectionOrder(forList listUuid: String) -> [String]? {
        return wrappedValue?.userSectionOrder(forList: listUuid)
    }
    
    func seletectedList() -> String? {
        return wrappedValue?.seletectedList()
    }
    
    func userUuid() -> String? {
        return wrappedValue?.userUuid()
    }
    
    func defaultList() -> String? {
        return wrappedValue?.defaultList()
    }
    
}

// We do a lot of copy() ! <<Type>>. So it makes snese to disable this here.
// swiftlint:disable force_cast

/// Responsible to manage the content of the currently selected `BringList`. The `BringListContentManager` gathers `BringItem`s and `BringSection`s from different sources and handles iteractions on these sections / items.
@objcMembers public class BringListContentManager: NSObject, BringModelManagable {
    
    public static let shared = BringListContentManager()
    public static let itemsChangedNotification = "kBringListItemsChangedNotification"
    private static var catalogLoader: BringCatalogLoader!
    private static var settingsProvider: ListContentSettingsProvider!
    private static var currentMaxRecently = BringListContent.maxItemsInRecentlyDefaultValue
    fileprivate var currentModel: BringListContent
    private let locationService: LocationService
    private let listSyncManager: ListChangeSyncer
    private let userItemStore: UserItemStore
    private let listSettings: BringListSettings
    private var listeners = [BringModelDelegate]()
    private let listContentReducer: BringListContentReducer
    private let localizationSystem: TranslationProvider
    private var currentlyLoadedCatalogChange: BringCatalogChange?
    private let logger: BringRemoteLoggingManager
    
    public var toBePurchasedItemCount: Int {
        return currentModel.getToBePurchasedSection().items.count
    }
    
    /// Indicates if the list state (purchase and recently) has been loaded from the local store.
    private var didLoadListState: Bool = false
    
    // MARK: Init
    
    public init(locationService: LocationService = BringLocationManager.shared, listSyncManager: ListChangeSyncer = BringListSyncManager.shared, userItemStore: UserItemStore = BringCoreDataUserItemStore.shared, listSettings: BringListSettings = BringListSettings.standard, listContentReducer: BringListContentReducer = BringListContentReducer(), localizationSystem: TranslationProvider = BringLocalizationSystem.shared, logger: BringRemoteLoggingManager = BringRemoteLoggingManager.shared) {
        assert(BringListContentManager.catalogLoader != nil)
        self.locationService = locationService
        self.listSyncManager = listSyncManager
        self.userItemStore = userItemStore
        self.listSettings = listSettings
        self.logger = logger
        self.listContentReducer = listContentReducer
        self.localizationSystem = localizationSystem
        
        if let listState = listSyncManager.getCurrentListState(forList: BringListContentManager.settingsProvider.seletectedList() ?? "") {
            
            let emptyModel = BringListContent(sections: [], recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(), recommendedSection: BringListContent.createRecommendedSection(), toBeBurchasedSection: BringListContent.createToBePurchasedSection(), listStatus: .unregistered, listUuid: BringListContentManager.settingsProvider.seletectedList() ?? "", maxItemsInRecently: BringListContentManager.currentMaxRecently)
            
            let updateAction = BringListContentUpdateAction(currentListState: listState, userItemsToAdd: [], sectionOrder: [], syncStartDate: Date(), translations: localizationSystem.getAdditionalTranslations())
            currentModel = listContentReducer.reduce(emptyModel, action: updateAction)
        } else {
            self.currentModel = BringListContent(sections: [], recentlyPurchasedSection: BringListContent.createRecentlyPurchasedSection(), recommendedSection: BringListContent.createRecommendedSection(), toBeBurchasedSection: BringListContent.createToBePurchasedSection(), listStatus: .unregistered, listUuid: BringListContentManager.settingsProvider.seletectedList() ?? "", maxItemsInRecently: BringListContentManager.currentMaxRecently)
        }
        
        super.init()
        self.listSyncManager.delegate = self
    }
    
    public static func setup(with catalogLoader: BringCatalogLoader, settingsProvider: ListContentSettingsProvider, maxRecently: Int) {
        BringListContentManager.currentMaxRecently = maxRecently
        BringListContentSettingsProvider.shared.wrappedValue = settingsProvider
        BringListContentManager.catalogLoader = catalogLoader
        BringListContentManager.settingsProvider = settingsProvider
        BringCatalogProvider.setup(settingsProvider: settingsProvider)
        BringAlamofireManager.sharedInstance.delegate = settingsProvider
    }
    
    // MARK: Public API
    
    /// Selects / deselects an item from the list content and updates all the corresponding sections accordingly. List content listeners will get notified about the list content change with a new copy of the list content. The change will be propagated to the remote server.
    ///
    /// - Parameter item: The item to select.
    @objc
    public func select(_ item: BringItem) {
        item.assignUuidIfNeeded()
        let resentlyItems = currentModel.getRecentlyPurchasedSection().items
        let itemToRemove = resentlyItems.count == currentModel.maxItemsInRecently ? resentlyItems.last : nil
        let selectAction = BringListContentSelectAction.init(item: item)
        self.currentModel = listContentReducer.reduce(currentModel, action: selectAction)
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: BringListContentManager.itemsChangedNotification)))
        self.modelChanged()
        self.pushListContentChangesToRemote(of: item, itemMovedFromRecentlyToCatalog: itemToRemove, onList: currentModel.listUuid)
    }
    
    // MARK: Public API
    
    /// Selects / deselects an item from the list content and updates all the corresponding sections accordingly. List content listeners will get notified about the list content change with a new copy of the list content. The change will be propagated to the remote server.
    ///
    /// - Parameter item: The item to select.
    @objc
    public func select(_ item: BringItem, onList listUuid: String) {
        item.assignUuidIfNeeded()
        
        if currentModel.listUuid == listUuid {
            self.select(item)
        } else {
            
            self.listSyncManager.getCurrentListState(forList: listUuid, callback: { (result) in
                switch result {
                case .success(let listState):
                    
                    let resentlyItems = listState.recently
                    
                    let itemToRemove = resentlyItems.count == self.currentModel.maxItemsInRecently ? resentlyItems.last : nil
                    
                    let bringItems = itemToRemove.map { BringItem(uuid: $0.uuid, itemId: $0.itemId) }
                    
                    self.pushListContentChangesToRemote(of: item, itemMovedFromRecentlyToCatalog: bringItems, onList: listUuid)
                case .failure(let error):
                    Log.ed(error)
                }
            })
        }
    }
    
    public func addItemToPurchase(_ item: BringItem) {
        item.assignUuidIfNeeded()
        item.touch()
        let action = BringListContentAddItemToPurchaseAction(item: item)
        self.currentModel = listContentReducer.reduce(self.currentModel, action: action)
        self.modelChanged()
        self.pushListContentChangesToRemote(of: item, itemMovedFromRecentlyToCatalog: nil, onList: currentModel.listUuid)
    }
    
    /// Removes the given `items` from `section`.
    ///
    /// - Parameters:
    ///   - items: The uuids of the items to remove
    ///   - section: The section id of the section to remove the items from.
    public func remove(_ items: [String], from section: String) {
        let action = BringListContentRemoveMultipleItemsAction(items: items, section: section)
        
        let itemsFromListContent = items.compactMap { [weak self] (uniqueIdentifier) in
            self?.currentModel.getRecentlyPurchasedSection().item(byUuid: uniqueIdentifier, orItemId: uniqueIdentifier)
        }
        
        self.currentModel = listContentReducer.reduce(self.currentModel, action: action)
        
        self.remove(items: itemsFromListContent, fromList: self.currentModel.listUuid)
        self.modelChanged()
    }
    
    /// Updates the recommended section.
    ///
    /// - Parameters:
    ///   - sectionName: The name of the new recommended section.
    ///   - items: The items for the recommended section.
    public func updateRecommendedSection(sectionName: String, items: [BringItem]) {
        let updateAction = BringListContentUpdateRecommendedAction(sectionName: sectionName, items: items)
        currentModel = listContentReducer.reduce(currentModel, action: updateAction)
    }
    
    private func remove(items: [BringItem], fromList listUuid: String) {
        var shouldLog = true
        
        let listChanges = items.compactMap { (item: BringItem) -> BringListChange? in
            guard let uuid = item.uuid else {
                logger.log(
                    cls: "BringListContentManager",
                    method: "removeItemsFromRemote(::)",
                    description: "Item does not have a uuid at a point where it should have one.",
                    issue: "itemId = \(item.itemId), listUuid = \(currentModel.listUuid)",
                    shouldLog: shouldLog
                )
                assertionFailure("We have to have a uuid at this point.")
                shouldLog = false
                return nil
            }
            
            return BringListChange(uuid: uuid, listUuid: listUuid, itemId: item.itemId, spec: item.specification, location: locationService.lastLocation, operation: .remove, timeStamp: Date().timeIntervalSince1970)
        }
        
        for listChange in listChanges {
            listSyncManager.sync(listChange, shouldSyncimmediately: false)
        }
    }
    
    /// Reorders the sections of the current list contents catalog.
    ///
    /// - Parameter newSectionOrder: The order which should be applied.
    public func reorderSections() {
        let reorderAction = BringListContentReorderSectionAction(sectionOrder: self.getSectionOrder())
        currentModel = listContentReducer.reduce(currentModel, action: reorderAction)
        modelChanged()
    }
    
    /// Moves an item to a specific section in the list content of the currently selected list.
    ///
    /// - Parameters:
    ///   - item: The item to move to the specified section.
    ///   - section: The section to move the item to.
    public func move(catalogItem: BringItem, toSection section: BringSection) {
        let moveAction = BringListContentMoveCatalogItemAction(itemToMove: catalogItem, sectionToMoveTo: section)
        currentModel = listContentReducer.reduce(currentModel, action: moveAction)
        self.modelChanged()
    }
    
    /// Returns a copy of the section which contains `item`.
    ///
    /// - Parameter item: The item to search the section for.
    /// - Returns: The section for the corresponding `item` or `nil` if no section is found
    public func section(of item: BringItem) -> BringSection? {
        return currentModel.getSections()
            .filter { (section) -> Bool in
                return section.items.contains(item) && section.isDefaultCatalogSection
            }
            .first?.copy() as! BringSection?
    }
    
    /// Adds an item to the specified section.
    ///
    /// - Parameters:
    ///   - item: The item to move to the specified section.
    ///   - sectionId: The id of the section to move the item to. If no section with the given `sectionId` is found. This method just returns without altering the list content.
    ///   - shouldNotifyListeners: A boolean indicating whether the listeners should be notified about this content change.
    public func add(_ item: BringItem, toSection sectionId: String, shouldNotifyListeners: Bool = false) {
        let action = BringListContentAddItemAction(item: item, sectionId: sectionId)
        currentModel = listContentReducer.reduce(currentModel, action: action)
        if shouldNotifyListeners {
            self.modelChanged()
        }
    }
    
    /// Enriches the list content with the specified `enrichment`
    ///
    /// - Parameter enrichment: The enrichment to apply
    func enrichListContent(catalogChange: BringCatalogChange) {
        addIconsToIconLoader(catalogChange: catalogChange)
        addTranslations(catalogChange.translationsToAdd)
        let catalogChangeAction = BringListContentCatalogChangeAction(catalogChange: catalogChange)
        currentModel = listContentReducer.reduce(currentModel, action: catalogChangeAction)
    }
    
    private func addIconsToIconLoader(catalogChange: BringCatalogChange) {
        var icons = [String: String]()
        
        for icon in catalogChange.iconsToAdd {
            icons[icon.itemId] = icon.base64
        }
        
        InMemoryIconLoader.shared.addImages(imagePerItem: icons)
    }
    
    private func addTranslations(_ translations: [BringCatalogChange.TranslationToAdd]) {
        
        var mutableDictionary = [String: String]()
        
        translations.forEach { (translation) in
            mutableDictionary[translation.itemId] = translation.itemName
        }
        
        let translationsFromAds = localizationSystem.getAdditionalTranslations()
        translationsFromAds.forEach { (k,v) in mutableDictionary[k] = v }
        
        localizationSystem.enrichLocalizationSystem(mutableDictionary)
    }
    
    /// Removes the given `item` from the list content catalog, to-be-purchased section and recently section. Listeners will get notified about this change.
    ///
    /// - Parameter item: The item to be removed.
    public func removeItems(withItemId itemId: String) {
        let action = BringListContentRemoveItemAction(itemId: itemId)
        currentModel = listContentReducer.reduce(currentModel, action: action)
        self.modelChanged()
    }
    
    /// Updates the `specification` of the item with the given `key` in the current list content. If there is no item found with the given key, the current list content is not altered.
    ///
    /// - Parameters:
    ///   - key: The key of the item to update the sepcification for.
    ///   - specification: The specification update.
    public func updateBringItemSpecification(forItemKey key: String, specification: String) {
        guard let item = self.item(byUniqueIdentifier: key) else {
            assertionFailure("There is no item in the current list content with id: \(key)")
            return
        }
        
        guard let uuid = item.uuid else {
            logger.log(
                cls: "BringListContentManager",
                method: "updateBringItemSpecification(::)",
                description: "Item does not have a uuid at a point where it should have one.",
                issue: "itemId = \(item.itemId), listUuid = \(currentModel.listUuid)"
            )
            assertionFailure("Item is not in purchase!")
            return
        }
        
        let updateSpecificationAction = BringListContentUpdateSpecificationAction(uniqueIdentifier: key, specification: specification)
        currentModel = listContentReducer.reduce(currentModel, action: updateSpecificationAction)
        
        modelChanged()
        
        let listChange = BringListChange(
            uuid: uuid,
            listUuid: currentModel.listUuid,
            itemId: item.itemId,
            spec: specification,
            location: nil,
            operation: .toPurchase,
            timeStamp: Date().timeIntervalSince1970
        )
        
        listSyncManager.sync(listChange, shouldSyncimmediately: false)
    }
    
    /// Switches the list content by loading the list state and loading the catalog data sources for the list with uuid `listUuid` from the local stores.
    ///
    /// - Parameters:
    ///   - listUuid: The list to switch to.
    ///   - callback: Callback to execute after the list switch has happend.
    @objc public func switchToList(listUuid: String, callback: @escaping () -> Void) {
        self.changeArticleLanguage(for: listUuid)
        listSyncManager.getCurrentListState(forList: listUuid) { (result) in
            switch result {
            case .success(let listState):
                BringListContentManager.catalogLoader.loadCatalog(ForList: listUuid) { (catalog, catalogExtension) in
                    self.currentModel = self.currentModel.createNewModel(catalogSections: catalog, listUuid: listUuid)
                    self.enrichListContent(catalogChange: catalogExtension)
                    self.currentModel = self.currentModel.createNewModel(didFetchListContentForCurrentList: false)
                    self.updateListContent(shouldReinizialize: true, listState, lastSyncDate: Date())
                    callback()
                }
            case .failure(let error):
                Log.e("Error while loading current list state: \(error.localizedDescription)")
                callback()
            }
        }
    }
    
    private func changeArticleLanguage(for listUuid: String) {
        guard let articleLanguage = BringListContentManager.settingsProvider.articleLanguage(forList: listUuid) else {
            assertionFailure("No article language set for list with uuid: \(listUuid)")
            return
        }
        self.localizationSystem.setArticleLanguage(articleLanguage)
        
    }
    
    /// Loads the currently saved list state and the catalog.
    ///
    /// - Parameters:
    ///   - shouldNotifyListeners: Indicates whether the listeners should be notified about this content change.
    ///   - callback: Callback to execute after the list state has been applied to the list content.
    public func initializeListContent(callback: @escaping () -> Void) {
        BringListContentManager.catalogLoader.loadCatalog(ForList: currentModel.listUuid) { (sections, catalogChange) in
            self.currentlyLoadedCatalogChange = catalogChange
            self.currentModel = self.currentModel.createNewModel(catalogSections: sections, listUuid: self.getCurrentListUuid())
            self.enrichListContent(catalogChange: catalogChange)
            
            self.listSyncManager.getCurrentListState(forList: self.currentModel.listUuid) { (result) in
                switch result {
                case .success(let listState):
                    self.updateListContent(shouldReinizialize: true, listState, lastSyncDate: Date())
                    self.resetNewItemFlags()
                    self.didLoadListState = true
                    self.performSnapshotCallbacks(listContent: self.currentModel)
                    callback()
                case .failure(let error):
                    Log.e("Error while loading current list state: \(error.localizedDescription)")
                    self.performSnapshotCallbacks(listContent: nil)
                    callback()
                }
            }
        }
    }
    
    private func getCurrentListUuid() -> String {
        guard let currentlistUuid = BringListContentManager.settingsProvider.seletectedList() else {
            return currentModel.listUuid
        }
        return currentModel.listUuid.isEmpty ? currentlistUuid : currentModel.listUuid
    }
    
    private var snapshotCallbacks = [((BringListContent?) -> Void)]()
    
    private func performSnapshotCallbacks(listContent: BringListContent?) {
        assert(Thread.isMainThread, "Should be called on the main thread only")
        snapshotCallbacks.forEach { (callback) in
            callback(listContent)
        }
        snapshotCallbacks.removeAll()
    }
    
    private func registerSnapshotCallback(_ callback: @escaping (BringListContent?) -> Void) {
        assert(Thread.isMainThread, "Should be called on the main thread only")
        snapshotCallbacks.append(callback)
    }
    
    @objc public func getSnaphot(callback: @escaping (BringListContent?) -> Void) {
        if didLoadListState {
            callback(currentModel.copy() as? BringListContent)
        } else {
            registerSnapshotCallback(callback)
        }
    }
    
    /// Loads the local list state.
    ///
    /// - Parameters:
    ///   - listUuid: List to be loaded.
    ///   - callback: Callback with list content.
    ///
    /// - Note: Purchase and recently are sorted alphabetically and not by section.
    public func getListContent(listUuid: String, callback: @escaping (BringListContent?) -> Void) {
        Log.v("listUuid = \(listUuid)")
        
        listSyncManager.getCurrentListState(forList: listUuid) { (result) in
            
            switch result {
            case .success(let state):
                
                let localization = self.getLocalizationSystem(listUuid)
                
                let purchase = state.purchase.map({ (listStateItem) -> BringItem in
                    let name = localization.localizedString(forKey: listStateItem.itemId)
                    return BringItem(uuid: nil, itemId: listStateItem.itemId, name: name, specification: listStateItem.specification)
                })
                
                let recently = state.recently.map({ (listStateItem) -> BringItem in
                    let name = localization.localizedString(forKey: listStateItem.itemId)
                    return BringItem(uuid: nil, itemId: listStateItem.itemId, name: name, specification: listStateItem.specification)
                })
                
                // @TODO: Check with Beat. Can this ever happen. Log this case to firebase
                let listStatus = BringListStatus(rawValue: state.status) ?? .unregistered
                
                let toBeBurchasedSection = BringListContent.createToBePurchasedSection(items: purchase)
                toBeBurchasedSection.sort()
                
                let recentlyPurchasedSection = BringListContent.createRecentlyPurchasedSection(items: recently)
                recentlyPurchasedSection.sort()
                
                let listContent = BringListContent(sections: [BringSection](), recentlyPurchasedSection: recentlyPurchasedSection, recommendedSection: BringListContent.createRecommendedSection(), toBeBurchasedSection: toBeBurchasedSection, listStatus: listStatus, listUuid: state.uuid, maxItemsInRecently: 0)
                
                callback(listContent)
                
            case .failure(let error):
                Log.w("Failed to get current list state: error = \(error)")
                callback(nil)
            }
        }
    }
    
    public func syncLists(callback: @escaping () -> Void) {
        listSyncManager.syncLists(mode: .all) { _ in
            callback()
        }
    }
    
    private func getLocalizationSystem(_ listUuid: String) -> BringLocalizationSystem {
        let localization = BringLocalizationSystem.shared
        
        if let language = BringListContentManager.settingsProvider.articleLanguage(forList: listUuid) {
            localization.setArticleLanguage(language)
        } else {
            Log.w("Localization article language not available.")
        }
        
        return localization
    }
    
    /// Fetches the current list state from the remote, saves the list content to the local store and notifies `ModelListener`s afterwards.
    ///
    /// - Parameters:
    ///   - shouldNotifyListeners: Indicates whether the listeners should be notified after the fetch.
    ///   - callback: The callback to execute after the list state has been applied.
    public func fetch(list listUuid: String, callback: @escaping (Result<Void, Error>) -> Void) {
        self.listSyncManager.syncLists(mode: .specific([listUuid]), callback: callback)
    }
    
    /// Reloads the catalog and notifies `ModelListener`s afterwards
    public func reloadListContent(callback: (() -> Void)? = nil) {
        BringListContentManager.catalogLoader.loadCatalog(ForList: currentModel.listUuid) { (catalog, catalogExtension) in
            self.currentModel = self.currentModel.createNewModel(catalogSections: catalog)
            self.enrichListContent(catalogChange: catalogExtension)
            
            self.listSyncManager.getCurrentListState(forList: self.currentModel.listUuid) { (result) in
                switch result {
                case .success(let listState):
                    self.updateListContent(shouldReinizialize: false, listState, lastSyncDate: self.listSettings.lastSyncDate() ?? Date())
                    callback?()
                case .failure(let error):
                    Log.e("Error while loading current list state: \(error.localizedDescription)")
                    callback?()
                }
            }
            
        }
    }
    
    /// Updates the list content with a new list state for the given section order.
    ///
    /// - Parameters:
    ///   - updateViews:
    ///   - newListstate: Indicates whether `ModelListener`s should be notified after the update.
    ///   - sectionOrder: The order of the sections in the catalog.
    func updateListContent(shouldReinizialize: Bool = false, _ newListstate: BringListState, lastSyncDate: Date) {
        
        let purchase = newListstate.purchase
        let recently = newListstate.recently
        
        userItemStore.getAllUserItems { (result) in
            switch result {
            case .success(let allUserItems):
                let allUserItemKeys = allUserItems.map { $0.itemKey }
                var userItemsToAdd = [BringItem]()
                for item in purchase + recently {
                    
                    let itemKey = item.itemId
                    let specification = item.specification
                    
                    if self.item(byUuid: item.uuid, orItemId: item.itemId) == nil {
                        let userItem = BringItem(uuid: UUID().uuidString, itemId: itemKey, name: BringLocalizationSystem.shared.localizedString(forKey: itemKey), specification: specification)
                        userItem.userItem = true
                        if !allUserItemKeys.contains(itemKey) {
                            self.userItemStore.create(itemKey, callback: { _ in})
                        }
                        userItemsToAdd.append(userItem)
                    }
                }
                
                self.performUpdate(currentListState: newListstate, userItemsToAdd: userItemsToAdd, sectionOrder: self.getSectionOrder(), lastSyncDate: lastSyncDate, shouldReinizialize: shouldReinizialize)
            case .failure(let error):
                Log.e(error.localizedDescription)
                self.performUpdate(currentListState: newListstate, userItemsToAdd: [], sectionOrder: self.getSectionOrder(), lastSyncDate: lastSyncDate, shouldReinizialize: shouldReinizialize)
            }
        }
    }
    
    private func performUpdate(currentListState: BringListState, userItemsToAdd: [BringItem], sectionOrder: [String], lastSyncDate: Date, shouldReinizialize: Bool) {
        let updateAction = BringListContentUpdateAction(currentListState: currentListState, userItemsToAdd: userItemsToAdd, sectionOrder: sectionOrder, syncStartDate: lastSyncDate, translations: localizationSystem.getAdditionalTranslations())
        currentModel = listContentReducer.reduce(currentModel, action: updateAction)
        
        if shouldReinizialize {
            self.modelReinitialized()
        } else {
            self.modelChanged()
        }
        
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: BringListContentManager.itemsChangedNotification)))
    }
    
    /// Resets the new item flag of every item in the to-be-purchased section.
    public func resetNewItemFlags() {
        let action = BringListContentResetNewItemFlagsAction()
        currentModel = listContentReducer.reduce(currentModel, action: action)
    }
    
    // MARK: Accessing BringItems and BringSections
    
    /// Get a copy of an item from the catalog sections (including recommended section).
    ///
    /// - Parameter itemId: The itemId of the `BringItem` to search for.
    /// - Returns: A copy of the found `BringItem` or `nil` if the item does not exist in the catalog.
    public func item(byItemId itemId: String) -> BringItem? {
        return currentModel.item(forItemId: itemId)?.copy() as! BringItem?
    }
    
    public func item(byUuid uuid: String) -> BringItem? {
        return currentModel.allItems
            .filter { $0.uuid == uuid && $0.uuid != nil }
            .first?
            .copy() as! BringItem?
    }
    
    public func item(byUniqueIdentifier identifier: BringItem.UniqueIdentifier) -> BringItem? {
        let foundItem = currentModel.allItems
            .filter { $0.uniqueIdentifier == identifier}
            .first?.copy() as! BringItem?
        
        if foundItem == nil {
            return item(byItemId: identifier)
        }
        
        return foundItem
    }
    
    /// Returns the item from the list content for the given `uuid`. If there is no item found for the given `uuid`, the `itemId` is considered.
    ///
    /// - Parameter uuid: The uuid of the item to find
    /// - Parameter itemId: The itemId of the item to find
    /// - Returns: The found item or `nil` if no item has been found.
    func item(byUuid uuid: String?, orItemId itemId: String) -> BringItem? {
        return currentModel.itemFor(uuid: uuid, orItemId: itemId)
    }
    
    /// Returns a copy of the content of the current list content.
    ///
    /// - Returns: The current list content
    public func getCurrentModel() -> BringListContent {
        return currentModel.copy() as! BringListContent
    }
    
    /// Returns a copy of the to-be-purchased section.
    ///
    /// - Returns: A copy of the to-be-purchased section.
    public func getToBePurchasedSection() -> BringSection {
        return currentModel.getToBePurchasedSection()
    }
    
    public func getRecentlyPurchasedSection() -> BringSection {
        return currentModel.getRecentlyPurchasedSection()
    }
    
    public func isItemInToBePurchasedSection(_ item: BringItem) -> Bool {
        return currentModel.isInToBePurchasedSection(item: item)
    }
    
    func isItemInToBePurchasedSection(itemId: String) -> Bool {
        return currentModel.isInToBePurchasedSection(itemId: itemId)
    }
    
    public func isItemInToBePurchasedSection(uniqueIdentifier: BringItem.UniqueIdentifier) -> Bool {
        return currentModel.isInToBePurchasedSection(uniqueIdentifier: uniqueIdentifier)
    }
    
    func isItemInRecentlySection(_ item: BringItem) -> Bool {
        return currentModel.isInRecentlyPurchased(item: item)
    }
    
    func isItemInRecommendedSection(_ item: BringItem) -> Bool {
        return currentModel.isRecommendedItem(item: item)
    }
    
    public func getCatalogSections() -> [BringSection] {
        return currentModel.getSectionsCopy()
    }
    
    func item(byName name: String) -> BringItem? {
        guard !name.isEmpty else { return nil }
        let predicate = NSPredicate(format: "name ==[cd] %@", name)
        return currentModel.allItems.filter { predicate.evaluate(with: $0) }.last
    }
    
    public func section(bySectionId sectionId: String) -> BringSection? {
        return currentModel.getSectionsCopy().filter { $0.sectionId == sectionId }.first
    }
    
    public func itemIsInCatalogAndNotAUserItem(item: BringItem) -> Bool {
        let sections = currentModel.getSections()
        let items =  sections.reduce([BringItem]()) { (currentResult: [BringItem], nextSection: BringSection) -> [BringItem] in
            if nextSection.sectionId == BringCatalogProvider.USER_SECTION_KEY {
                return currentResult
            }
            var currentResult = currentResult
            currentResult.append(contentsOf: nextSection.items)
            return currentResult
        }
        return items.contains(where: { (itemInCatalog) -> Bool in
            return itemInCatalog.itemId == item.itemId
        })
    }
    
    // MARK: Private methods
    
    /// Creates a list change and schedules the change to be pushed to the remote
    ///
    /// - Parameter item: The item to perform the list change on.
    /// - Parameter removedItem: The item to perform a remove operaiton on. If this is nil, no item was removed from the recently section.
    private func pushListContentChangesToRemote(of item: BringItem, itemMovedFromRecentlyToCatalog removedItem: BringItem?, onList listUuid: String) {
        
        guard let itemUuid = item.uuid else {
            self.logger.log(
                cls: "BringListContentManager",
                method: "pushListContentChangesToRemote(::)",
                description: "Item does not have a uuid at a point where it should have one.",
                issue: "itemId = \(item.itemId), listUuid = \(currentModel.listUuid)"
            )
            assertionFailure("At this point we need to have an item uuid")
            return
        }
        
        let operation = currentModel.isInToBePurchasedSection(item: item) ? BringListContentOperation.toPurchase : BringListContentOperation.toRecently
        let listChange = BringListChange(uuid: itemUuid, listUuid: listUuid, itemId: item.itemId, spec: item.specification, location: locationService.lastLocation, operation: operation, timeStamp: Date().timeIntervalSince1970)
        listSyncManager.sync(listChange, shouldSyncimmediately: false)
        
        if let removedItem = removedItem {
            if !currentModel.isInToBePurchasedSection(item: removedItem) {
                
                guard let uuid = removedItem.uuid else {
                    self.logger.log(
                        cls: "BringListContentManager",
                        method: "pushListContentChangesToRemote(::)",
                        description: "Item to remove does not have a uuid at a point where it should have one.",
                        issue: "itemId = \(removedItem.itemId), listUuid = \(currentModel.listUuid)"
                    )
                    assertionFailure("At this point we need to have an item uuid")
                    return
                }
                
                let removeListChange = BringListChange(uuid: uuid, listUuid: listUuid, itemId: removedItem.itemId, spec: removedItem.specification, location: locationService.lastLocation, operation: BringListContentOperation.remove, timeStamp: Date().timeIntervalSince1970)
                listSyncManager.sync(removeListChange, shouldSyncimmediately: false)
            }
        }
    }
    
    func getSortDescriptor() -> NSSortDescriptor {
        let descriptor = NSSortDescriptor(key: nil, ascending: false) { (dict1, dict2) -> ComparisonResult in
            
            let index1 = self.currentModel.getToBePurchasedSection().items.index(where: { (item) -> Bool in
                guard let state = dict1 as? BringListStateItem else {
                    return false
                }
                return state.itemId == item.itemId
            }) ?? 0
            
            let index2 = self.currentModel.getToBePurchasedSection().items.index(where: { (item) -> Bool in
                guard let state = dict1 as? BringListStateItem else {
                    return false
                }
                return state.itemId == item.itemId
            }) ?? 0
            
            if index1 < index2 {
                return .orderedAscending
            } else if index1 > index2 {
                return ComparisonResult.orderedDescending
            } else {
                return ComparisonResult.orderedSame
            }
        }
        
        return descriptor
    }
    
    // MARK: - ModelListeners
    
    public func addModelListener(delegate: BringModelDelegate) {
        listeners.append(delegate)
    }
    
    public func removeAllListeners() {
        listeners.removeAll()
    }
    
    public func removeModelListener(delegate: BringModelDelegate) {
        guard let index = listeners.index(where: {$0 === delegate}) else {
            return
        }
        listeners.remove(at: index)
    }
    
    public func modelChanged() {
        for listener in self.listeners {
            listener.listContentUpdated(listContent: getListContent())
        }
    }
    
    public func modelReinitialized() {
        for listener in self.listeners {
            listener.listContentLoaded(listContent: getListContent())
        }
    }
    
    public func modelRefreshed() {
        for listener in self.listeners {
            listener.listContentRefreshed(listContent: getListContent())
        }
    }
    
    public func getListContent() -> BringListContent {
        return currentModel.copy(with: nil) as! BringListContent
    }
    
    private func getSectionOrder() -> [String] {
        
        var userDefinedSectionOrder = BringCatalogProvider.shared.getSectionOrder(forListUuid: currentModel.listUuid)
        
        guard let catalogChange = self.currentlyLoadedCatalogChange else {
            return userDefinedSectionOrder
        }
        
        for section in catalogChange.sectionsToAdd {
            if userDefinedSectionOrder.count > section.position {
                userDefinedSectionOrder.insert(section.sectionId, at: section.position)
            } else {
                userDefinedSectionOrder.append(section.sectionId)
            }
        }
        
        return userDefinedSectionOrder
    }
}

// MARK: ListChangeSyncerDelegate

extension BringListContentManager: ListChangeSyncerDelegate {
    
    public func didFinishFetchingCurrentListContent(listState: BringListState, startSyncDate: Date) {
        if listState.uuid == currentModel.listUuid {
            self.updateListContent(listState, lastSyncDate: startSyncDate)
        }
    }
    
}

// swiftlint:enable force_cast
