//
//  File.swift
//  
//
//  Created by Renato Stauffer on 03.08.19.
//

import Foundation
import CoreLocation

@objc public class BringListChange: NSObject {
    public let uuid: String
    public let listUuid: String
    public let itemId: String
    public let operation: BringListContentOperation
    public let spec: String
    public let location: CLLocation?
    public let timeStamp: Double
    
    public init(uuid: String, listUuid: String, itemId: String, spec: String, location: CLLocation?, operation: BringListContentOperation, timeStamp: Double) {
        self.uuid = uuid
        self.listUuid = listUuid
        self.itemId = itemId
        self.spec = spec
        self.location = location
        self.operation = operation
        self.timeStamp = timeStamp
    }
    
    public override var description: String {
        return """
        ↗️ BringListChange ↗️
        ----------------
        itemUuid: \(uuid)
        listUuid: \(listUuid)
        itemId: \(itemId)
        operation: \(operation.rawValue)
        spec: \(String(describing: spec))
        location: \(String(describing: location?.description))
        timeStamp: \(timeStamp)
        ----------------
        \n
        """
    }
}
