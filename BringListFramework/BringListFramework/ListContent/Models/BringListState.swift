//
//  BringListState.swift
//  Bring
//
//  Created by Renato Stauffer on 27.03.19.
//

import Foundation

// MARK: List state

@objc
public class BringListState: NSObject {
    @objc public let purchase: [BringListStateItem]
    public let recently: [BringListStateItem]
    @objc public let status: String
    @objc public let uuid: String
    
    public init(contentResponse: BringListContentResponse, syncStartDate: Date) {
        self.purchase = contentResponse.items.purchase.compactMap { BringListStateItem(response: $0, modificationDate: syncStartDate) }
        self.recently = contentResponse.items.recently.compactMap { BringListStateItem(response: $0, modificationDate: syncStartDate) }
        self.status = contentResponse.status.rawValue
        self.uuid = contentResponse.uuid
    }
    
    public init(purchase: [BringListStateItem], recently: [BringListStateItem], status: String, uuid: String) {
        self.purchase = purchase
        self.recently = recently
        self.status = status
        self.uuid = uuid
    }
    
    public override var description: String {
        return """
        purchase: \(String(describing: purchase))
        recently: \(String(describing: recently))
        """
    }
}

@objc
public class BringListStateItem: NSObject {
    public let uuid: String
    public let specification: String
    public let itemId: String
    public var modificationDate: Date
    
    public init?(response: BringItemPurchaseResponse, modificationDate: Date) {
        
        guard let uuid = response.uuid else {
            assertionFailure("We should always have a uuid!")
            return nil
        }
        
        self.uuid = uuid
        self.specification = response.specification ?? ""
        self.itemId = response.itemId
        self.modificationDate = modificationDate
    }
    
    public init(uuid: String, specification: String, itemId: String, modificationDate: Date) {
        self.uuid = uuid
        self.specification = specification
        self.itemId = itemId
        self.modificationDate = modificationDate
    }
    
    public var uniqueIdentifier: BringItem.UniqueIdentifier {
        return uuid
    }
    
    public override func isEqual(_ object: Any?) -> Bool {
        if let anyItem = object as? BringListStateItem {
            return uuid == anyItem.uuid && itemId == anyItem.itemId
        } else {
            return false
        }
    }
    
    public override var description: String {
        return """
        \nListState Item
        --------------
        uuid: \(uuid)
        specification: \(String(describing: specification))
        itemId: \(itemId)
        modificationDate: \(String(describing: modificationDate))
        """
    }
}
