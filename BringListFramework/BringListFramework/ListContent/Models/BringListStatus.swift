//
//  BringListStatus.swift
//  Bring
//
//  Created by Renato Stauffer on 28.05.19.
//

import Foundation

public enum BringListStatus: String {
    case unregistered = "UNREGISTERED"
    case registered = "REGISTERED"
    case shared = "SHARED"
    case invitation = "INVITATION"
}
