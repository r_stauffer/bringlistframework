//
//  BringItem.swift
//  Bring
//
//  Created by Renato Stauffer on 22.01.19.
//

import Foundation

@objc
public protocol UuidGenerator: class {
    var uuidString: String { get }
}

@objc
public class UUIDWrapper: NSObject, UuidGenerator {
    
    private let generator: UUID
    
    public override init() {
        generator = UUID()
    }
    
    public var uuidString: String {
        return generator.uuidString
    }
    
}

@objc
final public class BringItem: NSObject, NSCopying {
    
    private var _uuid: String?
    public var uuid: String? {
        return _uuid
    }
    
    @objc
    public let itemId: String
    
    @objc
    public var name: String = ""
    
    @objc
    public var specification: String = ""
    public var lastModification: Date?
    public var userItem: Bool = false
    public var newItem: Bool = false
    private let uuidGenerator: UuidGenerator
    
    public typealias UniqueIdentifier = String
    
    public var uniqueIdentifier: UniqueIdentifier {
        guard let uuid = _uuid else {
            return itemId
        }
        return uuid
    }

    // MARK: Init
    @objc
    public init(uuid: String?, itemId: String, name: String = "", specification: String = "", userItem: Bool = false, newItem: Bool = false, uuidGenerator: UuidGenerator = UUIDWrapper()) {
        self._uuid = uuid
        self.itemId = itemId
        self.name = name
        self.userItem = userItem
        self.newItem = newItem
        self.specification = specification
        self.uuidGenerator = uuidGenerator
    }
    
    public func copyWithNewUuid( _ uuid: String?) -> BringItem {
        return BringItem(uuid: uuid, itemId: self.itemId, name: self.name, specification: self.specification, userItem: self.userItem, newItem: self.newItem)
    }
    
    // MARK: Public API
    
    public func hasSpecification() -> Bool {
        return self.specification.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
    }
    
    public func hasSpecification(equalTo specification: String) -> Bool {
        if self.specification.isEmpty && specification.isEmpty {
            return true
        }
        return self.specification == specification
    }
    
    public func touch() {
        self.lastModification = Date()
    }
    
    public func assignUuidIfNeeded() {
        if _uuid == nil {
            _uuid = uuidGenerator.uuidString
        }
    }
    
    public func resetUuid() {
        _uuid = nil
    }
    
    var dictionaryRepresentation: [String: String] {
        var dict = [String: String]()
        dict["name"] = name
        dict["specification"] = specification
        return dict
    }
    
    // MARK: CustomStringConvertible
    
    public override var description: String {
        return """
        uuid: \(String(describing: _uuid))
        itemId: \(itemId)
        name: \(name)
        specification: \(String(describing: specification))
        userItem: \(userItem)
        """
    }
    
    // MARK: Equatable
    
    public override func isEqual(_ object: Any?) -> Bool {
        if let anyItem = object as? BringItem {
            return _uuid == anyItem.uuid && itemId == anyItem.itemId
        } else {
            return false
        }
        
    }
    
    // MARK: Hashable
    
    public override var hash: Int {
        return self._uuid?.hash ?? 0 + self.itemId.hash
    }
    
    // MARK: NSCopying
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let newItem = BringItem(uuid: _uuid, itemId: itemId, name: name, specification: specification)
        newItem.lastModification = self.lastModification
        newItem.userItem = self.userItem
        newItem.newItem = self.newItem
        return newItem
    }
}
