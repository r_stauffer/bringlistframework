//
//  BringSection.swift
//  Bring
//
//  Created by Renato Stauffer on 22.01.19.
//

import Foundation

open class BringSection: NSObject, NSCopying {
    
    @objc
    public let sectionId: String
    public let name: String
    public let isDefaultCatalogSection: Bool
    
    private var _items = [BringItem] ()
    public var items: [BringItem] {
        return _items
    }
    
    // MARK: Init
    
    public init(sectionId: String, name: String, items: [BringItem] = [], isDefaultCatalogSection: Bool = false) {
        self.sectionId = sectionId
        self.name = name
        self._items = items
        self.isDefaultCatalogSection = isDefaultCatalogSection
    }
    
    // MARK: Public API
    
    public func add(item: BringItem) {
        self._items.insert(item, at: 0)
    }
    
    public func createVanillaSection(withNewItem item: BringItem) -> BringSection {
        var newItems = self._items.filter { $0.itemId != item.itemId || ($0.uuid != nil && $0.itemId == item.itemId)}
        newItems.append(item)
        let newSection = BringSection(sectionId: self.sectionId, name: self.name, items: newItems, isDefaultCatalogSection: self.isDefaultCatalogSection)
        newSection.sort()
        return newSection
    }
    
    public func add(_ item: BringItem) -> BringSection {
        var items = _items
        items.append(item)
        return BringSection(sectionId: self.sectionId, name: self.name, items: items, isDefaultCatalogSection: isDefaultCatalogSection)
    }
    
    public func addItemAtEnd(item: BringItem) {
        self._items.insert(item, at: 0)
    }
    
    public func add(item: BringItem, at index: Int) {
        self._items.insert(item, at: index)
    }
    
    public func insert(items: [BringItem], at index: Int) {
        self._items.insert(contentsOf: items, at: index)
    }
    
    public func replaceItems(with newItems: [BringItem]) {
        _items = newItems
    }
    
    public func remove(item: BringItem) {
        self._items = self._items.filter { $0 != item }
    }
    
    public func removeItems(withItemId itemId: String) {
        self._items = self._items.filter { $0.itemId != itemId }
    }
    
    public func removeItems(where searchCondition: (BringItem) -> Bool) {
        self._items.removeAll(where: searchCondition)
    }
    
    public func removeItemsWithNilUuid(ofItemWithItemId itemId: String) {
        self._items = self._items.filter { $0.itemId != itemId || ($0.uuid != nil && $0.itemId == itemId)}
    }
    
    public func containsItems(withItemId itemId: String) -> Bool {
        return _items.filter { $0.itemId == itemId }.first != nil
    }
    
    public func replace(itemAtIndex index: Int, withItem item: BringItem) {
        assert(index < _items.count && index >= 0, "Index needs to be in range of the items array")
        self._items[index] = item
    }
    
    @discardableResult public func removeLast() -> BringItem? {
        let lastItem = self._items.last
        self._items.removeLast()
        return lastItem
    }
    
    public func removeAllItems() {
        self._items.removeAll()
    }
    
    public func remove(_ itemId: String?) -> BringSection {
        guard let itemId = itemId else {
            return self.copy() as! BringSection
        }
        return BringSection(sectionId: self.sectionId, name: self.name, items: self._items.filter { $0.itemId != itemId })
    }
    
    public func item(byUuid uuid: String, orItemId itemId: String) -> BringItem? {
        let item = items.filter { $0.uuid == uuid }.first
        return item != nil ? item : items.filter { $0.itemId == itemId }.first
    }
    
    public func item(byUuid uuid: String) -> BringItem? {
        return items.filter { $0.uuid == uuid }.first
    }
    
    public func items(withItemId itemId: String) -> [BringItem] {
        return items.filter { $0.itemId == itemId }
    }
    
    public func sort() {
        self._items.sort { $0.name.localizedCaseInsensitiveCompare($1.name) == .orderedAscending }
    }
    
    override open var description: String {
        return """
        sectionId: \(sectionId)
        name: \(name)
        items: \(_items.description)
        defaultCatalog: \(isDefaultCatalogSection)
        """
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let newSection = BringSection(sectionId: sectionId, name: name, items: [], isDefaultCatalogSection: isDefaultCatalogSection)
        for item in self._items {
            if let newItem = item.copy() as? BringItem {
                newSection._items.append(newItem)
            }
        }
        return newSection
    }
    
    // MARK: Equatable
    
    open override func isEqual(_ object: Any?) -> Bool {
        if let anySection = object as? BringSection {
            return sectionId == anySection.sectionId
        } else {
            return false
        }
    }
    
    // MARK: Hashable
    
    open override var hash: Int {
        return sectionId.hash
    }
}
