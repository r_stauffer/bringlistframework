//
//  BringList.swift
//  Bring
//
//  Created by Renato Stauffer on 21.01.19.
//

import Foundation

// swiftlint:disable force_cast

/// A representation of the content for the currently selected `BringList`.
@objc
public class BringListContent: NSObject, NSCopying {
    
    private let sections: [BringSection]
    private let toBeBurchased: BringSection
    private let recentlyPurchased: BringSection
    private let recommendedSection: BringSection
    public static let recentlySectionKey = "Zuletzt verwendet"
    
    public var listStatus: BringListStatus
    var didFetchListContentForCurrentList: Bool
    @objc public let listUuid: String
    let initialRecommenedItemKeys: [String]
    let maxItemsInRecently: Int
    public static let maxItemsInRecentlyDefaultValue = 12
    
    /// Returns all the `BringItem`s of all catalog sections including the recommended section.
    public var allItems: [BringItem] {
        let catalogSectionItems = sections.reduce([BringItem]()) { (currentResult: [BringItem], nextSection: BringSection) -> [BringItem] in
            var currentResult = currentResult
            currentResult.append(contentsOf: nextSection.items)
            return currentResult
        }
        return recommendedSection.items + catalogSectionItems
    }
    
    /// Returns all the `BringItem`s of all catalog sections.
    var catalogItems: [BringItem] {
        return sections.reduce([BringItem]()) { (currentResult: [BringItem], nextSection: BringSection) -> [BringItem] in
            var currentResult = currentResult
            currentResult.append(contentsOf: nextSection.items)
            return currentResult
        }
    }
    
    // MARK: Init
    
    init(sections: [BringSection], recommendedItemKeys: [String] = [String](), recentlyPurchasedSection: BringSection, recommendedSection: BringSection, toBeBurchasedSection: BringSection, listStatus: BringListStatus, listUuid: String, didFetchListContentForCurrentList: Bool = false, maxItemsInRecently: Int) {
        self.sections = sections
        self.initialRecommenedItemKeys = recommendedItemKeys
        self.recommendedSection = recommendedSection
        self.recentlyPurchased = recentlyPurchasedSection
        self.toBeBurchased = toBeBurchasedSection
        self.listStatus = listStatus
        self.listUuid = listUuid
        self.didFetchListContentForCurrentList = didFetchListContentForCurrentList
        self.maxItemsInRecently = maxItemsInRecently
        super.init()
    }
    
    
    // MARK: Public API
    
    /// Returns a copy of the current catalog section.
    ///
    /// - Returns: A copy of the catalog section.
    public func getSectionsCopy() -> [BringSection] {
        return sections.map { $0.copy() as! BringSection }
    }
    
    /// Returns a deep copy of the to-be-purchased section.
    ///
    /// - Returns: A copy of the to-be-purchased section.
    public func getToBePurchasedSection() -> BringSection {
        return BringListContent.createToBePurchasedSection(items: self.toBeBurchased.items.map { $0.copy() as! BringItem })
    }
    
    /// Returns a deep copy of the recently section.
    ///
    /// - Returns: A copy of the recently section.
    public func getRecentlyPurchasedSection() -> BringSection {
        return BringListContent.createRecentlyPurchasedSection(items: self.recentlyPurchased.items.map { $0.copy() as! BringItem })
    }
    
    /// Returns a reference to the catalog sections.
    ///
    /// - Returns: The catalog sections.
    func getSections() -> [BringSection] {
        return sections
    }
    
    /// Returns a reference to the to-be-purchased section.
    ///
    /// - Returns: The to-be-purchased sectino.
    func getToBePurchasedSectionByReference() -> BringSection {
        return toBeBurchased
    }
    
    /// Returns a reference to the recently section.
    ///
    /// - Returns: The recently section.
    func getRecentlyPurchasedSectionByReference() -> BringSection {
        return recentlyPurchased
    }
    
    /// Returns a copy of the recommended section.
    ///
    /// - Returns: A copy of the recommended section.
    public func getRecommendedSection() -> BringSection {
        return BringListContent.createRecommendedSection(items: self.recommendedSection.items.map { $0.copy() as! BringItem }, name: self.recommendedSection.name)
    }
    
    public func getOwnArticlesSectionCopy() -> BringSection? {
        return getSectionsCopy().first(where: { (section) -> Bool in
            section.sectionId == BringCatalogProvider.USER_SECTION_KEY
        })
    }
    
    /// Returns a reference to the recommended section.
    ///
    /// - Returns: The recommended section.
    func getRecommendedSectionByReference() -> BringSection {
        return self.recommendedSection
    }
    
    func itemFor(uuid: String?, orItemId itemId: String) -> BringItem? {
        let itemByUuid = item(for: uuid)
        return itemByUuid != nil ? itemByUuid : item(forItemId: itemId)
    }
    
    @objc
    func item(for uuid: String?) -> BringItem? {
        guard uuid != nil else { return nil }
        return allItems.filter { $0.uuid == uuid }.first
    }
    
    func itemAndSectionByReference(for uuid: String?) -> (item: BringItem, section: BringSection)? {
        guard uuid != nil else { return nil }
        for section in sections {
            if let foundItem = (section.items.filter { $0.uuid == uuid }.first) {
                return (item: foundItem, section: section)
            }
        }
        return nil
    }
    
    @objc
    public func item(forItemId itemId: String) -> BringItem? {
        return allItems.filter { $0.itemId == itemId }.first
    }
    
    func items(forItemId itemId: String) -> [BringItem] {
        return allItems.filter { $0.itemId == itemId }
    }
    
    func itemsFromDefaultSections(forItemId itemId: String) -> [BringItem] {
        let catalogSectionItems = sections.reduce([BringItem]()) { (currentResult: [BringItem], nextSection: BringSection) -> [BringItem] in
            guard nextSection.isDefaultCatalogSection else {
                return currentResult
            }
            
            var currentResult = currentResult
            currentResult.append(contentsOf: nextSection.items)
            return currentResult
        }
        
        return catalogSectionItems.filter { $0.itemId == itemId }
    }
    
    public func isInToBePurchasedSection(item: BringItem) -> Bool {
        return self.toBeBurchased.items.contains(item)
    }
    
    public func isInToBePurchasedSection(uniqueIdentifier: String) -> Bool {
        return self.toBeBurchased.items.contains(where: { (item) -> Bool in
            item.uniqueIdentifier == uniqueIdentifier
        })
    }
    
    @objc public func isInToBePurchasedSection(itemId: String) -> Bool {
        return self.toBeBurchased.items.contains(where: { (item) -> Bool in
            item.itemId == itemId
        })
    }
    
    public func isInRecentlyPurchased(item: BringItem) -> Bool {
        return self.recentlyPurchased.items.contains(item)
    }
    
    public func isInRecentlyPurchased(uniqueIdentifier: String) -> Bool {
        return self.recentlyPurchased.items.contains(where: { (item) -> Bool in
            item.uniqueIdentifier == uniqueIdentifier
        })
    }
    
    func isInitialRecommendedItem(item: BringItem) -> Bool {
        return initialRecommenedItemKeys.contains(item.itemId)
    }
    
    func isRecommendedItem(item: BringItem) -> Bool {
        return recommendedSection.items.contains(where: { (bringItem) -> Bool in
            bringItem.itemId == item.itemId
        })
    }
    
    func getIndex(ofRecommendedItem item: BringItem) -> Int? {
        return initialRecommenedItemKeys.firstIndex(of: item.itemId)
    }
    
    func catalogSectionReference(bySectionId sectionId: String) -> BringSection? {
        for section in getSections() where sectionId == section.sectionId {
            return section
        }
        return nil
    }
    
    func catalogSection(bySectionId sectionId: String) -> BringSection? {
        for section in getSectionsCopy() where sectionId == section.sectionId {
            return section
        }
        return nil
    }
    
    func removeItems(fromToBePurchasedSection itemToRemoveFromToBePurchasedSection: String? = nil, fromRecentlySection itemToRemoveFromRecently: String? = nil) -> BringListContent {
        return createNewModel(
            recentlyPurchasedSection: getRecentlyPurchasedSectionByReference().remove(itemToRemoveFromRecently),
            toBePurchasedSection: getToBePurchasedSectionByReference().remove(itemToRemoveFromToBePurchasedSection)
        )
    }
    
    public func section(of item: BringItem, inListContent listContent: BringListContent) -> BringSection? {
        return listContent.getSections().filter { (section) -> Bool in
            return section.items.contains(item)
            }.first
    }
    
    func removeItemsBasedOnItemIds(_ items: [BringItem]) -> BringListContent {
        for item in items {
            getRecentlyPurchasedSectionByReference().remove(item: item)
            getToBePurchasedSectionByReference().remove(item: item)
            self.getSections()
                .filter { $0.items.contains(item) }
                .forEach {
                    $0.remove(item: item)
                }
        }
        return createNewModel()
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        
        let newSections = self.getSectionsCopy()
        let newRecentlyPurchased = self.getRecentlyPurchasedSection()
        let newRecommendedSection = self.getRecommendedSection()
        let newToBePurchasedSection = self.getToBePurchasedSection()
        
        return BringListContent(sections: newSections, recommendedItemKeys: self.initialRecommenedItemKeys, recentlyPurchasedSection: newRecentlyPurchased, recommendedSection: newRecommendedSection, toBeBurchasedSection: newToBePurchasedSection, listStatus: self.listStatus, listUuid: self.listUuid, didFetchListContentForCurrentList: self.didFetchListContentForCurrentList, maxItemsInRecently: self.maxItemsInRecently)
    }
}

extension BringListContent {
    static func createRecentlyPurchasedSection(items: [BringItem] = []) -> BringSection {
        let recentlySection = BringSection(sectionId: BringListContent.recentlySectionKey, name: BringLocalizationSystem.shared.localizedString(forKey: BringListContent.recentlySectionKey), items: items)
        return recentlySection
    }
    
    static func createRecommendedSection(items: [BringItem] = [], name: String = "") -> BringSection {
        let recommendedSection = BringSection(sectionId: "Recommended", name: name, items: items)
        return recommendedSection
    }
    
    static func createToBePurchasedSection(items: [BringItem] = []) -> BringSection {
        let toBePurchasedSection = BringSection(sectionId: "ToBePurchased", name: "", items: items)
        return toBePurchasedSection
    }
    
    func createNewModel(catalogSections sections: [BringSection]? = nil, recentlyPurchasedSection: BringSection? = nil, toBePurchasedSection: BringSection? = nil, initialRecommenedItemKeys: [String]? = nil, recommendedSection: BringSection? = nil, bringListStatus: BringListStatus? = nil, listUuid: String? = nil, didFetchListContentForCurrentList: Bool? = nil) -> BringListContent {
        return BringListContent(
            sections: sections ?? self.getSectionsCopy(),
            recommendedItemKeys: initialRecommenedItemKeys ?? self.initialRecommenedItemKeys,
            recentlyPurchasedSection: recentlyPurchasedSection ?? self.getRecentlyPurchasedSection(),
            recommendedSection: recommendedSection ?? self.getRecommendedSection(),
            toBeBurchasedSection: toBePurchasedSection ?? self.getToBePurchasedSection(),
            listStatus: bringListStatus ?? self.listStatus,
            listUuid: listUuid ?? self.listUuid,
            didFetchListContentForCurrentList: didFetchListContentForCurrentList ?? self.didFetchListContentForCurrentList,
            maxItemsInRecently: self.maxItemsInRecently
        )
    }
}

// swiftlint:enable force_cast
