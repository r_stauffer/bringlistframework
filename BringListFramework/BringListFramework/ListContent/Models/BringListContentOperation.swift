//
//  File.swift
//  
//
//  Created by Renato Stauffer on 03.08.19.
//

import Foundation

public enum BringListContentOperation: String, Codable {
    case toPurchase = "TO_PURCHASE"
    case toRecently = "TO_RECENTLY"
    case remove = "REMOVE"
}
