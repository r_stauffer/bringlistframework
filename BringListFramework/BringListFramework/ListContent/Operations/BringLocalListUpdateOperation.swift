//
//  BringLocalListUpdateOperation.swift
//  Bring
//
//  Created by Renato Stauffer on 31.01.19.
//

import Foundation

final class BringLocalListUpdateOperation: AsynchronousOperation {
    private let listStateStore: ListStateStore
    private let listChangesStore: ListChangesStore
    
    let listChange: BringListChange
    
    init(listStateStore: ListStateStore, listChangesStore: ListChangesStore, listChange: BringListChange) {
        self.listStateStore = listStateStore
        self.listChangesStore = listChangesStore
        self.listChange = listChange
    }
    
    override func main() {
        self.listChangesStore.add(listChange, isWaiting: false, callback: { (result) in
            switch result {
            case .success:
                self.updateListState()
            case .failure(let error):
                Log.e("Failed to add list change to listChangesStore: \(error.localizedDescription)")
                self.finish()
            }
        })
    }
    
    private func updateListState() {
        let listChangeMerger = BringDefaultListChangeMerger(listChange: self.listChange)
        self.listStateStore.update(listState: self.listChange.listUuid, withListChangeUsing: listChangeMerger) { _ in
            self.finish()
        }
    }
}
