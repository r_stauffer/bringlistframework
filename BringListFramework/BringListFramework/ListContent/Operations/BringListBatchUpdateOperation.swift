//
//  BringListBatchUpdateOperation.swift
//  Bring
//
//  Created by Renato Stauffer on 31.01.19.
//

import Foundation

protocol BringListBatchUpdateOperationDelegate: class {
    func didFinishBatchupdate(with error: BringListBatchUpdateOperationError, forListWithUuid listUuid: String)
    func didFinishBatchupdate(forListWithUuid listUuid: String)
}

struct BringListBatchUpdateOperationError: Error {
    
    enum Reason {
        case listChangeDeletion
        case batchUpdateService
        case listChangesFetch
    }
    
    let underlyingError: Error
    let reason: Reason
}

final class BringListBatchUpdateOperation: AsynchronousOperation, ListSyncOperation {
    
    private let listContentService: ListContentService
    private let listChangesStore: ListChangesStore
    let listUuid: String
    private let maxRetryCount = 15
    private let apnsToken: String?
    private let syncStartDate: Date
    weak var delegate: BringListBatchUpdateOperationDelegate?
    
    init(listContentService: ListContentService, listChangesStore: ListChangesStore, listUuid: String, apnsToken: String?, syncStartDate: Date) {
        self.listContentService = listContentService
        self.listChangesStore = listChangesStore
        self.listUuid = listUuid
        self.apnsToken = apnsToken
        self.syncStartDate = syncStartDate
    }
    
    override func main() {
        if isCancelled {
            finish()
            return
        }
        
        // Get all changes from the local store
        let sortDescriptor = NSSortDescriptor.init(key: "timestamp", ascending: true)
        listChangesStore.allChanges(forListUuid: listUuid, isWaiting: false, sortDescriptors: [sortDescriptor]) { (result) in
            switch result {
            case .success(let listChanges):
                
                if self.isCancelled {
                    self.finish()
                    return
                }
                
                if listChanges.count <= 0 {
                    self.finish()
                    return
                }
                
                self.sendBatchUpdate(listChanges: listChanges)
            case .failure(let error):
                Log.e("Error while getting changes from local store: \(error.localizedDescription)")
                let error = BringListBatchUpdateOperationError(underlyingError: error, reason: .listChangesFetch)
                self.delegate?.didFinishBatchupdate(with: error, forListWithUuid: self.listUuid)
                self.finish()
            }
        }
    }
    
    private func sendBatchUpdate(listChanges: [BringListChange]) {
        let request = BringBatchUpdateRequest(sender: self.apnsToken, listChanges: listChanges)
        self.listContentService.batchUpdate(request, listUuid: self.listUuid, callback: { (result) in
            switch result {
            case .success:
                self.deleteAllListChangesChangesAndFinishOperation(listUuid: self.listUuid)
            case .failure(let error):
                if let error = error as? ListContentServiceError {
                    if error == ListContentServiceError.listNotExisting {
                        self.deleteAllListChangesChangesAndFinishOperation(listUuid: self.listUuid)
                        return
                    } else {
                        let error = BringListBatchUpdateOperationError(underlyingError: error, reason: .batchUpdateService)
                        self.delegate?.didFinishBatchupdate(with: error, forListWithUuid: self.listUuid)
                    }
                }
                self.finish()
            }
        })
    }
    
    private func deleteAllListChangesChangesAndFinishOperation(listUuid: String) {
        self.listChangesStore.deleteChanges(forListUuid: listUuid, syncDate: self.syncStartDate, isWaiting: false, callback: { result in
            switch result {
            case .success:
                self.delegate?.didFinishBatchupdate(forListWithUuid: listUuid)
            case .failure(let error):
                let error = BringListBatchUpdateOperationError(underlyingError: error, reason: .listChangeDeletion)
                self.delegate?.didFinishBatchupdate(with: error, forListWithUuid: listUuid)
            }
            self.finish()
        })
    }
}
