//
//  BringListBatchUpdateOperation.swift
//  Bring
//
//  Created by Renato Stauffer on 31.01.19.
//

import Foundation

protocol BringListFetchOperationDelegate: class {
    func didFetch(_ listState: BringListState, startSyncDate: Date)
    func didFinish(with error: Error)
}

final class BringListFetchOperation: AsynchronousOperation, ListSyncOperation {
    
    private let listContentService: ListContentService
    private let listStateStore: ListStateStore
    let listUuid: String
    private let syncStartDate: Date
    weak var delegate: BringListFetchOperationDelegate?
    
    init(listContentService: ListContentService, listStateStore: ListStateStore, listUuid: String, syncStartDate: Date) {
        self.listContentService = listContentService
        self.listStateStore = listStateStore
        self.listUuid = listUuid
        self.syncStartDate = syncStartDate
    }
    
    override func main() {
        if isCancelled {
            finish()
            return
        }
        
        self.listContentService.getItems(forListUuid: listUuid, syncStartDate: self.syncStartDate) { (result) in
            switch result {
            case .success(let remoteListState):
                let merger = BringDefaultListStateMerger(syncStartDate: self.syncStartDate)
                
                self.listStateStore.update(remoteListState, listStateMerger: merger) { _ in
                    self.listStateStore.getListState(forUuid: self.listUuid, isWaiting: true) { result in
                        
                        switch result {
                        case .success(let localListState):
                            guard let newListState = localListState else {
                                self.finish()
                                return
                            }
                            
                            self.delegate?.didFetch(newListState, startSyncDate: self.syncStartDate)
                            self.finish()
                        case .failure(let error):
                            self.delegate?.didFinish(with: error)
                            self.finish()
                            return
                        }
                    }
                    
                }
            case .failure(let error):
                self.delegate?.didFinish(with: error)
                self.finish()
                return
            }
        }
    }
}
