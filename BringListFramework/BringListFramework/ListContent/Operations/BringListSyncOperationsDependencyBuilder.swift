//
//  BringListSyncOperationsDependencyBuilder.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

protocol ListSyncOperation: Operation {
    var listUuid: String { get }
}

struct BringListSyncOperationGroup {
    let operations: [ListSyncOperation]
    let priority: Int
}

struct BringListSyncOperationsDependencyBuilder {
    
    private init() {}
    
    static func buildDependencies(for operationGroup: [BringListSyncOperationGroup], finishOperation: Operation) -> [Operation] {
        
        let sortedGroups = operationGroup.sorted { $0.priority < $1.priority }
        
        for (index, group) in sortedGroups.enumerated() {
            guard index != 0 else {
                group.operations.forEach { finishOperation.addDependency($0) }
                continue
            }
            
            for operation in group.operations {
                finishOperation.addDependency(operation)
                for previousOperation in sortedGroups[index - 1].operations where previousOperation.listUuid == operation.listUuid {
                    operation.addDependency(previousOperation)
                }
            }
            
            if index == sortedGroups.count - 1 {
                for operation in group.operations {
                    finishOperation.addDependency(operation)
                }
            }
        }
        
        var allOperations: [Operation] = sortedGroups.flatMap { $0.operations }
        allOperations.append(finishOperation)
        
        return allOperations
    }
}
