//
//  BringIconUtils.swift
//  Bring
//
//  Created by Renato Stauffer on 06.11.18.
//

import Foundation
import UIKit

@objc public final class BringIconUtils: NSObject {
    
    @objc
    public static let shared: BringIconUtils = BringIconUtils()
    
    public static func sanitizedFileNameString(fileName: String) -> String {
        let illegalFileNameCharacters = CharacterSet(charactersIn: "/\\?%*|\"<>")
        return fileName.components(separatedBy: illegalFileNameCharacters).joined(separator: "")
    }
    
    public static func fallbackImage(forKey key: String) -> String {
        let cleanName = BringItemUtils.removeQuantityInfo(fromKey: key)
        if !cleanName.isEmpty {
            return cleanName[...cleanName.startIndex].capitalized
        }
        return key
    }
    
    public static func getCleanImageKey(for imageKey: String) -> String {
        let cleanName = BringItemUtils.removeQuantityInfo(fromKey: imageKey)
        if !cleanName.isEmpty {
            return cleanName.replacingOccurrences(of: " ", with: "-")
        }
        return imageKey
    }
    
    public static func decodeBase64ToIcon(_ encoded: String) -> UIImage? {
        
        guard let data = Data(base64Encoded: encoded, options: .ignoreUnknownCharacters) else {
            return nil
        }
        
        guard let icon = UIImage(data: data) else {
            return nil
        }
        
        let size = max(icon.size.width, icon.size.height) * 1.5
        
        return icon.canvas(size: CGSize(width: size, height: size))
        
    }
}
