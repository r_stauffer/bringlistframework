//
//  BringCatalogProvider.swift
//  Bring
//
//  Created by Sascha Thöni on 13.01.17.
//
//

@objcMembers
public class BringCatalogProvider: NSObject {
    
    public static let shared = BringCatalogProvider()
    
    public static let USER_SECTION_KEY = "Eigene Artikel"
    public static let DEFAULT_LANGUAGE_CODE = "en-US"
    
    private var rawCataloges = [String : [[String: Any]]]()
    private var cataloges = [String : [BringSection]]()
    private var bundles = [String : Bundle]()
    private static var settingsProvider: ListContentSettingsProvider!
    
    private override init() {
        super.init()
    }
    
    static func setup(settingsProvider: ListContentSettingsProvider) {
        BringCatalogProvider.settingsProvider = settingsProvider
    }
    
    public func getDefaultSectionOrder(forListUuid listUuid : String) -> [String] {
        if let listLanguage = BringCatalogProvider.settingsProvider.articleLanguage(forList: listUuid) {
            return getDefaultSectionOrder(forLanguage: listLanguage)
        } else {
            return getDefaultSectionOrder()
        }
    }
    
    public func getDefaultSectionOrder(forLanguage languageCode : String = DEFAULT_LANGUAGE_CODE) -> [String] {
        let rawCatalog = getRawCatalog(forLanguage: languageCode)
        
        var sectionOrders = [String]()
        
        for sectionDict in rawCatalog {
            
            if let sectionName = sectionDict["sectionName"] as? String {
                sectionOrders.append(sectionName)
            }
        }
        
        sectionOrders.append(BringCatalogProvider.USER_SECTION_KEY)
        
        return sectionOrders
    }
    
    public func getSectionOrder(forListUuid listUuid : String?) -> [String] {
        
        guard let listUuid = listUuid, !listUuid.isEmpty else {
            return getDefaultSectionOrder()
        }
        
        if let listSectionOrder = BringCatalogProvider.settingsProvider.userSectionOrder(forList: listUuid) {
            
            var mutatingSectionOrder = listSectionOrder
            
            // 1.) get defaultSectionOrder for this list(language)
            let defaultSectionOrder = self.getDefaultSectionOrder(forListUuid: listUuid)
            
            let sectionOrderDifference = Set<String>(defaultSectionOrder).symmetricDifference(Set<String>(listSectionOrder))
            let addedSections = sectionOrderDifference.filter({ (sectionKey) -> Bool in
                defaultSectionOrder.contains(sectionKey)
            })
            
            // 3.) get all sections which are not anymore in the defaultSectionOrder and remove them
            let removedSections = sectionOrderDifference.filter({ (sectionKey) -> Bool in
                listSectionOrder.contains(sectionKey)
            })
            
            // 2.) if Eigene Artikel is at the end, add it before this, otherwise append it to the end
            if let lastSectionKey = listSectionOrder.last, lastSectionKey == BringCatalogProvider.USER_SECTION_KEY {
                mutatingSectionOrder.insert(contentsOf: addedSections, at: listSectionOrder.count - 1)
            } else {
                mutatingSectionOrder.insert(contentsOf: addedSections, at: listSectionOrder.count)
            }

            for removedSection in removedSections {
                if let index = listSectionOrder.index(of: removedSection) {
                     mutatingSectionOrder.remove(at: index)
                }
            }
            
            return mutatingSectionOrder
            
        } else if let listLanguage = BringCatalogProvider.settingsProvider.articleLanguage(forList: listUuid) {
            return getDefaultSectionOrder(forLanguage: listLanguage)
        } else {
            return getDefaultSectionOrder()
        }
    }
    
    public func getCurrentSectionOrder() -> [String] {
        if let listUuid = BringCatalogProvider.settingsProvider.seletectedList() {
            return getSectionOrder(forListUuid: listUuid)
        }else {
            return getDefaultSectionOrder()
        }
    }
    
    public func getSectionOrder(listUuid: String?) -> [String] {
        if let listUuid = listUuid {
            return getSectionOrder(forListUuid: listUuid)
        }else {
            return getDefaultSectionOrder()
        }
    }
    
    public func getCurrentCatalog() -> [BringSection]{
        return self.getCatalog(forLanguage: getLanguageCodeForCurrentList())
    }
    
    public func doesSectionExistsInCurrentCatalog(forSectionKey sectionKey: String) -> Bool {
        return getCurrentCatalog().map({$0.sectionId}).contains(where: {$0 == sectionKey})
    }
    
    
    private func getLanguageCodeForCurrentList() -> String {
        guard let listLangaugeCode = BringCatalogProvider.settingsProvider.seletectedList() else {
            return BringCatalogProvider.DEFAULT_LANGUAGE_CODE
        }
        
        return listLangaugeCode
    }
    
    public func getCatalog(forLanguage languageCode : String) -> [BringSection] {
        if let catalog = cataloges[languageCode] {
            return catalog.map({$0.copy() as! BringSection})
        } else {
            let catalog = self.createCatalog(forLanguage: languageCode)
            cataloges[languageCode] = catalog
            return catalog.map({$0.copy() as! BringSection})
        }
    }
    
    private func createCatalog(forLanguage languageCode : String) -> [BringSection] {
        var sections = [BringSection]()
        
        for sectionDict in getRawCatalog(forLanguage: languageCode) {
            
            var items = [BringItem]()
            
            if let rawItems = sectionDict["items"] as? Array<String> {
                for itemKey in rawItems {
                    let item = BringItem(uuid: nil, itemId: itemKey, name: self.getLocalizedName(forKey: itemKey, andLanguage: languageCode))
                    items.append(item)
                }
            }
            
            let sectionId = sectionDict["sectionName"] as? String ?? ""
            let section = BringSection(
                sectionId: sectionId,
                name: self.getLocalizedName(forKey: sectionId, andLanguage: languageCode),
                items: items,
                isDefaultCatalogSection: true
            )
            
            section.sort()
            sections.append(section)
        }
        
        // add section for eigene artikel
        let section = BringSection(sectionId: BringCatalogProvider.USER_SECTION_KEY, name: self.getLocalizedName(forKey: BringCatalogProvider.USER_SECTION_KEY, andLanguage: languageCode), isDefaultCatalogSection: true)
        
        sections.append(section)
        
        return sections
    }
    
    public func getLocalizedName(forKey key : String) -> String {
        return getLocalizedName(forKey: key, andLanguage: self.getLanguageCodeForCurrentList())
    }

    
    public func getLocalizedName(forKey key : String, andLanguage languageCode : String) -> String {
        var bundle = getBundle(forLanguage: languageCode)
        // if bundle could not be found for language, fallback to en-US bundle
        if (bundle == nil) {
            let path = Bundle(for: type(of: self)).path(forResource: BringCatalogProvider.DEFAULT_LANGUAGE_CODE, ofType: "lproj")!
            bundle = Bundle(path: path)!
        }
        
        if let bundle = bundle {
           return bundle.localizedString(forKey: key, value: "", table: "Articles")
        } else {
            return key
        }
    }
    
    public func getSectionKey(forItemKey itemKey : String) -> String {
        return getSection(forItemKey: itemKey)?.sectionId ?? BringCatalogProvider.USER_SECTION_KEY
    }
    
    public func getSection(forItemKey itemKey : String) -> BringSection? {
        return getSection(forItemKey: itemKey, andLanguage: self.getLanguageCodeForCurrentList())
    }
    
    public func getSection(forItemKey itemKey : String, andLanguage languageCode : String) -> BringSection? {
        
        let sections = getCatalog(forLanguage: languageCode)
        
        for section in sections {
            if section.items.contains(where: {$0.itemId == itemKey}) {
                return section
            }
        }
        
        return nil
    }
    
    private func getRawCatalog(forLanguage languageCode : String) -> [[String: Any]] {
        if let catalog = rawCataloges[languageCode] {
            return catalog
        } else {
            var bundleOptional = getBundle(forLanguage: languageCode)
            
            // if bundle could not be found for language, fallback to en-US bundle
            if (bundleOptional == nil) {
                bundleOptional = getBundle(forLanguage: BringCatalogProvider.DEFAULT_LANGUAGE_CODE)
            }
            
            guard let bundle = bundleOptional else {
                return [[String: Any]]()
            }
            
            let rawCatalog = readRawCatalog(fromBundle: bundle)
            rawCataloges[languageCode] = rawCatalog
            return rawCatalog
        }
    }
    
    private func getBundle(forLanguage languageCode : String) -> Bundle? {
        if let bundle = bundles[languageCode] {
            return bundle
        }
        
        guard let path = Bundle(for: type(of: self)).path(forResource: languageCode, ofType: "lproj") else {
            return nil
        }
        
        if let bundle = Bundle(path: path) {
            bundles[languageCode] = bundle
            return bundle
        }else {
            return nil
        }
    }
    
    private func readRawCatalog(fromBundle bundle : Bundle) -> [[String: Any]] {
        let emptyArray = [[String: Any]]()

        guard let path = bundle.path(forResource: "bringItems", ofType: "plist") else {
            return emptyArray
        }
        
        guard let array = NSArray(contentsOfFile: path) as? [[String: Any]] else {
            return emptyArray
        }
        
        return array
    }
}
