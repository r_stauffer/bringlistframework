//
//  BringModelDelegate.swift
//  Bring
//
//  Created by Renato Stauffer on 28.01.19.
//

import Foundation

public protocol BringModelDelegate: class {
    func listContentUpdated(listContent: BringListContent)
    func listContentRefreshed(listContent: BringListContent)
    func listContentLoaded(listContent: BringListContent)
}
