//
//  InMemoryIconLoader.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation

public class InMemoryIconLoader: IconLoadable {
    
    public static let shared = InMemoryIconLoader()
    
    public let identifier = "InMemoryIconLoader"
    
    var dictionary = [String: String]()
    
    private init() {}
    
    public func addImages(imagePerItem: [String: String]) {
        self.dictionary = imagePerItem // @TODO: add instead of replace?
    }
    
    public func loadIcon(forKey key: String, alernativeIconKey alternativeKey: String?) -> IconLoadResult {
        if let result = dictionary[key] {
            guard let data = Data(base64Encoded: result, options: .ignoreUnknownCharacters), let image = UIImage(data: data) else {
                return IconLoadResult(icon: nil, loadableIdentifier: self.identifier, isFallbackImage: false)
            }
            return IconLoadResult(icon: image, loadableIdentifier: self.identifier, isFallbackImage: false)
        } else {
            return IconLoadResult(icon: nil, loadableIdentifier: self.identifier, isFallbackImage: false)
        }
    }
    
}
