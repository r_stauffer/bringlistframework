//
//  BringListContentResponse.swift
//  Bring
//
//  Created by Renato Stauffer on 23.01.19.
//

import Foundation

// MARK: Service responses

public struct BringListContentResponse: Decodable {
    /// The uuid of the corresponding list
    public let uuid: String
    public let status: BringListStatusResponse
    public let items: BringItemsResponse
}

public struct BringItemsResponse: Decodable {
    public let purchase: [BringItemPurchaseResponse]
    public let recently: [BringItemPurchaseResponse]
}

public struct BringItemPurchaseResponse: Decodable {
    public let itemId: String
    public let uuid: String?
    public let specification: String?
}

public enum BringListStatusResponse: String, Decodable {
    case unregistered = "UNREGISTERED"
    case registered = "REGISTERED"
    case shared = "SHARED"
    case invitation = "INVITATION"
}

public class BringListContentDecoder: JSONDecoder {
    public override init() {
        super.init()
        self.dateDecodingStrategy = .formatted(DateFormatter.yyymmddTHHmmssSSSZ())
    }
}

// MARK: Service requests

public struct BringBatchUpdateRequest: Codable {
    public let sender: String?
    public let changes: [BringListContentUpdate]
    
    public init(sender: String?, listChanges: [BringListChange]) {
        self.sender = sender
        self.changes = listChanges.map { BringListContentUpdate(listChange: $0) }
    }
}

public struct BringListContentUpdate: Codable, CustomDebugStringConvertible {
    public let uuid: String?
    public let itemId: String
    public let operation: BringListContentOperation
    public let latitude: String?
    public let longitude: String?
    public let altitude: String?
    public let accuracy: String?
    public let spec: String
    
    public init(listChange: BringListChange) {
        self.uuid = listChange.uuid
        self.itemId = listChange.itemId
        self.operation = listChange.operation
        self.spec = listChange.spec
        if let latitude = listChange.location?.coordinate.latitude {
            self.latitude = "\(latitude)"
        } else {
            self.latitude = nil
        }
        
        if let longitude = listChange.location?.coordinate.longitude {
            self.longitude = "\(longitude)"
        } else {
            self.longitude = nil
        }
        
        if let altitude = listChange.location?.altitude {
            self.altitude = "\(altitude)"
        } else {
            self.altitude = nil
        }
        
        if let accuracy = listChange.location?.horizontalAccuracy {
            self.accuracy = "\(accuracy)"
        } else {
            self.accuracy = nil
        }
        
        
    }
    
    public var debugDescription: String {
        return """
        uuid: \(String(describing: uuid))
        itemId: \(itemId)
        operation: \(operation.rawValue)
        """
    }
}
