//
//  LocationService.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation
import CoreLocation

public protocol LocationService {
    var lastLocation: CLLocation? { get }
}
