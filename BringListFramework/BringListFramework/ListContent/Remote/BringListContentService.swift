//
//  BringListContentService.swift
//  Bring
//
//  Created by Renato Stauffer on 23.01.19.
//
import Foundation
import Alamofire

public protocol ListContentService {
    func getItems(forListUuid uuid: String, syncStartDate: Date, callback: @escaping (Result<BringListState>) -> Void)
    func batchUpdate(_ request: BringBatchUpdateRequest, listUuid: String, callback: @escaping (Result<Void>) -> Void)
}

public enum ListContentServiceError: Int, Error {
    case listNotExisting = 404
    case unknown = 0
}

public final class BringAlamofireListContentService: ListContentService {
    
    typealias JSON = [String: Any]
    
    private let alamofire = BringAlamofireManager.sharedInstance
    public static let shared = BringAlamofireListContentService()
    
    private enum Router: URLRequestConvertible {
        
        case items(listUuid: String)
        case batchUpdate(request: BringBatchUpdateRequest, listUuid: String)
        
        var method: HTTPMethod {
            switch self {
            case .items:
                return .get
            case .batchUpdate:
                return .put
            }
        }
        
        var path: String {
            switch self {
            case .items(let listUuid):
                return "/v2/bringlists/\(listUuid)"
            case .batchUpdate(_, let listUuid):
                return "/v2/bringlists/\(listUuid)/items"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            
            let url = URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: url.appendingPathComponent(self.path))
            mutableURLRequest.httpMethod = self.method.rawValue
            mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            switch self {
            case .items:
                return try JSONEncoding.default.encode(mutableURLRequest, withJSONObject: nil)
            case .batchUpdate(let request, _):
                let encoder = JSONEncoder()
                let batchUpdate = try? encoder.encode(request)
                mutableURLRequest.httpBody = batchUpdate
                return mutableURLRequest
            }
        }
    }
    
    // MARK: Public API

    public func getItems(forListUuid uuid: String, syncStartDate: Date, callback: @escaping (Result<BringListState>) -> Void) {
        alamofire.defaultManager.request(Router.items(listUuid: uuid)).validate().responseData { (response) in
            switch response.result {
            case .success(let data):
                do {
                    let response = try BringListContentDecoder().decode(BringListContentResponse.self, from: data)
                    let listState = BringListState(contentResponse: response, syncStartDate: syncStartDate)
                    callback(Result.success(listState))
                } catch let error {
                    Log.e("Could not decode response: \(error.localizedDescription)")
                    callback(Result.failure(error))
                }
            case .failure(let error):
                Log.e("Service error: \(error.localizedDescription)")
                callback(Result.failure(error))
            }
        }
    }
    
    public func batchUpdate(_ request: BringBatchUpdateRequest, listUuid: String, callback: @escaping (Result<Void>) -> Void) {
        alamofire.defaultManager.request(Router.batchUpdate(request: request, listUuid: listUuid)).validate().responseData { (dataResponse) in
            if dataResponse.response?.statusCode == ListContentServiceError.listNotExisting.rawValue {
                callback(Result.failure(ListContentServiceError.listNotExisting))
            } else if dataResponse.response?.statusCode != 200 {
                callback(Result.failure(ListContentServiceError.unknown))
            } else {
                switch dataResponse.result {
                case .success:
                    callback(Result.success(()))
                case .failure(let error):
                    Log.e("Service error: \(error.localizedDescription)")
                    callback(Result.failure(error))
                }
            }
        }
    }
}
