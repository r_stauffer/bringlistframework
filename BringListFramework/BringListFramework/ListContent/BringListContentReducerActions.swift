//
//  BringListContentReducerActions.swift
//  Bring
//
//  Created by Renato Stauffer on 25.02.19.
//

import Foundation

public protocol BringListContentAction {}

struct BringListContentSelectAction: BringListContentAction {
    let item: BringItem
}

struct BringListContentUpdateAction: BringListContentAction {
    let currentListState: BringListState
    let userItemsToAdd: [BringItem]
    let sectionOrder: [String]
    let syncStartDate: Date
    let translations: [String: String]
}

struct BringListContentMoveCatalogItemAction: BringListContentAction {
    let itemToMove: BringItem
    let sectionToMoveTo: BringSection
}

struct BringListContentUpdateRecommendedAction: BringListContentAction {
    let sectionName: String
    let items: [BringItem]
}

struct BringListContentReorderSectionAction: BringListContentAction {
    let sectionOrder: [String]
}

struct BringListContentUpdateSpecificationAction: BringListContentAction {
    let uniqueIdentifier: BringItem.UniqueIdentifier
    let specification: String
}

struct BringListContentAddItemAction: BringListContentAction {
    let item: BringItem
    let sectionId: String
}

struct BringListContentResetNewItemFlagsAction: BringListContentAction {}

struct BringListContentRemoveItemAction: BringListContentAction {
    let itemId: String
}

struct BringListContentCatalogChangeAction: BringListContentAction {
    let catalogChange: BringCatalogChange
}

struct BringListContentRemoveMultipleItemsAction: BringListContentAction {
    let items: [String]
    let section: String
}

struct BringListContentAddItemToPurchaseAction: BringListContentAction {
    let item: BringItem
}
