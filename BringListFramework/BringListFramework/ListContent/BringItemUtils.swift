//
//  BringItemUtils.swift
//  Bring
//
//  Created by Beat on 21.01.19.
//

import Foundation

@objc
public class BringItemUtils: NSObject {
    
    private static let regex = "^[\\d]+[\\w\\/\\.\\,]*[.]*[\\s]+((gr|g|kg|ml|dl|cl|l|liter|liters|pkg|dose|dosen|flasche|flaschen|pack|pkg.|kasten|bund|stk|stk.|büchsen|büchse|litres|litro|litri|pcs|boîte|scatola|can|boîtes|scatole|cans|bouteille|bottiglia|bottle|bouteilles|bottiglie|bottles|pezzi|pezzo|caisse|pacchi|bag|pièces|pacco|bags|pièce|sachet)[\\s]+)?"
    
    public static func extractQuantity(name : String) -> String {
        
        guard let regex = try? NSRegularExpression(pattern: regex, options: .caseInsensitive) else {
            return name
        }
        
        let rangeOfFirstMatch = regex.rangeOfFirstMatch(in: name, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSMakeRange(0, name.count))
        
        if !NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)) {
            return (name as NSString).substring(with: rangeOfFirstMatch).trimmingCharacters(in: .whitespaces)
        }
        return ""
    }
    
    public static func removeQuantityInfo(fromKey key: String) -> String {
        
        guard let regex = try? NSRegularExpression(pattern: regex, options: .caseInsensitive) else {
            return key
        }
        
        var modifiedName: String = regex.stringByReplacingMatches(in: key, options:NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSMakeRange(0, key.count), withTemplate: "")
        modifiedName = modifiedName.trimmingCharacters(in: CharacterSet.whitespaces)
        return modifiedName
    }

}
