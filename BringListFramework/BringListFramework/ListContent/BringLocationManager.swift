//
//  BringLocationManager.swift
//  Bring
//
//  Created by Sascha Thöni on 15.08.17.
//

import Foundation
import CoreLocation

// TODO: Move all this to a Bring! Location framework / Library
public protocol BringLocationManagerDelegate: class {
    func changedLocation(_ location: CLLocation)
    func changedPermission(_ state: BringLocationManager.PermissionState)
}

public protocol LocalLocationService {
    func requestLocation(with accuracy: BringLocalLocationService.Accuracy, callback: @escaping (Result<(BringGeoLocation?, BringLocalLocationService.LocationAccuracyResult), Error>) -> Void)
}

public class BringLocalLocationService: NSObject, CLLocationManagerDelegate, LocalLocationService {
    
    public enum Accuracy {
        case bestForNavigation
        case bestAccuracy
        case nearestTenMeters
        case hundredMeters
        case kilometer
        case threeKilometers
    }
    
    enum PermissionState: Int {
        case denied
        case granted
    }
    
    public enum LocationAccuracyResult {
        case accurate
        case inaccurate
        case none // The user has not granted permission for the location services
    }
    
    private typealias Callback = (Result<(BringGeoLocation?, LocationAccuracyResult), Error>) -> Void
    
    private var callbacks = [Callback]()
    
    private let locationManager: CLLocationManager
    private let locationManagerType: CLLocationManager.Type
    private var currentLocation: CLLocation?
    
    public init(locationManager: CLLocationManager = CLLocationManager(), locationManagerType: CLLocationManager.Type = CLLocationManager.self) {
        self.locationManager = locationManager
        self.locationManagerType = locationManagerType
        super.init()
        self.locationManager.delegate = self
    }
    
    public func requestLocation(with accuracy: Accuracy, callback: @escaping (Result<(BringGeoLocation?, LocationAccuracyResult), Error>) -> Void) {
        callbacks.append(callback)
        self.startTracking(with: accuracy)
    }
    
    private func getPermissionState(_ status: CLAuthorizationStatus) -> PermissionState {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            return .granted
        default:
            return .denied
        }
    }
    
    // MARK: CLLocationManagerDelegate
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Log.e("Failed to get location: \(error.localizedDescription)")
        
        locationManager.stopUpdatingLocation()
        
        let currentCallbacks = callbacks
        self.callbacks.removeAll()
        
        for callback in currentCallbacks {
            callback(Result.failure(error))
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last!
        
        // Instead of returning a new location fix, the location manager may initially give the most recently found location under the assumption that we might not have moved much in the last few seconds. We simply ignore these cases
        if newLocation.timestamp.timeIntervalSinceNow < -5 {
            return
        }
        
        // negative accuracies are invalide according to documentation
        if newLocation.horizontalAccuracy < 0 {
            return
        }
        
        var distance = CLLocationDistance(Double.greatestFiniteMagnitude)
        
        if let location = currentLocation {
            distance = newLocation.distance(from: location)
        }
        
        if currentLocation == nil || currentLocation!.horizontalAccuracy > newLocation.horizontalAccuracy {
            
            currentLocation = newLocation
            
            guard newLocation.horizontalAccuracy <= locationManager.desiredAccuracy else {
                return
            }
            
            self.stopTrackingLocation(with: newLocation, resultType: .accurate)
            
        } else if distance < 1 {
            
            // If we moved less than a meter, check if we tried getting the user location for 10 seconds
            let timeInterval = newLocation.timestamp.timeIntervalSince(currentLocation!.timestamp)
            
            // If we did not see any improvements in getting the user location for at least 10 seconds, then stop the tracking as it is unlikely that the location manager will finde a more accurate loaction. This is here so save battery power.
            guard timeInterval > 10 else {
                return
            }
            
            self.stopTrackingLocation(with: newLocation, resultType: .inaccurate)
        }
    }
    
    // MARK: MISC
    
    private func startTracking(with accuracy: Accuracy) {
        
        guard locationManagerType.locationServicesEnabled() else {
            Log.d("Location service disabled.")
            finishWithoutLocation()
            return
        }
        
        guard locationManagerType.authorizationStatus() == .authorizedWhenInUse || locationManagerType.authorizationStatus() == .authorizedAlways else {
            Log.d("Location permission missing.")
            finishWithoutLocation()
            return
        }
        
        locationManager.desiredAccuracy = getCoreLocationAccuracy(for: accuracy)
        locationManager.distanceFilter = kCLDistanceFilterNone  // In meters.
        locationManager.delegate = self
        
        locationManager.startUpdatingLocation()
    }
    
    private func finishWithoutLocation() {
        let currentCallbacks = callbacks
        self.callbacks.removeAll()
        
        for callback in currentCallbacks {
            callback(Result.success((nil, .none)))
        }
    }
    
    private func stopTrackingLocation(with location: CLLocation, resultType: LocationAccuracyResult) {
        
        locationManager.stopUpdatingLocation()
        
        let location = BringGeoLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, altitude: location.altitude, accuracy: location.horizontalAccuracy)
        
        let currentCallbacks = callbacks
        self.callbacks.removeAll()
        
        for callback in currentCallbacks {
            callback(Result.success((location, resultType)))
        }
    }
    
    private func getCoreLocationAccuracy(for accuracy: Accuracy) -> CLLocationAccuracy {
        switch accuracy {
        case .bestForNavigation:
            return kCLLocationAccuracyBestForNavigation
        case .bestAccuracy:
            return kCLLocationAccuracyBest
        case .nearestTenMeters:
            return kCLLocationAccuracyNearestTenMeters
        case .hundredMeters:
            return kCLLocationAccuracyHundredMeters
        case .kilometer:
            return kCLLocationAccuracyKilometer
        case .threeKilometers:
            return kCLLocationAccuracyThreeKilometers
        }
    }
}

public class BringLocationManager: NSObject, CLLocationManagerDelegate, LocationService {
    
    @objc
    public enum PermissionState: Int {
        case denied
        case granted
    }
    
    @objc
    public static let shared = BringLocationManager()
    
    public weak var delegate: BringLocationManagerDelegate?
    
    private var lastUpdatedLocations = [CLLocation]()
    
    public var lastLocation: CLLocation? {
        return self.lastUpdatedLocations.first
    }
    
    private let locationManager = CLLocationManager()
    
    override init() {
        super.init()
        self.locationManager.delegate = self
    }
    
    
    public func hasPermittedLocation() -> Bool {
        return CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
    }
    
    public func hasPermissionDenied() -> Bool {
        return CLLocationManager.authorizationStatus() == .denied
    }
    
    public func canRequestAuthorization() -> Bool {
        return CLLocationManager.authorizationStatus() == .notDetermined
    }
    
    public func requestLocationAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        Log.v()
        
        let state = getPermissionState(status)
        
        switch state {
        case .denied:
            Log.d("Permission denied.")
        case .granted:
            Log.d("Permission granted.")
            startTracking()
        }
        
        self.delegate?.changedPermission(state)
    }
    
    private func getPermissionState(_ status: CLAuthorizationStatus) -> PermissionState {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            return .granted
        default:
            return .denied
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Log.e("Failed to get location: \(error.localizedDescription)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.lastUpdatedLocations = locations
        
        guard let lastLocation = self.lastLocation else {
            return
        }
        
        let location = BringGeoLocation(latitude: lastLocation.coordinate.latitude, longitude:lastLocation.coordinate.longitude, altitude : lastLocation.altitude, accuracy: lastLocation.horizontalAccuracy)
        BringAlamofireManager.sharedInstance.setLocation(location: location)
        
        self.delegate?.changedLocation(lastLocation)
    }
    
    @objc
    func startTracking() {
        
        guard CLLocationManager.locationServicesEnabled() else {
            Log.d("Location service disabled.")
            return
        }
        
        guard CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways else {
            Log.d("Location permission missing.")
            return
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone  // In meters.
        locationManager.delegate = self
        
        locationManager.startUpdatingLocation()
    }
    
    @objc
    func stopTrackingLocation() {
        locationManager.stopUpdatingLocation()
    }

}
