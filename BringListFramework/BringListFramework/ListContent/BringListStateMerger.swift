//
//  BringListStateMerger.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

class BringDefaultListStateMerger: ListStateMerger {
    
    let syncStartDate: Date
    
    init(syncStartDate: Date) {
        self.syncStartDate = syncStartDate
    }
    
    func merge(_ listState: BringListState, withOther otherListState: BringListState) -> BringListState {
        let remoteListState = listState
        let localListState = otherListState

        
        var newListStatePurchaseSection = localListState.purchase
        var newListStateRecentlySection = localListState.recently
        
        self.merge(remotePurchase: remoteListState.purchase, toLocalPurchase: &newListStatePurchaseSection, andRecently: &newListStateRecentlySection)
        self.merge(remoteRecently: remoteListState.recently, toLocalPurchase: &newListStatePurchaseSection, andRecently: &newListStateRecentlySection)
        
        let sortedRecently = newListStateRecentlySection.sortOrder(accordingTo: otherListState.recently)
        
        let mergedListState = BringListState(
            purchase: newListStatePurchaseSection,
            recently: sortedRecently,
            status: remoteListState.status,
            uuid: remoteListState.uuid)
        return mergedListState
    }
    
    private func merge(remoteRecently: [BringListStateItem], toLocalPurchase localPurchase: inout [BringListStateItem], andRecently localRecently: inout [BringListStateItem]) {
        let localPurchaseCopy = localPurchase
        let localRecentlyCopy = localRecently
        
        localRecently.removeAll { localItem -> Bool in
            localItem.modificationDate.timeIntervalSince1970 < syncStartDate.timeIntervalSince1970
        }
        
        for item in remoteRecently {
            let remoteTimeStamp = syncStartDate.timeIntervalSince1970
            
            if localPurchaseCopy.contains(item) {
                for localItem in localPurchaseCopy where localItem == item {
                    let localItemTimeStamp = localItem.modificationDate.timeIntervalSince1970
                    if remoteTimeStamp > localItemTimeStamp {
                        localPurchase = localPurchase.filter { $0 != localItem }
                        localRecently.insert(item, at: 0)
                    }
                }
            } else if localRecentlyCopy.contains(item) {
                for localItem in localRecentlyCopy where localItem == item {
                    let localItemTimeStamp = localItem.modificationDate.timeIntervalSince1970
                    if remoteTimeStamp > localItemTimeStamp {
                        localRecently = localRecently.filter { $0 != localItem }
                        localRecently.insert(item, at: 0)
                    }
                }
            } else {
                localRecently.insert(item, at: 0)
            }
        }
    }
    
    private func merge(remotePurchase: [BringListStateItem], toLocalPurchase localPurchase: inout [BringListStateItem], andRecently localRecently: inout [BringListStateItem]) {
        let localPurchaseCopy = localPurchase
        let localRecentlyCopy = localRecently
        
        localPurchase.removeAll { localItem -> Bool in
            localItem.modificationDate.timeIntervalSince1970 < syncStartDate.timeIntervalSince1970
        }
        
        for item in remotePurchase {
            let remoteTimeStamp = syncStartDate.timeIntervalSince1970
            
            if localPurchaseCopy.contains(item) {
                for localItem in localPurchaseCopy where localItem == item {
                    let localItemTimeStamp = localItem.modificationDate.timeIntervalSince1970
                    if remoteTimeStamp > localItemTimeStamp {
                        localPurchase = localPurchase.filter { $0 != localItem }
                        localPurchase.append(item)
                    }
                }
            } else if localRecentlyCopy.contains(item) {
                for localItem in localRecentlyCopy where localItem == item {
                    let localItemTimeStamp = localItem.modificationDate.timeIntervalSince1970
                    if remoteTimeStamp > localItemTimeStamp {
                        localRecently = localRecently.filter { $0 != localItem }
                        localPurchase.append(item)
                    }
                }
            } else {
                localPurchase.append(item)
            }
        }
    }
}
