//
//  BringDefaultIconLoader.swift
//  Bring
//
//  Created by Renato Stauffer on 06.11.18.
//

import Foundation
import UIKit

public final class BringDefaultIconLoader: NSObject, IconLoadable {
    
    public let identifier = "BringDefaultIconLoader"
    
    public func loadIcon(forKey key: String, alernativeIconKey alternativeKey: String?) -> IconLoadResult {
        if key.hasPrefix("Bring!") {
            return IconLoadResult(icon: UIImage(namedFrameworkImage: "Bring"), loadableIdentifier: self.identifier, isFallbackImage: false)
        }

        if let alternativeIconKey = alternativeKey {
            if !alternativeIconKey.isEmpty {
                if let image = UIImage(namedFrameworkImage: BringIconUtils.getCleanImageKey(for: alternativeIconKey)) {
                    return IconLoadResult(icon: image, loadableIdentifier: self.identifier, isFallbackImage: false)
                }
            }
        }

        let sanitizedName = BringIconUtils.sanitizedFileNameString(fileName: key)
        if let image = UIImage(namedFrameworkImage: BringIconUtils.getCleanImageKey(for: sanitizedName)) {
            return IconLoadResult(icon: image, loadableIdentifier: self.identifier, isFallbackImage: false)
        }
        let image = UIImage(namedFrameworkImage: BringIconUtils.fallbackImage(forKey: sanitizedName))
        return IconLoadResult(icon: image, loadableIdentifier: self.identifier, isFallbackImage: true)
    }
}
