//
//  IconLoadingManager.swift
//  Bring
//
//  Created by Renato Stauffer on 16.10.18.
//

import Foundation
import UIKit

@objc public protocol IconProvidable: class {
    func loadIcon(forKey key: String, alernativeIconKey alternativeKey: String?) -> UIImage?
    func loadIconAsync(forRequest request: IconLoaderRequest, callback: @escaping (UIImage?) -> Void)
    func register(_ newLoader: IconLoadable)
}

@objc public class IconLoaderRequest: NSObject {
    let key: String
    let alternativeKey: String?
    let ignoredIconLoaders: [IconLoadable]
    let ignoreImageCache: Bool
    
    public init(key: String, alternativeKey: String? = nil, ignoredIconLoaders: [IconLoadable] = [], ignoreImageCache: Bool = false) {
        self.key = key
        self.alternativeKey = alternativeKey
        self.ignoredIconLoaders = ignoredIconLoaders
        self.ignoreImageCache = ignoreImageCache
    }
}

@objc public final class IconProvider: NSObject, IconProvidable {
    
    private var imageCache: Dictionary<String, UIImage>
    
    /// Do not access this outside of the `concurrentIconLoadablesQueue` queue. Use `iconLoadables` to access the loadables.
    private var unsafeLoadables: [IconLoadable] = []
    
    private var iconLoadables: [IconLoadable] {
        var copy: [IconLoadable] = []
        concurrentIconLoadablesQueue.sync { [weak self] in
            guard let strongSelf = self else { return }
            copy = strongSelf.unsafeLoadables
        }
        return copy
    }
    
    /// Post this notification to flush the cache.
    @objc public static let flushCacheNotification = Notification(name: Notification.Name.init("Bring.Notification.FlushCacheNotification"))
    
    /// This notification gets posted when a image has been flushed from the cache
    @objc public static let flushedCacheNotification = Notification(name: Notification.Name.init("Bring.Notification.FlushedCacheNotification"))
    
    private let concurrentIconLoadablesQueue = DispatchQueue(label: "ch.publisheria.bring.concurrentIconLoadablesQueue", attributes: .concurrent)
    
    public override init() {
        imageCache = Dictionary<String, UIImage>()
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveFlushNotification(notification:)), name: IconProvider.flushCacheNotification.name, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: IconProvider.flushCacheNotification.name, object: nil)
    }
    
    @objc private func didReceiveFlushNotification(notification: NSNotification) {
        if let key = notification.userInfo?["key"] as? NSString {
            flushCachedImage(forKey: String(key))
        } else {
            flushCache()
        }
    }

    
    // MARK: Public API
    
    @objc public func flushCache() {
        imageCache.removeAll()
    }
    
    @objc public func flushCachedImage(forKey key: String) {
        imageCache.removeValue(forKey: key)
        NotificationCenter.default.post(name: IconProvider.flushedCacheNotification.name, object: self, userInfo: ["key": key])
    }
    
    public func register(_ newLoader: IconLoadable) {
        if iconLoadables.contains(where: { $0.identifier == newLoader.identifier} ) {
            return
        }
        concurrentIconLoadablesQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.unsafeLoadables.append(newLoader)
        }
    }
    
    @objc public func loadIcon(forKey key: String, alernativeIconKey alternativeKey: String?) -> UIImage? {
        if let image = imageCache[key] {
            return image
        }
        for iconloader in iconLoadables {
            let result = iconloader.loadIcon(forKey: key, alernativeIconKey: alternativeKey)
            if result.icon != nil {
                if !result.isFallbackImage {
                    imageCache[key] = result.icon
                }
                return result.icon
            }
        }
        return nil
    }
    
    @objc public func loadIconAsync(forRequest request: IconLoaderRequest, callback: @escaping (UIImage?) -> Void) {
        if !request.ignoreImageCache {
            if let image = imageCache[request.key] {
                callback(image)
                return
            }
        }
        DispatchQueue.global(qos: .default).async { [weak self] in
            
            guard let strongSelf = self else {
                callback(nil)
                return
            }
            
            autoreleasepool {
                var ignoredLoaders = [String: IconLoadable]()
                request.ignoredIconLoaders.forEach({ (iconLoader) in
                    ignoredLoaders[iconLoader.identifier] = iconLoader
                })
                
                for iconloader in strongSelf.iconLoadables {
                    if (ignoredLoaders[iconloader.identifier] != nil) {
                        continue
                    }
                    let result = iconloader.loadIcon(forKey: request.key, alernativeIconKey: request.alternativeKey)
                    if result.icon != nil {
                        if !result.isFallbackImage {
                            DispatchQueue.main.async {
                                strongSelf.imageCache[request.key] = result.icon
                            }
                        }
                        DispatchQueue.main.async {
                            callback(result.icon)
                        }
                        return
                    }
                }
                DispatchQueue.main.async {
                    callback(nil)
                }
            }
        }
    }
    
}
