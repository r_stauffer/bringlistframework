//
//  IconLoadable.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation
import UIKit

@objc public protocol IconLoadable: class {
    var identifier: String { get }
    func loadIcon(forKey key: String, alernativeIconKey alternativeKey: String?) -> IconLoadResult
}

@objc public final class IconLoadResult: NSObject {
    let icon: UIImage?
    let loadableIdentifier: String
    let isFallbackImage: Bool
    
    public init(icon: UIImage?, loadableIdentifier: String, isFallbackImage: Bool) {
        self.icon = icon
        self.loadableIdentifier = loadableIdentifier
        self.isFallbackImage = isFallbackImage
    }
}
