//
//  IconProvider+Optimized.swift
//  Bring
//
//  Created by Beat on 04.12.18.
//

import Foundation
#if os(iOS)
import UIKit
#endif

#if os(iOS)
extension IconProvider {
    
    public func createOptimizedImage(_ image: UIImage?, color: UIColor?) -> UIImage? {
        
        guard let image = image, let color = color else {
            return nil
        }
        
        var optimized: UIImage?
        
        UIGraphicsBeginImageContextWithOptions(image.size, true, UIScreen.main.scale)
        
        if let ctx = UIGraphicsGetCurrentContext() {
            
            let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            let colorRect = CGRect(x: 0, y: 0, width: image.size.width + 1, height: image.size.height + 1)
            
            color.setFill()
            ctx.fill(colorRect)
            image.draw(in: rect)
            
            optimized = UIGraphicsGetImageFromCurrentImageContext()
            
        } else {
            Log.w("Failed to get context.")
            optimized = image
        }
        
        UIGraphicsEndImageContext()
        
        return optimized
    }
}
#endif
