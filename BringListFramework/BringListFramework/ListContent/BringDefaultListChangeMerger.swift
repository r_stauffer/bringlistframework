//
//  BringDefaultListChangeMerger.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

final class BringDefaultListChangeMerger: ListChangeMerger {
    
    let listChange: BringListChange
    let logger: BringRemoteLoggingManager
    
    init(listChange: BringListChange, logger: BringRemoteLoggingManager = BringRemoteLoggingManager.shared) {
        self.listChange = listChange
        self.logger = logger
    }
    
    func mergeListChange(into currentListState: BringListState?) -> BringListState {
        
        if currentListState == nil {
            logger.log(
                cls: "BringDefaultListChangeMerger",
                method: "mergeListChange(:)",
                description: "No list state is saved yet",
                issue: "listUuid = \(self.listChange.listUuid)"
            )
        }
        
        let listState = currentListState ?? BringListState(purchase: [], recently: [], status: BringListStatus.unregistered.rawValue, uuid: self.listChange.listUuid)
        
        var newPurchase = listState.purchase
        var newRecently = listState.recently
        
        if self.listChange.operation == BringListContentOperation.toPurchase {
            if let itemInPurchase = self.getItem(in: listState.purchase) {
                if itemInPurchase.specification != self.listChange.spec {
                    newPurchase = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.purchase)
                    
                    let item = BringListStateItem(
                        uuid: self.listChange.uuid,
                        specification: self.listChange.spec,
                        itemId: self.listChange.itemId,
                        modificationDate: Date.init(timeIntervalSince1970: self.listChange.timeStamp)
                    )
                    
                    newPurchase.append(item)
                }
            } else {
                let item = BringListStateItem(
                    uuid: self.listChange.uuid,
                    specification: self.listChange.spec,
                    itemId: self.listChange.itemId,
                    modificationDate: Date.init(timeIntervalSince1970: self.listChange.timeStamp)
                )
                newPurchase.append(item)
            }
            newRecently = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.recently)
        }
        
        if self.listChange.operation == BringListContentOperation.toRecently {
            if let itemInRecently = self.getItem(in: listState.recently) {
                if itemInRecently.specification != self.listChange.spec {
                    newRecently = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.recently)
                    let item = BringListStateItem(
                        uuid: self.listChange.uuid,
                        specification: self.listChange.spec,
                        itemId: self.listChange.itemId,
                        modificationDate: Date.init(timeIntervalSince1970: self.listChange.timeStamp)
                    )
                    newRecently.insert(item, at: 0)
                }
            } else {
                let item = BringListStateItem(
                    uuid: self.listChange.uuid,
                    specification: self.listChange.spec,
                    itemId: self.listChange.itemId,
                    modificationDate: Date.init(timeIntervalSince1970: self.listChange.timeStamp)
                )
                newRecently.insert(item, at: 0)
            }
            newPurchase = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.purchase)
        }
        
        if self.listChange.operation == BringListContentOperation.remove {
            newPurchase = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.purchase)
            newRecently = self.removeItem(uuid: self.listChange.uuid, itemId: self.listChange.itemId, fromItems: listState.recently)
        }
        
        let newListState = BringListState(purchase: newPurchase, recently: newRecently, status: listState.status, uuid: listState.uuid)
        return newListState

    }
    
    private func removeItem(uuid: String?, itemId: String, fromItems items: [BringListStateItem]) -> [BringListStateItem] {
        return items.filter {$0.uuid != uuid }
    }
    
    private func getItem(in items: [BringListStateItem]) -> BringListStateItem? {
        return items.filter { $0.uuid == self.listChange.uuid }.first
    }
    
}
