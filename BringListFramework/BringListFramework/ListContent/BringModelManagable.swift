//
//  BringModelManagable.swift
//  BringShared
//
//  Created by Renato Stauffer on 07.08.19.
//

import Foundation

public protocol BringModelManagable {
    func section(bySectionId sectionId: String) -> BringSection?
    func move(catalogItem: BringItem, toSection section: BringSection)
}
