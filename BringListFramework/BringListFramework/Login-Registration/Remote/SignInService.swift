//
//  SignInService.swift
//  BringWatch
//
//  Created by Renato Stauffer on 14.10.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation
import Alamofire

public protocol SignInService {
    func login(withEmail email : String, password : String, listUuid : String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
    func magicLogin(code: String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
    func socialSignIn(authProvider: String, authToken: String, currentUserUuid: String?, email: String?, name: String?, fastlane: Bool, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
    func doesUserExist(forEmail email : String, onUserExists: @escaping () -> (), onUnknownUser: @escaping () -> (), onInvalidEmail: @escaping () -> ())
    func completeRegistration(withPrivateUserUuid privateUuid : String, email:String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
    func createUser(forEmail email : String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
    func resendPassword(toEmail email : String, onSuccess: @escaping () -> (), onUnknownUser: @escaping () -> (), onFailure: @escaping () -> ())
    func requestMagicLink(forEmail email: String, onSuccess: @escaping () -> (), onFailure: @escaping () -> ())
    func createAnonymousUser(onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ())
}

class BringAlamofireSignInService {
    
    enum Router: URLRequestConvertible {
        
        case login(String, String, String)
        case socialSignIn(String, String, String?, String?, String?, Bool)
        case magicLogin(String)
        case createUser(String)
        case createAnonymousUser
        case resendPassword(String)
        case doesUserExist(String)
        case completeRegistration(String, String)
        case requestMagicLink(String)
        
        
        var method: HTTPMethod {
            switch self {
            case .login:
                return .post
            case .socialSignIn:
                return .post
            case .magicLogin:
                return .post
            case .createUser:
                return .post
            case .createAnonymousUser:
                return .post
            case .resendPassword:
                return .post
            case .doesUserExist:
                return .get
            case .completeRegistration:
                return .put
            case .requestMagicLink:
                return .post
            }
        }
        
        var path: String {
            switch self {
            case .login(_,_,_):
                return "v2/bringauth/"
            case .socialSignIn(_,_,_,_,_,_):
            return "v2/bringauth/socialsignin"
            case .magicLogin(_):
                return "v2/bringauth/magiclogin"
            case .createUser(_):
                return "v2/bringauth/signup"
            case .createAnonymousUser:
                return "v2/bringauth/signupanonym"
            case .resendPassword(_):
                return "bringusers/passwords"
            case .doesUserExist(_):
                return "bringusers"
            case .completeRegistration(let privateUuid, _):
                return "bringusers/\(privateUuid)"
            case .requestMagicLink(_):
                return "bringusers/magiclink"
            }
        }
        
        // MARK: URLRequestConvertible
        
        func asURLRequest() throws -> URLRequest {
            let URL = Foundation.URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue
            
            switch self {
            case .login(let email, let password, let listUuid):
                var parameters = [String:Any]()
                parameters["email"] = email
                parameters["password"] = password
                
                if !listUuid.isEmpty {
                    parameters["currentListUuid"] = listUuid
                }
                
                let encodingUrl = URLEncoding.default
                
                try mutableURLRequest = encodingUrl.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
                
            case .socialSignIn(let authProvider, let authToken, let currentUserUuid, let email, let name, let fastlane):
                
                var parameters = [String:Any]()
                parameters["authProvider"] = authProvider
                parameters["authToken"] = authToken
                
                if let email = email {
                    parameters["email"] = email
                }
                
                if let name = name {
                    parameters["name"] = name
                }
                
                if let currentUserUuid = currentUserUuid {
                    parameters["currentUserUuid"] = currentUserUuid
                }
                
                parameters["fastlane"] = fastlane
                
                let encodingUrl = URLEncoding.default
                
                try mutableURLRequest = encodingUrl.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
                
            case .magicLogin(let code):
                
                var parameters = [String:Any]()
                parameters["code"] = code
                
                let encodingUrl = URLEncoding.default
                
                try mutableURLRequest = encodingUrl.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
                
            case .createUser(let email), .resendPassword(let email), .doesUserExist(let email), .requestMagicLink(let email):
                var parameters = [String:Any]()
                parameters["email"] = email
                try mutableURLRequest = URLEncoding.default.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
                
            case .completeRegistration(_, let email):
                var parameters = [String:Any]()
                parameters["email"] = email
                try mutableURLRequest = URLEncoding.default.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
            default:
                return mutableURLRequest
            }
        }
    }
    
    public func login(withEmail email : String, password : String, listUuid : String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.login(email, password, listUuid)).validate()
            .responseObject {(response:DataResponse<BringAuthResponse>) in
                if response.result.isSuccess {
                    guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                        onFailure()
                        return
                    }
                    
                    onSuccess(loginData)
                } else {
                    onFailure()
                }
        }
    }
    
    public func socialSignIn(authProvider: String, authToken: String, currentUserUuid: String?, email: String? = nil, name: String? = nil, fastlane: Bool, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.socialSignIn(authProvider, authToken, currentUserUuid, email, name, fastlane)).validate()
            .responseObject {(response:DataResponse<BringAuthResponse>) in
                if response.result.isSuccess {
                    guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                        onFailure()
                        return
                    }
                    
                    onSuccess(loginData)
                } else {
                    onFailure()
                }
        }
    }
    
    public func magicLogin(code: String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.magicLogin(code)).validate()
            .responseObject {(response:DataResponse<BringAuthResponse>) in
                if response.result.isSuccess {
                    guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                        onFailure()
                        return
                    }
                    
                    onSuccess(loginData)
                } else {
                    onFailure()
                }
        }
    }
    
    public func createUser(forEmail email : String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> () ) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.createUser(email)).validate().responseObject {(response:DataResponse<BringAuthResponse>) in
            if response.result.isSuccess {
                guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                    onFailure()
                    return
                }
                
                onSuccess(loginData)
            } else {
                onFailure()
            }
        }
    }
    
    public func requestMagicLink(forEmail email: String, onSuccess: @escaping () -> (), onFailure: @escaping () -> ()) {
        
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.requestMagicLink(email)).validate().response { (response) in
            if response.error == nil {
                onSuccess()
            } else {
                Log.e("\(response.error?.localizedDescription ?? "")")
                onFailure()
            }
        }
    }
    
    public func completeRegistration(withPrivateUserUuid privateUuid : String, email:String, onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> () ) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.completeRegistration(privateUuid, email)).validate()
            .responseObject {(response:DataResponse<BringAuthResponse>) in
                
                switch response.result {
                case .success:
                    guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                        onFailure()
                        return
                    }
                    
                    onSuccess(loginData)
                case .failure(let error):
                    Log.e(error.localizedDescription)
                    onFailure()
                }
        }
    }
    
    public func doesUserExist(forEmail email : String, onUserExists: @escaping () -> (), onUnknownUser: @escaping () -> (), onInvalidEmail: @escaping () -> () ) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.doesUserExist(email)).validate().responseString { (response) in
            if response.result.isSuccess {
                onUserExists()
            } else if response.result.isFailure {
                // get status code
                if let statusCode = response.response?.statusCode, statusCode == 404 {
                    onUnknownUser()
                } else {
                    onInvalidEmail()
                }
            }
        }
    }
    
    public func resendPassword(toEmail email : String, onSuccess: @escaping () -> (), onUnknownUser: @escaping () -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.resendPassword(email)).validate().responseString { (response) in
            if response.result.isSuccess {
                onSuccess()
            } else if response.result.isFailure {
                // get status code
                if let statusCode = response.response?.statusCode, statusCode == 404 {
                    onUnknownUser()
                } else {
                    onFailure()
                }
            }
        }
    }
    
    public func createAnonymousUser(onSuccess: @escaping (BringUserLoginData) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.createAnonymousUser).validate().responseObject {(response:DataResponse<BringAuthResponse>) in
            if response.result.isSuccess {
                guard let loginData = self.map(authResponse: response.result.value, status: response.response?.statusCode) else {
                    onFailure()
                    return
                }
                
                onSuccess(loginData)
            } else {
                onFailure()
            }
        }
    }
    
    private func map(authResponse : BringAuthResponse?, status: Int?) -> BringUserLoginData? {
        
        guard let response = authResponse else {
            return nil
        }
        
        let loginData = BringUserLoginData()
        
        loginData.accessToken = response.accessToken != nil ? response.accessToken! : ""
        loginData.refreshToken = response.refreshToken != nil ? response.refreshToken! : ""
        
        loginData.email = response.email != nil ? response.email! : ""
        loginData.name = response.name != nil ? response.name! : ""
        loginData.photoPath = response.photoPath != nil ? response.photoPath! : ""
        loginData.publicUuid = response.publicUuid != nil ? response.publicUuid! : ""
        loginData.privateUuid = response.privateUuid != nil ? response.privateUuid! : ""
        loginData.currenListUuid = response.currenListUuid
        
        loginData.statusCode = status ?? 0
        
        return loginData
    }
    
}
