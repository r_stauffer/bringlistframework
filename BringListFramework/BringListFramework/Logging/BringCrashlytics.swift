//
//  BringCrashlytics.swift
//  BringWatch
//
//  Created by Renato Stauffer on 05.11.19.
//  Copyright © 2019 Bring! Labs AG. All rights reserved.
//

import Foundation

public protocol RemoteLogger {
    func recordError(_ error: Error, withAdditionalUserInfo userInfo: [String : Any]?)
    func crash()
}

private class BringEmptyLogger: RemoteLogger {
    func recordError(_ error: Error, withAdditionalUserInfo userInfo: [String : Any]?) {}
    func crash() {}
}

public class BringRemoteLoggingManager {
    
    // MARK: Log
    private static var remoteLogger: RemoteLogger? = BringEmptyLogger()
    
    public static let shared: BringRemoteLoggingManager = BringRemoteLoggingManager()
    
    public static func setup(logger: RemoteLogger) {
        remoteLogger = logger
    }
    
    private init() {}
    
    public func log(cls: String, method: String, description: String, issue: String? = nil, error: Error? = nil, shouldLog: Bool = true) {
        
        guard shouldLog else { return }
        
        Log.w("cls = \(cls), method = \(method), description = \(description)")
        
        var userInfo = [String: String]()
        
        if let issue = issue {
            userInfo["issue"] = issue
        }
        
        if let err = error as NSError? {
            userInfo["error"] = "\(err.domain) (\(err.code)): \(err.localizedDescription) \(err.localizedFailureReason ?? "Unknown reason.") \(err.localizedRecoverySuggestion ?? "Unknown recovery.")"
        }
        
        let exception = NSError(domain: String(format: "%@ - %@: %@", cls, method, description), code: 0, userInfo: nil)
        
        BringRemoteLoggingManager.remoteLogger?.recordError(exception, withAdditionalUserInfo: userInfo)
    }
    
    
    // MARK: Crash
    
    public func crash() {
        Log.w()
        
        BringRemoteLoggingManager.remoteLogger?.crash()
    }
}
