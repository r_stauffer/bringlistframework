//
//  Log.swift
//  BringExtensionKit
//
//  Created by Beat on 13.03.18.
//

import Foundation

// @TODO: Move this to a core framework!
class Log {

    #if DEBUG
    public static func i(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[\((filename as NSString).lastPathComponent):\(line)] \(function) %@", message())
    }
    public static func v(_ message:  @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[\((filename as NSString).lastPathComponent):\(line)] \(function) %@", message())
    }
    public static func d(_ message:  @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[\((filename as NSString).lastPathComponent):\(line)] \(function) %@", message())
    }
    public static func c(_ message:  @autoclosure () -> String = "", filename: String = #file) {
        NSLog("[\((filename as NSString).lastPathComponent)] %@", message())
    }
    public static func w(_ message:  @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[WARNING] [\((filename as NSString).lastPathComponent):\(line)] \(function) ⚠️⚠️⚠️ %@ ⚠️⚠️⚠️", message())
    }
    public static func e(_ message:  @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[ERROR] [\((filename as NSString).lastPathComponent):\(line)] \(function) ⛔️⛔️⛔️ %@ ⛔️⛔️⛔️", message())
    }
    public static func ed(_ message:  @autoclosure () -> Error, filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("[ERROR] [\((filename as NSString).lastPathComponent):\(line)] \(function) ⛔️⛔️⛔️ %@ ⛔️⛔️⛔️", message().localizedDescription)
    }
    #else
    public static func i(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {}
    public static func v(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {}
    public static func d(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {}
    public static func c(_ message:  @autoclosure () -> String = "", filename: String = #file) {}
    public static func w(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {}
    public static func e(_ message: @autoclosure () -> String = "", filename: String = #file, function: String = #function, line: Int = #line) {}
    public static func ed(_ message:  @autoclosure () -> Error, filename: String = #file, function: String = #function, line: Int = #line) {}
    #endif
}
