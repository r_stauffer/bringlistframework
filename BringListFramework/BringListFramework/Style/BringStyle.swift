//
//  BringStyle.swift
//  Bring
//
//  Created by Renato Stauffer on 29.08.19.
//

import Foundation
import UIKit

#if os(iOS)
public struct BringStyle {
    public struct Colors {
        public static let recently = UIColor(namedFrameworkColor: "recently")!
        public static let purchase = UIColor(namedFrameworkColor: "purchase")!
        public static let background = UIColor(namedFrameworkColor: "background")!
        public static let primaryButton = UIColor(namedFrameworkColor: "primary-button")!
        public static let primaryTextColor = UIColor(namedFrameworkColor: "primary-text-color")!
        public static let secondaryButton = UIColor(namedFrameworkColor: "secondary-button")!
    }
    
    public struct Fonts {
        public static let title = UIFont(name: "MuseoSans-500", size: 17.0)
        public static let subtitle = UIFont(name: "MuseoSans-500", size: 17.0)
        public static let text = UIFont(name: "MuseoSans-500", size: 15.0)
    }
}
#elseif os(watchOS)
public struct BringStyle {
    public struct Colors {
        public static let purchase = UIColor(red: 0.918, green: 0.322, blue: 0.322, alpha: 1.0)
        public static let recently = UIColor(red: 0.294, green: 0.667, blue: 0.631, alpha: 1.0)
        public static let background = UIColor(red: 0.184, green: 0.235, blue: 0.259, alpha: 1.0)
        public static let primaryButton = UIColor(red: 0.310, green: 0.671, blue: 0.635, alpha: 1.0)
        public static let primaryTextColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        public static let secondaryButton = UIColor(red: 0.827, green: 0.827, blue: 0.827, alpha: 1.0)
    }
    
    public struct Fonts {
        public static let title = UIFont(name: "MuseoSans-500", size: 17.0)
        public static let subtitle = UIFont(name: "MuseoSans-500", size: 17.0)
        public static let text = UIFont(name: "MuseoSans-500", size: 15.0)
    }
}
#endif

extension UIColor {
    
    #if os(iOS)
    convenience init?(namedFrameworkColor colorName: String) {
        self.init(named: colorName, in: Bundle.sharedFramework, compatibleWith: nil)
    }
    #endif
}
