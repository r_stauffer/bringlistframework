//
//  BringLocation.swift
//  BringExtensionKit
//
//  Created by Sascha Thöni on 24.10.17.
//

import Foundation

@objc
public class BringGeoLocation: NSObject, NSCoding, Codable {
    
    public var latitude : Double
    public var longitude : Double
    public var altitude : Double?
    public var accuracy : Double?
    
    public init(latitude : Double, longitude : Double, altitude: Double? = nil , accuracy : Double? = nil) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.accuracy = accuracy
    }
    
    required public convenience init(coder aDecoder: NSCoder) {
        
        let latitude = aDecoder.decodeDouble(forKey: "latitude")
        let longitude = aDecoder.decodeDouble(forKey: "longitude")
        
        var altitude: Double?
        if let alt = aDecoder.decodeObject(forKey: "altitude") as? Double {
            altitude = alt
        }
        
        var accuracy: Double?
        if let acc = aDecoder.decodeObject(forKey: "accuracy") as? Double {
            accuracy = acc
        }
        
        self.init(latitude: latitude, longitude: longitude, altitude: altitude, accuracy: accuracy)
    }
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.latitude, forKey: "latitude")
        aCoder.encode(self.longitude, forKey: "longitude")
        
        aCoder.encode(self.altitude, forKey: "altitude")
        aCoder.encode(self.accuracy, forKey: "accuracy")
        
    }
}
