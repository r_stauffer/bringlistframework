//
//  UIImage+Extensions.swift
//  Bring
//
//  Created by Renato Stauffer on 30.08.19.
//

import Foundation

extension UIImage {
    
    convenience init?(namedFrameworkImage name: String) {
        #if os(iOS)
        self.init(named: name, in: Bundle.sharedFramework, compatibleWith: nil)
        #elseif os(watchOS)
        self.init(named: name, in: Bundle.sharedFramework, with: nil)
        #endif
    }
}
