//
//  UIImage+Utils.swift
//  BringExtensionKit
//
//  Created by Beat on 22.03.18.
//

import UIKit

public extension UIImage {
    
    static func image(color: UIColor) -> UIImage {
        return UIImage.image(color: color, size: CGSize(width: 1, height: 1))
    }
    
    static func image(color: UIColor, size: CGSize) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor);
        context?.fill(rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        return image ?? UIImage()
    }
    
    func canvas(size: CGSize) -> UIImage? {
        
        guard let cgimage = self.cgImage else {
            return nil
        }
        
        let factor = 1.0 / self.scale;
        let source = CGSize(width: self.size.width * self.scale, height: self.size.height * self.scale)
        let target = CGSize(width: size.width * self.scale, height: size.height * self.scale)
        
        let src = CGRect(x: 0, y: 0, width: source.width, height: source.height)
        let dest = CGRect(x: round(((target.width - source.width) / 2.0) * factor), y: round(((target.height - source.height) / 2.0) * factor), width: round(source.width * factor), height: round(source.height * factor))
        
        if let ref = cgimage.cropping(to: src) {
            
            UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
            
            let image = UIImage(cgImage: ref, scale: self.scale, orientation: self.imageOrientation)
            image.draw(in: dest)
            
            let processed = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            
            return processed
            
        } else {
            return nil
        }
    }
    
    func colorized(background: UIColor, tint: UIColor) -> UIImage? {
        
        guard let cgimage = self.cgImage else {
            return nil
        }
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()
        
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        background.setFill()
        
        context?.setBlendMode(.normal)
        context?.fill(rect)
        
        tint.setFill()
        
        context?.setBlendMode(.normal)
        context?.draw(cgimage, in: rect)
        
        let rendered = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return rendered
    }
}
