//
//  Bundle+Extensions.swift
//  Bring
//
//  Created by Renato Stauffer on 30.08.19.
//

import Foundation

extension Bundle {
    
    private class Dummy {}
    
    public static let sharedFramework: Bundle = { return Bundle(for: Bundle.Dummy.self) }()
}
