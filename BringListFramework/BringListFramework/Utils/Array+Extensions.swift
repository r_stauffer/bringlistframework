//
//  Array+Extensions.swift
//  Bring
//
//  Created by Renato Stauffer on 07.02.19.
//

import Foundation

extension Array where Element: Equatable {
    /// Checks if two arrays are ordered equally.
    ///
    /// - Parameter otherArray: The array to compare `self` to
    /// - Returns: Returns true if the same objects are at the same indexes of `self` and `otherArray`. Otherwise returns false. If the two arrays do not have the same `count` the method returns `false`.
    func hasSameOrder(as otherArray: [Element]) -> Bool {
        if otherArray.count != self.count {
            return false
        }
        for (index, element) in self.enumerated() {
            let otherElement = otherArray[index]
            if element != otherElement {
                return false
            }
        }
        return true
    }
    
    func sortOrder(accordingTo otherArray: [Element]) -> [Element] {

        let newArray = self.sorted { (value1, value2) -> Bool in
            
            guard let indexValue1 = otherArray.firstIndex(of: value1) else {
                return true
            }
            
            guard let indexValue2 = otherArray.firstIndex(of: value2) else {
                return false
            }
            
            return indexValue1 < indexValue2
        }
        
        return newArray
    }
}
