//
//  DateFormatter+Extensions.swift
//  Bring
//
//  Created by Renato Stauffer on 12.09.18.
//

import Foundation

extension DateFormatter {
    
    /// Creates date strings looking like this 2018-09-14T09:21:49.000Z
    static func yyymmddTHHmmssSSSZ (timeZone: TimeZone? = TimeZone(abbreviation: "UTC")) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.timeZone = timeZone
        return formatter
    }
    
    static let yyyyMMdd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    /// Creates date strings looking like this 2018-09-14
    static func yyyymmdd (timeZone: TimeZone? = TimeZone(abbreviation: "UTC")) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.timeZone = timeZone
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
    
    func date(fromNilString nilString: String?) -> Date? {
        guard let string = nilString else {
            return nil
        }
        return self.date(from: string)
    }
    
    func string(fromNilDate nilDate: Date?) -> String? {
        guard let date = nilDate else {
            return nil
        }
        return self.string(from: date)
    }
}
