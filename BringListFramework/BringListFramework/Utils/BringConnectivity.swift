//
//  BringConnectivity.swift
//  Bring
//
//  Created by Renato Stauffer on 25.01.19.
//
import Foundation
import Alamofire

public protocol ConnectionTestable {
    var hasConnection: Bool { get }
}

public class BringConnectivity: ConnectionTestable {
    
    public init() {}
    
    public var hasConnection: Bool {
        // Not abailable for the watch...
        return true
    }
}
