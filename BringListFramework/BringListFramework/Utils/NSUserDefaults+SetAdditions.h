//
//  NSUserDefaults+SetAdditions.h
//  Bring
//
//  Created by Renato Stauffer on 25.07.19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (SetAdditions)

- (NSSet *)setForKey:(NSString *)defaultName;
- (void)setSet:(NSSet *)set forKey:(NSString *)defaultName;

@end

NS_ASSUME_NONNULL_END
