//
//  NSUserDefaults+SetAdditions.m
//  Bring
//
//  Created by Renato Stauffer on 25.07.19.
//

#import "NSUserDefaults+SetAdditions.h"

@implementation NSUserDefaults (SetAdditions)

- (NSSet *)setForKey:(NSString *)defaultName
{
    NSArray *array = [self arrayForKey:defaultName];
    if (array) {
        return [NSSet setWithArray:array];
    } else {
        return nil;
    }
}

- (void)setSet:(NSSet *)set forKey:(NSString *)defaultName
{
    if (set) {
        [self setObject:[set allObjects] forKey:defaultName];
    } else {
        [self setObject:nil forKey:defaultName];
    }
}

@end
