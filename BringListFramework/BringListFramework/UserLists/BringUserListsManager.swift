//
//  BringUserListsManager.swift
//  Bring
//
//  Created by Renato Stauffer on 13.08.19.
//

import Foundation

enum BringUserListsManagerError: Error {
    case missingUuid
    case unknown
    case listNotCreated
}

public enum BringUserListsManagerSyncResult {
    case newDefaultList(String)
    case sameDefaultList(String)
}

@objc public class BringUserListsManager: NSObject {
    
    private let store: LocalListStore
    private let service: ListService
    private let listStateStore: ListStateStore
    @objc public static let shared = BringUserListsManager()
    
    public init(store: LocalListStore = BringCoreDataUserListStore.shared, service: ListService = BringAlamofireUserListService(), listStateStore: ListStateStore = BringCoreDataListStateStore.shared) {
        self.store = store
        self.service = service
        self.listStateStore = listStateStore
    }
    
    @objc public func syncObjc(withUuid userUuid: String, onSuccess: @escaping () -> Void, onFailure: @escaping () -> Void) {
        self.syncListsForUser(withUuid: userUuid) { (result) in
            switch result {
            case .success:
                onSuccess()
            case .failure:
                onFailure()
            }
        }
    }
    
    public func syncListsForUser(withUuid userUuid: String, callback: @escaping (Result<[BringUserList], Error>) -> Void) {
        store.allLists(forPredicate: nil) { [weak self] (result) in
            switch result {
            case .success(let localLists):
                self?.service.loadListsForUser(userUuid, onSuccess: { (remoteLists) in
                    self?.store.saveOrUpdate(remoteLists) { (result) in
                        let predicate = self?.createDeletionPredicate(localListUuids: localLists.map { $0.uuid }, remoteLists: remoteLists)
                        
                        let groupe = DispatchGroup()
                        
                        groupe.enter()
                        groupe.enter()
                        
                        self?.store.delete(predicate: predicate) { (result) in
                            groupe.leave()
                        }
                        
                        self?.listStateStore.delete(withPredicate: predicate, callback: { (result) in
                            groupe.leave()
                        })
                        
                        groupe.notify(queue: .main) {
                            callback(Result.success(remoteLists))
                        }
                    }
                }, onFailure: {
                    callback(Result.failure(BringUserListsManagerError.unknown))
                })
            case .failure(let error):
                callback(Result.failure(error))
            }
        }
    }
    
    public func getUserLists(callback: @escaping ([BringUserList]) -> Void) {
        store.allLists(forPredicate: nil) { result in
            switch result {
            case .success(let lists):
                callback(lists)
            case .failure(let error):
                Log.ed(error)
            }
        }
    }
    
    @objc public func createUserListForUser(withUuid userUuid: String, name: String, andThemeIdentifier themeIdentifier: String, callback: @escaping (BringUserList?) -> Void) {
        
        guard let identifier = BringThemeIdentifier(rawValue: themeIdentifier) else {
            assertionFailure("The given theme does not exist!")
            callback(nil)
            return
        }
        
        self.createUserListForUser(withUuid: userUuid, named: name, andTheme: identifier) { (result) in
            switch result {
            case .success(let list):
                callback(list)
            case .failure:
                callback(nil)
            }
        }
    }
    
    public func createUserListForUser(withUuid userUuid: String, named name: String, andTheme themeIdentifier: BringThemeIdentifier, callback: @escaping (Result<BringUserList, Error>) -> Void) {
        self.service.createList(forUserUuid: userUuid, withName: name, andThemeKey: themeIdentifier.rawValue, onSuccess: { listUuid in
            self.syncListsForUser(withUuid: userUuid) { (result) in
                switch result {
                case .success:
                    self.store.allLists(forPredicate: self.store.getPredicateForSingleUserList(with: listUuid)) { result in
                        switch result {
                        case .success(let userLists):
                            
                            guard let userList = userLists.first else {
                                callback(Result.failure(BringUserListsManagerError.listNotCreated))
                                return
                            }
                            
                            callback(Result.success(userList))
                        case .failure(let error):
                            callback(Result.failure(error))
                        }
                    }
                case .failure(let error):
                    callback(Result.failure(error))
                }
            }
        }, onFailure: {
            callback(Result.failure(BringUserListsManagerError.unknown))
        })
    }
    
    @objc public func updateUserList(_ userList: BringUserList, onSuccess: @escaping () -> Void, onFailure: @escaping () -> Void) {
        self.service.updateUserList(with: userList.uuid, name: userList.name, themeKey: userList.theme, onSuccess: {
            self.store.saveOrUpdate([userList]) { result in
                switch result {
                case .success:
                    onSuccess()
                case .failure:
                    onFailure()
                }
            }
        }, onFailure: onFailure)
    }
    
    @objc public func getUserLists() -> [BringUserList] {
        return store.allLists(forPredicate: nil)
    }
    
    @objc public func getSortedUserLists(userListOrder: [String], callback: @escaping ([BringUserList]) -> Void) {
        let listUuids = userListOrder
        
        guard listUuids.count != 0 else {
            getUserLists(callback: callback)
            return
        }
        
        let userLists = store.allLists(forPredicate: nil)
        
        let sortedList = userLists.sorted { (list1, list2) -> Bool in
            return listUuids.index(of: list1.uuid) ?? 0 < listUuids.index(of: list2.uuid) ?? 0
        }
        
        callback(sortedList)
    }
    
    @objc public func getUserList(withUuid uuid: String, callback: @escaping (BringUserList?) -> Void) {
        let predicate = NSPredicate(format: "uuid == %@", uuid)
        store.allLists(forPredicate: predicate) { (result) in
            switch result {
            case .success(let userLists):
                let userList = userLists.first
                callback(userList)
            case .failure(let error):
                Log.ed(error)
            }
        }
    }
    
    @objc public func getUserList(withUuid uuid: String) -> BringUserList? {
        return store.getUserList(for: uuid)
    }
    
    public func deleteUserList(withUuid uuid: String, callback: @escaping () -> Void) {
        let predicate = NSPredicate(format: "uuid == %@", uuid)
        store.delete(predicate: predicate) { (result) in
            switch result {
            case .success:
                callback()
            case .failure(let error):
                Log.ed(error)
                callback()
            }
        }
    }
    
    private func createDeletionPredicate(localListUuids: [String], remoteLists: [BringUserList]) -> NSPredicate {
        
        var localListUuids = localListUuids
        
        for remoteList in remoteLists {
            localListUuids.removeAll { (uuid) -> Bool in
                uuid == remoteList.uuid
            }
        }
        
        return NSPredicate(format:"uuid IN %@", localListUuids)
    }
}
