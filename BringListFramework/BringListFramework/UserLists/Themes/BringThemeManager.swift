//
//  BringThemeManager.swift
//  Bring
//
//  Created by Renato Stauffer on 27.08.19.
//

import Foundation

public final class BringThemeFactory {
    
    private let createImageSuffix = "_create_list"
    private let inviteImageSuffix = "_invite"
    private let inviteImageOverlaySuffix = "_invite_overlay"
    private let visualImageSuffix = "_visual"
    private let headerImageSuffix = "_header"
    
    public func createTheme(for identifier: BringThemeIdentifier) -> BringTheme {
        
        let filePrefix = getFilePrefix(for: identifier)
        
        return BringTheme(
        identifier: identifier,
        createListImage: filePrefix + self.createImageSuffix,
        themeImage: filePrefix,
        visualImage: filePrefix + self.visualImageSuffix,
        headerImage: filePrefix + self.headerImageSuffix,
        sendInviteImage: filePrefix,
        inviteImage: filePrefix + self.inviteImageSuffix,
        inviteImageOverlay: filePrefix + self.inviteImageOverlaySuffix
        )
    }
    
    func getFilePrefix(for identifier: BringThemeIdentifier) -> String {
        switch identifier {
        case .office:
            return "office"
        case .home:
            return "home"
        case .party:
            return "party"
        case .holiday:
            return "holiday"
        case .grocery:
            return "grocery"
        case .shared:
            return "signup"
        }
    }
}

public final class BringListThemeManager: NSObject {
    
    @objc public static let shared = BringListThemeManager()
    
    private let themeFactory: BringThemeFactory
    private let localeHelper: BringLocaleHelper
    private let loadedThemes: [BringTheme]
    private let signupThemes: [BringTheme]
    private let userListStore: LocalListStore
    private let orderedThemeIdentifiers: [BringThemeIdentifier]
    private let localHelper: LanguageProvider
    
    private init(themeFactory: BringThemeFactory = BringThemeFactory(), localeHelper: BringLocaleHelper = BringLocaleHelper.sharedInstance, userListStore: LocalListStore = BringCoreDataUserListStore.shared, localHelper: LanguageProvider = BringLocaleHelper.sharedInstance) {
        self.loadedThemes = BringThemeIdentifier.allCases.compactMap { identifier in
            if identifier == .shared {
                return nil
            }
            return themeFactory.createTheme(for: identifier)
        }
        self.signupThemes = [themeFactory.createTheme(for: .shared)]
        self.themeFactory = themeFactory
        self.localeHelper = localeHelper
        self.userListStore = userListStore
        self.localHelper = localHelper
        self.orderedThemeIdentifiers = [BringThemeIdentifier.home, BringThemeIdentifier.office, BringThemeIdentifier.grocery, BringThemeIdentifier.party, BringThemeIdentifier.holiday]
        super.init()
    }
    
    @objc public var allThemes: [BringTheme] {
        let themes = self.loadedThemes.sorted {
            $0.identifier.rawValue < $1.identifier.rawValue
        }.filter {
            self.isValid($0) && !self.isRestricted($0)
        }
        return themes
    }
    
    @objc public func allUsedThemes() -> Set<BringTheme> {
        let allUserThemes = userListStore.allLists(forPredicate: nil).compactMap { (list) -> BringTheme? in
            
            guard let theme = self.getTheme(for: list.theme) else {
                return nil
            }
            
            return theme
        }
        
        let uniqueThemes = Set<BringTheme>(allUserThemes)
        return uniqueThemes
    }
    
    @objc public func allThemesPrioritized() -> [BringTheme] {
        var allThemes = [String]()
        
        for userList in userListStore.allLists(forPredicate: nil) {
            allThemes.append(userList.theme)
        }
        
        let sortedThemes = self.loadedThemes.sorted { (t1, t2) -> Bool in
            
            if allThemes.contains(t1.identifier.rawValue) && !allThemes.contains(t2.identifier.rawValue) {
                return false
            } else if !allThemes.contains(t1.identifier.rawValue) && allThemes.contains(t2.identifier.rawValue) {
                return true
            }
            
            guard let index1 = self.orderedThemeIdentifiers.index(of: t1.identifier) else {
                return false
            }
            
            guard let index2 = self.orderedThemeIdentifiers.index(of: t2.identifier) else {
                return true
            }
            
            return index1 < index2
        }
        
        let nonExpiredthemes =  self.removeExpired(sortedThemes)
        let nonRestricedThemes = self.removeExpired(nonExpiredthemes)
        return nonRestricedThemes
    }
    
    private func removeExpired(_ themes: [BringTheme]) -> [BringTheme] {
        return themes.filter { (theme) -> Bool in
            if let activeFrom = theme.activeFrom, let expireyDate = theme.expiryDate {
                let today = Date()
                return activeFrom.timeIntervalSince1970 <= today.timeIntervalSince1970 && today.timeIntervalSince1970 < expireyDate.timeIntervalSince1970
            }
            return true
        }
    }
    
    public func doesThemeExist(for identifier: BringThemeIdentifier) -> Bool {
        return self.loadedThemes.contains { (theme) -> Bool in
            return theme.identifier == identifier
        }
    }
    
    @objc public func doesThemeExistObjc(for identifier: String) -> Bool {
        return self.loadedThemes.contains { (theme) -> Bool in
            return theme.identifier.rawValue == identifier
        }
    }
    
    @objc public func getTheme(for identifier: String) -> BringTheme? {
        guard let identifier = BringThemeIdentifier(rawValue: identifier) else {
            return nil
        }
        return self.getTheme(for: identifier)
    }
    
    public func getTheme(for identifier: BringThemeIdentifier) -> BringTheme? {
        return loadedThemes.filter { $0.identifier == identifier }.first
    }
    
    @objc public func allSignupThemes() -> [BringTheme] {
        return signupThemes
    }
    
    // MARK: MISC
    
    private final func isValid(_ theme: BringTheme) -> Bool {
        
        guard let activeFrom = theme.activeFrom else {
            return true
        }
        
        guard let expiryDate = theme.expiryDate else {
            return true
        }
        
        return activeFrom.timeIntervalSince1970 <= Date().timeIntervalSince1970 && Date().timeIntervalSince1970 < expiryDate.timeIntervalSince1970
    }
    
    private func isRestricted(_ theme: BringTheme) -> Bool {
        
        guard let restrictions = theme.countryRestrictions else {
            return false
        }
        
        return !restrictions.contains(self.localeHelper.getCountryCode())
    }
    
    @objc public var defaultTheme: BringTheme {
        return themeFactory.createTheme(for: .home)
    }
}
