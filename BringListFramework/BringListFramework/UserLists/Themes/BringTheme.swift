//
//  BringTheme.swift
//  Bring
//
//  Created by Renato Stauffer on 27.08.19.
//

import Foundation
import UIKit

public enum BringThemeIdentifier: String, CaseIterable {
    case office = "ch.publisheria.bring.theme.office"
    case home = "ch.publisheria.bring.theme.home"
    case party = "ch.publisheria.bring.theme.party"
    case holiday = "ch.publisheria.bring.theme.holiday"
    case grocery = "ch.publisheria.bring.theme.grocery"
    case shared = "ch.publisheria.bring.theme.signup.shared"
}

public class BringTheme: NSObject {
    public let identifier: BringThemeIdentifier
    @objc public let createListImage: String
    @objc public let themeImage: String
    @objc public let visualImage: String
    @objc public let headerImage: String
    @objc public let sendInviteImage: String
    @objc public let inviteImage: String
    @objc public let inviteImageOverlay: String
    
    init(identifier: BringThemeIdentifier, createListImage: String, themeImage: String, visualImage: String, headerImage: String, sendInviteImage: String, inviteImage: String, inviteImageOverlay: String) {
        self.identifier = identifier
        self.createListImage = createListImage
        self.themeImage = themeImage
        self.visualImage = visualImage
        self.headerImage = headerImage
        self.sendInviteImage = sendInviteImage
        self.inviteImage = inviteImage
        self.inviteImageOverlay = inviteImageOverlay
    }
    
    @objc public var objcIdentifier: String {
        return identifier.rawValue
    }
}

public class BringThemeActivator: NSObject {
    @objc public let fireAfterDays: Int
    @objc public let inlineFeature: Bool
    @objc public let fullscreenFeature: Bool
    @objc public let promoted: Bool
    @objc public let inlineActivator: String?
    @objc public let inlineActivatorListname: String?
    @objc public let fullscreenTitle: String?
    @objc public let fullscreenText: String?
    @objc public let fullscreenButton: String?
    @objc public let fullscreenImage: String?
    @objc public let campaign: String?
    @objc public let fullscreenBackground: UIColor?
    
    @objc public init(fireAfterDays: Int = 0, inlineFeature: Bool = false, fullscreenFeature: Bool = false, promoted: Bool = false, inlineActivator: String? = nil, inlineActivatorListname: String? = nil, fullscreenTitle: String? = nil, fullscreenText: String? = nil, fullscreenButton: String? = nil, fullscreenImage: String? = nil, campaign: String? = nil, fullscreenBackground: UIColor? = nil) {
        self.fireAfterDays = fireAfterDays
        self.inlineFeature = inlineFeature
        self.fullscreenFeature = fullscreenFeature
        self.promoted = promoted
        self.inlineActivator = inlineActivator
        self.inlineActivatorListname = inlineActivatorListname
        self.fullscreenTitle = fullscreenTitle
        self.fullscreenText = fullscreenText
        self.fullscreenButton = fullscreenButton
        self.fullscreenImage = fullscreenImage
        self.campaign = campaign
        self.fullscreenBackground = fullscreenBackground
    }
    
}

extension BringTheme {
    @objc public func loadHeaderImage() -> UIImage? {
        return UIImage(namedFrameworkImage: headerImage)
    }
    
    @objc public var headerColor: UIColor? {
        return UIColor(hex: headerColorHexCode)
    }
}

// MARK: Hard coded attributes
extension BringTheme {
    
    @objc public var themeActivator: BringThemeActivator {
        
        switch identifier {
        case .grocery:
            return createGroceryThemeActivator()
        case .holiday:
            return createHolidayThemeActivator()
        case .home:
            return createHomeThemeActivator()
        case .office:
            return createOfficeThemeActivator()
        case .party:
            return createPartyThemeActivator()
        case .shared:
            return createSharedThemeActivator()
        }
    }
    
    private func createGroceryThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            inlineFeature: false,
            inlineActivator: NSLocalizedString("GROCERY_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("GROCERY_THEME_ACTIVATOR_LISTNAME", comment: "")
        )
    }
    
    private func createHolidayThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            inlineFeature: true,
            inlineActivator: NSLocalizedString("HOLIDAY_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("HOLIDAY_THEME_ACTIVATOR_LISTNAME", comment: "")
        )
    }
    
    private func createHomeThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            inlineFeature: true,
            inlineActivator: NSLocalizedString("HOME_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("HOME_THEME_ACTIVATOR_LISTNAME", comment: "")
        )
    }
    
    private func createOfficeThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            fireAfterDays: 8,
            inlineFeature: true,
            fullscreenFeature: true,
            inlineActivator: NSLocalizedString("OFFICE_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("OFFICE_THEME_ACTIVATOR_LISTNAME", comment: ""),
            fullscreenTitle: NSLocalizedString("OFFICE_TITLE", comment: ""),
            fullscreenText: NSLocalizedString("OFFICE_TEXT", comment: ""),
            fullscreenButton: NSLocalizedString("OFFICE_CREATE", comment: ""),
            fullscreenImage: "office_activator.jpg",
            fullscreenBackground: UIColor(red: 213/255, green: 205/255, blue: 185/255, alpha: 1)
        )
    }
    
    private func createPartyThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            inlineFeature: true,
            inlineActivator: NSLocalizedString("PARTY_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("PARTY_THEME_ACTIVATOR_LISTNAME", comment: "")
        )
    }
    
    private func createSharedThemeActivator() -> BringThemeActivator {
        return BringThemeActivator(
            inlineFeature: true,
            inlineActivator: NSLocalizedString("SIGNUP_THEME_ACTIVATOR", comment: ""),
            inlineActivatorListname: NSLocalizedString("SIGNUP_THEME_ACTIVATOR_LISTNAME", comment: "")
        )
    }
    
    public var countryRestrictions: [String]? {
        switch identifier {
        case .grocery, .holiday, .office, .home, .party, .shared:
            return nil
        }
    }
    
    @objc public var activeFrom: Date? {
        switch identifier {
        case .grocery, .holiday, .office, .home, .party, .shared:
            return nil
        }
    }
    
    @objc public var expiryDate: Date? {
        switch identifier {
        case .grocery, .holiday, .office, .home, .party, .shared:
            return nil
        }
    }
    
    @objc public var placeholderName: String {
        switch identifier {
        case .office:
            return NSLocalizedString("OFFICE_THEME_PLACEHOLDER", comment: "")
        case .home:
            return NSLocalizedString("HOME_THEME_PLACEHOLDER", comment: "")
        case .party:
            return NSLocalizedString("PARTY_THEME_PLACEHOLDER", comment: "")
        case .holiday:
            return NSLocalizedString("HOLIDAY_THEME_PLACEHOLDER", comment: "")
        case .grocery:
            return NSLocalizedString("GROCERY_THEME_PLACEHOLDER", comment: "")
        case .shared:
            return "" // No placeholder available
        }
    }
    
    public var headerColorHexCode: String {
        switch identifier {
        case .office:
            return "586972"
        case .home:
            return "435B69"
        case .party:
            return "4E475E"
        case .holiday:
            return "476B7D"
        case .grocery:
            return "A1887E"
        case .shared:
            return ""
        }
    }
}
