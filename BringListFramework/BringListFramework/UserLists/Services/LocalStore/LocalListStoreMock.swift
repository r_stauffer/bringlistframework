//
//  LocalListStoreMock.swift
//  Bring
//
//  Created by Renato Stauffer on 10.08.19.
//

import Foundation

public protocol LocalListStore {
    func allLists(forPredicate predicate: NSPredicate?, callback: @escaping (Result<[BringUserList], Error>) -> Void)
    func allLists(forPredicate predicate: NSPredicate?) -> [BringUserList]
    func store(lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void)
    func delete(predicate: NSPredicate?, callback: @escaping (Result<Void, Error>) -> Void)
    func saveOrUpdate(_ lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void)
}

public class LocalListStoreMock: LocalListStore {
    
    public var didCallDelete = false
    public var didCallStore = false
    public var didCallAllLists = false
    public var didCallAllListsSynchrousnous = false
    public var didCallSaveOrUpdate = false
    
    public var allListsResult = [BringUserList]()
    public var allListsSynchrousnousResult = [BringUserList]()
    
    
    public func delete(predicate: NSPredicate?, callback: @escaping (Result<Void, Error>) -> Void) {
        didCallDelete = true
        callback(Result.success(()))
    }
    
    public func store(lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void) {
        didCallStore = true
        callback(Result.success(()))
    }
    
    
    public func allLists(forPredicate predicate: NSPredicate?, callback: @escaping (Result<[BringUserList], Error>) -> Void) {
        didCallAllLists = true
        callback(Result.success(allListsResult))
    }
    
    public func allLists(forPredicate predicate: NSPredicate?) -> [BringUserList] {
        didCallAllListsSynchrousnous = true
        return allListsSynchrousnousResult
    }
    
    public func saveOrUpdate(_ lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void) {
        didCallSaveOrUpdate = true
        callback(Result.success(()))
    }
    
    public init() {}
    
}
