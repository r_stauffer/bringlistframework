//
//  BringCoreDataLocalListStore.swift
//  Bring
//
//  Created by Renato Stauffer on 13.08.19.
//

import Foundation
import CoreData

extension NSManagedObject {
    convenience init(usedContext: NSManagedObjectContext) {
        let name = String(describing: type(of: self))
        let entity = NSEntityDescription.entity(forEntityName: name, in: usedContext)!
        self.init(entity: entity, insertInto: usedContext)
    }
}

public extension LocalListStore {
    func getPredicateForSingleUserList(with listUuid: String) -> NSPredicate {
        return NSPredicate(format: "uuid == %@", listUuid)
    }
    
    func getUserList(for listUuid: String) -> BringUserList? {
        return self.allLists(forPredicate: getPredicateForSingleUserList(with: listUuid)).first
    }
    
    func getUserList(for listUuid: String, callback: @escaping (BringUserList?) -> Void) {
        self.allLists(forPredicate: getPredicateForSingleUserList(with: listUuid)) { (result) in
            switch result {
            case .success(let userLists):
                callback(userLists.first)
            case .failure(let error):
                Log.ed(error)
                callback(nil)
            }
        }
    }
}

public final class BringCoreDataUserListStore: LocalListStore {
    
    private let stack: BringCoreDataListsStack
    private let queue = DispatchQueue(label: "BringCoreDataLocalListStoreQueue")
    
    public static let shared = BringCoreDataUserListStore()
    
    public init(stack: BringCoreDataListsStack = BringCoreDataListsStackFactory().makeCoreDataStack()) {
        self.stack = stack
    }
    
    public func allLists(forPredicate predicate: NSPredicate?, callback: @escaping (Result<[BringUserList], Error>) -> Void) {
        queue.async {
            let context = self.stack.createBackgroundContext()
            
            context.performAndWait {
                let fetchrequest: NSFetchRequest<BringUserListEntity> = NSFetchRequest<BringUserListEntity>(entityName: "BringUserListEntity")
                fetchrequest.predicate = predicate
                
                do {
                    let result = try context.fetch(fetchrequest)
                    let lists = result.compactMap { (entity) -> BringUserList? in
                        let list = BringUserList(entity: entity)
                        assert(list != nil, "Corrupt data! This should always succeed")
                        return list
                    }
                    DispatchQueue.main.async {
                        callback(Result.success(lists))
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
            
        }
    }
    
    public func allLists(forPredicate predicate: NSPredicate?) -> [BringUserList] {
        let context = self.stack.mainContext
        
        var returnValue = [BringUserList]()
        
        context.performAndWait {
            let fetchrequest: NSFetchRequest<BringUserListEntity> = NSFetchRequest<BringUserListEntity>(entityName: "BringUserListEntity")
            fetchrequest.predicate = predicate
            
            do {
                let result = try context.fetch(fetchrequest)
                let lists = result.compactMap { (entity) -> BringUserList? in
                    let list = BringUserList(entity: entity)
                    assert(list != nil, "Corrupt data! This should always succeed")
                    return list
                }
                returnValue = lists
            } catch let error {
                Log.ed(error)
            }
        }
        
        return returnValue
    }
    
    public func store(lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void) {
        queue.async {
            let context = self.stack.createBackgroundContext()
            
            context.performAndWait {
                for list in lists {
                    let entity = BringUserListEntity(usedContext: context)
                    entity.name = list.name
                    entity.theme = list.theme
                    entity.uuid = list.uuid
                }
            }
            
            self.stack.save(context)
            
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
        }
    }
    
    public func saveOrUpdate(_ lists: [BringUserList], callback: @escaping (Result<Void, Error>) -> Void) {
        queue.async {
            let context = self.stack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    for list in lists {
                        let fetchrequest: NSFetchRequest<BringUserListEntity> = NSFetchRequest<BringUserListEntity>(entityName: "BringUserListEntity")
                        fetchrequest.predicate = NSPredicate(format: "uuid = %@", list.uuid)
                        
                        if let result = try context.fetch(fetchrequest).first {
                            result.uuid = list.uuid
                            result.name = list.name
                            result.theme = list.theme
                        } else {
                            let entity = BringUserListEntity(usedContext: context)
                            entity.name = list.name
                            entity.theme = list.theme
                            entity.uuid = list.uuid
                        }
                    }
                    
                    self.stack.save(context)
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
            
            self.stack.save(context)
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
        }
    }
    
    public func delete(predicate: NSPredicate?, callback: @escaping (Result<Void, Error>) -> Void) {
        queue.async {
            let context = self.stack.createBackgroundContext()
            
            context.performAndWait {
                do {
                    let fetchrequest: NSFetchRequest<BringUserListEntity> = NSFetchRequest<BringUserListEntity>(entityName: "BringUserListEntity")
                    fetchrequest.predicate = predicate
                    
                    let result = try context.fetch(fetchrequest)
                    
                    for list in result {
                        context.delete(list)
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        callback(Result.failure(error))
                    }
                }
            }
            
            self.stack.save(context)
            DispatchQueue.main.async {
                callback(Result.success(()))
            }
        }
    }
    
}

extension BringUserList {
    convenience init?(entity: BringUserListEntity) {
        guard let uuid = entity.uuid else {
            return nil
        }
        
        guard let name = entity.name else {
            return nil
        }
        
        guard let theme = entity.theme else {
            return nil
        }
        
        self.init(uuid: uuid, name: name, theme: theme)
    }
}
