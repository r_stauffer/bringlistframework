//
//  BringUserListResponse.swift
//  Bring
//
//  Created by Sascha Thöni on 02.05.17.
//
//

import UIKit
import ObjectMapper

class BringUserListResponse: Mappable {

    var listUuid: String?
    var name: String?
    var theme: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        listUuid <- map["listUuid"]
        name <- map["name"]
        theme <- map["theme"]
    }
}
