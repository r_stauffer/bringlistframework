//
//  BringAlamofireUserListService.swift
//  BringListFramework
//
//  Created by Renato Stauffer on 26.12.19.
//

import Foundation
import Alamofire
import AlamofireImage

public protocol ListService {
    func loadListsForUser(_ publicUuid : String, onSuccess: @escaping ([BringUserList]) -> (), onFailure: @escaping () -> () )
    func createList(forUserUuid userUuid : String, withName name : String, andThemeKey themeKey : String, onSuccess: @escaping (String)->(), onFailure: @escaping () -> ())
    func updateUserList(with uuid: String, name: String, themeKey: String, onSuccess: @escaping () -> Void, onFailure: @escaping () -> Void)
}

public class BringAlamofireUserListService: ListService {
    
    public init() {}
    
    enum Router: URLRequestConvertible {
        case loadListsForUser(String)
        case createList(String, String, String)
        case updateList(String, String, String)
        
        
        var method: HTTPMethod {
            switch self {
            case .loadListsForUser:
                return .get
            case .createList:
                return .post
            case .updateList:
                return .post
            }
        }
        
        var path: String {
            switch self {
            case .loadListsForUser(let privateUuid):
                return "bringusers/\(privateUuid)/lists"
            case .createList(let privateUserUuid, _, _):
                return "/bringusers/\(privateUserUuid)/lists"
            case .updateList(let uuid, _, _):
                return "/bringlists/\(uuid)"
            }
        }
        
        // MARK: URLRequestConvertible
        
        func asURLRequest() throws -> URLRequest {
            let URL = Foundation.URL(string: BringEnvironment.sharedInstance().getEndpoint(BringEndpointCore))!
            var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue
            
            switch self {
            case .createList(_ , let name, let themeKey):
                var parameters = [String:Any]()
                parameters["name"] = name
                parameters["theme"] = themeKey
                try mutableURLRequest = URLEncoding.default.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
            case .updateList(_ , let name, let theme):
                var parameters = [String:Any]()
                parameters["name"] = name
                parameters["theme"] = theme
                try mutableURLRequest = URLEncoding.default.encode(mutableURLRequest, with: parameters)
                return mutableURLRequest
            case .loadListsForUser:
                return mutableURLRequest
            }
        }
    }

    
    public func loadListsForUser(_ publicUuid: String, onSuccess: @escaping ([BringUserList]) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.loadListsForUser(publicUuid)).validate()
            .responseArray(queue: nil, keyPath: "lists", context: nil, completionHandler: { (response:DataResponse<[BringUserListResponse]>) in
                switch response.result {
                case .success:
                    guard let userListResponses = response.result.value else {
                        onFailure()
                        return
                    }
                    onSuccess(self.map(userListReponses: userListResponses))
                case .failure(let error):
                    Log.e("Error: \(error)")
                    onFailure()
                }

            })
    }
    
    public func createList(forUserUuid userUuid: String, withName name: String, andThemeKey themeKey: String, onSuccess: @escaping (String) -> (), onFailure: @escaping () -> ()) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.createList(userUuid, name, themeKey)).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                
                if let result = response.result.value {
                    let jsonObject = result as! NSDictionary
                    if let listUuid = jsonObject.object(forKey: "bringListUUID") as? String {
                        onSuccess(listUuid)
                    } else {
                        onFailure()
                    }
                }
            case .failure(let error):
                Log.e(error.localizedDescription)
                onFailure()
            }
        }
    }
    
    public func updateUserList(with uuid: String, name: String, themeKey: String, onSuccess: @escaping () -> Void, onFailure: @escaping () -> Void) {
        BringAlamofireManager.sharedInstance.defaultManager.request(Router.updateList(uuid, name, themeKey)).validate().responseData { (response) in
            switch response.result {
            case .success:
                onSuccess()
            case .failure(let error):
                Log.e(error.localizedDescription)
                onFailure()
            }
        }
    }
    
    private func map(userListReponses : [BringUserListResponse]) -> [BringUserList] {
        let mappedResponses = userListReponses.compactMap {
            // @TODO: log mapping errors
            BringUserList(uuid: $0.listUuid ?? "", name: $0.name ?? "", theme: $0.theme ?? "")
        }
        return mappedResponses
    }
}
