//
//  BringUserList.swift
//  BringWatchApp WatchKit Extension
//
//  Created by Renato Stauffer on 16.07.19.
//  Copyright © 2019 bring.watch.standalone. All rights reserved.
//

import Foundation

@objc public class BringUserList: NSObject {
    @objc public let uuid: String
    @objc public let name: String
    @objc public let theme: String
    
    @objc public init(uuid: String, name: String, theme: String) {
        self.uuid = uuid
        self.name = name
        self.theme = theme
    }
    
    @objc public var uuidPredicate: NSPredicate {
        return NSPredicate(format: "uuid == %@", uuid)
    }
}
